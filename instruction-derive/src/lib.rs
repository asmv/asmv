extern crate proc_macro;

use crate::proc_macro::TokenStream;
use proc_macro2::Span;

use quote::quote;

use syn::{
    parse::{Parse, ParseStream},
    parse_macro_input,
    punctuated::Punctuated,
    Fields, Ident, ItemEnum, Meta, NestedMeta, Path, PathArguments, Token, Variant,
};

#[proc_macro_derive(ParsableInstruction, attributes(instruction))]
pub fn instruction_derive(input: TokenStream) -> TokenStream {
    let input = parse_macro_input!(input as ItemEnum);

    // get the name of the type we want to implement the trait for
    let ItemEnum {
        ident,
        generics,
        variants,
        ..
    } = &input;

    let mut display_impls = Punctuated::<_, Token![,]>::new();
    let mut parse_impls = Punctuated::<_, Token![,]>::new();
    let mut partial_parse_impls = Punctuated::<_, Token![,]>::new();
    for variant in variants {
        let Variant {
            attrs,
            ident,
            fields,
            ..
        } = &variant;
        let format = attrs
            .iter()
            .filter_map(|a| a.parse_meta().ok())
            .find(|p| p.path().is_ident("instruction"));
        let format = match format {
            Some(Meta::List(mut f)) => {
                if f.nested.len() != 1 {
                    return syn::Error::new_spanned(
                        variant,
                        "Too much instruction types, please use only one",
                    )
                    .to_compile_error()
                    .into();
                }
                f.nested.pop().unwrap().into_value()
            }
            Some(_) => {
                return syn::Error::new_spanned(
                    variant,
                    "Wrong instruction format, please use `instruction(<format>)`",
                )
                .to_compile_error()
                .into()
            }
            None => {
                return syn::Error::new_spanned(variant, "No instruction format specified")
                    .to_compile_error()
                    .into()
            }
        };
        let mut segments = if let NestedMeta::Meta(Meta::Path(Path {
            leading_colon: None,
            segments,
        })) = format
        {
            segments
        } else {
            return syn::Error::new_spanned(
                variant,
                "Wrong instruction format specified, please use a function path",
            )
            .to_compile_error()
            .into();
        };
        if segments.len() != 1 {
            return syn::Error::new_spanned(variant, "Specify a single instruction format")
                .to_compile_error()
                .into();
        }
        let s = segments.pop().unwrap().into_value();
        if s.arguments != PathArguments::None {
            return syn::Error::new_spanned(variant, "No parameters allowed for instruction type")
                .to_compile_error()
                .into();
        }
        let s = s.ident;
        let function = Ident::new(&s.to_string(), s.span());

        match fields {
            Fields::Unnamed(fields) => {
                let format = ident.to_string().to_lowercase();
                let mut params = Punctuated::<_, Token![,]>::new();
                let mut assignments = Punctuated::<_, Token![;]>::new();
                let mut c = b'a';
                for (i, _) in (&fields.unnamed).into_iter().enumerate() {
                    let ident = Ident::new(&(c as char).to_string(), Span::call_site());
                    params.push(ident.clone());
                    let idx = syn::Index::from(i);
                    assignments.push(quote! {
                        let #ident = values.#idx
                            .ok_or(crate::parser::ParseError::Incomplete)?;
                    });
                    c += 1;
                }
                display_impls.push(quote! {
                    Self::#ident (#params) => {
                        write!(f, "{} {}", #format, crate::instructions::#function (#params))?;
                    }
                });
                parse_impls.push(quote! {
                    #format => {
                        let values = crate::parser::#function(remainder)?;
                        #assignments
                        Ok(Self::#ident (#params))
                    }
                });
                partial_parse_impls.push(quote! {
                    #format => Format::#function(remainder)
                });
            }
            _ => unimplemented!(),
        }
    }
    let (impl_generics, ty_generics, where_clause) = generics.split_for_impl();
    let expanded = quote! {
      impl #impl_generics ::core::fmt::Display for #ident #ty_generics #where_clause {
        fn fmt(&self, f: &mut::core::fmt::Formatter<'_>) -> ::core::fmt::Result {
            match *self {
                #display_impls
            }
            Ok(())
        }
      }

      impl #impl_generics crate::parser::PartialParse for #ident #ty_generics #where_clause {
          fn partial_parse(s: &str) -> Result<crate::parser::Format, crate::parser::ParseError> {
              use crate::parser::{Format, ParseError};
              let mut words = s.splitn(2, ' ');
              let name = words.next().unwrap();
              let remainder = words.next().ok_or(ParseError::WrongFormat)?;
              debug_assert!(words.next().is_none());
              match name {
                  #partial_parse_impls,
                  _ => Err(ParseError::UnknownInstruction)
              }
          }
      }

      impl #impl_generics ::core::str::FromStr for #ident #ty_generics #where_clause {
          type Err = crate::parser::ParseError;

          fn from_str(s: &str) -> Result<Self, Self::Err> {
              let mut words = s.splitn(2, ' ');
              let name = words.next().unwrap();
              let remainder = words.next().ok_or(crate::parser::ParseError::WrongFormat)?;
              debug_assert!(words.next().is_none());
              match name {
                  #parse_impls,
                  _ => Err(crate::parser::ParseError::UnknownInstruction)
              }
          }
      }
    };

    TokenStream::from(expanded)
}

#[derive(Debug, Clone)]
struct ComputerType {
    register: Path,
    mult: Path,
    floats: Path,
    extensions: Punctuated<Ident, Token![,]>,
}

impl Parse for ComputerType {
    fn parse(input: ParseStream) -> syn::parse::Result<Self> {
        let register = Path::parse(&input)
            .map_err(|_| input.error("First parameter should be the register type"))?;
        <Token![,]>::parse(&input).map_err(|_| {
            input.error(
                "Should provide at least 3 parameters (register, multiplication support, then floats)",
            )
        })?;
        let mult = Path::parse(&input)
            .map_err(|_| input.error("Second parameter should be mult support"))?;
        <Token![,]>::parse(&input).map_err(|_| {
            input.error(
                "Should provide at least 3 parameters (register, multiplication support, then floats)",
            )
        })?;
        let floats = Path::parse(&input)
            .map_err(|_| input.error("Second parameter should be mult support"))?;
        let extensions = if <Token![:]>::parse(&input).is_ok() {
            Punctuated::<Ident, Token![,]>::parse_separated_nonempty(input)
                .map_err(|_| input.error("No parameters allowed for instruction type"))?
        } else {
            Punctuated::default()
        };
        Ok(Self {
            register,
            mult,
            floats,
            extensions,
        })
    }
}
/*
impl Executable for Computer<$r> {
    fn run_all(&mut self) {
        while !self.at_end() {
            self.execute()
        }
    }

    fn execute(&mut self) {
        let instruction = self.instructions[<$r as AsPrimitive<usize>>::as_(self.pc.0)];
        self.pc.0 = self.pc.0 + 1 as $r;
            match instruction {
                Instruction::Base(instruction) => self.execute_base(instruction),
                $(
                    paste::expr! { <Instruction:: $methods> (instruction) } => paste::expr! {self.<execute_ $methods> }(instruction),
                ),*
                _ => (),
        }
    }
}
*/
#[proc_macro]
pub fn computer_type(input: TokenStream) -> TokenStream {
    let input = parse_macro_input!(input as ComputerType);

    let mut impls = Punctuated::<_, Token![,]>::new();
    for extension in input.extensions {
        let variant = Ident::new(&extension.to_string().to_uppercase(), extension.span());
        let function = Ident::new(&format!("execute_{}", extension), extension.span());
        impls.push_value(quote! {
            Instruction::#variant(instruction) => self.#function(instruction)
        });
        impls.push_punct(<Token![,]>::default());
    }
    let register = input.register;
    let mult = input.mult;
    let floats = input.floats;
    let expanded = quote! {
        impl Executable for Computer<#register, #mult, #floats> {
            fn run_all(&mut self) {
                while !self.at_end() {
                    self.execute()
                }
            }

            fn execute(&mut self) {
                let instruction = self.instructions[<#register as AsPrimitive<usize>>::as_(self.pc.0)];
                self.pc.0 += 1 as #register;
                match instruction {
                    Instruction::Base(instruction) => self.execute_base(instruction),
                    #impls
                    _ => (),
                }
            }
        }
    };

    TokenStream::from(expanded)
}
