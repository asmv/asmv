mod highlight;
mod ui;

use asmv::{Computer, Mult, NoFloats, Register32Bit};
use std::fs::File;
use std::io::Read;
use std::path::PathBuf;

#[derive(Debug, structopt::StructOpt)]
/// The Assembler visualizer
///
/// Interactive visualisation of a Risc-V machine executing assembly.
/// Check the state being updated dynamically, more easily than a
/// debugger.
struct Args {
    /// Program to preload for the visualization
    #[structopt(short, long, parse(from_os_str))]
    program: Option<PathBuf>,
    /// File containing the (binary) initial memory for the visualization
    #[structopt(short, long, parse(from_os_str))]
    memory: Option<PathBuf>,
    /// The number of bytes of the computer
    #[structopt(short = "l", long, default_value = "100")]
    memory_len: usize,
}

#[paw::main]
fn main(args: Args) {
    let mut computer = Computer::<Register32Bit, Mult, NoFloats>::new(args.memory_len);
    if let Some(file) = args.memory {
        if let Err(e) =
            File::open(file).and_then(|mut f| std::io::copy(&mut f, &mut computer.mem_mut()))
        {
            eprintln!("Could not read memory file: {}", e);
            std::process::exit(1);
        }
    }
    if let Some(file) = args.program {
        let mut s = String::new();
        if let Err(e) = File::open(file).and_then(|mut f| f.read_to_string(&mut s)) {
            eprintln!("Could not read program file: {}", e);
            std::process::exit(1);
        } else {
            if let Err(e) = computer.queue_prog(&s) {
                eprintln!("Could not parse program: {}", e);
                std::process::exit(1);
            }
        }
    }
    if let Err(e) = ui::terminal(&mut computer) {
        eprintln!("Failure while running UI: {}", e);
        std::process::exit(1);
    }
}
