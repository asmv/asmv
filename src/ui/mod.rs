use crossterm::{
    cursor::MoveTo,
    event::{self, Event as CEvent, KeyCode},
    execute,
    terminal::{disable_raw_mode, enable_raw_mode, EnterAlternateScreen, LeaveAlternateScreen},
};
use std::{
    error::Error,
    fmt::Write as _,
    io::{stdout, Write},
    sync::mpsc,
    thread,
    time::Duration,
};
use tui::{
    backend::CrosstermBackend,
    layout::{Constraint, Direction, Layout},
    style::{Color, Modifier, Style},
    widgets::{Block, BorderType, Borders, List, ListState, Paragraph, Row, Table, Text},
    Terminal,
};

use super::highlight::{Highlight, HighlightState, RegisterHighlight};
use asmv::{
    instructions::Executable, Computer, ExtensionMult, NoFloats, Register32Bit, RegisterIdx,
};

#[derive(Clone, Copy, Debug, PartialEq, Eq)]
enum Event<I> {
    Input(I),
    Tick,
}

#[derive(Clone, Copy, Debug, PartialEq, Eq)]
enum InputMode {
    Normal,
    Editing { cursor: u16, append: bool },
}

/// App holds the state of the application
struct App {
    /// Current value of the input box
    input: String,
    /// Current input mode
    input_mode: InputMode,
}

impl Default for App {
    fn default() -> App {
        App {
            input: String::new(),
            input_mode: InputMode::Normal,
        }
    }
}

pub fn terminal<M: ExtensionMult>(
    computer: &mut Computer<Register32Bit, M, NoFloats>,
) -> Result<(), Box<dyn Error>>
where
    Computer<Register32Bit, M, NoFloats>: Executable,
{
    enable_raw_mode()?;

    let mut stdout = stdout();
    execute!(stdout, EnterAlternateScreen)?;

    let backend = CrosstermBackend::new(stdout);

    let mut terminal = Terminal::new(backend)?;
    terminal.hide_cursor()?;

    let (tx, rx) = mpsc::channel();
    thread::spawn(move || {
        loop {
            // poll for tick rate duration, if no events, sent tick event.
            if event::poll(Duration::from_millis(250)).unwrap() {
                //Check every 250 mill for input, if not send tick event
                if let CEvent::Key(key) = event::read().unwrap() {
                    tx.send(Event::Input(key)).unwrap();
                }
            }

            tx.send(Event::Tick).unwrap();
        }
    });

    let mut app = App::default();

    let should_quit = false;
    let mut program_state = ListState::default();
    program_state.select(Some(0));

    loop {
        let mut cursor_pos = (0, 0);
        terminal.draw(|mut f| {
            // Draw UI

            let highlight = Highlight::new(&app.input, &computer);

            // Divide the screen in two half
            let chunks = Layout::default()
                .direction(Direction::Horizontal)
                .constraints(vec![
                    Constraint::Length(16 * 4 + 2),
                    Constraint::Length(1), // Gap
                    Constraint::Min(20),
                ])
                .split(f.size());
            // Divide the left part of the screen for the registers and the memory
            let chunks_g = Layout::default()
                .direction(Direction::Vertical)
                .constraints(vec![Constraint::Length(10), Constraint::Min(20)])
                .split(chunks[0]);
            // Divide the right part of the screen for the command list and the input line
            let chunks_d = Layout::default()
                .direction(Direction::Vertical)
                .constraints(vec![Constraint::Min(18), Constraint::Length(6)])
                .split(chunks[2]);

            // Biggest width for the memory block
            let memory_block;
            let program_block;
            if chunks_d[0].width > chunks_g[1].width {
                memory_block = chunks_d[0];
                program_block = chunks_g[1];
            } else {
                memory_block = chunks_g[1];
                program_block = chunks_d[0];
            }

            // Memory
            let memory_width = (memory_block.width - 15) / 5;
            let addr_width = memory_block.width - 3 - 5 * memory_width;
            let header = std::iter::once(" base addr".to_string())
                .chain((0..memory_width).into_iter().map(|i| format!(" +{}", i)))
                .collect::<Vec<_>>();
            let constraints = std::iter::once(addr_width)
                .chain((0..memory_width).into_iter().map(|_| 4))
                .map(Constraint::Length)
                .collect::<Vec<_>>();
            let rows = (0..computer.mem().len())
                .into_iter()
                .step_by(memory_width as _)
                .map(|i| {
                    let row = std::iter::once(format!(" {:#010x}", i))
                        .chain(
                            (i..usize::min(i + memory_width as usize, computer.mem().len()))
                                .into_iter()
                                .map(|n| format!("{:#04x}", computer.mem()[n])),
                        )
                        .collect::<Vec<_>>();
                    Row::Data(row.into_iter())
                });
            let t = Table::new(header.iter(), rows)
                .header_style(Style::new().modifier(Modifier::BOLD))
                .block(
                    Block::default()
                        .borders(Borders::ALL)
                        .border_type(BorderType::Rounded)
                        .title("Memory"),
                )
                .widths(&constraints);
            f.render_widget(t, memory_block);

            // Registers
            let block = Block::default()
                .title("Registers")
                .border_type(BorderType::Rounded)
                .borders(Borders::ALL);
            f.render_widget(block, chunks_g[0]);
            let colonne = Layout::default()
                .direction(Direction::Horizontal)
                .constraints(vec![
                    Constraint::Length(16),
                    Constraint::Length(16),
                    Constraint::Length(16),
                    Constraint::Length(16),
                ])
                .split(block.inner(chunks_g[0]));

            let mut z = 0;
            let mut highlighted = highlight.registers();
            for col in colonne {
                let row = Layout::default()
                    .direction(Direction::Vertical)
                    .constraints(vec![
                        Constraint::Length(1),
                        Constraint::Length(1),
                        Constraint::Length(1),
                        Constraint::Length(1),
                        Constraint::Length(1),
                        Constraint::Length(1),
                        Constraint::Length(1),
                        Constraint::Length(1),
                    ])
                    .split(col);
                for case in row {
                    let v = computer.get(RegisterIdx(z));
                    let highlight =
                        if let Some((_, a)) = highlighted.first().filter(|(r, _)| *r == z) {
                            highlighted = &highlighted[1..];
                            Some(a)
                        } else {
                            None
                        };
                    let style = match highlight {
                        Some(RegisterHighlight::Result) => {
                            Style::new().fg(Color::Black).bg(Color::Green)
                        }
                        Some(RegisterHighlight::Operand) => {
                            Style::new().fg(Color::Black).bg(Color::Blue)
                        }
                        None => Style::new(),
                    };
                    let text = [
                        Text::styled(
                            format!("r{:02}: ", z),
                            Style::new().fg(Color::DarkGray).modifier(Modifier::BOLD),
                        ),
                        Text::styled(format!("{:#010x}", v), style),
                    ];
                    let paragraph = Paragraph::new(text.iter());
                    f.render_widget(paragraph, case);
                    z += 1;
                }
            }

            // Display program
            let list = List::new(computer.instructions().iter().enumerate().map(|(i, t)| {
                let style = if highlight.program().map_or(false, |c| c == i) {
                    Style::default().bg(Color::Green).fg(Color::Black)
                } else {
                    Style::default()
                };
                Text::styled(format!("{:3}: {}", i, t), style)
            }))
            .block(
                Block::default()
                    .title("Program")
                    .border_type(BorderType::Rounded)
                    .borders(Borders::ALL),
            )
            .style(Style::default().modifier(Modifier::DIM | Modifier::BOLD))
            .highlight_style(Style::default().fg(Color::Yellow).modifier(Modifier::BOLD))
            .highlight_symbol(" ->");
            f.render_stateful_widget(list, program_block, &mut program_state);

            // Display input box
            let inputbox = Layout::default()
                .direction(Direction::Vertical)
                .constraints(vec![
                    Constraint::Length(3),
                    Constraint::Length(1),
                    Constraint::Length(1),
                ])
                .split(chunks_d[1]);
            cursor_pos = (inputbox[0].left() + 1, inputbox[0].top() + 1);

            let text = [Text::raw(" press a to append, e to edit, q to quit")];
            let help_message = Paragraph::new(text.iter());
            f.render_widget(help_message, inputbox[2]);

            let text = [
                Text::raw(&app.input),
                Text::styled(
                    highlight.placeholder(),
                    Style::default().fg(Color::DarkGray).modifier(Modifier::DIM),
                ),
            ];
            let style = if app.input_mode == InputMode::Normal {
                Style::default().fg(Color::DarkGray)
            } else if highlight.state() == HighlightState::Invalid {
                Style::default().fg(Color::Red)
            } else if highlight.state() == HighlightState::Valid {
                Style::default().fg(Color::Green)
            } else {
                Style::default()
            };
            let input = Paragraph::new(text.iter())
                .style(Style::default().fg(Color::Yellow))
                .block(
                    Block::default()
                        .title("Input")
                        .title_style(style)
                        .borders(Borders::ALL)
                        .border_style(style)
                        .border_type(BorderType::Rounded),
                );
            f.render_widget(input, inputbox[0]);
        })?;

        if let InputMode::Editing { cursor, .. } = app.input_mode {
            execute!(
                terminal.backend_mut(),
                MoveTo(cursor_pos.0 + cursor, cursor_pos.1)
            )?;
        }
        match rx.recv()? {
            // Deal with inputs
            Event::Input(input) => match app.input_mode {
                InputMode::Normal => match input.code {
                    KeyCode::Char('a') => {
                        app.input_mode = InputMode::Editing {
                            cursor: 0,
                            append: true,
                        };
                        terminal.show_cursor()?;
                    }
                    KeyCode::Char('e') => {
                        if let Some(instr) = computer.instructions().get(computer.pos().0 as usize)
                        {
                            write!(&mut app.input, "{}", instr)?;
                            app.input_mode = InputMode::Editing {
                                cursor: app.input.len() as u16,
                                append: false,
                            };
                            terminal.show_cursor()?;
                        }
                    }
                    KeyCode::Char('q') => {
                        disable_raw_mode()?;
                        execute!(terminal.backend_mut(), LeaveAlternateScreen)?;
                        terminal.show_cursor()?;
                        break;
                    }
                    KeyCode::Char('d') => {
                        // commandline.pop()
                    }
                    KeyCode::Enter => {
                        // Execute the next command line
                        if !computer.at_end() {
                            computer.execute();
                        }
                        program_state.select(Some(computer.pos().0 as _));
                    }
                    KeyCode::Up => {
                        computer.step_back();
                        program_state.select(Some(computer.pos().0 as _));
                    }
                    KeyCode::Down => {
                        computer.step();
                        program_state.select(Some(computer.pos().0 as _));
                    }
                    _ => {}
                },
                InputMode::Editing { cursor, append } => match input.code {
                    KeyCode::Enter => {
                        if let Ok(instr) = app.input.parse() {
                            let addr = computer.pos().0 as usize;
                            if append && addr < computer.instructions().len() {
                                computer.instructions_mut().insert(addr + 1, instr);
                            } else if append {
                                computer.instructions_mut().push(instr);
                            } else {
                                computer.instructions_mut()[addr] = instr;
                            }
                            app.input.clear();
                            app.input_mode = InputMode::Normal;
                            terminal.hide_cursor()?;
                        }
                    }
                    KeyCode::Char(c) => {
                        app.input.insert(cursor as usize, c);
                        app.input_mode = InputMode::Editing {
                            cursor: cursor + 1,
                            append,
                        };
                    }
                    KeyCode::Up => app.input_mode = InputMode::Editing { cursor: 0, append },
                    KeyCode::Down => {
                        app.input_mode = InputMode::Editing {
                            cursor: app.input.len() as u16,
                            append,
                        }
                    }
                    KeyCode::Left if cursor > 0 => {
                        app.input_mode = InputMode::Editing {
                            cursor: cursor - 1,
                            append,
                        }
                    }
                    KeyCode::Right if cursor < app.input.len() as u16 => {
                        app.input_mode = InputMode::Editing {
                            cursor: cursor + 1,
                            append,
                        }
                    }
                    KeyCode::Backspace if cursor > 0 => {
                        app.input.remove(cursor as usize - 1);
                        app.input_mode = InputMode::Editing {
                            cursor: cursor - 1,
                            append,
                        }
                    }
                    KeyCode::Delete if cursor < app.input.len() as u16 => {
                        app.input.remove(cursor as usize);
                        app.input_mode = InputMode::Editing { cursor, append }
                    }
                    KeyCode::Esc => {
                        app.input_mode = InputMode::Normal;
                        terminal.hide_cursor()?;
                        app.input.drain(..);
                    }
                    _ => {}
                },
            },
            Event::Tick => {}
        }
        if should_quit {
            break;
        }
    }
    Ok(())
}
