use asmv::{
    parser::{Format, PartialParse},
    Computer, ExtensionMult, FRegister, Instruction, Register, RegisterIdx,
};
use num_traits::AsPrimitive;

#[derive(Clone, Copy, Debug)]
pub enum RegisterHighlight {
    Result,
    Operand,
}

#[derive(Clone, Copy, Debug, PartialEq, Eq)]
pub enum HighlightState {
    Invalid,
    Partial,
    Valid,
}

#[derive(Clone, Debug, Default)]
pub struct Highlight {
    registers: Vec<(u8, RegisterHighlight)>,
    program: Option<usize>,
    memory: Option<usize>,
    placeholder: &'static str,
    state: HighlightState,
}

impl Default for HighlightState {
    fn default() -> Self {
        Self::Invalid
    }
}

impl Highlight {
    pub fn new<
        R: Register + AsPrimitive<u8> + AsPrimitive<usize>,
        M: ExtensionMult,
        F: FRegister,
    >(
        s: &str,
        computer: &Computer<R, M, F>,
    ) -> Self {
        let mut highlight = Highlight::default();
        if let Ok(format) = Instruction::<R>::partial_parse(s) {
            highlight.state = HighlightState::Partial;
            match format {
                Format::Registers(a, b, c) => {
                    highlight.placeholder = " x00, x00, x00";
                    if let Some(RegisterIdx(r)) = a {
                        highlight.placeholder = &highlight.placeholder[5..];
                        highlight
                            .registers
                            .push((r.as_(), RegisterHighlight::Result));
                    }
                    if let Some(RegisterIdx(r)) = b {
                        highlight.placeholder = &highlight.placeholder[4..];
                        highlight
                            .registers
                            .push((r.as_(), RegisterHighlight::Operand));
                    }
                    if let Some(RegisterIdx(r)) = c {
                        highlight.placeholder = "";
                        if s.parse::<Instruction<R>>().is_ok() {
                            highlight.state = HighlightState::Valid;
                        }
                        highlight
                            .registers
                            .push((r.as_(), RegisterHighlight::Operand));
                    }
                }
                Format::Immediate(a, b, c) => {
                    highlight.placeholder = " x00, x00, 0xFFF";
                    if let Some(RegisterIdx(r)) = a {
                        highlight.placeholder = &highlight.placeholder[5..];
                        highlight
                            .registers
                            .push((r.as_(), RegisterHighlight::Result));
                    }
                    if let Some(RegisterIdx(r)) = b {
                        highlight.placeholder = &highlight.placeholder[4..];
                        highlight
                            .registers
                            .push((r.as_(), RegisterHighlight::Operand));
                    }
                    if c.is_some() {
                        highlight.placeholder = "";
                        if s.parse::<Instruction<R>>().is_ok() {
                            highlight.state = HighlightState::Valid;
                        }
                    }
                }
                Format::LongAdd(a, b) => {
                    highlight.placeholder = " x00, 0xFFFFF";
                    if let Some(RegisterIdx(r)) = a {
                        highlight.placeholder = &highlight.placeholder[5..];
                        highlight
                            .registers
                            .push((r.as_(), RegisterHighlight::Result));
                    }
                    if b.is_some() {
                        highlight.placeholder = "";
                        if s.parse::<Instruction<R>>().is_ok() {
                            highlight.state = HighlightState::Valid;
                        }
                    }
                }
                Format::StoreLoad(a, b, c) => {
                    highlight.placeholder = " x00, x00(0x0)";
                    if let Some(RegisterIdx(r)) = a {
                        highlight.placeholder = &highlight.placeholder[5..];
                        highlight
                            .registers
                            .push((r.as_(), RegisterHighlight::Result));
                    }
                    if let Some(RegisterIdx(r)) = b {
                        highlight.placeholder = &highlight.placeholder[4..];
                        highlight
                            .registers
                            .push((r.as_(), RegisterHighlight::Operand));
                        if let Some(c) = c {
                            highlight.placeholder = "";
                            if s.parse::<Instruction<R>>().is_ok() {
                                highlight.state = HighlightState::Valid;
                            }
                            let v = computer.get(RegisterIdx(r)) + R::from_immediate_low(c);
                            highlight.memory = Some(AsPrimitive::<usize>::as_(v));
                        }
                    }
                }
                Format::ShortJump(a, b, c) => {
                    highlight.placeholder = " x00, x00(0x0)";
                    if let Some(RegisterIdx(r)) = a {
                        highlight.placeholder = &highlight.placeholder[5..];
                        highlight
                            .registers
                            .push((r.as_(), RegisterHighlight::Result));
                    }
                    if let Some(RegisterIdx(r)) = b {
                        highlight.placeholder = &highlight.placeholder[4..];
                        highlight
                            .registers
                            .push((r.as_(), RegisterHighlight::Operand));
                        if let Some(c) = c {
                            highlight.placeholder = "";
                            if s.parse::<Instruction<R>>().is_ok() {
                                highlight.state = HighlightState::Valid;
                            }
                            let v = computer.get(RegisterIdx(r)) + R::from_immediate_low(c);
                            highlight.program = Some(AsPrimitive::<usize>::as_(v) / 4);
                        }
                    }
                }
                Format::LongJump(a, b) => {
                    highlight.placeholder = " x00(0x0)";
                    if let Some(RegisterIdx(r)) = a {
                        highlight.placeholder = &highlight.placeholder[4..];
                        highlight
                            .registers
                            .push((r.as_(), RegisterHighlight::Result));
                        if let Some(b) = b {
                            highlight.placeholder = "";
                            if s.parse::<Instruction<R>>().is_ok() {
                                highlight.state = HighlightState::Valid;
                            }
                            let v = computer.get(RegisterIdx(r)) + R::from_immediate_high(b);
                            highlight.program = Some(AsPrimitive::<usize>::as_(v) / 4);
                        }
                    }
                }
                Format::Branch(a, b, c) => {
                    highlight.placeholder = " x00, x00, 0x0";
                    if let Some(RegisterIdx(r)) = a {
                        highlight.placeholder = &highlight.placeholder[5..];
                        highlight
                            .registers
                            .push((r.as_(), RegisterHighlight::Result));
                    }
                    if let Some(RegisterIdx(r)) = b {
                        highlight.placeholder = &highlight.placeholder[4..];
                        highlight
                            .registers
                            .push((r.as_(), RegisterHighlight::Operand));
                        if let Some(c) = c {
                            highlight.placeholder = "";
                            if s.parse::<Instruction<R>>().is_ok() {
                                highlight.state = HighlightState::Valid;
                            }
                            let v = AsPrimitive::<usize>::as_(computer.get(RegisterIdx(r))) / 4
                                + AsPrimitive::<usize>::as_(R::from_immediate_low(c)) / 2;
                            highlight.program = Some(v);
                        }
                    }
                }
                Format::MulAdd(a, b, c, d) => {
                    highlight.placeholder = " x00, x00, x00, x00";
                    if let Some(RegisterIdx(r)) = a {
                        highlight.placeholder = &highlight.placeholder[5..];
                        highlight
                            .registers
                            .push((r.as_(), RegisterHighlight::Result));
                    }
                    if let Some(RegisterIdx(r)) = b {
                        highlight.placeholder = &highlight.placeholder[4..];
                        highlight
                            .registers
                            .push((r.as_(), RegisterHighlight::Operand));
                    }
                    if let Some(RegisterIdx(r)) = c {
                        highlight.placeholder = &highlight.placeholder[4..];
                        highlight
                            .registers
                            .push((r.as_(), RegisterHighlight::Operand));
                    }
                    if let Some(RegisterIdx(r)) = d {
                        highlight.placeholder = "";
                        if s.parse::<Instruction<R>>().is_ok() {
                            highlight.state = HighlightState::Valid;
                        }
                        highlight
                            .registers
                            .push((r.as_(), RegisterHighlight::Operand));
                    }
                }
                Format::Registers2(a, b) => {
                    highlight.placeholder = " x00, x00";
                    if let Some(RegisterIdx(r)) = a {
                        highlight.placeholder = &highlight.placeholder[5..];
                        highlight
                            .registers
                            .push((r.as_(), RegisterHighlight::Result));
                    }
                    if let Some(RegisterIdx(r)) = b {
                        highlight.placeholder = "";
                        if s.parse::<Instruction<R>>().is_ok() {
                            highlight.state = HighlightState::Valid;
                        }
                        highlight
                            .registers
                            .push((r.as_(), RegisterHighlight::Operand));
                    }
                }
            }
        } else if !s.contains(' ') {
            highlight.placeholder = " Type your instruction";
            highlight.state = HighlightState::Partial;
        }
        highlight.registers.sort_unstable_by_key(|s| s.0);
        highlight
    }

    pub fn registers(&self) -> &[(u8, RegisterHighlight)] {
        &self.registers
    }

    pub fn program(&self) -> Option<usize> {
        self.program
    }

    pub fn memory(&self) -> Option<usize> {
        self.memory
    }

    pub fn state(&self) -> HighlightState {
        self.state
    }

    pub fn placeholder(&self) -> &'static str {
        self.placeholder
    }
}
