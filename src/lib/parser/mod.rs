use super::{ImmediateHigh, ImmediateLow, RegisterIdx};
use std::fmt;
use std::iter::Peekable;
use std::str::Chars;

const MAX_IMMEDIATE_LOW: u32 = 0xFFF;
const MAX_IMMEDIATE_HIGH: u32 = 0xFFFFF;

#[derive(Debug, Clone, PartialEq, Eq)]
pub enum ParseError {
    WrongFormat,
    OutOfRange,
    UnknownInstruction,
    Incomplete,
}

#[derive(Debug, Clone)]
pub struct Lexer<'a> {
    inner: Peekable<Chars<'a>>,
}

pub type Result<T> = std::result::Result<T, ParseError>;

#[derive(Debug, PartialEq, Eq, Default, Clone, Copy, Hash)]
pub struct Comma;
#[derive(Debug, PartialEq, Eq, Default, Clone, Copy, Hash)]
pub struct ParOpen;
#[derive(Debug, PartialEq, Eq, Default, Clone, Copy, Hash)]
pub struct ParClose;

#[derive(Debug, Clone, PartialEq, Eq)]
pub enum Format {
    Registers(
        Option<RegisterIdx>,
        Option<RegisterIdx>,
        Option<RegisterIdx>,
    ),
    Immediate(
        Option<RegisterIdx>,
        Option<RegisterIdx>,
        Option<ImmediateLow>,
    ),
    LongAdd(Option<RegisterIdx>, Option<ImmediateHigh>),
    StoreLoad(
        Option<RegisterIdx>,
        Option<RegisterIdx>,
        Option<ImmediateLow>,
    ),
    ShortJump(
        Option<RegisterIdx>,
        Option<RegisterIdx>,
        Option<ImmediateLow>,
    ),
    LongJump(Option<RegisterIdx>, Option<ImmediateHigh>),
    Branch(
        Option<RegisterIdx>,
        Option<RegisterIdx>,
        Option<ImmediateLow>,
    ),
    MulAdd(
        Option<RegisterIdx>,
        Option<RegisterIdx>,
        Option<RegisterIdx>,
        Option<RegisterIdx>,
    ),
    Registers2(Option<RegisterIdx>, Option<RegisterIdx>),
}

pub trait PartialParse {
    fn partial_parse(s: &str) -> Result<Format>;
}

impl std::error::Error for ParseError {}

impl fmt::Display for ParseError {
    fn fmt(&self, f: &mut fmt::Formatter) -> fmt::Result {
        match self {
            Self::WrongFormat => write!(f, "Wrong format used for instruction"),
            Self::OutOfRange => write!(f, "A value is out of range"),
            Self::UnknownInstruction => write!(f, "The instruction specified is unknown"),
            Self::Incomplete => write!(f, "The instruction is missing some arguments"),
        }
    }
}

impl Format {
    // r : register
    // i : immediate
    // c : comma
    // p : parenthesis

    pub fn registers(s: &str) -> Result<Self> {
        registers(s).map(|(r1, r2, r3)| Self::Registers(r1, r2, r3))
    }

    pub fn immediate(s: &str) -> Result<Self> {
        immediate(s).map(|(r1, r2, i1)| Self::Immediate(r1, r2, i1))
    }

    pub fn long_add(s: &str) -> Result<Self> {
        long_add(s).map(|(r1, i1)| Self::LongAdd(r1, i1))
    }

    pub fn store_load(s: &str) -> Result<Self> {
        store_load(s).map(|(r1, r2, i1)| Self::StoreLoad(r1, r2, i1))
    }

    pub fn short_jump(s: &str) -> Result<Self> {
        short_jump(s).map(|(r1, r2, i1)| Self::ShortJump(r1, r2, i1))
    }

    pub fn long_jump(s: &str) -> Result<Self> {
        long_jump(s).map(|(r1, i1)| Self::LongJump(r1, i1))
    }

    pub fn branch(s: &str) -> Result<Self> {
        branch(s).map(|(r1, r2, i1)| Self::Branch(r1, r2, i1))
    }

    pub fn muladd(s: &str) -> Result<Self> {
        muladd(s).map(|(r1, r2, r3, r4)| Self::MulAdd(r1, r2, r3, r4))
    }

    pub fn registers_2(s: &str) -> Result<Self> {
        registers_2(s).map(|(r1, r2)| Self::Registers2(r1, r2))
    }
}

impl<'a> Lexer<'a> {
    /// Creates a new Lexer from an input string slice
    fn new(input: &'a str) -> Self {
        Self {
            inner: input.chars().peekable(),
        }
    }

    /// Parses text expecting an integer in the specified base.
    ///
    /// # Arguments
    ///
    /// // TODO
    ///
    /// # Returns
    ///
    /// Returns an integer wrapped in an Option and a Result.
    ///
    /// `None` is returned if the end of the text before any character is parsed,
    /// allowing for partial parsing.
    ///
    /// `Error` is returned if the format is invalid for a number.
    ///
    /// [RegisterIdx]: asmv::registers::RegisterIdx
    ///
    fn get_number<T: Iterator<Item = char>>(
        iter: &mut Peekable<T>,
        radix: u32,
    ) -> Result<Option<u32>> {
        // Accumulator for the result
        let mut number = 0u32;
        // At least one number
        let mut one = false;
        while let Some(n) = iter.peek() {
            if let Some(digit) = n.to_digit(radix) {
                one = true;
                number = number
                    .checked_mul(radix)
                    .and_then(|x| x.checked_add(digit))
                    .ok_or(ParseError::OutOfRange)?;
                iter.next();
            } else if one {
                break;
            } else {
                return Err(ParseError::WrongFormat);
            }
        }
        if one {
            Ok(Some(number))
        } else {
            Ok(None)
        }
    }

    fn num(&mut self) -> Result<Option<u32>> {
        match self.inner.peek() {
            Some('0') => {
                self.inner.next();
                match self.inner.peek() {
                    Some('b') => {
                        self.inner.next();
                        Self::get_number(&mut self.inner, 2)
                    }
                    Some('o') => {
                        self.inner.next();
                        Self::get_number(&mut self.inner, 8)
                    }
                    Some('x') => {
                        self.inner.next();
                        Self::get_number(&mut self.inner, 16)
                    }
                    Some('0'..='9') => Self::get_number(&mut self.inner, 10),
                    _ => Ok(Some(0)),
                }
            }
            Some('1'..='9') => Self::get_number(&mut self.inner, 10),
            Some(_) => Err(ParseError::WrongFormat),
            None => Ok(None),
        }
    }

    /// Parse Register
    ///
    /// Parses text expecting a [Register]. The correct format for a Register is `'x'` followed by a
    /// number less than 32, since there are 32 Registers.
    ///
    /// # Returns
    ///
    /// Returns a [Register's ID][RegisterIdx] wrapped in an Option and a Result.
    ///
    /// `None` is returned if the end of the text is reached and the format is still respected,
    /// allowing for partial parsing.
    ///
    /// `Error` is returned if the format is invalid or if the number is out of range.
    ///
    /// # Examples
    ///
    /// [Register]: asmv::registers::Register
    /// [RegisterIdx]: asmv::registers::RegisterIdx
    ///
    fn reg(&mut self) -> Result<Option<RegisterIdx>> {
        // Must start with 'x'
        if let Some(c) = self.inner.next() {
            if c != 'x' {
                return Err(ParseError::WrongFormat);
            }
        }

        // No more than two digits ("00013" invalid)
        let digit_number = self.inner.clone().take_while(|c| c.is_digit(10)).count();
        if digit_number > 2 {
            return Err(ParseError::WrongFormat);
        }

        // 0 <= reg id < 32
        let n = Self::get_number(&mut self.inner, 10)?;
        match n {
            Some(n @ 0..=31) => Ok(Some(RegisterIdx(n as _))),
            Some(_) => Err(ParseError::OutOfRange),
            None => Ok(None),
        }
    }

    fn imm_low(&mut self) -> Result<Option<ImmediateLow>> {
        match self.num()? {
            Some(n) if n <= MAX_IMMEDIATE_LOW => Ok(Some(ImmediateLow(n as u16))),
            Some(_) => Err(ParseError::OutOfRange),
            None => Ok(None),
        }
    }

    fn imm_high(&mut self) -> Result<Option<ImmediateHigh>> {
        match self.num()? {
            Some(n) if n <= MAX_IMMEDIATE_HIGH => Ok(Some(ImmediateHigh(n))),
            Some(_) => Err(ParseError::OutOfRange),
            None => Ok(None),
        }
    }

    fn par_open(&mut self) -> Result<Option<ParOpen>> {
        match self.inner.next() {
            Some('(') => Ok(Some(ParOpen)),
            None => Ok(None),
            _ => Err(ParseError::WrongFormat),
        }
    }

    fn par_close(&mut self) -> Result<Option<ParClose>> {
        match self.inner.next() {
            Some(')') => Ok(Some(ParClose)),
            None => Ok(None),
            _ => Err(ParseError::WrongFormat),
        }
    }

    fn comma(&mut self) -> Result<Option<Comma>> {
        let ret = match self.inner.next() {
            Some(',') => Ok(Some(Comma)),
            None => Ok(None),
            _ => Err(ParseError::WrongFormat),
        };
        // Clean spaces after comma
        while self.inner.peek() == Some(&' ') || self.inner.peek() == Some(&'\t') {
            self.inner.next();
        }
        ret
    }
}

pub fn registers(
    s: &str,
) -> Result<(
    Option<RegisterIdx>,
    Option<RegisterIdx>,
    Option<RegisterIdx>,
)> {
    let mut lexer = Lexer::new(s);

    let r1 = lexer.reg()?;
    lexer.comma()?;
    let r2 = lexer.reg()?;
    lexer.comma()?;
    let r3 = lexer.reg()?;

    Ok((r1, r2, r3))
}

pub fn immediate(
    s: &str,
) -> Result<(
    Option<RegisterIdx>,
    Option<RegisterIdx>,
    Option<ImmediateLow>,
)> {
    let mut lexer = Lexer::new(s);

    let r1 = lexer.reg()?;
    lexer.comma()?;
    let r2 = lexer.reg()?;
    lexer.comma()?;
    let i1 = lexer.imm_low()?;

    Ok((r1, r2, i1))
}

pub fn long_add(s: &str) -> Result<(Option<RegisterIdx>, Option<ImmediateHigh>)> {
    let mut lexer = Lexer::new(s);

    let r1 = lexer.reg()?;
    lexer.comma()?;
    let i1 = lexer.imm_high()?;

    Ok((r1, i1))
}

pub fn store_load(
    s: &str,
) -> Result<(
    Option<RegisterIdx>,
    Option<RegisterIdx>,
    Option<ImmediateLow>,
)> {
    let mut lexer = Lexer::new(s);

    let r1 = lexer.reg()?;
    lexer.comma()?;
    let r2 = lexer.reg()?;
    lexer.par_open()?;
    let i1 = lexer.imm_low()?;
    lexer.par_close()?;

    Ok((r1, r2, i1))
}

pub fn branch(
    s: &str,
) -> Result<(
    Option<RegisterIdx>,
    Option<RegisterIdx>,
    Option<ImmediateLow>,
)> {
    let mut lexer = Lexer::new(s);

    let r1 = lexer.reg()?;
    lexer.comma()?;
    let r2 = lexer.reg()?;
    lexer.comma()?;
    let i1 = lexer.imm_low()?;

    Ok((r1, r2, i1))
}

pub fn short_jump(
    s: &str,
) -> Result<(
    Option<RegisterIdx>,
    Option<RegisterIdx>,
    Option<ImmediateLow>,
)> {
    let mut lexer = Lexer::new(s);

    let r1 = lexer.reg()?;
    lexer.comma()?;
    let r2 = lexer.reg()?;
    lexer.par_open()?;
    let i1 = lexer.imm_low()?;
    lexer.par_close()?;

    Ok((r1, r2, i1))
}

pub fn long_jump(s: &str) -> Result<(Option<RegisterIdx>, Option<ImmediateHigh>)> {
    let mut lexer = Lexer::new(s);

    let r1 = lexer.reg()?;
    lexer.par_open()?;
    let i1 = lexer.imm_high()?;
    lexer.par_close()?;

    Ok((r1, i1))
}

pub fn muladd(
    s: &str,
) -> Result<(
    Option<RegisterIdx>,
    Option<RegisterIdx>,
    Option<RegisterIdx>,
    Option<RegisterIdx>,
)> {
    let mut lexer = Lexer::new(s);

    let r1 = lexer.reg()?;
    lexer.comma()?;
    let r2 = lexer.reg()?;
    lexer.comma()?;
    let r3 = lexer.reg()?;
    lexer.comma()?;
    let r4 = lexer.reg()?;

    Ok((r1, r2, r3, r4))
}

pub fn registers_2(s: &str) -> Result<(Option<RegisterIdx>, Option<RegisterIdx>)> {
    let mut lexer = Lexer::new(s);

    let r1 = lexer.reg()?;
    lexer.comma()?;
    let r2 = lexer.reg()?;

    Ok((r1, r2))
}

#[cfg(test)]
mod tests {
    use super::*;

    #[cfg(test)]
    mod lexer {
        use super::*;

        #[test]
        fn new() {
            let mut lexer = Lexer::new("42");
            assert_eq!(lexer.inner.next(), Some('4'));
            assert_eq!(lexer.inner.next(), Some('2'));
            assert_eq!(lexer.inner.next(), None);

            let mut lexer = Lexer::new("");
            assert_eq!(lexer.inner.next(), None);
        }

        #[cfg(test)]
        mod get_number {
            use super::*;

            #[test]
            fn basic_10() {
                let expected = [
                    // Text, Result
                    ("1", Ok(Some(1))),
                    ("15", Ok(Some(15))),
                    ("868549654", Ok(Some(868549654))),
                ];

                for (text, res) in &expected {
                    let mut lexer = Lexer::new(text);
                    assert_eq!(Lexer::get_number(&mut lexer.inner, 10), *res);
                }
            }

            #[test]
            fn zeroes() {
                let expected = [
                    ("01", Ok(Some(1))),
                    ("00015", Ok(Some(15))),
                    (
                        "000000000000000000000000000000000000000868549654",
                        Ok(Some(868549654)),
                    ),
                ];

                for (text, res) in &expected {
                    let mut lexer = Lexer::new(text);
                    assert_eq!(Lexer::get_number(&mut lexer.inner, 10), *res);
                }
            }

            #[test]
            fn out_of_range_u32() {
                // Ok
                let mut lexer = Lexer::new("FFFFFFFF");
                assert_eq!(
                    Lexer::get_number(&mut lexer.inner, 16),
                    Ok(Some(0xFFFF_FFFFu32))
                );

                let mut lexer = Lexer::new("4294967295");
                assert_eq!(
                    Lexer::get_number(&mut lexer.inner, 10),
                    Ok(Some(0xFFFF_FFFFu32))
                );

                let mut lexer = Lexer::new("37777777777");
                assert_eq!(
                    Lexer::get_number(&mut lexer.inner, 8),
                    Ok(Some(0xFFFF_FFFFu32))
                );

                let mut lexer = Lexer::new("11111111111111111111111111111111");
                assert_eq!(
                    Lexer::get_number(&mut lexer.inner, 2),
                    Ok(Some(0xFFFF_FFFFu32))
                );

                // Err
                let mut lexer = Lexer::new("100000000");
                assert_eq!(
                    Lexer::get_number(&mut lexer.inner, 16),
                    Err(ParseError::OutOfRange)
                );

                let mut lexer = Lexer::new("4294967296");
                assert_eq!(
                    Lexer::get_number(&mut lexer.inner, 10),
                    Err(ParseError::OutOfRange)
                );

                let mut lexer = Lexer::new("40000000000");
                assert_eq!(
                    Lexer::get_number(&mut lexer.inner, 8),
                    Err(ParseError::OutOfRange)
                );

                let mut lexer = Lexer::new("100000000000000000000000000000000");
                assert_eq!(
                    Lexer::get_number(&mut lexer.inner, 2),
                    Err(ParseError::OutOfRange)
                );
            }

            #[test]
            fn invalid_digit() {
                let expected = [
                    (2, "2"),
                    (2, "a0101"), // Base 2
                    (8, "9"),
                    (8, "87"), // Base 8
                    (10, "a"),
                    (10, " 9b"),
                    (10, "f15"), // Base 10
                    (16, "g"),
                    (16, ")@"),
                    (16, "h01"), // Base 16
                    (2, "-01"),
                    (8, "-5"),
                    (10, "-5"),
                    (16, "-5"), // Negative
                ];

                for (radix, text) in &expected {
                    let mut lexer = Lexer::new(text);
                    assert_eq!(
                        Lexer::get_number(&mut lexer.inner, *radix),
                        Err(ParseError::WrongFormat)
                    );
                }
            }

            #[test]
            fn basic_radixes() {
                let input = [
                    // (number, radix, expected)
                    ("1010", 2, 0b1010),
                    ("1010", 8, 0o1010),
                    ("1010", 10, 1010),
                    ("1010", 16, 0x1010),
                ];

                for (input, radix, output) in &input {
                    let mut lexer = Lexer::new(input);
                    assert_eq!(
                        Lexer::get_number(&mut lexer.inner, *radix),
                        Ok(Some(*output))
                    );
                }
            }

            #[test]
            fn partial_parsing() {
                // Empty text
                let mut lexer = Lexer::new("");
                assert_eq!(Lexer::get_number(&mut lexer.inner, 2), Ok(None));
                assert_eq!(Lexer::get_number(&mut lexer.inner, 8), Ok(None));
                assert_eq!(Lexer::get_number(&mut lexer.inner, 10), Ok(None));
                assert_eq!(Lexer::get_number(&mut lexer.inner, 16), Ok(None));

                // End of text
                let mut lexer = Lexer::new("0b");
                lexer.inner.next();
                lexer.inner.next();
                assert_eq!(Lexer::get_number(&mut lexer.inner, 2), Ok(None));

                let mut lexer = Lexer::new("0x");
                lexer.inner.next();
                lexer.inner.next();
                assert_eq!(Lexer::get_number(&mut lexer.inner, 16), Ok(None));

                let mut lexer = Lexer::new("rS");
                lexer.inner.next();
                lexer.inner.next();
                assert_eq!(Lexer::get_number(&mut lexer.inner, 10), Ok(None));
            }
        }

        #[cfg(test)]
        mod reg {
            use super::*;

            #[test]
            fn basic() {
                let mut lexer = Lexer::new("x14");
                assert_eq!(lexer.reg(), Ok(Some(RegisterIdx(14))));

                let mut lexer = Lexer::new("x31");
                assert_eq!(lexer.reg(), Ok(Some(RegisterIdx(31))));

                let mut lexer = Lexer::new("x32");
                assert_eq!(lexer.reg(), Err(ParseError::OutOfRange));

                let mut lexer = Lexer::new("x45");
                assert_eq!(lexer.reg(), Err(ParseError::OutOfRange));
            }

            #[test]
            fn partial_parsing() {
                let mut lexer = Lexer::new("");
                assert_eq!(lexer.reg(), Ok(None));

                let mut lexer = Lexer::new("x");
                assert_eq!(lexer.reg(), Ok(None));
            }

            #[test]
            fn zeroes() {
                let mut lexer = Lexer::new("x0");
                assert_eq!(lexer.reg(), Ok(Some(RegisterIdx(0))));

                let mut lexer = Lexer::new("x00");
                assert_eq!(lexer.reg(), Ok(Some(RegisterIdx(0))));

                let mut lexer = Lexer::new("x000");
                assert_eq!(lexer.reg(), Err(ParseError::WrongFormat));

                let mut lexer = Lexer::new("x0000");
                assert_eq!(lexer.reg(), Err(ParseError::WrongFormat));

                let mut lexer = Lexer::new("x01");
                assert_eq!(lexer.reg(), Ok(Some(RegisterIdx(1))));

                let mut lexer = Lexer::new("x001");
                assert_eq!(lexer.reg(), Err(ParseError::WrongFormat));
            }

            #[test]
            fn wrong_format() {
                for wrong in &["b14", "x0032", "x456", "X14", "x 4", "xx4", "xC8"] {
                    let mut lexer = Lexer::new(wrong);
                    assert!(lexer.reg().is_err());
                }
            }
        }

        #[cfg(test)]
        mod num {
            use super::*;

            #[test]
            fn basic_radix() {
                let input = [
                    // (number, expected)
                    ("0b1010", Ok(Some(0b1010))),
                    ("0o1010", Ok(Some(0o1010))),
                    ("1010", Ok(Some(1010))),
                    ("001010", Ok(Some(1010))),
                    ("0x1010", Ok(Some(0x1010))),
                    ("0g1010", Ok(Some(0))), // Wrong format dealt with afterward
                ];

                for (input, output) in &input {
                    let mut lexer = Lexer::new(input);
                    assert_eq!(lexer.num(), *output);
                }
            }

            #[test]
            fn wrong_format() {
                for wrong in &["(", "x01", "b1010", "FFF", "0xG"] {
                    let mut lexer = Lexer::new(wrong);
                    assert!(lexer.imm_low().is_err());
                }
            }

            #[test]
            fn partial_parsing() {
                let mut lexer = Lexer::new("");
                assert_eq!(lexer.num(), Ok(None));

                let mut lexer = Lexer::new("0b");
                assert_eq!(lexer.num(), Ok(None));

                let mut lexer = Lexer::new("0o");
                assert_eq!(lexer.num(), Ok(None));

                let mut lexer = Lexer::new("0x");
                assert_eq!(lexer.num(), Ok(None));
            }
        }

        #[cfg(test)]
        mod imm_low {
            use super::*;

            #[test]
            fn basic() {
                let mut lexer = Lexer::new("469");
                assert_eq!(lexer.imm_low(), Ok(Some(ImmediateLow(469))));

                let mut lexer = Lexer::new("0b1100");
                assert_eq!(lexer.imm_low(), Ok(Some(ImmediateLow(0b1100))));

                let mut lexer = Lexer::new("0xFF2");
                assert_eq!(lexer.imm_low(), Ok(Some(ImmediateLow(0xFF2))));
            }

            #[test]
            fn partial_parsing() {
                let mut lexer = Lexer::new("");
                assert_eq!(lexer.imm_low(), Ok(None));

                let mut lexer = Lexer::new("0b");
                assert_eq!(lexer.imm_low(), Ok(None));

                let mut lexer = Lexer::new("0x");
                assert_eq!(lexer.imm_low(), Ok(None));
            }

            #[test]
            fn wrong_format() {
                for wrong in &["(", "x01", "b1010", "FFF", "0xG"] {
                    let mut lexer = Lexer::new(wrong);
                    assert!(lexer.imm_low().is_err());
                }
            }

            #[test]
            fn out_of_bounds() {
                let mut lexer = Lexer::new("0xFFF");
                assert_eq!(lexer.imm_low(), Ok(Some(ImmediateLow(0xFFF))));

                let mut lexer = Lexer::new("0x1000");
                assert_eq!(lexer.imm_low(), Err(ParseError::OutOfRange));

                let mut lexer = Lexer::new("99999999999999999999");
                assert_eq!(lexer.imm_low(), Err(ParseError::OutOfRange));
            }

            #[test]
            fn zeroes() {
                let mut lexer = Lexer::new("00000042");
                assert_eq!(lexer.imm_low(), Ok(Some(ImmediateLow(42))));

                let mut lexer = Lexer::new("10000000");
                assert_eq!(lexer.imm_low(), Err(ParseError::OutOfRange));

                let mut lexer = Lexer::new("00x3");
                // Will return 0, then x will cause an error elsewhere
                assert_ne!(lexer.imm_low(), Ok(Some(ImmediateLow(3))));
            }

            #[test]
            fn greedy() {
                let mut lexer = Lexer::new("0x40b10");
                let res1 = lexer.imm_low();
                let res2 = lexer.imm_low();

                // If it was lasy: (0x4)(0b10)
                assert_ne!(res1, Ok(Some(ImmediateLow(0x4))));
                assert_ne!(res2, Ok(Some(ImmediateLow(0b10))));

                // Since it's greedy: (0x40b10)
                assert_eq!(res1, Err(ParseError::OutOfRange));
                assert_eq!(res2, Ok(None));
            }
        }

        #[cfg(test)]
        mod imm_high {
            use super::*;

            #[test]
            fn basic() {
                let mut lexer = Lexer::new("350469");
                assert_eq!(lexer.imm_high(), Ok(Some(ImmediateHigh(350469))));

                let mut lexer = Lexer::new("0b1100");
                assert_eq!(lexer.imm_high(), Ok(Some(ImmediateHigh(0b1100))));

                let mut lexer = Lexer::new("0xFFF03");
                assert_eq!(lexer.imm_high(), Ok(Some(ImmediateHigh(0xFFF03))));
            }

            #[test]
            fn partial_parsing() {
                let mut lexer = Lexer::new("");
                assert_eq!(lexer.imm_high(), Ok(None));

                let mut lexer = Lexer::new("0b");
                assert_eq!(lexer.imm_high(), Ok(None));

                let mut lexer = Lexer::new("0x");
                assert_eq!(lexer.imm_high(), Ok(None));
            }

            #[test]
            fn wrong_format() {
                for wrong in &["(", "x01", "b1010", "FFF", "0xG"] {
                    let mut lexer = Lexer::new(wrong);
                    assert!(lexer.imm_high().is_err());
                }
            }

            #[test]
            fn out_of_bounds() {
                let mut lexer = Lexer::new("0xFFFFF");
                assert_eq!(lexer.imm_high(), Ok(Some(ImmediateHigh(0xFFFFF))));

                let mut lexer = Lexer::new("0x100000");
                assert_eq!(lexer.imm_high(), Err(ParseError::OutOfRange));

                let mut lexer = Lexer::new("99999999999999999999");
                assert_eq!(lexer.imm_high(), Err(ParseError::OutOfRange));
            }

            #[test]
            fn zeroes() {
                let mut lexer = Lexer::new("00000042");
                assert_eq!(lexer.imm_high(), Ok(Some(ImmediateHigh(42))));

                let mut lexer = Lexer::new("10000000");
                assert_eq!(lexer.imm_high(), Err(ParseError::OutOfRange));

                let mut lexer = Lexer::new("00x3");
                // Will return 0, then x will cause an error after
                assert_ne!(lexer.imm_high(), Ok(Some(ImmediateHigh(3))));
            }

            #[test]
            fn greedy() {
                let mut lexer = Lexer::new("0x40b10");
                let res1 = lexer.imm_high();
                let res2 = lexer.imm_high();

                // If it was lasy: (0x4)(0b10)
                assert_ne!(res1, Ok(Some(ImmediateHigh(0x4))));
                assert_ne!(res2, Ok(Some(ImmediateHigh(0b10))));

                // Since it's greedy: (0x40b10)
                assert_eq!(res1, Ok(Some(ImmediateHigh(0x40B10))));
                assert_eq!(res2, Ok(None));
            }
        }

        #[test]
        fn comma() {
            // Basic
            let mut lexer = Lexer::new(",*");
            assert_eq!(lexer.comma(), Ok(Some(Comma)));
            assert_eq!(lexer.comma(), Err(ParseError::WrongFormat));

            // Partial parsing
            let mut lexer = Lexer::new("");
            assert_eq!(lexer.comma(), Ok(None));

            // Error
            let mut lexer = Lexer::new("(");
            assert_eq!(lexer.comma(), Err(ParseError::WrongFormat));

            // Takes only one
            let mut lexer = Lexer::new(",,*");
            assert_eq!(lexer.comma(), Ok(Some(Comma)));
            assert_eq!(lexer.comma(), Ok(Some(Comma)));
            assert_eq!(lexer.comma(), Err(ParseError::WrongFormat));
        }

        #[test]
        fn par_open() {
            // Basic
            let mut lexer = Lexer::new("(*");
            assert_eq!(lexer.par_open(), Ok(Some(ParOpen)));
            assert_eq!(lexer.par_open(), Err(ParseError::WrongFormat));

            // Partial parsing
            let mut lexer = Lexer::new("");
            assert_eq!(lexer.par_open(), Ok(None));

            // Error
            let mut lexer = Lexer::new(")");
            assert_eq!(lexer.par_open(), Err(ParseError::WrongFormat));

            // Takes only one
            let mut lexer = Lexer::new("((*");
            assert_eq!(lexer.par_open(), Ok(Some(ParOpen)));
            assert_eq!(lexer.par_open(), Ok(Some(ParOpen)));
            assert_eq!(lexer.par_open(), Err(ParseError::WrongFormat));
        }

        #[test]
        fn par_close() {
            // Basic
            let mut lexer = Lexer::new(")*");
            assert_eq!(lexer.par_close(), Ok(Some(ParClose)));
            assert_eq!(lexer.par_close(), Err(ParseError::WrongFormat));

            // Partial parsing
            let mut lexer = Lexer::new("");
            assert_eq!(lexer.par_close(), Ok(None));

            // Error
            let mut lexer = Lexer::new("(");
            assert_eq!(lexer.par_close(), Err(ParseError::WrongFormat));

            // Takes only one
            let mut lexer = Lexer::new("))*");
            assert_eq!(lexer.par_close(), Ok(Some(ParClose)));
            assert_eq!(lexer.par_close(), Ok(Some(ParClose)));
            assert_eq!(lexer.par_close(), Err(ParseError::WrongFormat));
        }

        #[cfg(test)]
        mod space {
            use super::*;

            #[test]
            fn take_spaces() {
                // Spaces
                let mut lexer = Lexer::new("a,    x");
                assert_eq!(lexer.inner.next(), Some('a'));
                lexer.comma().unwrap();
                assert_eq!(lexer.inner.next(), Some('x'));

                // Tabs
                let mut lexer = Lexer::new("b,\t\ty");
                assert_eq!(lexer.inner.next(), Some('b'));
                lexer.comma().unwrap();
                assert_eq!(lexer.inner.next(), Some('y'));

                // Party mix
                let mut lexer = Lexer::new("c,\t  \t z");
                assert_eq!(lexer.inner.next(), Some('c'));
                lexer.comma().unwrap();
                assert_eq!(lexer.inner.next(), Some('z'));
            }
        }
    }

    #[cfg(test)]
    mod registers {
        use super::*;

        #[test]
        fn syntax() {
            let texts = [
                "x21,x31,x12",
                "x21, x31,x12",
                "x21,  x31,x12",
                "x21,x31, x12",
                "x21, x31, x12",
                "x21,  x31, x12",
                "x21,x31,  x12",
                "x21, x31,  x12",
                "x21,  x31,  x12",
            ];

            for text in &texts {
                assert_eq!(
                    registers(text),
                    Ok((
                        Some(RegisterIdx(21)),
                        Some(RegisterIdx(31)),
                        Some(RegisterIdx(12))
                    )),
                );
            }
        }

        #[test]
        fn basic() {
            assert_eq!(
                registers("x21, x31, x0"),
                Ok((
                    Some(RegisterIdx(21)),
                    Some(RegisterIdx(31)),
                    Some(RegisterIdx(0))
                ))
            );
        }

        #[test]
        fn zero_zero() {
            assert_eq!(
                registers("x21, x01, x00"),
                Ok((
                    Some(RegisterIdx(21)),
                    Some(RegisterIdx(1)),
                    Some(RegisterIdx(0))
                )),
            );

            assert!(registers("x21, x01, x000").is_err());
            assert!(registers("x21, x01, x013").is_err());
            assert!(registers("x21, x01, x00031").is_err());
        }

        #[test]
        fn out_of_range() {
            assert!(registers("x21, x31, x31").is_ok());
            assert!(registers("x21, x31, x32").is_err());
            assert!(registers("x21, x31, x286").is_err());
        }

        #[test]
        fn no_separator() {
            assert_eq!(registers("x21x31x0"), Err(ParseError::WrongFormat));
            assert_eq!(registers("x21 x31 x0"), Err(ParseError::WrongFormat));
            assert_eq!(registers("x21,x31x0"), Err(ParseError::WrongFormat));
            assert_eq!(registers("x21x31,x0"), Err(ParseError::WrongFormat));
        }

        #[test]
        fn partial_parsing() {
            assert_eq!(registers("x21"), Ok((Some(RegisterIdx(21)), None, None)));
            assert_eq!(registers("x21,"), Ok((Some(RegisterIdx(21)), None, None)));
            assert_eq!(registers("x21, "), Ok((Some(RegisterIdx(21)), None, None)));
            assert_eq!(registers("x21, x"), Ok((Some(RegisterIdx(21)), None, None)));
            assert_eq!(
                registers("x21, x31"),
                Ok((Some(RegisterIdx(21)), Some(RegisterIdx(31)), None))
            );
        }

        #[test]
        fn wrongformat() {
            assert_eq!(registers("x21("), Err(ParseError::WrongFormat));
        }
    }

    #[cfg(test)]
    mod immediate_low {
        use super::*;

        #[test]
        fn syntax() {
            let texts = [
                "x21,x31,0x0",
                "x21, x31,0x0",
                "x21,  x31,0x0",
                "x21,x31, 0x0",
                "x21, x31, 0x0",
                "x21,  x31, 0x0",
                "x21,x31,  0x0",
                "x21, x31,  0x0",
                "x21,  x31,  0x0",
            ];

            for text in &texts {
                assert_eq!(
                    immediate(text),
                    Ok((
                        Some(RegisterIdx(21)),
                        Some(RegisterIdx(31)),
                        Some(ImmediateLow(0))
                    )),
                );
            }
        }

        #[test]
        fn basic_zero() {
            let texts = [
                "x21, x31, 0",
                "x21, x31, 0000000",
                "x21, x31, 0b0",
                "x21, x31, 0b0000000",
                "x21, x31, 0o0",
                "x21, x31, 0o0000000",
                "x21, x31, 0x0",
                "x21, x31, 0x0000000",
            ];

            for text in &texts {
                assert_eq!(
                    immediate(text),
                    Ok((
                        Some(RegisterIdx(21)),
                        Some(RegisterIdx(31)),
                        Some(ImmediateLow(0))
                    )),
                );
            }
        }

        #[test]
        fn wrong_format() {
            assert!(immediate("x21, x31, x23").is_err());
            assert!(immediate("x21, x31, 0xx23").is_err());
            assert!(immediate("x21, x31, 0bx23").is_err());
        }

        #[test]
        fn zero_repeat() {
            let texts = [
                "x21, x31, 00",
                "x21, x31, 00000000",
                "x21, x31, 0b00",
                "x21, x31, 0b00000000",
                "x21, x31, 0o00",
                "x21, x31, 0o00000000",
                "x21, x31, 0x00",
                "x21, x31, 0x00000000",
            ];

            for text in &texts {
                assert_eq!(
                    immediate(text),
                    Ok((
                        Some(RegisterIdx(21)),
                        Some(RegisterIdx(31)),
                        Some(ImmediateLow(0))
                    )),
                );
            }
        }

        #[test]
        fn out_of_bounds() {
            assert!(immediate("x21, x31, 0xFFF").is_ok());
            assert_eq!(immediate("x21, x31, 0x1000"), Err(ParseError::OutOfRange));
            assert_eq!(immediate("x21, x31, 0xFFFF"), Err(ParseError::OutOfRange));
        }

        #[test]
        fn radix() {
            let texts = [
                "x21, x31, 0xFF",
                "x21, x31, 255",
                "x21, x31, 0b11111111",
                "x21, x31, 0o377",
            ];

            for text in &texts {
                assert_eq!(
                    immediate(text),
                    Ok((
                        Some(RegisterIdx(21)),
                        Some(RegisterIdx(31)),
                        Some(ImmediateLow(255))
                    )),
                )
            }
        }

        #[test]
        fn partial_parsing() {
            assert_eq!(
                immediate("x21, x31"),
                Ok((Some(RegisterIdx(21)), Some(RegisterIdx(31)), None)),
            );
            assert_eq!(
                immediate("x21, x31,"),
                Ok((Some(RegisterIdx(21)), Some(RegisterIdx(31)), None)),
            );

            assert_eq!(
                immediate("x21, x31, "),
                Ok((Some(RegisterIdx(21)), Some(RegisterIdx(31)), None)),
            );

            assert_eq!(
                immediate("x21, x31, 0x"),
                Ok((Some(RegisterIdx(21)), Some(RegisterIdx(31)), None)),
            );
        }
    }

    #[cfg(test)]
    mod immediate_high {
        use super::*;

        #[test]
        fn syntax() {
            let texts = ["x21,0x0", "x21, 0x0", "x21,  0x0"];

            for text in &texts {
                assert_eq!(
                    long_add(text),
                    Ok((Some(RegisterIdx(21)), Some(ImmediateHigh(0)))),
                );
            }
        }

        #[test]
        fn basic_zero() {
            let texts = [
                "x21, 0",
                "x21, 0000000",
                "x21, 0b0",
                "x21, 0b0000000",
                "x21, 0o0",
                "x21, 0o0000000",
                "x21, 0x0",
                "x21, 0x0000000",
            ];

            for text in &texts {
                assert_eq!(
                    long_add(text),
                    Ok((Some(RegisterIdx(21)), Some(ImmediateHigh(0)))),
                );
            }
        }

        #[test]
        fn out_of_bounds() {
            assert!(long_add("x21, 0xFFFFF").is_ok());
            assert_eq!(long_add("x21, 0x100000"), Err(ParseError::OutOfRange));
            assert_eq!(long_add("x21, 0xFFFFFFF"), Err(ParseError::OutOfRange));
        }

        #[test]
        fn wrong_format() {
            assert!(long_add("x21, x23").is_err());
            assert!(long_add("x21, 0xx23").is_err());
            assert!(long_add("x21, 0bx23").is_err());
        }

        #[test]
        fn zero_repeat() {
            let texts = [
                "x21, 00",
                "x21, 00000000",
                "x21, 0b00",
                "x21, 0b00000000",
                "x21, 0o00",
                "x21, 0o00000000",
                "x21, 0x00",
                "x21, 0x00000000",
            ];

            for text in &texts {
                assert_eq!(
                    long_add(text),
                    Ok((Some(RegisterIdx(21)), Some(ImmediateHigh(0)))),
                );
            }
        }

        #[test]
        fn radix() {
            let texts = ["x21, 0xFF", "x21, 255", "x21, 0b11111111", "x21, 0o377"];

            for text in &texts {
                assert_eq!(
                    long_add(text),
                    Ok((Some(RegisterIdx(21)), Some(ImmediateHigh(255)))),
                )
            }
        }

        #[test]
        fn partial_parsing() {
            assert_eq!(long_add("x21"), Ok((Some(RegisterIdx(21)), None)),);
            assert_eq!(long_add("x21,"), Ok((Some(RegisterIdx(21)), None)),);

            assert_eq!(long_add("x21, "), Ok((Some(RegisterIdx(21)), None)),);

            assert_eq!(long_add("x21, 0x"), Ok((Some(RegisterIdx(21)), None)),);
        }
    }

    #[cfg(test)]
    mod short_jump {
        use super::*;

        #[test]
        fn syntax() {
            let pairs = [
                (
                    "x21,x31(0x0)",
                    Ok((
                        Some(RegisterIdx(21)),
                        Some(RegisterIdx(31)),
                        Some(ImmediateLow(0)),
                    )),
                ),
                (
                    "x21, x31(0x0)",
                    Ok((
                        Some(RegisterIdx(21)),
                        Some(RegisterIdx(31)),
                        Some(ImmediateLow(0)),
                    )),
                ),
                (
                    "x21,  x31(0x0)",
                    Ok((
                        Some(RegisterIdx(21)),
                        Some(RegisterIdx(31)),
                        Some(ImmediateLow(0)),
                    )),
                ),
                ("x21, x31 (0x0)", Err(ParseError::WrongFormat)),
                ("x21, x31( 0x0)", Err(ParseError::WrongFormat)),
                ("x21, x31(0x0 )", Err(ParseError::WrongFormat)),
            ];

            for (input, res) in &pairs {
                assert_eq!(short_jump(input), *res);
            }
        }

        #[test]
        fn zeroes() {
            let texts = [
                "x21, x31(0)",
                "x21, x31(00)",
                "x21, x31(0000000)",
                "x21, x31(0b0)",
                "x21, x31(0b00)",
                "x21, x31(0b0000000)",
                "x21, x31(0o0)",
                "x21, x31(0o00)",
                "x21, x31(0o0000000)",
                "x21, x31(0x0)",
                "x21, x31(0x00)",
                "x21, x31(0x0000000)",
            ];

            for text in &texts {
                assert_eq!(
                    short_jump(text),
                    Ok((
                        Some(RegisterIdx(21)),
                        Some(RegisterIdx(31)),
                        Some(ImmediateLow(0))
                    )),
                );
            }
        }

        #[test]
        fn partial_parsing() {
            assert_eq!(
                short_jump("x21, x31"),
                Ok((Some(RegisterIdx(21)), Some(RegisterIdx(31)), None)),
            );
            assert_eq!(
                short_jump("x21, x31("),
                Ok((Some(RegisterIdx(21)), Some(RegisterIdx(31)), None)),
            );

            assert_eq!(
                short_jump("x21, x31(0x"),
                Ok((Some(RegisterIdx(21)), Some(RegisterIdx(31)), None)),
            );
        }
    }

    #[cfg(test)]
    mod branch {
        use super::*;

        #[test]
        fn syntax() {
            let texts = [
                "x21,x31,0x0",
                "x21, x31,0x0",
                "x21,  x31,0x0",
                "x21,x31, 0x0",
                "x21, x31, 0x0",
                "x21,  x31, 0x0",
                "x21,x31,  0x0",
                "x21, x31,  0x0",
                "x21,  x31,  0x0",
            ];

            for text in &texts {
                assert_eq!(
                    branch(text),
                    Ok((
                        Some(RegisterIdx(21)),
                        Some(RegisterIdx(31)),
                        Some(ImmediateLow(0))
                    )),
                );
            }
        }

        #[test]
        fn basic_zero() {
            let texts = [
                "x21, x31, 0",
                "x21, x31, 00",
                "x21, x31, 0000000",
                "x21, x31, 0b0",
                "x21, x31, 0b00",
                "x21, x31, 0b0000000",
                "x21, x31, 0o0",
                "x21, x31, 0o00",
                "x21, x31, 0o0000000",
                "x21, x31, 0x0",
                "x21, x31, 0x00",
                "x21, x31, 0x0000000",
            ];

            for text in &texts {
                assert_eq!(
                    branch(text),
                    Ok((
                        Some(RegisterIdx(21)),
                        Some(RegisterIdx(31)),
                        Some(ImmediateLow(0))
                    )),
                );
            }
        }

        #[test]
        fn partial_parsing() {
            assert_eq!(
                branch("x21, x31"),
                Ok((Some(RegisterIdx(21)), Some(RegisterIdx(31)), None)),
            );
            assert_eq!(
                branch("x21, x31,"),
                Ok((Some(RegisterIdx(21)), Some(RegisterIdx(31)), None)),
            );

            assert_eq!(
                branch("x21, x31, "),
                Ok((Some(RegisterIdx(21)), Some(RegisterIdx(31)), None)),
            );

            assert_eq!(
                branch("x21, x31, 0x"),
                Ok((Some(RegisterIdx(21)), Some(RegisterIdx(31)), None)),
            );
        }
    }

    #[cfg(test)]
    mod store_load {
        use super::*;

        #[test]
        fn syntax() {
            let pairs = [
                (
                    "x21,x31(0x0)",
                    Ok((
                        Some(RegisterIdx(21)),
                        Some(RegisterIdx(31)),
                        Some(ImmediateLow(0)),
                    )),
                ),
                (
                    "x21, x31(0x0)",
                    Ok((
                        Some(RegisterIdx(21)),
                        Some(RegisterIdx(31)),
                        Some(ImmediateLow(0)),
                    )),
                ),
                (
                    "x21,  x31(0x0)",
                    Ok((
                        Some(RegisterIdx(21)),
                        Some(RegisterIdx(31)),
                        Some(ImmediateLow(0)),
                    )),
                ),
                ("x21, x31 (0x0)", Err(ParseError::WrongFormat)),
                ("x21, x31( 0x0)", Err(ParseError::WrongFormat)),
                ("x21, x31(0x0 )", Err(ParseError::WrongFormat)),
            ];

            for (input, res) in &pairs {
                assert_eq!(short_jump(input), *res);
            }
        }

        #[test]
        fn zeroes() {
            let texts = [
                "x21, x31(0)",
                "x21, x31(00)",
                "x21, x31(0000000)",
                "x21, x31(0b0)",
                "x21, x31(0b00)",
                "x21, x31(0b0000000)",
                "x21, x31(0o0)",
                "x21, x31(0o00)",
                "x21, x31(0o0000000)",
                "x21, x31(0x0)",
                "x21, x31(0x00)",
                "x21, x31(0x0000000)",
            ];

            for text in &texts {
                assert_eq!(
                    short_jump(text),
                    Ok((
                        Some(RegisterIdx(21)),
                        Some(RegisterIdx(31)),
                        Some(ImmediateLow(0))
                    )),
                );
            }
        }

        #[test]
        fn partial_parsing() {
            assert_eq!(
                store_load("x21, x31"),
                Ok((Some(RegisterIdx(21)), Some(RegisterIdx(31)), None)),
            );
            assert_eq!(
                store_load("x21, x31("),
                Ok((Some(RegisterIdx(21)), Some(RegisterIdx(31)), None)),
            );

            assert_eq!(
                store_load("x21, x31(0x"),
                Ok((Some(RegisterIdx(21)), Some(RegisterIdx(31)), None)),
            );
        }
    }

    #[cfg(test)]
    mod long_jump {
        use super::*;

        #[test]
        fn syntax() {
            let pairs = [
                (
                    "x31(0x0)",
                    Ok((Some(RegisterIdx(31)), Some(ImmediateHigh(0)))),
                ),
                (
                    "x31(0x0)",
                    Ok((Some(RegisterIdx(31)), Some(ImmediateHigh(0)))),
                ),
                (
                    "x31(0x0)",
                    Ok((Some(RegisterIdx(31)), Some(ImmediateHigh(0)))),
                ),
                ("x31 (0x0)", Err(ParseError::WrongFormat)),
                ("x31( 0x0)", Err(ParseError::WrongFormat)),
                ("x31(0x0 )", Err(ParseError::WrongFormat)),
            ];

            for (input, res) in &pairs {
                assert_eq!(long_jump(input), *res);
            }
        }

        #[test]
        fn zeroes() {
            let texts = [
                "x31(0)",
                "x31(00)",
                "x31(0000000)",
                "x31(0b0)",
                "x31(0b00)",
                "x31(0b0000000)",
                "x31(0o0)",
                "x31(0o00)",
                "x31(0o0000000)",
                "x31(0x0)",
                "x31(0x00)",
                "x31(0x0000000)",
            ];

            for text in &texts {
                assert_eq!(
                    long_jump(text),
                    Ok((Some(RegisterIdx(31)), Some(ImmediateHigh(0)))),
                );
            }
        }

        #[test]
        fn partial_parsing() {
            assert_eq!(long_jump("x31"), Ok((Some(RegisterIdx(31)), None)),);
            assert_eq!(long_jump("x31("), Ok((Some(RegisterIdx(31)), None)),);

            assert_eq!(long_jump("x31(0x"), Ok((Some(RegisterIdx(31)), None)),);
        }
    }

    #[cfg(test)]
    mod muladd {
        use super::*;

        #[test]
        fn syntax() {
            let texts = [
                "x21,x31,x12,x13",
                "x21, x31,x12,x13",
                "x21,  x31, x12,x13",
                "x21,x31,x12, x13",
                "x21, x31,x12, x13",
                "x21,  x31, x12, x13",
                "x21,x31,x12,  x13",
                "x21, x31,x12,  x13",
                "x21,  x31, x12,  x13",
            ];

            for text in &texts {
                assert_eq!(
                    muladd(text),
                    Ok((
                        Some(RegisterIdx(21)),
                        Some(RegisterIdx(31)),
                        Some(RegisterIdx(12)),
                        Some(RegisterIdx(13)),
                    )),
                );
            }
        }

        #[test]
        fn basic() {
            assert_eq!(
                muladd("x21, x31, x14, x0"),
                Ok((
                    Some(RegisterIdx(21)),
                    Some(RegisterIdx(31)),
                    Some(RegisterIdx(14)),
                    Some(RegisterIdx(0))
                ))
            );
        }

        #[test]
        fn zero_zero() {
            assert_eq!(
                muladd("x21, x01, x14, x00"),
                Ok((
                    Some(RegisterIdx(21)),
                    Some(RegisterIdx(1)),
                    Some(RegisterIdx(14)),
                    Some(RegisterIdx(0)),
                )),
            );
            assert_eq!(
                muladd("x21, x01, x14, x0"),
                Ok((
                    Some(RegisterIdx(21)),
                    Some(RegisterIdx(1)),
                    Some(RegisterIdx(14)),
                    Some(RegisterIdx(0)),
                )),
            );

            assert!(muladd("x21, x01, x14, x000").is_err());
            assert!(muladd("x21, x01, x14, x013").is_err());
            assert!(muladd("x21, x01, x14, x00031").is_err());
        }

        #[test]
        fn out_of_range() {
            assert!(muladd("x21, x31, x14, x31").is_ok());
            assert!(muladd("x21, x31, x14, x32").is_err());
            assert!(muladd("x21, x31, x14, x286").is_err());
        }

        #[test]
        fn no_separator() {
            assert_eq!(muladd("x21x31x13x0"), Err(ParseError::WrongFormat));
            assert_eq!(muladd("x21x31x13 x0"), Err(ParseError::WrongFormat));
            assert_eq!(muladd("x21x31 x13x0"), Err(ParseError::WrongFormat));
            assert_eq!(muladd("x21 x31x13x0"), Err(ParseError::WrongFormat));
            assert_eq!(muladd("x21x31 x13 x0"), Err(ParseError::WrongFormat));
            assert_eq!(muladd("x21 x31x13 x0"), Err(ParseError::WrongFormat));
            assert_eq!(muladd("x21 x31 x13x0"), Err(ParseError::WrongFormat));
            assert_eq!(muladd("x21 x31 x13 x0"), Err(ParseError::WrongFormat));
            assert_eq!(muladd("x21,x31,x13x0"), Err(ParseError::WrongFormat));
            assert_eq!(muladd("x21,x31x13,x0"), Err(ParseError::WrongFormat));
            assert_eq!(muladd("x21,x31 x13,x0"), Err(ParseError::WrongFormat));
            assert_eq!(muladd("x21,x31,,x13,x0"), Err(ParseError::WrongFormat));
        }

        #[test]
        fn partial_parsing() {
            assert_eq!(muladd("x21"), Ok((Some(RegisterIdx(21)), None, None, None)));
            assert_eq!(
                muladd("x21,"),
                Ok((Some(RegisterIdx(21)), None, None, None))
            );
            assert_eq!(
                muladd("x21, "),
                Ok((Some(RegisterIdx(21)), None, None, None))
            );
            assert_eq!(
                muladd("x21, x"),
                Ok((Some(RegisterIdx(21)), None, None, None))
            );
            assert_eq!(
                muladd("x21, x31"),
                Ok((Some(RegisterIdx(21)), Some(RegisterIdx(31)), None, None))
            );
        }

        #[test]
        fn wrongformat() {
            assert_eq!(muladd("x21("), Err(ParseError::WrongFormat));
        }
    }

    #[cfg(test)]
    mod registers_2 {
        use super::*;

        #[test]
        fn syntax() {
            let texts = ["x21,x31", "x21, x31", "x21,  x31"];

            for text in &texts {
                assert_eq!(
                    registers_2(text),
                    Ok((Some(RegisterIdx(21)), Some(RegisterIdx(31)))),
                );
            }
        }

        #[test]
        fn zero_zero() {
            assert_eq!(
                registers_2("x21, x0"),
                Ok((Some(RegisterIdx(21)), Some(RegisterIdx(0)))),
            );
            assert_eq!(
                registers_2("x21, x00"),
                Ok((Some(RegisterIdx(21)), Some(RegisterIdx(0)))),
            );

            assert!(registers_2("x21, x000").is_err());
            assert!(registers_2("x21, x013").is_err());
            assert!(registers_2("x21, x00031").is_err());
        }

        #[test]
        fn out_of_range() {
            assert!(registers_2("x21, x31").is_ok());
            assert!(registers_2("x21, x32").is_err());
            assert!(registers_2("x21, x286").is_err());
        }

        #[test]
        fn no_separator() {
            assert_eq!(registers_2("x21x31"), Err(ParseError::WrongFormat));
            assert_eq!(registers_2("x21 x31"), Err(ParseError::WrongFormat));
            assert_eq!(registers_2("x21,,x31"), Err(ParseError::WrongFormat));
        }

        #[test]
        fn partial_parsing() {
            assert_eq!(registers_2("x21"), Ok((Some(RegisterIdx(21)), None)));
            assert_eq!(registers_2("x21,"), Ok((Some(RegisterIdx(21)), None)));
            assert_eq!(registers_2("x21, "), Ok((Some(RegisterIdx(21)), None)));
            assert_eq!(registers_2("x21, x"), Ok((Some(RegisterIdx(21)), None)));
            assert_eq!(
                registers_2("x21, x31"),
                Ok((Some(RegisterIdx(21)), Some(RegisterIdx(31))))
            );
        }

        #[test]
        fn wrongformat() {
            assert_eq!(registers_2("x21("), Err(ParseError::WrongFormat));
        }
    }

    #[test]
    fn from_str() {
        use crate::instructions;

        assert_eq!(
            "addi x31, x0, 23".parse::<instructions::base::Instruction>(),
            Ok(instructions::base::Instruction::Addi(
                RegisterIdx(31),
                RegisterIdx(0),
                ImmediateLow(23)
            ))
        )
    }
}
