use super::CodeIdx;
use num_traits::{AsPrimitive, PrimInt, Signed, WrappingAdd, WrappingSub};
use std::convert::TryFrom;

#[derive(Debug, PartialEq, Eq, Default, Clone, Copy, Hash)]
pub struct ImmediateLow(pub u16);
#[derive(Debug, PartialEq, Eq, Default, Clone, Copy, Hash)]
pub struct ImmediateHigh(pub u32);

pub type Register32Bit = u32;
pub type Register64Bit = u64;
pub type Register128Bit = u128;

/// Yo!
pub trait Register:
    private::Sealed
    + Default
    + PrimInt
    + WrappingAdd
    + WrappingSub
    + std::fmt::Debug
    + std::fmt::UpperHex
    + std::fmt::Binary
    + 'static
    + From<u32>
    + From<u16>
    + From<u8>
    + AsPrimitive<usize>
{
    type Signed: PrimInt
        + 'static
        + From<i32>
        + AsPrimitive<Self>
        + Signed
        + std::fmt::Debug
        + std::fmt::UpperHex
        + std::fmt::Binary;

    type HigherUnsigned: PrimInt
        + 'static
        + AsPrimitive<Self>
        + TryFrom<Self::HigherSigned>
        + std::fmt::Debug
        + std::fmt::UpperHex
        + std::fmt::Binary;

    type HigherSigned: PrimInt
        + 'static
        + From<i32>
        + AsPrimitive<Self>
        + TryFrom<Self::HigherUnsigned>
        + Signed
        + std::fmt::Debug
        + std::fmt::UpperHex
        + std::fmt::Binary;

    /// Convert an low-width immediate to a full register value
    fn from_immediate_low(value: ImmediateLow) -> Self {
        Self::sign_extend(value.0, 12)
    }

    /// Convert an high-width immediate to a full register value
    fn from_immediate_high(value: ImmediateHigh) -> Self {
        Self::sign_extend(value.0 << 12, 32)
    }

    /// Prints the representation in bytes
    fn write_bytes(self, buf: &mut [u8]);

    #[doc(hidden)]
    // Keep the signed value properties true even when resizing
    //
    // Modern computers use two's complement to represent negative
    // numbers. The basic idea is that `x + -x = 0`, so -x is simply
    // what is needed to add to x to give 0. The trick is that, on
    // integer overflow, numbers can wrap to zero.
    //
    // For example, if using a bit width of 8, 3 = 0b0000_0011.
    // Then, -3 = 0b1111_1101, because then 3 + -3 = 0b1000_00000.
    // However, because the width is only 8, it is in fact 0b0000_0000, or 0
    //
    // When changing the width of the integer, one wants to keep
    // this property. This means padding the difference in width with
    // the value of the highest bit.
    //
    // In a sense, it's like "propagating the overflow". You don't want
    // 0b0000_0001_0000_0000 as a result, you want 0b0000_0000_0000_0000.
    // So you really want to add 1's before the negative.
    //
    // -3 goes from 0b1111_1101 to 0b1111_1111_1111_1101, not 0b0000_0000_1111_1101 to keep the overflow property true.
    // This is implemented natively when shifting right with signed
    // values, so shift left then right to acheive this result
    fn sign_extend<T: Into<Self>>(value: T, nbits: u32) -> Self {
        let notherbits = std::mem::size_of::<Self>() as u32 * 8 - nbits;
        value.into().signed_shl(notherbits).signed_shr(notherbits)
    }

    fn to_signed(&self) -> Self::Signed;

    fn to_higher_unsigned(&self) -> Self::HigherUnsigned;

    fn to_higher_signed(&self) -> Self::HigherSigned;
}

/// Prevent the Register trait to be implemented outside the crate
mod private {
    pub trait Sealed {}
    impl Sealed for super::Register32Bit {}
    impl Sealed for super::Register64Bit {}
    impl Sealed for super::Register128Bit {}
}

impl Register for Register32Bit {
    type Signed = i32;
    type HigherUnsigned = u64;
    type HigherSigned = i64;

    fn write_bytes(self, buf: &mut [u8]) {
        buf.copy_from_slice(&self.to_ne_bytes()[..buf.len()]);
    }

    fn to_signed(&self) -> Self::Signed {
        *self as Self::Signed
    }

    fn to_higher_unsigned(&self) -> Self::HigherUnsigned {
        *self as Self::HigherUnsigned
    }

    fn to_higher_signed(&self) -> Self::HigherSigned {
        *self as Self::Signed as Self::HigherSigned
    }
}

impl Register for Register64Bit {
    type Signed = i64;
    type HigherUnsigned = u128;
    type HigherSigned = i128;

    fn write_bytes(self, buf: &mut [u8]) {
        buf.copy_from_slice(&self.to_ne_bytes()[..buf.len()]);
    }

    fn to_signed(&self) -> Self::Signed {
        *self as Self::Signed
    }

    fn to_higher_unsigned(&self) -> Self::HigherUnsigned {
        *self as Self::HigherUnsigned
    }

    fn to_higher_signed(&self) -> Self::HigherSigned {
        *self as Self::Signed as Self::HigherSigned
    }
}

impl Register for Register128Bit {
    type Signed = i128;
    type HigherUnsigned = asmv_big_int::U256;
    type HigherSigned = asmv_big_int::I256;

    fn write_bytes(self, buf: &mut [u8]) {
        buf.copy_from_slice(&self.to_ne_bytes()[..buf.len()]);
    }

    fn to_signed(&self) -> Self::Signed {
        *self as Self::Signed
    }

    fn to_higher_unsigned(&self) -> Self::HigherUnsigned {
        Self::HigherUnsigned::from(*self)
    }

    fn to_higher_signed(&self) -> Self::HigherSigned {
        AsPrimitive::<Self::HigherSigned>::as_(*self)
    }
}

impl ImmediateLow {
    pub fn new(v: u16) -> Option<Self> {
        if v < (1 << 12) {
            Some(Self(v))
        } else {
            None
        }
    }
}

impl ImmediateHigh {
    pub fn new(v: u32) -> Option<Self> {
        if v < (1 << 20) {
            Some(Self(v))
        } else {
            None
        }
    }

    pub fn to_pc<R: Register>(self) -> CodeIdx<R> {
        CodeIdx(R::from_immediate_high(self))
    }
}

use std::fmt;

impl fmt::Display for ImmediateLow {
    fn fmt(&self, f: &mut fmt::Formatter) -> fmt::Result {
        write!(f, "0x{:x}", self.0)
    }
}

impl fmt::Display for ImmediateHigh {
    fn fmt(&self, f: &mut fmt::Formatter) -> fmt::Result {
        write!(f, "0x{:x}", self.0)
    }
}
