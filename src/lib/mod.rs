pub mod instructions;
pub mod parser;
mod registers;
#[cfg(test)]
mod tests;

pub use instructions::Instruction;
pub use registers::*;

use num_traits::AsPrimitive;

use std::fmt;
use std::marker::PhantomData;

#[derive(Debug, PartialEq, Eq)]
pub struct Computer<R: Register, M: ExtensionMult, F: FRegister> {
    /// The extensions of this computer
    extensions: PhantomData<M>,
    /// The 32 registers
    ///
    /// Since 16, 32 and 64 bits are all valid word size, use a trait bound
    registers: [R; 32],
    /// The 32 float registers
    ///
    /// Determine the actual type using a trait bound
    f_registers: [F; 32],
    /// Instruction counter (pc)
    ///
    /// Increment when executing an instruction
    pc: CodeIdx<R>,
    /// The main memory (RAM)
    ///
    /// Size cannot be changed, but is determined at runtime, so use
    /// a boxed slice.
    memory: Box<[u8]>,
    /// The executed instructions
    instructions: Vec<Instruction<R>>,
}

pub struct Mult;
pub struct NoMult;

pub trait ExtensionMult {}
impl ExtensionMult for Mult {}
impl ExtensionMult for NoMult {}

pub type NoFloats = ();
pub type F32 = f32;
pub type F64 = f64;

pub trait FRegister: Default + fmt::Debug + Copy + Clone + 'static {
    const WIDTH: usize;

    fn from_bytes(bytes: &[u8]) -> Self;
    fn write_bytes(self, bytes: &mut [u8]);
}

impl FRegister for NoFloats {
    const WIDTH: usize = 0;

    fn from_bytes(bytes: &[u8]) -> Self {
        assert!(bytes.is_empty());
        ()
    }

    fn write_bytes(self, bytes: &mut [u8]) {
        assert!(bytes.is_empty());
    }
}
impl FRegister for F32 {
    const WIDTH: usize = std::mem::size_of::<F32>();

    fn from_bytes(bytes: &[u8]) -> Self {
        let mut t = [0; Self::WIDTH];
        t.copy_from_slice(bytes);
        Self::from_ne_bytes(t)
    }

    fn write_bytes(self, bytes: &mut [u8]) {
        bytes.copy_from_slice(&self.to_ne_bytes());
    }
}
impl FRegister for F64 {
    const WIDTH: usize = std::mem::size_of::<F64>();

    fn from_bytes(bytes: &[u8]) -> Self {
        let mut t = [0; Self::WIDTH];
        t.copy_from_slice(bytes);
        Self::from_ne_bytes(t)
    }

    fn write_bytes(self, bytes: &mut [u8]) {
        bytes.copy_from_slice(&self.to_ne_bytes());
    }
}

pub struct FloatFormat<'a, F>(&'a [F]);

impl fmt::Display for FloatFormat<'_, F32> {
    fn fmt(&self, f: &mut fmt::Formatter) -> fmt::Result {
        for (i, r) in self.0.iter().enumerate() {
            write!(f, "f{:02}: {:<12}", i, r)?;
            if i % 4 == 3 {
                writeln!(f)?;
            }
        }
        Ok(())
    }
}

impl fmt::Display for FloatFormat<'_, F64> {
    fn fmt(&self, f: &mut fmt::Formatter) -> fmt::Result {
        for (i, r) in self.0.iter().enumerate() {
            write!(f, "f{:02}: {:<12}", i, r)?;
            if i % 4 == 3 {
                writeln!(f)?;
            }
        }
        Ok(())
    }
}

impl fmt::Display for FloatFormat<'_, NoFloats> {
    fn fmt(&self, f: &mut fmt::Formatter) -> fmt::Result {
        write!(f, "Floats not supported")
    }
}

impl<R: Register, M: ExtensionMult, F: FRegister> fmt::Display for Computer<R, M, F>
where
    for<'a> FloatFormat<'a, F>: fmt::Display,
{
    fn fmt(&self, f: &mut fmt::Formatter) -> fmt::Result {
        //Registers
        writeln!(f, "--- REGISTERS ---")?;
        for (i, r) in self.registers.iter().enumerate() {
            write!(f, "x{:02}: 0x{:<10X}", i, r)?;
            if i % 4 == 3 {
                writeln!(f)?;
            }
        }
        writeln!(f)?;

        // Float register
        writeln!(f, "--- FLOAT REGISTERS ---")?;
        writeln!(f, "{}", FloatFormat(&self.f_registers))?;

        //Pc
        writeln!(f, "--- PC ---")?;
        writeln!(f, "pc: {:#x}\n", self.pc.to_address())?;

        //Memory
        writeln!(f, "--- MEMORY ---")?;
        for (i, m) in self.memory.iter().enumerate() {
            write!(f, "{:<4X}", m)?;
            if i % 16 == 15 {
                writeln!(f)?;
            }
        }
        writeln!(f)?;

        //Instructions
        writeln!(f, "--- INSTRUCTIONS ---")?;
        for i in &self.instructions {
            writeln!(f, "{}", i)?;
        }

        Ok(())
    }
}

#[derive(Debug, PartialEq, Eq, Default, Clone, Copy, Hash)]
pub struct RegisterIdx(pub u8);
#[derive(Debug, PartialEq, Eq, Default, Clone, Copy, Hash)]
pub struct CodeIdx<R: Register>(pub R);

impl<R: Register> From<ImmediateLow> for CodeIdx<R> {
    fn from(f: ImmediateLow) -> Self {
        Self((f.0 as u32 / 4u32).into())
    }
}

impl<R: Register> CodeIdx<R> {
    fn to_address(self) -> u32 {
        (self.0.as_()) as u32 * 4u32
    }
}

impl<R: Register> std::ops::Add for CodeIdx<R> {
    type Output = Self;

    fn add(self, other: Self) -> Self::Output {
        Self(self.0 + other.0)
    }
}

impl<R: Register> CodeIdx<R>
where
    u32: AsPrimitive<R>,
{
    fn from_address(r: R) -> Self {
        Self(r / 4u32.as_())
    }
}

impl fmt::Display for RegisterIdx {
    fn fmt(&self, f: &mut fmt::Formatter) -> fmt::Result {
        write!(f, "x{:02}", self.0)
    }
}

impl<R: Register, M: ExtensionMult, F: FRegister> Computer<R, M, F> {
    /// Create a computer with `mem_size` bytes of memory
    pub fn new(mem_size: usize) -> Self {
        let memory = vec![0; mem_size].into_boxed_slice();
        Self::with_mem(memory)
    }

    /// Create a computer with preloaded memory
    pub fn with_mem<T: Into<Box<[u8]>>>(mem: T) -> Self {
        Self {
            registers: Default::default(),
            f_registers: Default::default(),
            pc: CodeIdx::default(),
            memory: mem.into(),
            instructions: Vec::default(),
            extensions: PhantomData,
        }
    }

    pub fn set_mem(&mut self, width: usize, location: R, value: R) {
        let pos = location.as_();
        value.write_bytes(&mut self.memory[pos..pos + width]);
    }

    pub fn load_mem(&self, width: usize, location: R, sign_extend: bool) -> R {
        let pos = location.as_();
        let mut t = R::default();
        for byte in 0..width {
            t = (t << 8) + self.memory[pos + byte].into();
        }
        R::from_be(if sign_extend {
            R::sign_extend(t, width as u32 * 8)
        } else {
            t
        })
    }

    pub fn set_mem_f<T: FRegister>(&mut self, location: R, value: T) {
        let pos = location.as_();
        value.write_bytes(&mut self.memory[pos..pos + T::WIDTH]);
    }

    pub fn load_mem_f<T: FRegister>(&self, location: R) -> F
    where
        T: AsPrimitive<F>,
    {
        let pos = location.as_();

        T::from_bytes(&self.memory[pos..pos + T::WIDTH]).as_()
    }

    pub fn mem(&self) -> &[u8] {
        &self.memory
    }

    pub fn mem_mut(&mut self) -> &mut [u8] {
        &mut self.memory
    }

    /// Get the current location as pointer
    pub fn pos(&self) -> CodeIdx<R> {
        self.pc
    }

    /// Jump to an arbitrary location in the program (set PC)
    pub fn jump<T: Into<CodeIdx<R>>>(&mut self, pos: T) {
        self.pc = pos.into();
    }

    /// Move the PC to the next instruction without executing the command
    pub fn step(&mut self) {
        if self.pc.0.as_() < self.instructions.len().saturating_sub(1) {
            self.pc.0 = self.pc.0 + 1u32.into();
        }
    }

    /// Move the PC to the previous instruction without executing the command
    pub fn step_back(&mut self) {
        if self.pc.0.as_() > 0usize {
            self.pc.0 = self.pc.0 - 1u32.into();
        }
    }

    /// Returns the index of last instruction queued
    pub fn loc_ref(&self) -> usize {
        (self.instructions.len() - 1) * 2
    }

    /// Add an instruction to the program
    pub fn queue(&mut self, instruction: Instruction<R>) {
        self.instructions.push(instruction);
    }

    /// Add a serialized list of instructions to the program
    pub fn queue_prog(&mut self, instructions: &str) -> Result<(), crate::parser::ParseError> {
        for line in instructions.trim().lines() {
            let instruction = line.trim().parse()?;
            self.instructions.push(instruction);
        }
        Ok(())
    }

    /// Returns the current list of instructions waiting to execute
    pub fn instructions(&self) -> &[Instruction<R>] {
        &self.instructions
    }

    /// Returns the current mutable list of instructions waiting to execute.
    pub fn instructions_mut(&mut self) -> &mut Vec<Instruction<R>> {
        &mut self.instructions
    }

    /// Is the computer at the end of its program?
    pub fn at_end(&self) -> bool {
        self.pc.0.as_() == self.instructions.len()
    }

    /// Set a register to a specific value
    fn set(&mut self, idx: RegisterIdx, val: R) {
        // Ignore writes to register 0
        if idx.0 != 0 {
            self.registers[idx.0 as usize] = val;
        }
    }

    /// Get the value of a register
    pub fn get(&self, idx: RegisterIdx) -> R {
        self.registers[idx.0 as usize]
    }

    /// Set a register to a specific value
    fn set_f(&mut self, idx: RegisterIdx, val: F) {
        // Ignore writes to register 0
        if idx.0 != 0 {
            self.f_registers[idx.0 as usize] = val;
        }
    }

    /// Get the value of a register
    pub fn get_f(&self, idx: RegisterIdx) -> F {
        self.f_registers[idx.0 as usize]
    }

    /// Get the value of a register as signed
    pub fn get_signed(&self, idx: RegisterIdx) -> R::Signed {
        self.registers[idx.0 as usize].to_signed()
    }

    /// Get the value of a register as higher
    pub fn get_higher_unsigned(&self, idx: RegisterIdx) -> R::HigherUnsigned {
        self.registers[idx.0 as usize].to_higher_unsigned()
    }

    /// Get the value of a register as higher signed
    pub fn get_higher_signed(&self, idx: RegisterIdx) -> R::HigherSigned {
        self.registers[idx.0 as usize].to_higher_signed()
    }
}
