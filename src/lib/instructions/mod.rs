pub mod base;
pub mod rv128i;
pub mod rv128m;
pub mod rv32f;
pub mod rv32m;
pub mod rv64i;
pub mod rv64m;

use super::{
    instructions,
    parser::{Format, ParseError, PartialParse},
    Computer, ImmediateHigh, ImmediateLow, Register, RegisterIdx,
};
use asmv_derive::computer_type;
use num_traits::AsPrimitive;
use std::marker::PhantomData;
use std::str::FromStr;

#[derive(Debug, Eq, PartialEq, Hash, Clone, Copy)]
pub enum Instruction<R: Register> {
    // Trigger error
    Invalid(PhantomData<R>),
    // Base instructions
    Base(base::Instruction),
    // RV64i
    RV64I(rv64i::Instruction),
    // RV128i
    RV128I(rv128i::Instruction),
    // RVM : Multiply-Divide Instruction Extension
    RV32M(rv32m::Instruction),
    // RVM for 64bit Register
    RV64M(rv64m::Instruction),
    // RVM for 128bit Register
    RV128M(rv128m::Instruction),
    // RVF for single precision floating points (float)
    RV32F(rv32f::Instruction),
}

pub trait Executable {
    /// Start the program and run until completion
    fn run_all(&mut self);

    /// Execute an instruction
    ///
    /// Panics if there is no more instructions to execute. Use at_end
    /// to avoid it
    fn execute(&mut self);
}

computer_type!(
    crate::registers::Register32Bit,
    crate::NoMult,
    crate::NoFloats
);
computer_type!(
    crate::registers::Register64Bit,
    crate::NoMult,
    crate::NoFloats: rv64i
);
computer_type!(
    crate::registers::Register128Bit,
    crate::NoMult,
    crate::NoFloats: rv64i,
    rv128i
);
computer_type!(
    crate::registers::Register32Bit,
    crate::Mult,
    crate::NoFloats: rv32m
);
computer_type!(
    crate::registers::Register64Bit,
    crate::Mult,
    crate::NoFloats: rv64i,
    rv32m,
    rv64m
);
computer_type!(
    crate::registers::Register128Bit,
    crate::Mult,
    crate::NoFloats: rv64i,
    rv128i,
    rv32m,
    rv64m,
    rv128m
);
computer_type!(
    crate::registers::Register32Bit,
    crate::NoMult,
    crate::F32: rv32f
);
computer_type!(
    crate::registers::Register64Bit,
    crate::NoMult,
    crate::F32: rv64i,
    rv32f
);
computer_type!(
    crate::registers::Register128Bit,
    crate::NoMult,
    crate::F32: rv64i,
    rv128i,
    rv32f
);
computer_type!(
    crate::registers::Register32Bit,
    crate::Mult,
    crate::F32: rv32m,
    rv32f
);
computer_type!(
    crate::registers::Register64Bit,
    crate::Mult,
    crate::F32: rv64i,
    rv32m,
    rv64m,
    rv32f
);
computer_type!(
    crate::registers::Register128Bit,
    crate::Mult,
    crate::F32: rv64i,
    rv128i,
    rv32m,
    rv64m,
    rv128m,
    rv32f
);
// TODO: Add for f64f
/*
computer_type!(crate::registers::Register32Bit, crate::NoMult);
computer_type!(crate::registers::Register64Bit, crate::NoMult: rv64i);
computer_type!(
    crate::registers::Register128Bit,
    crate::NoMult: rv64i,
    rv128i
);
computer_type!(crate::registers::Register32Bit, crate::Mult: rv32m);
computer_type!(
    crate::registers::Register64Bit,
    crate::Mult: rv64i,
    rv32m,
    rv64m
);
computer_type!(
    crate::registers::Register128Bit,
    crate::Mult: rv64i,
    rv128i,
    rv32m,
    rv64m,
    rv128m
);
*/

use std::fmt;

impl<R: Register> fmt::Display for Instruction<R> {
    fn fmt(&self, f: &mut fmt::Formatter) -> fmt::Result {
        use Instruction::*;

        match *self {
            // Base instructions
            Base(instruction) => write!(f, "{}", instruction),
            // RV64i
            RV64I(instruction) if std::mem::size_of::<R>() >= 8 => write!(f, "{}", instruction),
            // RV128i
            RV128I(instruction) if std::mem::size_of::<R>() >= 16 => write!(f, "{}", instruction),
            // RV32M
            RV32M(instruction) => write!(f, "{}", instruction),
            // RV64M
            RV64M(instruction) if std::mem::size_of::<R>() >= 8 => write!(f, "{}", instruction),
            // RV128i
            RV128M(instruction) if std::mem::size_of::<R>() >= 16 => write!(f, "{}", instruction),
            // RV32F
            RV32F(instruction) => write!(f, "{}", instruction),
            // Invalid
            _ => write!(f, "Invalid"),
        }
    }
}

// TODO: better way to do this then try each one
impl<R: Register> FromStr for Instruction<R> {
    type Err = ParseError;

    fn from_str(s: &str) -> Result<Self, Self::Err> {
        match instructions::base::Instruction::from_str(s) {
            Err(ParseError::UnknownInstruction) => (),
            i => return i.map(Self::Base),
        }
        match instructions::rv32m::Instruction::from_str(s) {
            Err(ParseError::UnknownInstruction) => (),
            i => return i.map(Self::RV32M),
        }
        if std::mem::size_of::<R>() >= 8 {
            match instructions::rv64i::Instruction::from_str(s) {
                Err(ParseError::UnknownInstruction) => (),
                i => return i.map(Self::RV64I),
            }
            match instructions::rv64m::Instruction::from_str(s) {
                Err(ParseError::UnknownInstruction) => (),
                i => return i.map(Self::RV64M),
            }
        }
        if std::mem::size_of::<R>() >= 16 {
            match instructions::rv128i::Instruction::from_str(s) {
                Err(ParseError::UnknownInstruction) => (),
                i => return i.map(Self::RV128I),
            }
            match instructions::rv128m::Instruction::from_str(s) {
                Err(ParseError::UnknownInstruction) => (),
                i => return i.map(Self::RV128M),
            }
        }
        match instructions::rv32f::Instruction::from_str(s) {
            Err(ParseError::UnknownInstruction) => (),
            i => return i.map(Self::RV32F),
        }
        Err(ParseError::UnknownInstruction)
    }
}

// TODO: better way to do this then try each one
impl<R: Register> PartialParse for Instruction<R> {
    fn partial_parse(s: &str) -> Result<Format, ParseError> {
        match instructions::base::Instruction::partial_parse(s) {
            Err(ParseError::UnknownInstruction) => (),
            i => return i,
        }
        match instructions::rv32m::Instruction::partial_parse(s) {
            Err(ParseError::UnknownInstruction) => (),
            i => return i,
        }
        if std::mem::size_of::<R>() >= 8 {
            match instructions::rv64i::Instruction::partial_parse(s) {
                Err(ParseError::UnknownInstruction) => (),
                i => return i,
            }
            match instructions::rv64m::Instruction::partial_parse(s) {
                Err(ParseError::UnknownInstruction) => (),
                i => return i,
            }
        }
        if std::mem::size_of::<R>() >= 16 {
            match instructions::rv128i::Instruction::partial_parse(s) {
                Err(ParseError::UnknownInstruction) => (),
                i => return i,
            }
            match instructions::rv128m::Instruction::partial_parse(s) {
                Err(ParseError::UnknownInstruction) => (),
                i => return i,
            }
        }
        match instructions::rv32f::Instruction::partial_parse(s) {
            Err(ParseError::UnknownInstruction) => (),
            i => return i,
        }
        Err(ParseError::UnknownInstruction)
    }
}

struct LazyFormat<F>(F)
where
    F: Fn(&'_ mut fmt::Formatter<'_>) -> fmt::Result;

impl<F> fmt::Display for LazyFormat<F>
where
    F: Fn(&'_ mut fmt::Formatter<'_>) -> fmt::Result,
{
    #[inline]
    fn fmt(self: &'_ Self, fmt: &'_ mut fmt::Formatter<'_>) -> fmt::Result {
        (self.0)(fmt)
    }
}

macro_rules! lazy_format {(
    $($args:tt)+
) => (LazyFormat(
    move |fmt: &'_ mut ::core::fmt::Formatter<'_>| {
        write!(fmt, $($args)+)
    }
))}

fn registers(a: RegisterIdx, b: RegisterIdx, c: RegisterIdx) -> impl fmt::Display {
    lazy_format!("{}, {}, {}", a, b, c)
}

fn immediate(a: RegisterIdx, b: RegisterIdx, c: ImmediateLow) -> impl fmt::Display {
    lazy_format!("{}, {}, {}", a, b, c)
}

fn long_add(a: RegisterIdx, b: ImmediateHigh) -> impl fmt::Display {
    lazy_format!("{}, {}", a, b)
}

fn store_load(a: RegisterIdx, b: RegisterIdx, c: ImmediateLow) -> impl fmt::Display {
    lazy_format!("{}, {}({})", a, b, c)
}

fn short_jump(a: RegisterIdx, b: RegisterIdx, c: ImmediateLow) -> impl fmt::Display {
    lazy_format!("{}, {}({})", a, b, c)
}

fn long_jump(a: RegisterIdx, b: ImmediateHigh) -> impl fmt::Display {
    lazy_format!("{}({})", a, b)
}

fn branch(a: RegisterIdx, b: RegisterIdx, c: ImmediateLow) -> impl fmt::Display {
    lazy_format!("{}, {}, {}", a, b, c)
}

fn muladd(a: RegisterIdx, b: RegisterIdx, c: RegisterIdx, d: RegisterIdx) -> impl fmt::Display {
    lazy_format!("{}, {}, {}, {}", a, b, c, d)
}

fn registers_2(a: RegisterIdx, b: RegisterIdx) -> impl fmt::Display {
    lazy_format!("{}, {}", a, b)
}
