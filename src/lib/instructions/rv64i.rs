#![warn(missing_docs)]

//! RV64I Base Integer Instruction Set
//!
//! This instruction set extends the base RV32I, providing instructions that operate on word
//! (32-bits) integer when working with larger registers. These word instructions operates on the
//! lower 32 bits of the integers, sign-extending all the other bits to [XLEN].
//!
//! [XLEN]: ../../trait.Register.html
//!

use crate::{Computer, ExtensionMult, FRegister, ImmediateLow, Register, RegisterIdx};

pub use Instruction::*;

use asmv_derive::ParsableInstruction;
use num_traits::{AsPrimitive, PrimInt};

/// Instructions for the [RV64I](index.html) extension.
///
/// Contains various operations (memory manipulation, arithmetic, boolean) that operate on word
/// (32-bits) integers.
#[derive(Debug, Eq, PartialEq, Hash, Clone, Copy, ParsableInstruction)]
pub enum Instruction {
    // Load
    /// Load Double-Word Instruction
    ///
    /// Loads a Double-Word (64 bits, 8 bytes) value from memory to `rd`. If the register size is
    /// larger than 64 bits, the value is sign-extended to the size of the register. The address in
    /// memory can be obtained by adding the value in `rs1` to the sign-extended `imm` offset.
    ///
    /// # Arguments
    ///
    /// * `rd` - Destination register specifier, where the result is loaded.
    /// * `rs1` - Base memory address, in bytes, to which is added the offset.
    /// * `imm` - Offset, in bytes, to add to `rs1` to obtain the memory address.
    ///
    /// # Examples
    ///
    /// ```
    /// # use asmv::{Computer, Register64Bit, NoMult, NoFloats, Instruction, RegisterIdx, ImmediateLow, ImmediateHigh, ExtensionMult, FRegister, Register};
    /// # use asmv::instructions::{self, Executable};
    /// let mut computer = Computer::<Register64Bit, NoMult, NoFloats>::new(20);
    /// # use std::marker::PhantomData;
    /// # use asmv::CodeIdx;
    /// # struct PubComputer<R: Register, M: ExtensionMult, F: FRegister> {
    /// #     extensions: PhantomData<M>, registers: [R; 32], f_registers: [F; 32], pc: CodeIdx<R>,
    /// #     memory: Box<[u8]>, instructions: Vec<Instruction<R>>,
    /// # }
    /// # let mut computer = unsafe {
    /// #     let mut computer: PubComputer::<Register64Bit, NoMult, NoFloats> = std::mem::transmute(computer);
    /// #     computer.registers[12] = 0x87868584_83828180;
    /// #     let mut computer: Computer::<Register64Bit, NoMult, NoFloats> = std::mem::transmute(computer);
    /// #     computer
    /// # };
    ///
    /// // 12x = 0x87_86_85_84_83_82_81_80
    /// computer.queue(Instruction::RV64I(instructions::rv64i::Sd(
    ///     // Stores 80, 81, 82, 83, 84, 85, 86, 87 from 12x
    ///     RegisterIdx(12),
    ///     RegisterIdx(0),
    ///     ImmediateLow::new(0x000).unwrap(),
    /// )));
    /// computer.queue(Instruction::RV64I(instructions::rv64i::Ld(
    ///     // Loads 0x80_81_82_83_84_85_86_87 into 10x
    ///     RegisterIdx(10),
    ///     RegisterIdx(0),
    ///     ImmediateLow::new(0x000).unwrap(),
    /// )));
    /// computer.run_all();
    ///
    /// assert_eq!(computer.get(RegisterIdx(10)), 0x80_81_82_83_84_85_86_87u64.to_be());
    /// ```
    ///
    #[instruction(store_load)]
    Ld(RegisterIdx, RegisterIdx, ImmediateLow),

    /// Load Word Unsigned Instruction
    ///
    /// Loads a Word (32 bits, 4 bytes) value, zero-extended, from memory to `rd`. The address in
    /// memory can be obtained by adding the value in `rs1` to the sign-extended `imm` offset.
    ///
    /// # Arguments
    ///
    /// * `rd` - Destination register specifier, where the result is loaded.
    /// * `rs1` - Base memory address, in bytes, to which is added the offset.
    /// * `imm` - Offset, in bytes, to add to `rs1` to obtain the memory address.
    ///
    /// # Examples
    ///
    /// ```
    /// # use asmv::{Computer, Register64Bit, NoMult, NoFloats, Instruction, RegisterIdx, ImmediateLow, ImmediateHigh, ExtensionMult, FRegister, Register};
    /// # use asmv::instructions::{self, Executable};
    /// let mut computer = Computer::<Register64Bit, NoMult, NoFloats>::new(20);
    /// # use std::marker::PhantomData;
    /// # use asmv::CodeIdx;
    /// # struct PubComputer<R: Register, M: ExtensionMult, F: FRegister> {
    /// #     extensions: PhantomData<M>, registers: [R; 32], f_registers: [F; 32], pc: CodeIdx<R>,
    /// #     memory: Box<[u8]>, instructions: Vec<Instruction<R>>,
    /// # }
    /// # let mut computer = unsafe {
    /// #     let mut computer: PubComputer::<Register64Bit, NoMult, NoFloats> = std::mem::transmute(computer);
    /// #     computer.registers[12] = 0x87868584_83828180;
    /// #     let mut computer: Computer::<Register64Bit, NoMult, NoFloats> = std::mem::transmute(computer);
    /// #     computer
    /// # };
    ///
    /// // 12x = 0x87_86_85_84_83_82_81_80
    /// computer.queue(Instruction::RV64I(instructions::rv64i::Sd(
    ///     // Stores 80, 81, 82, 83, 84, 85, 86, 87 from 12x
    ///     RegisterIdx(12),
    ///     RegisterIdx(0),
    ///     ImmediateLow::new(0x000).unwrap(),
    /// )));
    /// computer.queue(Instruction::RV64I(instructions::rv64i::Lwu(
    ///     // Loads 0x80_81_82_83 into 10x
    ///     RegisterIdx(10),
    ///     RegisterIdx(0),
    ///     ImmediateLow::new(0x000).unwrap(),
    /// )));
    /// computer.run_all();
    ///
    /// assert_eq!(computer.get(RegisterIdx(10)), 0x00_00_00_00_80_81_82_83u64.to_be());
    /// ```
    ///
    #[instruction(store_load)]
    Lwu(RegisterIdx, RegisterIdx, ImmediateLow),

    // Store
    /// Store Double-Word Instruction
    ///
    /// Stores a Double-Word (64 bits, 8 bytes) value to memory, value taken from `rs2`. The address
    /// in memory can be obtained by adding the value in `rs1` to the sign-extended `imm` offset.
    ///
    /// # Arguments
    ///
    /// * `rd`  - Source register, of which the result is stored.
    /// * `rs1` - Base memory address, in bytes, to which is added the offset.
    /// * `imm` - Offset, in bytes, to add to `rs1` to obtain the memory address.
    ///
    /// # Examples
    ///
    /// ```
    /// # use asmv::{Computer, Register64Bit, NoMult, NoFloats, Instruction, RegisterIdx, ImmediateLow, ImmediateHigh, ExtensionMult, FRegister, Register};
    /// # use asmv::instructions::{self, Executable};
    /// let mut computer = Computer::<Register64Bit, NoMult, NoFloats>::new(20);
    /// # use std::marker::PhantomData;
    /// # use asmv::CodeIdx;
    /// # struct PubComputer<R: Register, M: ExtensionMult, F: FRegister> {
    /// #     extensions: PhantomData<M>, registers: [R; 32], f_registers: [F; 32], pc: CodeIdx<R>,
    /// #     memory: Box<[u8]>, instructions: Vec<Instruction<R>>,
    /// # }
    /// # let mut computer = unsafe {
    /// #     let mut computer: PubComputer::<Register64Bit, NoMult, NoFloats> = std::mem::transmute(computer);
    /// #     computer.registers[12] = 0x87868584_83828180;
    /// #     let mut computer: Computer::<Register64Bit, NoMult, NoFloats> = std::mem::transmute(computer);
    /// #     computer
    /// # };
    ///
    /// // 12x = 0x87_86_85_84_83_82_81_80
    /// computer.queue(Instruction::RV64I(instructions::rv64i::Sd(
    ///     // Stores 80, 81, 82, 83, 84, 85, 86, 87 from 12x
    ///     RegisterIdx(12),
    ///     RegisterIdx(0),
    ///     ImmediateLow::new(0x000).unwrap(),
    /// )));
    /// computer.queue(Instruction::RV64I(instructions::rv64i::Ld(
    ///     // Loads 0x80_81_82_83_84_85_86_87 into 10x
    ///     RegisterIdx(10),
    ///     RegisterIdx(0),
    ///     ImmediateLow::new(0x000).unwrap(),
    /// )));
    /// computer.run_all();
    ///
    /// assert_eq!(computer.get(RegisterIdx(10)), 0x80_81_82_83_84_85_86_87u64.to_be());
    /// ```
    ///
    #[instruction(store_load)]
    Sd(RegisterIdx, RegisterIdx, ImmediateLow),

    // Shift
    /// Word Logical Left Shift Instruction
    ///
    /// Instruction that performs the bitwise left shift operation of the lower 32 bits of `rs1` by
    /// the number of bits specified in the lower 7 bits of `rs2`. The result is sign-extended and
    /// written in `rd`.
    ///
    /// # Arguments
    ///
    /// * `rd`  - Destination register specifier, where the result is stored.
    /// * `rs1` - 1st source register specifier, first operand.
    /// * `rs2` - 2nd source register specifier, shift amount.
    ///
    /// # Examples
    ///
    /// ```
    /// # use asmv::{Computer, Register64Bit, NoMult, NoFloats, Instruction, RegisterIdx, ImmediateLow};
    /// # use asmv::instructions::{self, Executable};
    /// let mut computer = Computer::<Register64Bit, NoMult, NoFloats>::new(0);
    /// # computer.queue(Instruction::Base(instructions::base::Addi(
    /// #     // 12x = 18
    /// #     RegisterIdx(12),
    /// #     RegisterIdx(0),
    /// #     ImmediateLow::new(0x012).unwrap(),
    /// # )));
    /// # computer.queue(Instruction::Base(instructions::base::Addi(
    /// #     // 13x = 2
    /// #     RegisterIdx(13),
    /// #     RegisterIdx(0),
    /// #     ImmediateLow::new(0x002).unwrap(),
    /// # )));
    ///
    /// computer.queue(Instruction::RV64I(instructions::rv64i::Sllw(
    ///     RegisterIdx(10), // rd: 18 << 2
    ///     RegisterIdx(12), // rs1: 18
    ///     RegisterIdx(13), // rs2: 2
    /// )));
    /// computer.run_all();
    ///
    /// assert_eq!(computer.get(RegisterIdx(10)), 72u32.into());
    /// ```
    ///
    #[instruction(registers)]
    Sllw(RegisterIdx, RegisterIdx, RegisterIdx),

    /// Word Immediate Logical Left Shift Instruction
    ///
    /// Instruction that performs the bitwise left shift operation of the lower 32 bits of `rs1` by
    /// the number of bits specified in `imm`. The result is sign-extended and written in `rd`.
    ///
    /// # Arguments
    ///
    /// * `rd` - Destination register specifier, where the result is stored.
    /// * `rs1` - 1st source register specifier, first operand.
    /// * `imm` - Immediate 12-bit integer value, shift amount.
    ///
    /// # Examples
    ///
    /// ```
    /// # use asmv::{Computer, Register64Bit, NoMult, NoFloats, Instruction, RegisterIdx, ImmediateLow};
    /// # use asmv::instructions::{self, Executable};
    ///
    /// let mut computer = Computer::<Register64Bit, NoMult, NoFloats>::new(0);
    /// # computer.queue(Instruction::Base(instructions::base::Addi(
    /// #     // 12x = 18
    /// #     RegisterIdx(12),
    /// #     RegisterIdx(0),
    /// #     ImmediateLow::new(0x012).unwrap(),
    /// # )));
    ///
    /// computer.queue(Instruction::RV64I(instructions::rv64i::Slliw(
    ///     RegisterIdx(10), // rd: 18 << 2
    ///     RegisterIdx(12), // rs1: 18
    ///     ImmediateLow::new(0x002).unwrap(), // imm: 2
    /// )));
    /// computer.run_all();
    ///
    /// assert_eq!(computer.get(RegisterIdx(10)), 72u32.into());
    /// ```
    ///
    #[instruction(immediate)]
    Slliw(RegisterIdx, RegisterIdx, ImmediateLow),

    /// Word Logical Right Shift Instruction
    ///
    /// Instruction that performs the bitwise logical right shift operation of the lower 32 bits of
    /// `rs1` by the number of bits specified in the lower 7 bits of `rs2`, 0-filled on the left.
    /// The result is sign-extended and written in `rd`.
    ///
    /// # Arguments
    ///
    /// * `rd` - Destination register specifier, where the result is stored.
    /// * `rs1` - 1st source register specifier, first operand.
    /// * `rs2` - 2nd source register specifier, shift amount.
    ///
    /// # Examples
    ///
    /// ```
    /// # use asmv::{Computer, Register64Bit, NoMult, NoFloats, Instruction, RegisterIdx, ImmediateLow};
    /// # use asmv::instructions::{self, Executable};
    /// let mut computer = Computer::<Register64Bit, NoMult, NoFloats>::new(0);
    /// # computer.queue(Instruction::Base(instructions::base::Addi(
    /// #     // 12x = 18
    /// #     RegisterIdx(12),
    /// #     RegisterIdx(0),
    /// #     ImmediateLow::new(0x012).unwrap(),
    /// # )));
    /// # computer.queue(Instruction::Base(instructions::base::Addi(
    /// #     // 13x = 2
    /// #     RegisterIdx(13),
    /// #     RegisterIdx(0),
    /// #     ImmediateLow::new(0x002).unwrap(),
    /// # )));
    ///
    /// computer.queue(Instruction::RV64I(instructions::rv64i::Srlw(
    ///     RegisterIdx(10), // rd: 18 >> 2
    ///     RegisterIdx(12), // rs1: 18
    ///     RegisterIdx(13), // rs2: 2
    /// )));
    /// computer.run_all();
    ///
    /// assert_eq!(computer.get(RegisterIdx(10)), 4u32.into());
    /// ```
    ///
    #[instruction(registers)]
    Srlw(RegisterIdx, RegisterIdx, RegisterIdx),

    /// Word Immediate Logical Right Shift Instruction
    ///
    /// Instruction that performs the bitwise logical right shift operation of the lower 32 bits of
    /// `rs1` by the number of bits specified in the lower 7 bits of `imm`, 0-filled on the left.
    /// The result is sign-extended and written in `rd`.
    ///
    /// # Arguments
    ///
    /// * `rd` - Destination register specifier, where the result is stored.
    /// * `rs1` - 1st source register specifier, first operand.
    /// * `imm` - Immediate 12-bit integer value, the shift amount.
    ///
    /// # Examples
    ///
    /// ```
    /// # use asmv::{Computer, Register64Bit, NoMult, NoFloats, Instruction, RegisterIdx, ImmediateLow};
    /// # use asmv::instructions::{self, Executable};
    /// let mut computer = Computer::<Register64Bit, NoMult, NoFloats>::new(0);
    /// # computer.queue(Instruction::Base(instructions::base::Addi(
    /// #     // 12x = 18
    /// #     RegisterIdx(12),
    /// #     RegisterIdx(0),
    /// #     ImmediateLow::new(0x012).unwrap(),
    /// # )));
    /// # computer.queue(Instruction::Base(instructions::base::Addi(
    /// #     // 13x = 2
    /// #     RegisterIdx(13),
    /// #     RegisterIdx(0),
    /// #     ImmediateLow::new(0x002).unwrap(),
    /// # )));
    ///
    /// computer.queue(Instruction::RV64I(instructions::rv64i::Srliw(
    ///     RegisterIdx(10), // rd: 18 >> 2
    ///     RegisterIdx(12), // rs1: 18
    ///     ImmediateLow::new(0x002).unwrap(), // imm: 2
    /// )));
    /// computer.run_all();
    ///
    /// assert_eq!(computer.get(RegisterIdx(10)), 4u32.into());
    /// ```
    ///
    #[instruction(immediate)]
    Srliw(RegisterIdx, RegisterIdx, ImmediateLow),

    /// Word Arithmetic Right Shift Instruction
    ///
    /// Instruction that performs the bitwise arithmetic (also called algebraic) right shift
    /// operation of the lower 32 bits of `rs1` by the number of bits specified in the lower 7 bits
    /// of `rs2`, copying the sign on the left. The result is sign-extended and written in `rd`.
    ///
    /// # Arguments
    ///
    /// * `rd` - Destination register specifier, where the result is stored.
    /// * `rs1` - 1st source register specifier, first operand.
    /// * `rs2` - 2nd source register specifier, shift amount.
    ///
    /// # Examples
    ///
    /// ```
    /// # use asmv::{Computer, Register64Bit, NoMult, NoFloats, Instruction, RegisterIdx, ImmediateLow};
    /// # use asmv::instructions::{self, Executable};
    /// let mut computer = Computer::<Register64Bit, NoMult, NoFloats>::new(0);
    /// # computer.queue(Instruction::Base(instructions::base::Addi(
    /// #     // 12x = 18
    /// #     RegisterIdx(12),
    /// #     RegisterIdx(0),
    /// #     ImmediateLow::new(0x012).unwrap(),
    /// # )));
    /// # computer.queue(Instruction::Base(instructions::base::Addi(
    /// #     // 13x = 2
    /// #     RegisterIdx(13),
    /// #     RegisterIdx(0),
    /// #     ImmediateLow::new(0x002).unwrap(),
    /// # )));
    ///
    /// computer.queue(Instruction::RV64I(instructions::rv64i::Sraw(
    ///     RegisterIdx(10), // rd: 18 >> 2
    ///     RegisterIdx(12), // rs1: 18
    ///     RegisterIdx(13), // rs2: 2
    /// )));
    /// computer.run_all();
    ///
    /// assert_eq!(computer.get(RegisterIdx(10)), 4u32.into());
    /// ```
    ///
    #[instruction(registers)]
    Sraw(RegisterIdx, RegisterIdx, RegisterIdx),

    /// Word Immediate Arithmetic Right Shift Instruction
    ///
    /// Instruction that performs the bitwise arithmetic (also called algebraic) right shift
    /// operation of the lower 32 bits of `rs1` by the number of bits specified in the lower 7 bits
    /// of `imm`, copying the sign on the left. The result is sign-extended and written in `rd`.
    ///
    /// # Arguments
    ///
    /// * `rd` - Destination register specifier, where the result is stored.
    /// * `rs1` - 1st source register specifier, first operand.
    /// * `imm` - Immediate 12-bit integer value, the shift amount.
    ///
    /// # Examples
    ///
    /// ```
    /// # use asmv::{Computer, Register64Bit, NoMult, NoFloats, Instruction, RegisterIdx, ImmediateLow};
    /// # use asmv::instructions::{self, Executable};
    /// let mut computer = Computer::<Register64Bit, NoMult, NoFloats>::new(0);
    /// # computer.queue(Instruction::Base(instructions::base::Addi(
    /// #     // 12x = 18
    /// #     RegisterIdx(12),
    /// #     RegisterIdx(0),
    /// #     ImmediateLow::new(0x012).unwrap(),
    /// # )));
    ///
    /// computer.queue(Instruction::RV64I(instructions::rv64i::Sraiw(
    ///     RegisterIdx(10), // rd: 18 >> 2
    ///     RegisterIdx(12), // rs1: 18
    ///     ImmediateLow::new(0x002).unwrap(), // imm: 2
    /// )));
    /// computer.run_all();
    ///
    /// assert_eq!(computer.get(RegisterIdx(10)), 4u32.into());
    /// ```
    ///
    #[instruction(immediate)]
    Sraiw(RegisterIdx, RegisterIdx, ImmediateLow),

    // Arithmetic
    /// Word Addition Instruction
    ///
    /// Instruction that performs the addition of the lower 32 bits of `rs1` and `rs2`, wrapping
    /// around the overflow. The result is sign-extended and written in `rd`.
    ///
    /// # Arguments
    ///
    /// * `rd` - Destination register specifier, where the result is stored.
    /// * `rs1` - 1st source register specifier, first operand.
    /// * `rs2` - 2nd source register specifier, second operand.
    ///
    /// # Examples
    ///
    /// ```
    /// # use asmv::{Computer, Register64Bit, NoMult, NoFloats, Instruction, RegisterIdx, ImmediateLow};
    /// # use asmv::instructions::{self, Executable};
    /// let mut computer = Computer::<Register64Bit, NoMult, NoFloats>::new(0);
    /// # computer.queue(Instruction::Base(instructions::base::Addi(
    /// #     // 12x = 18
    /// #     RegisterIdx(12),
    /// #     RegisterIdx(0),
    /// #     ImmediateLow::new(0x012).unwrap(),
    /// # )));
    /// # computer.queue(Instruction::Base(instructions::base::Addi(
    /// #     // 13x = 5
    /// #     RegisterIdx(13),
    /// #     RegisterIdx(0),
    /// #     ImmediateLow::new(0x005).unwrap(),
    /// # )));
    ///
    /// computer.queue(Instruction::RV64I(instructions::rv64i::Addw(
    ///     RegisterIdx(10), // rd: 18 + 5
    ///     RegisterIdx(12), // rs1: 18
    ///     RegisterIdx(13), // rs2: 5
    /// )));
    /// computer.run_all();
    ///
    /// assert_eq!(computer.get(RegisterIdx(10)), 23u32.into());
    /// ```
    ///
    #[instruction(registers)]
    Addw(RegisterIdx, RegisterIdx, RegisterIdx),

    /// Word Immediate Addition Instruction
    ///
    /// Instruction that performs the addition of the lower 32 bits of `rs1` and the sign-extended
    /// 12-bits immediate integer `imm`. The overflow is ignored, and the sign-extended result is
    /// written in `rd`.
    ///
    /// # Arguments
    ///
    /// * `rd` - Destination register specifier, where the result is stored.
    /// * `rs1` - 1st source register specifier, first operand.
    /// * `imm` - Immediate 12-bit integer value, the second operand.
    ///
    /// # Examples
    ///
    /// ```
    /// # use asmv::{Computer, Register64Bit, NoMult, NoFloats, Instruction, RegisterIdx, ImmediateLow};
    /// # use asmv::instructions::{self, Executable};
    /// let mut computer = Computer::<Register64Bit, NoMult, NoFloats>::new(0);
    /// # computer.queue(Instruction::Base(instructions::base::Addi(
    /// #     // 12x = 18
    /// #     RegisterIdx(12),
    /// #     RegisterIdx(0),
    /// #     ImmediateLow::new(0x012).unwrap(),
    /// # )));
    ///
    /// computer.queue(Instruction::RV64I(instructions::rv64i::Addiw(
    ///     RegisterIdx(10), // rd: 18 + 5
    ///     RegisterIdx(12), // rs1: 18
    ///     ImmediateLow::new(0x005).unwrap(), // imm: 5
    /// )));
    /// computer.run_all();
    ///
    /// assert_eq!(computer.get(RegisterIdx(10)), 23u32.into());
    /// ```
    ///
    #[instruction(immediate)]
    Addiw(RegisterIdx, RegisterIdx, ImmediateLow),

    /// Word Subtraction Instruction
    ///
    /// Instruction that performs the subtraction of the lower 32 bits of `rs1` by the corresponding
    /// bits of `rs2`, ignoring the overflow. The sign-extended result is written in `rd`.
    ///
    /// # Arguments
    ///
    /// * `rd` - Destination register specifier, where the result is stored.
    /// * `rs1` - 1st source register specifier, first operand.
    /// * `rs2` - 2nd source register specifier, second operand.
    ///
    /// # Examples
    ///
    /// ```
    /// # use asmv::{Computer, Register64Bit, NoMult, NoFloats, Instruction, RegisterIdx, ImmediateLow};
    /// # use asmv::instructions::{self, Executable};
    /// let mut computer = Computer::<Register64Bit, NoMult, NoFloats>::new(0);
    /// # computer.queue(Instruction::Base(instructions::base::Addi(
    /// #     // 12x = 18
    /// #     RegisterIdx(12),
    /// #     RegisterIdx(0),
    /// #     ImmediateLow::new(0x012).unwrap(),
    /// # )));
    /// # computer.queue(Instruction::Base(instructions::base::Addi(
    /// #     // 13x = 5
    /// #     RegisterIdx(13),
    /// #     RegisterIdx(0),
    /// #     ImmediateLow::new(0x005).unwrap(),
    /// # )));
    ///
    /// computer.queue(Instruction::RV64I(instructions::rv64i::Subw(
    ///     RegisterIdx(10), // rd: 18 - 5
    ///     RegisterIdx(12), // rs1: 18
    ///     RegisterIdx(13), // rs2: 5
    /// )));
    /// computer.run_all();
    ///
    /// assert_eq!(computer.get(RegisterIdx(10)), 13u32.into());
    /// ```
    ///
    #[instruction(registers)]
    Subw(RegisterIdx, RegisterIdx, RegisterIdx),
}

impl<R, M, F> Computer<R, M, F>
where
    F: FRegister,
    M: ExtensionMult,
    R: AsPrimitive<u32>,
    R: Register,
{
    /// Execute an instruction
    ///
    /// Panics if there is no more instructions to execute. Use at_end
    /// to avoid it
    pub fn execute_rv64i(&mut self, instruction: Instruction) {
        use Instruction::*;
        match instruction {
            // TODO change for "32"
            // Load
            Ld(result, target, offset) => {
                let val = self.get(target) + R::from_immediate_low(offset);
                self.set(result, self.load_mem(8, val, true));
            }
            Lwu(result, target, offset) => {
                let val = self.get(target) + R::from_immediate_low(offset);
                self.set(result, self.load_mem(4, val, false));
            }
            // Stores
            Sd(value, target, offset) => self.set_mem(
                8,
                self.get(target) + R::from_immediate_low(offset),
                self.get(value),
            ),
            // Shifts
            Sllw(result, a, b) => self.set(
                result,
                R::sign_extend::<u32>(
                    PrimInt::unsigned_shl(
                        AsPrimitive::<u32>::as_(self.get(a)),
                        AsPrimitive::<_>::as_(self.get(b)),
                    ),
                    32,
                ),
            ),
            Slliw(result, a, b) => self.set(
                result,
                R::sign_extend::<u32>(
                    PrimInt::unsigned_shl(
                        AsPrimitive::<u32>::as_(self.get(a)),
                        AsPrimitive::<_>::as_(R::from_immediate_low(b)),
                    ),
                    32,
                ),
            ),
            Srlw(result, a, b) => self.set(
                result,
                PrimInt::unsigned_shr(
                    AsPrimitive::<u32>::as_(self.get(a)),
                    AsPrimitive::<_>::as_(self.get(b)),
                )
                .into(),
            ),
            Srliw(result, a, b) => self.set(
                result,
                PrimInt::unsigned_shr(
                    AsPrimitive::<u32>::as_(self.get(a)),
                    AsPrimitive::<_>::as_(R::from_immediate_low(b)),
                )
                .into(),
            ),
            Sraw(result, a, b) => self.set(
                result,
                R::sign_extend::<u32>(
                    PrimInt::signed_shr(
                        AsPrimitive::<u32>::as_(self.get(a)),
                        AsPrimitive::<_>::as_(self.get(b)),
                    ),
                    32,
                ),
            ),
            Sraiw(result, a, b) => self.set(
                result,
                R::sign_extend::<u32>(
                    PrimInt::signed_shr(
                        AsPrimitive::<u32>::as_(self.get(a)),
                        AsPrimitive::<_>::as_(R::from_immediate_low(b)),
                    ),
                    32,
                ),
            ),
            // Arithmetic
            Addw(result, a, b) => self.set(
                result,
                R::sign_extend(
                    AsPrimitive::<u32>::as_(self.get(a)).wrapping_add(self.get(b).as_()),
                    32,
                ),
            ),
            Addiw(result, a, b) => self.set(
                result,
                R::sign_extend(
                    AsPrimitive::<u32>::as_(self.get(a))
                        .wrapping_add(R::from_immediate_low(b).as_()),
                    32,
                ),
            ),
            Subw(result, a, b) => self.set(
                result,
                R::sign_extend(
                    AsPrimitive::<u32>::as_(self.get(a)).wrapping_sub(self.get(b).as_()),
                    32,
                ),
            ),
        }
    }
}
