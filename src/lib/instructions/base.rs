// #![warn(missing_docs)]

//! RV32I Base Instruction Set
//!
//! This base instruction set is included by default as the base instruction set for all program.
//! It provides a variety of instructions for:
//! * Loading and storing memory;
//! * Arithmetic and logical operation on integers;
//! * Comparison and conditional branching.
//!

use crate::{
    CodeIdx, Computer, ExtensionMult, FRegister, ImmediateHigh, ImmediateLow, Register, RegisterIdx,
};

pub use Instruction::*;

use asmv_derive::ParsableInstruction;
use num_traits::AsPrimitive;

/// Instructions for the RV32I base instructions set
#[derive(Debug, Eq, PartialEq, Hash, Clone, Copy, ParsableInstruction)]
pub enum Instruction {
    // Load
    /// Load Byte Instruction
    ///
    /// Loads a Byte (8 bits, 1 byte) value, sign-extended, from memory to `rd`. The address in
    /// memory can be obtained by adding the value in `rs1` to the sign-extended `imm` offset.
    ///
    /// # Arguments
    ///
    /// * `rd` - Destination register specifier, where the result is loaded.
    /// * `rs1` - Base memory address, in bytes, to which is added the offset.
    /// * `imm` - Offset, in bytes, to add to `rs1` to obtain the memory address.
    ///
    /// # Examples
    ///
    /// ```
    /// # use asmv::{Computer, Register32Bit, NoMult, NoFloats, Instruction, RegisterIdx, ImmediateLow, ImmediateHigh};
    /// # use asmv::instructions::{self, Executable};
    /// let mut computer = Computer::<Register32Bit, NoMult, NoFloats>::new(20);
    /// # computer.queue(Instruction::Base(instructions::base::Lui(
    /// #     // 12x = 0x83_82_80_00
    /// #     RegisterIdx(12),
    /// #     ImmediateHigh::new(0x83_82_8).unwrap(),
    /// # )));
    /// # computer.queue(Instruction::Base(instructions::base::Addi(
    /// #     // 12x = 0x83_82_81_80
    /// #     RegisterIdx(12),
    /// #     RegisterIdx(12),
    /// #     ImmediateLow::new(0x1_80).unwrap(),
    /// # )));
    ///
    /// // 12x = 0x83_82_81_80
    ///
    /// computer.queue(Instruction::Base(instructions::base::Sw(
    ///     // Stores 80, 81, 82, 83 from 12x
    ///     RegisterIdx(12),
    ///     RegisterIdx(0),
    ///     ImmediateLow::new(0x000).unwrap(),
    /// )));
    /// computer.queue(Instruction::Base(instructions::base::Lb(
    ///     // Loads 0x80 into 10x
    ///     RegisterIdx(10),
    ///     RegisterIdx(0),
    ///     ImmediateLow::new(0x000).unwrap(),
    /// )));
    /// computer.run_all();
    ///
    /// assert_eq!(computer.get(RegisterIdx(10)), u32::from_be(0xFF_FF_FF_80u32));
    /// ```
    ///
    #[instruction(store_load)]
    Lb(RegisterIdx, RegisterIdx, ImmediateLow),

    /// Load Byte Unsigned Instruction
    ///
    /// Loads a Byte (8 bits, 1 byte) value, zero-extended, from memory to `rd`. The address in
    /// memory can be obtained by adding the value in `rs1` to the sign-extended `imm` offset.
    ///
    /// # Arguments
    ///
    /// * `rd` - Destination register specifier, where the result is loaded.
    /// * `rs1` - Base memory address, in bytes, to which is added the offset.
    /// * `imm` - Offset, in bytes, to add to `rs1` to obtain the memory address.
    ///
    /// # Examples
    ///
    /// ```
    /// # use asmv::{Computer, Register32Bit, NoMult, NoFloats, Instruction, RegisterIdx, ImmediateLow, ImmediateHigh};
    /// # use asmv::instructions::{self, Executable};
    /// let mut computer = Computer::<Register32Bit, NoMult, NoFloats>::new(20);
    /// # computer.queue(Instruction::Base(instructions::base::Lui(
    /// #     // 12x = 0x83_82_80_00
    /// #     RegisterIdx(12),
    /// #     ImmediateHigh::new(0x83_82_8).unwrap(),
    /// # )));
    /// # computer.queue(Instruction::Base(instructions::base::Addi(
    /// #     // 12x = 0x83_82_81_80
    /// #     RegisterIdx(12),
    /// #     RegisterIdx(12),
    /// #     ImmediateLow::new(0x1_80).unwrap(),
    /// # )));
    ///
    /// // 12x = 0x83_82_81_80
    ///
    /// computer.queue(Instruction::Base(instructions::base::Sw(
    ///     // Stores 80, 81, 82, 83 from 12x
    ///     RegisterIdx(12),
    ///     RegisterIdx(0),
    ///     ImmediateLow::new(0x000).unwrap(),
    /// )));
    /// computer.queue(Instruction::Base(instructions::base::Lbu(
    ///     // Loads 0x80 into 10x
    ///     RegisterIdx(10),
    ///     RegisterIdx(0),
    ///     ImmediateLow::new(0x000).unwrap(),
    /// )));
    /// computer.run_all();
    ///
    /// assert_eq!(computer.get(RegisterIdx(10)), u32::from_be(0x00_00_00_80u32));
    /// ```
    ///
    #[instruction(store_load)]
    Lbu(RegisterIdx, RegisterIdx, ImmediateLow),

    /// Load Half-Word Instruction
    ///
    /// Loads a Half-Word (16 bits, 2 bytes) value, sign-extended, from memory to `rd`. The address
    /// in memory can be obtained by adding the value in `rs1` to the sign-extended `imm` offset.
    ///
    /// # Arguments
    ///
    /// * `rd` - Destination register specifier, where the result is loaded.
    /// * `rs1` - Base memory address, in bytes, to which is added the offset.
    /// * `imm` - Offset, in bytes, to add to `rs1` to obtain the memory address.
    ///
    /// # Examples
    ///
    /// ```
    /// # use asmv::{Computer, Register32Bit, NoMult, NoFloats, Instruction, RegisterIdx, ImmediateLow, ImmediateHigh};
    /// # use asmv::instructions::{self, Executable};
    /// let mut computer = Computer::<Register32Bit, NoMult, NoFloats>::new(20);
    /// # computer.queue(Instruction::Base(instructions::base::Lui(
    /// #     // 12x = 0x83_82_80_00
    /// #     RegisterIdx(12),
    /// #     ImmediateHigh::new(0x83_82_8).unwrap(),
    /// # )));
    /// # computer.queue(Instruction::Base(instructions::base::Addi(
    /// #     // 12x = 0x83_82_81_80
    /// #     RegisterIdx(12),
    /// #     RegisterIdx(12),
    /// #     ImmediateLow::new(0x1_80).unwrap(),
    /// # )));
    ///
    /// // 12x = 0x83_82_81_80
    ///
    /// computer.queue(Instruction::Base(instructions::base::Sw(
    ///     // Stores 80, 81, 82, 83 from 12x
    ///     RegisterIdx(12),
    ///     RegisterIdx(0),
    ///     ImmediateLow::new(0x000).unwrap(),
    /// )));
    /// computer.queue(Instruction::Base(instructions::base::Lh(
    ///     // Loads 0x80_81 into 10x
    ///     RegisterIdx(10),
    ///     RegisterIdx(0),
    ///     ImmediateLow::new(0x000).unwrap(),
    /// )));
    /// computer.run_all();
    ///
    /// assert_eq!(computer.get(RegisterIdx(10)), u32::from_be(0xFF_FF_80_81u32));
    /// ```
    ///
    #[instruction(store_load)]
    Lh(RegisterIdx, RegisterIdx, ImmediateLow),

    /// Load Half-Word Instruction
    ///
    /// Loads a Half-Word (16 bits, 2 bytes) value, zero-extended, from memory to `rd`. The address
    /// in memory can be obtained by adding the value in `rs1` to the sign-extended `imm` offset.
    ///
    /// # Arguments
    ///
    /// * `rd` - Destination register specifier, where the result is loaded.
    /// * `rs1` - Base memory address, in bytes, to which is added the offset.
    /// * `imm` - Offset, in bytes, to add to `rs1` to obtain the memory address.
    ///
    /// # Examples
    ///
    /// ```
    /// # use asmv::{Computer, Register32Bit, NoMult, NoFloats, Instruction, RegisterIdx, ImmediateLow, ImmediateHigh};
    /// # use asmv::instructions::{self, Executable};
    /// let mut computer = Computer::<Register32Bit, NoMult, NoFloats>::new(20);
    /// # computer.queue(Instruction::Base(instructions::base::Lui(
    /// #     // 12x = 0x83_82_80_00
    /// #     RegisterIdx(12),
    /// #     ImmediateHigh::new(0x83_82_8).unwrap(),
    /// # )));
    /// # computer.queue(Instruction::Base(instructions::base::Addi(
    /// #     // 12x = 0x83_82_81_80
    /// #     RegisterIdx(12),
    /// #     RegisterIdx(12),
    /// #     ImmediateLow::new(0x1_80).unwrap(),
    /// # )));
    ///
    /// // 12x = 0x83_82_81_80
    ///
    /// computer.queue(Instruction::Base(instructions::base::Sw(
    ///     // Stores 80, 81, 82, 83 from 12x
    ///     RegisterIdx(12),
    ///     RegisterIdx(0),
    ///     ImmediateLow::new(0x000).unwrap(),
    /// )));
    /// computer.queue(Instruction::Base(instructions::base::Lhu(
    ///     // Loads 0x80_81 into 10x
    ///     RegisterIdx(10),
    ///     RegisterIdx(0),
    ///     ImmediateLow::new(0x000).unwrap(),
    /// )));
    /// computer.run_all();
    ///
    /// assert_eq!(computer.get(RegisterIdx(10)), u32::from_be(0x00_00_80_81u32));
    /// ```
    ///
    #[instruction(store_load)]
    Lhu(RegisterIdx, RegisterIdx, ImmediateLow),

    /// Load Word Instruction
    ///
    /// Loads a Word (32 bits, 4 bytes) value from memory to `rd`. If the register size is larger
    /// than 32 bits, the value is sign-extended to the size of the register. The address in memory
    /// can be obtained by adding the value in `rs1` to the sign-extended `imm` offset.
    ///
    /// # Arguments
    ///
    /// * `rd` - Destination register specifier, where the result is loaded.
    /// * `rs1` - Base memory address, in bytes, to which is added the offset.
    /// * `imm` - Offset, in bytes, to add to `rs1` to obtain the memory address.
    ///
    /// # Examples
    ///
    /// ```
    /// # use asmv::{Computer, Register32Bit, NoMult, NoFloats, Instruction, RegisterIdx, ImmediateLow, ImmediateHigh};
    /// # use asmv::instructions::{self, Executable};
    /// let mut computer = Computer::<Register32Bit, NoMult, NoFloats>::new(20);
    /// # computer.queue(Instruction::Base(instructions::base::Lui(
    /// #     // 12x = 0x83_82_80_00
    /// #     RegisterIdx(12),
    /// #     ImmediateHigh::new(0x83_82_8).unwrap(),
    /// # )));
    /// # computer.queue(Instruction::Base(instructions::base::Addi(
    /// #     // 12x = 0x83_82_81_80
    /// #     RegisterIdx(12),
    /// #     RegisterIdx(12),
    /// #     ImmediateLow::new(0x1_80).unwrap(),
    /// # )));
    ///
    /// // 12x = 0x83_82_81_80
    ///
    /// computer.queue(Instruction::Base(instructions::base::Sw(
    ///     // Stores 80, 81, 82, 83 from 12x
    ///     RegisterIdx(12),
    ///     RegisterIdx(0),
    ///     ImmediateLow::new(0x000).unwrap(),
    /// )));
    /// computer.queue(Instruction::Base(instructions::base::Lw(
    ///     // Loads 0x80_81_82_83 into 10x
    ///     RegisterIdx(10),
    ///     RegisterIdx(0),
    ///     ImmediateLow::new(0x000).unwrap(),
    /// )));
    /// computer.run_all();
    ///
    /// assert_eq!(computer.get(RegisterIdx(10)), u32::from_be(0x80_81_82_83u32));
    /// ```
    ///
    #[instruction(store_load)]
    Lw(RegisterIdx, RegisterIdx, ImmediateLow),

    // Store
    /// Store Byte Instruction
    ///
    /// Stores a Byte (8 bits, 1 byte) value to memory, value taken from `rs2`. The address in
    /// memory can be obtained by adding the value in `rs1` to the sign-extended `imm` offset.
    /// Stores follow the little-endian convention, meaning the bytes are written from the least
    /// significant to the most significant.
    ///
    /// # Arguments
    ///
    /// * `rd` - Source register, of which the result is stored.
    /// * `rs1` - Base memory address, in bytes, to which is added the offset.
    /// * `imm` - Offset, in bytes, to add to `rs1` to obtain the memory address.
    ///
    /// # Examples
    ///
    /// ```
    /// # use asmv::{Computer, Register32Bit, NoMult, NoFloats, Instruction, RegisterIdx, ImmediateLow, ImmediateHigh};
    /// # use asmv::instructions::{self, Executable};
    /// let mut computer = Computer::<Register32Bit, NoMult, NoFloats>::new(20);
    /// # computer.queue(Instruction::Base(instructions::base::Lui(
    /// #     // 12x = 0x83_82_80_00
    /// #     RegisterIdx(12),
    /// #     ImmediateHigh::new(0x83_82_8).unwrap(),
    /// # )));
    /// # computer.queue(Instruction::Base(instructions::base::Addi(
    /// #     // 12x = 0x83_82_81_80
    /// #     RegisterIdx(12),
    /// #     RegisterIdx(12),
    /// #     ImmediateLow::new(0x1_80).unwrap(),
    /// # )));
    ///
    /// // 12x = 0x83_82_81_80
    ///
    /// computer.queue(Instruction::Base(instructions::base::Sb(
    ///     // Stores 80 from 12x
    ///     RegisterIdx(12),
    ///     RegisterIdx(0),
    ///     ImmediateLow::new(0x000).unwrap(),
    /// )));
    /// computer.queue(Instruction::Base(instructions::base::Lw(
    ///     // Loads 0x80_00_00_00 into 10x
    ///     RegisterIdx(10),
    ///     RegisterIdx(0),
    ///     ImmediateLow::new(0x000).unwrap(),
    /// )));
    /// computer.run_all();
    ///
    /// assert_eq!(computer.get(RegisterIdx(10)), 0x80_00_00_00u32.to_be());
    /// ```
    ///
    #[instruction(store_load)]
    Sb(RegisterIdx, RegisterIdx, ImmediateLow),

    /// Store Half-Word Instruction
    ///
    /// Stores a Half-Word (16 bits, 2 bytes) value to memory, value taken from `rs2`. The address
    /// in memory can be obtained by adding the value in `rs1` to the sign-extended `imm` offset.
    /// Stores follow the little-endian convention, meaning the bytes are written from the least
    /// significant to the most significant.
    ///
    /// # Arguments
    ///
    /// * `rd` - Source register, of which the result is stored.
    /// * `rs1` - Base memory address, in bytes, to which is added the offset.
    /// * `imm` - Offset, in bytes, to add to `rs1` to obtain the memory address.
    ///
    /// # Examples
    ///
    /// ```
    /// # use asmv::{Computer, Register32Bit, NoMult, NoFloats, Instruction, RegisterIdx, ImmediateLow, ImmediateHigh};
    /// # use asmv::instructions::{self, Executable};
    /// let mut computer = Computer::<Register32Bit, NoMult, NoFloats>::new(20);
    /// # computer.queue(Instruction::Base(instructions::base::Lui(
    /// #     // 12x = 0x83_82_80_00
    /// #     RegisterIdx(12),
    /// #     ImmediateHigh::new(0x83_82_8).unwrap(),
    /// # )));
    /// # computer.queue(Instruction::Base(instructions::base::Addi(
    /// #     // 12x = 0x83_82_81_80
    /// #     RegisterIdx(12),
    /// #     RegisterIdx(12),
    /// #     ImmediateLow::new(0x1_80).unwrap(),
    /// # )));
    ///
    /// // 12x = 0x83_82_81_80
    ///
    /// computer.queue(Instruction::Base(instructions::base::Sh(
    ///     // Stores 80, 81 from 12x
    ///     RegisterIdx(12),
    ///     RegisterIdx(0),
    ///     ImmediateLow::new(0x000).unwrap(),
    /// )));
    /// computer.queue(Instruction::Base(instructions::base::Lw(
    ///     // Loads 0x80_81_00_00 into 10x
    ///     RegisterIdx(10),
    ///     RegisterIdx(0),
    ///     ImmediateLow::new(0x000).unwrap(),
    /// )));
    /// computer.run_all();
    ///
    /// assert_eq!(computer.get(RegisterIdx(10)), 0x80_81_00_00u32.to_be());
    /// ```
    ///
    #[instruction(store_load)]
    Sh(RegisterIdx, RegisterIdx, ImmediateLow),

    /// Store Word Instruction
    ///
    /// Stores a Word (32 bits, 4 bytes) value to memory, value taken from `rs2`. The address in
    /// memory can be obtained by adding the value in `rs1` to the sign-extended `imm` offset.
    /// Stores follow the little-endian convention, meaning the bytes are written from the least
    /// significant to the most significant.
    ///
    /// # Arguments
    ///
    /// * `rd` - Source register, of which the result is stored.
    /// * `rs1` - Base memory address, in bytes, to which is added the offset.
    /// * `imm` - Offset, in bytes, to add to `rs1` to obtain the memory address.
    ///
    /// # Examples
    ///
    /// ```
    /// # use asmv::{Computer, Register32Bit, NoMult, NoFloats, Instruction, RegisterIdx, ImmediateLow, ImmediateHigh};
    /// # use asmv::instructions::{self, Executable};
    /// let mut computer = Computer::<Register32Bit, NoMult, NoFloats>::new(20);
    /// # computer.queue(Instruction::Base(instructions::base::Lui(
    /// #     // 12x = 0x83_82_80_00
    /// #     RegisterIdx(12),
    /// #     ImmediateHigh::new(0x83_82_8).unwrap(),
    /// # )));
    /// # computer.queue(Instruction::Base(instructions::base::Addi(
    /// #     // 12x = 0x83_82_81_80
    /// #     RegisterIdx(12),
    /// #     RegisterIdx(12),
    /// #     ImmediateLow::new(0x1_80).unwrap(),
    /// # )));
    ///
    /// // 12x = 0x83_82_81_80
    ///
    /// computer.queue(Instruction::Base(instructions::base::Sw(
    ///     // Stores 80, 81, 82, 83 from 12x
    ///     RegisterIdx(12),
    ///     RegisterIdx(0),
    ///     ImmediateLow::new(0x000).unwrap(),
    /// )));
    /// computer.queue(Instruction::Base(instructions::base::Lw(
    ///     // Loads 0x80_81_82_83 into 10x
    ///     RegisterIdx(10),
    ///     RegisterIdx(0),
    ///     ImmediateLow::new(0x000).unwrap(),
    /// )));
    /// computer.run_all();
    ///
    /// assert_eq!(computer.get(RegisterIdx(10)), 0x80_81_82_83u32.to_be());
    /// ```
    ///
    #[instruction(store_load)]
    Sw(RegisterIdx, RegisterIdx, ImmediateLow),

    // Shift
    /// Logical Left Shift Instruction
    ///
    /// Instruction that performs the bitwise left shift operation of `rs1` by the number of bits
    /// specified in the lower 7 bits of `rs2`. The result is written in `rd`.
    ///
    /// # Arguments
    ///
    /// * `rd` - Destination register specifier, where the result is stored.
    /// * `rs1` - 1st source register specifier, first operand.
    /// * `rs2` - 2nd source register specifier, shift amount.
    ///
    /// # Examples
    ///
    /// ```
    /// # use asmv::{Computer, Register32Bit, NoMult, NoFloats, Instruction, RegisterIdx, ImmediateLow};
    /// # use asmv::instructions::{self, Executable};
    /// let mut computer = Computer::<Register32Bit, NoMult, NoFloats>::new(0);
    /// # computer.queue(Instruction::Base(instructions::base::Addi(
    /// #     // 12x = 18
    /// #     RegisterIdx(12),
    /// #     RegisterIdx(0),
    /// #     ImmediateLow::new(0x012).unwrap(),
    /// # )));
    /// # computer.queue(Instruction::Base(instructions::base::Addi(
    /// #     // 13x = 2
    /// #     RegisterIdx(13),
    /// #     RegisterIdx(0),
    /// #     ImmediateLow::new(0x002).unwrap(),
    /// # )));
    ///
    /// computer.queue(Instruction::Base(instructions::base::Sll(
    ///     RegisterIdx(10), // rd: 18 << 2
    ///     RegisterIdx(12), // rs1: 18
    ///     RegisterIdx(13), // rs2: 2
    /// )));
    /// computer.run_all();
    ///
    /// assert_eq!(computer.get(RegisterIdx(10)), 72u32.into());
    /// ```
    ///
    #[instruction(registers)]
    Sll(RegisterIdx, RegisterIdx, RegisterIdx),

    /// Immediate Logical Left Shift Instruction
    ///
    /// Instruction that performs the bitwise left shift operation of `rs1` by the number of bits
    /// specified in `imm`. The result is written in `rd`.
    ///
    /// # Arguments
    ///
    /// * `rd` - Destination register specifier, where the result is stored.
    /// * `rs1` - 1st source register specifier, first operand.
    /// * `imm` - Immediate 12-bit integer value, shift amount.
    ///
    /// # Examples
    ///
    /// ```
    /// # use asmv::{Computer, Register32Bit, NoMult, NoFloats, Instruction, RegisterIdx, ImmediateLow};
    /// # use asmv::instructions::{self, Executable};
    /// let mut computer = Computer::<Register32Bit, NoMult, NoFloats>::new(0);
    /// # computer.queue(Instruction::Base(instructions::base::Addi(
    /// #     // 12x = 18
    /// #     RegisterIdx(12),
    /// #     RegisterIdx(0),
    /// #     ImmediateLow::new(0x012).unwrap(),
    /// # )));
    ///
    /// computer.queue(Instruction::Base(instructions::base::Slli(
    ///     RegisterIdx(10), // rd: 18 << 2
    ///     RegisterIdx(12), // rs1: 18
    ///     ImmediateLow::new(0x002).unwrap(), // imm: 2
    /// )));
    /// computer.run_all();
    ///
    /// assert_eq!(computer.get(RegisterIdx(10)), 72u32.into());
    /// ```
    ///
    #[instruction(immediate)]
    Slli(RegisterIdx, RegisterIdx, ImmediateLow),

    /// Logical Right Shift Instruction
    ///
    /// Instruction that performs the bitwise logical right shift operation of `rs1` by the number
    /// of bits specified in the lower 7 bits of `rs2`, 0-filled on the left. The result is written
    /// in `rd`.
    ///
    /// # Arguments
    ///
    /// * `rd` - Destination register specifier, where the result is stored.
    /// * `rs1` - 1st source register specifier, first operand.
    /// * `rs2` - 2nd source register specifier, shift amount.
    ///
    /// # Examples
    ///
    /// ```
    /// # use asmv::{Computer, Register32Bit, NoMult, NoFloats, Instruction, RegisterIdx, ImmediateLow};
    /// # use asmv::instructions::{self, Executable};
    /// let mut computer = Computer::<Register32Bit, NoMult, NoFloats>::new(0);
    /// # computer.queue(Instruction::Base(instructions::base::Addi(
    /// #     // 12x = 18
    /// #     RegisterIdx(12),
    /// #     RegisterIdx(0),
    /// #     ImmediateLow::new(0x012).unwrap(),
    /// # )));
    /// # computer.queue(Instruction::Base(instructions::base::Addi(
    /// #     // 13x = 2
    /// #     RegisterIdx(13),
    /// #     RegisterIdx(0),
    /// #     ImmediateLow::new(0x002).unwrap(),
    /// # )));
    ///
    /// computer.queue(Instruction::Base(instructions::base::Srl(
    ///     RegisterIdx(10), // rd: 18 >> 2
    ///     RegisterIdx(12), // rs1: 18
    ///     RegisterIdx(13), // rs2: 2
    /// )));
    /// computer.run_all();
    ///
    /// assert_eq!(computer.get(RegisterIdx(10)), 4u32.into());
    /// ```
    ///
    #[instruction(registers)]
    Srl(RegisterIdx, RegisterIdx, RegisterIdx),

    /// Immediate Logical Right Shift Instruction
    ///
    /// Instruction that performs the bitwise logical right shift operation of `rs1` by the number
    /// of bits specified in the lower 7 bits of `imm`, 0-filled on the left. The result is written
    /// in `rd`.
    ///
    /// # Arguments
    ///
    /// * `rd` - Destination register specifier, where the result is stored.
    /// * `rs1` - 1st source register specifier, first operand.
    /// * `imm` - Immediate 12-bit integer value, the shift amount.
    ///
    /// # Examples
    ///
    /// ```
    /// # use asmv::{Computer, Register32Bit, NoMult, NoFloats, Instruction, RegisterIdx, ImmediateLow};
    /// # use asmv::instructions::{self, Executable};
    /// let mut computer = Computer::<Register32Bit, NoMult, NoFloats>::new(0);
    /// # computer.queue(Instruction::Base(instructions::base::Addi(
    /// #     // 12x = 18
    /// #     RegisterIdx(12),
    /// #     RegisterIdx(0),
    /// #     ImmediateLow::new(0x012).unwrap(),
    /// # )));
    /// # computer.queue(Instruction::Base(instructions::base::Addi(
    /// #     // 13x = 2
    /// #     RegisterIdx(13),
    /// #     RegisterIdx(0),
    /// #     ImmediateLow::new(0x002).unwrap(),
    /// # )));
    ///
    /// computer.queue(Instruction::Base(instructions::base::Srli(
    ///     RegisterIdx(10), // rd: 18 >> 2
    ///     RegisterIdx(12), // rs1: 18
    ///     ImmediateLow::new(0x002).unwrap(), // imm: 2
    /// )));
    /// computer.run_all();
    ///
    /// assert_eq!(computer.get(RegisterIdx(10)), 4u32.into());
    /// ```
    ///
    #[instruction(immediate)]
    Srli(RegisterIdx, RegisterIdx, ImmediateLow),

    /// Arithmetic Right Shift Instruction
    ///
    /// Instruction that performs the bitwise arithmetic (also called algebraic) right shift
    /// operation of `rs1` by the number of bits specified in the lower 7 bits of `rs2`, copying
    /// the sign on the left. The result is written in `rd`.
    ///
    /// # Arguments
    ///
    /// * `rd` - Destination register specifier, where the result is stored.
    /// * `rs1` - 1st source register specifier, first operand.
    /// * `rs2` - 2nd source register specifier, shift amount.
    ///
    /// # Examples
    ///
    /// ```
    /// # use asmv::{Computer, Register32Bit, NoMult, NoFloats, Instruction, RegisterIdx, ImmediateLow};
    /// # use asmv::instructions::{self, Executable};
    /// let mut computer = Computer::<Register32Bit, NoMult, NoFloats>::new(0);
    /// # computer.queue(Instruction::Base(instructions::base::Addi(
    /// #     // 12x = 18
    /// #     RegisterIdx(12),
    /// #     RegisterIdx(0),
    /// #     ImmediateLow::new(0x012).unwrap(),
    /// # )));
    /// # computer.queue(Instruction::Base(instructions::base::Addi(
    /// #     // 13x = 2
    /// #     RegisterIdx(13),
    /// #     RegisterIdx(0),
    /// #     ImmediateLow::new(0x002).unwrap(),
    /// # )));
    ///
    /// computer.queue(Instruction::Base(instructions::base::Sra(
    ///     RegisterIdx(10), // rd: 18 >> 2
    ///     RegisterIdx(12), // rs1: 18
    ///     RegisterIdx(13), // rs2: 2
    /// )));
    /// computer.run_all();
    ///
    /// assert_eq!(computer.get(RegisterIdx(10)), 4u32.into());
    /// ```
    ///
    #[instruction(registers)]
    Sra(RegisterIdx, RegisterIdx, RegisterIdx),

    /// Immediate Arithmetic Right Shift Instruction
    ///
    /// Instruction that performs the bitwise arithmetic (also called algebraic) right shift
    /// operation of `rs1` by the number of bits specified in the lower 7 bits of `imm`, copying the
    /// sign on the left. The result is written in `rd`.
    ///
    /// # Arguments
    ///
    /// * `rd` - Destination register specifier, where the result is stored.
    /// * `rs1` - 1st source register specifier, first operand.
    /// * `imm` - Immediate 12-bit integer value, the shift amount.
    ///
    /// # Examples
    ///
    /// ```
    /// # use asmv::{Computer, Register32Bit, NoMult, NoFloats, Instruction, RegisterIdx, ImmediateLow};
    /// # use asmv::instructions::{self, Executable};
    /// let mut computer = Computer::<Register32Bit, NoMult, NoFloats>::new(0);
    /// # computer.queue(Instruction::Base(instructions::base::Addi(
    /// #     // 12x = 18
    /// #     RegisterIdx(12),
    /// #     RegisterIdx(0),
    /// #     ImmediateLow::new(0x012).unwrap(),
    /// # )));
    ///
    /// computer.queue(Instruction::Base(instructions::base::Srai(
    ///     RegisterIdx(10), // rd: 18 >> 2
    ///     RegisterIdx(12), // rs1: 18
    ///     ImmediateLow::new(0x002).unwrap(), // imm: 2
    /// )));
    /// computer.run_all();
    ///
    /// assert_eq!(computer.get(RegisterIdx(10)), 4u32.into());
    /// ```
    ///
    #[instruction(immediate)]
    Srai(RegisterIdx, RegisterIdx, ImmediateLow),

    // Arithmetic
    /// Addition Instruction
    ///
    /// Instruction that performs the addition of `rs1` and `rs2`, wrapping around the overflow. The
    /// result is written in `rd`.
    ///
    /// # Arguments
    ///
    /// * `rd` - Destination register specifier, where the result is stored.
    /// * `rs1` - 1st source register specifier, first operand.
    /// * `rs2` - 2nd source register specifier, second operand.
    ///
    /// # Examples
    ///
    /// ```
    /// # use asmv::{Computer, Register32Bit, NoMult, NoFloats, Instruction, RegisterIdx, ImmediateLow};
    /// # use asmv::instructions::{self, Executable};
    /// let mut computer = Computer::<Register32Bit, NoMult, NoFloats>::new(0);
    /// # computer.queue(Instruction::Base(instructions::base::Addi(
    /// #     // 12x = 18
    /// #     RegisterIdx(12),
    /// #     RegisterIdx(0),
    /// #     ImmediateLow::new(0x012).unwrap(),
    /// # )));
    /// # computer.queue(Instruction::Base(instructions::base::Addi(
    /// #     // 13x = 5
    /// #     RegisterIdx(13),
    /// #     RegisterIdx(0),
    /// #     ImmediateLow::new(0x005).unwrap(),
    /// # )));
    ///
    /// computer.queue(Instruction::Base(instructions::base::Add(
    ///     RegisterIdx(10), // rd: 18 + 5
    ///     RegisterIdx(12), // rs1: 18
    ///     RegisterIdx(13), // rs2: 5
    /// )));
    /// computer.run_all();
    ///
    /// assert_eq!(computer.get(RegisterIdx(10)), 23u32.into());
    /// ```
    ///
    #[instruction(registers)]
    Add(RegisterIdx, RegisterIdx, RegisterIdx),

    /// Immediate Addition Instruction
    ///
    /// Instruction that performs the addition of `rs1` and the sign-extended 12-bits immediate
    /// integer `imm`. The overflow is ignored, and the result is written in `rd`.
    ///
    /// # Arguments
    ///
    /// * `rd` - Destination register specifier, where the result is stored.
    /// * `rs1` - 1st source register specifier, first operand.
    /// * `imm` - Immediate 12-bit integer value, the second operand.
    ///
    /// # Examples
    ///
    /// ```
    /// # use asmv::{Computer, Register32Bit, NoMult, NoFloats, Instruction, RegisterIdx, ImmediateLow};
    /// # use asmv::instructions::{self, Executable};
    /// let mut computer = Computer::<Register32Bit, NoMult, NoFloats>::new(0);
    /// # computer.queue(Instruction::Base(instructions::base::Addi(
    /// #     // 12x = 18
    /// #     RegisterIdx(12),
    /// #     RegisterIdx(0),
    /// #     ImmediateLow::new(0x012).unwrap(),
    /// # )));
    ///
    /// computer.queue(Instruction::Base(instructions::base::Addi(
    ///     RegisterIdx(10), // rd: 18 + 5
    ///     RegisterIdx(12), // rs1: 18
    ///     ImmediateLow::new(0x005).unwrap(), // imm: 5
    /// )));
    /// computer.run_all();
    ///
    /// assert_eq!(computer.get(RegisterIdx(10)), 23u32.into());
    /// ```
    ///
    #[instruction(immediate)]
    Addi(RegisterIdx, RegisterIdx, ImmediateLow),

    /// Subtraction Instruction
    ///
    /// Instruction that performs the subtraction of `rs1` by `rs2`, ignoring the overflow. The
    /// result is written in `rd`.
    ///
    /// # Arguments
    ///
    /// * `rd` - Destination register specifier, where the result is stored.
    /// * `rs1` - 1st source register specifier, first operand.
    /// * `rs2` - 2nd source register specifier, second operand.
    ///
    /// # Examples
    ///
    /// ```
    /// # use asmv::{Computer, Register32Bit, NoMult, NoFloats, Instruction, RegisterIdx, ImmediateLow};
    /// # use asmv::instructions::{self, Executable};
    /// let mut computer = Computer::<Register32Bit, NoMult, NoFloats>::new(0);
    /// # computer.queue(Instruction::Base(instructions::base::Addi(
    /// #     // 12x = 18
    /// #     RegisterIdx(12),
    /// #     RegisterIdx(0),
    /// #     ImmediateLow::new(0x012).unwrap(),
    /// # )));
    /// # computer.queue(Instruction::Base(instructions::base::Addi(
    /// #     // 13x = 5
    /// #     RegisterIdx(13),
    /// #     RegisterIdx(0),
    /// #     ImmediateLow::new(0x005).unwrap(),
    /// # )));
    ///
    /// computer.queue(Instruction::Base(instructions::base::Sub(
    ///     RegisterIdx(10), // rd: 18 - 5
    ///     RegisterIdx(12), // rs1: 18
    ///     RegisterIdx(13), // rs2: 5
    /// )));
    /// computer.run_all();
    ///
    /// assert_eq!(computer.get(RegisterIdx(10)), 13u32.into());
    /// ```
    ///
    #[instruction(registers)]
    Sub(RegisterIdx, RegisterIdx, RegisterIdx),

    // TODO: fix broken doc links to Addi and Slli variants
    /// Load Upper Immediate Instruction
    ///
    /// Instruction used to build 32 bits constants. Places the 20-bits value `imm` in bits 12:31 of
    /// `rd`. The lower 12 bits of `rd` are filled with zeros.
    ///
    /// To build 32 bits constants, the recommended code sequence is to first load the top 20 bits,
    /// then [`ADDI`] the lower 12 bits. Since 12-bits immediate are sign-extended, if the immediate
    /// value of the ADDI instruction has its most significant bit set to 1, the `imm`, the
    /// immediate value of the `LUI` instruction will need to be incremented by one.
    ///
    /// ```text
    /// LUI  x10, 0xDEADC    # x10 = 0xDEADC000
    /// ADDI x10, x10, 0xEEF # x10 = 0xDEADBEEF
    /// ```
    ///
    /// If [XLEN] is bigger than 32 bits (`Register64Bit` or `Register128Bit`), the 32-bits result
    /// is sign-extended to XLEN. To build 64-bits constants, two registers are needed. First, we
    /// build the upper word (32 bits) with the same sequence as with 32-bits registers. This upper
    /// word needs to be incremented by one if the lower one alone is negative. We [shift left] this
    /// upper word by 32 bits. After this, we build the lower word, and add the them together.
    ///
    /// ```text
    /// LUI  x10, 0xDEADC     # x10 = 0xFFFFFFFF_DEADC000
    /// ADDI x10, x10, 0xEF0  # x10 = 0xFFFFFFFF_DEADBEF0
    /// SLLI x10, x10, 32     # x10 = 0xDEADBEF0_00000000
    ///
    /// LUI  x11, 0xDEADC     # x11 = 0xFFFFFFFF_DEADC000
    /// ADDI x11, x11, 0xEEF  # x11 = 0xFFFFFFFF_DEADBEEF
    ///
    /// ADD  x10, x10, x11    # x10 = 0xDEADBEEF_DEADBEEF
    /// ```
    ///
    /// # Arguments
    ///
    /// * `rd` - Destination register specifier, where the result is stored.
    /// * `imm` - 20-bit upper-immediate value.
    ///
    /// # Examples
    ///
    /// Example with 32-bits registers
    /// ```
    /// # use asmv::{Computer, Register32Bit, NoMult, NoFloats, Instruction, RegisterIdx, ImmediateLow, ImmediateHigh};
    /// # use asmv::instructions::{self, Executable};
    /// let mut computer = Computer::<Register32Bit, NoMult, NoFloats>::new(0);
    /// computer.queue(Instruction::Base(instructions::base::Lui(
    ///     RegisterIdx(10),
    ///     ImmediateHigh::new(0xDEADC).unwrap(),
    /// )));
    /// computer.queue(Instruction::Base(instructions::base::Addi(
    ///     RegisterIdx(10), // rd: 18 + 5
    ///     RegisterIdx(10), // rs1: 18
    ///     ImmediateLow::new(0xEEF).unwrap(), // imm: 5
    /// )));
    /// computer.run_all();
    ///
    /// assert_eq!(computer.get(RegisterIdx(10)), 0xDEADBEEF_u32.into());
    /// ```
    ///
    /// Example with 64 bits registers
    /// ```
    /// # use asmv::{Computer, Register64Bit, NoMult, NoFloats, Instruction, RegisterIdx, ImmediateLow, ImmediateHigh};
    /// # use asmv::instructions::{self, Executable};
    /// let mut computer = Computer::<Register64Bit, NoMult, NoFloats>::new(0);
    ///
    /// // Build upper word
    /// computer.queue(Instruction::Base(instructions::base::Lui(
    ///     RegisterIdx(12),
    ///     ImmediateHigh::new(0xDEADC).unwrap(),
    /// )));
    /// computer.queue(Instruction::Base(instructions::base::Addi(
    ///     RegisterIdx(12),
    ///     RegisterIdx(12),
    ///     ImmediateLow::new(0xEF0).unwrap(),
    /// )));
    /// // Shift upper word
    /// computer.queue(Instruction::Base(instructions::base::Slli(
    ///     RegisterIdx(12),
    ///     RegisterIdx(12),
    ///     ImmediateLow::new(32).unwrap(),
    /// )));
    /// // Build lower word
    /// computer.queue(Instruction::Base(instructions::base::Lui(
    ///     RegisterIdx(13),
    ///     ImmediateHigh::new(0xDEADC).unwrap(),
    /// )));
    /// computer.queue(Instruction::Base(instructions::base::Addi(
    ///     RegisterIdx(13),
    ///     RegisterIdx(13),
    ///     ImmediateLow::new(0xEEF).unwrap(),
    /// )));
    /// // Add both words
    /// computer.queue(Instruction::Base(instructions::base::Add(
    ///     RegisterIdx(10),
    ///     RegisterIdx(12),
    ///     RegisterIdx(13),
    /// )));
    /// computer.run_all();
    ///
    /// assert_eq!(computer.get(RegisterIdx(10)), 0xDEADBEEF_DEADBEEF_u64.into());
    /// ```
    ///
    /// [ADDI]: ../enum.Instruction.html#variant.Addi
    /// [shift left]: ../enum.Instruction.html#Slli.v
    /// [XLEN]: ../../trait.Register.html
    ///
    #[instruction(long_add)]
    Lui(RegisterIdx, ImmediateHigh),

    /// Add Upper Immediate to Program Counter
    ///
    /// Instruction used to build 32 bits pc-relative addresses. Places the 20-bits value `imm` in
    /// bits 12:31 of `rd`, filling in the lower 12 bits with zeros, then adds in the pc address
    /// (the address is in bytes) of this instruction.
    ///
    /// # Arguments
    ///
    /// * `rd` - Destination register specifier, where the result is stored.
    /// * `imm` - 20-bit upper-immediate value.
    ///
    /// # Examples
    ///
    /// ```
    /// # use asmv::{Computer, Register32Bit, NoMult, NoFloats, Instruction, RegisterIdx, ImmediateLow, ImmediateHigh};
    /// # use asmv::instructions::{self, Executable};
    /// let mut computer = Computer::<Register32Bit, NoMult, NoFloats>::new(0);
    ///
    /// // Increment pc with dummy operations
    /// computer.queue(Instruction::Base(instructions::base::Add(
    ///     // pc: 0x4
    ///     RegisterIdx(0), RegisterIdx(0), RegisterIdx(0),
    /// )));
    /// computer.queue(Instruction::Base(instructions::base::Add(
    ///     // pc: 0x8
    ///     RegisterIdx(0), RegisterIdx(0), RegisterIdx(0),
    /// )));
    /// computer.queue(Instruction::Base(instructions::base::Add(
    ///     // pc: 0xC
    ///     RegisterIdx(0), RegisterIdx(0), RegisterIdx(0),
    /// )));
    ///
    /// // Save current pc
    /// computer.queue(Instruction::Base(instructions::base::Auipc(
    ///     // pc: 0x10
    ///     RegisterIdx(10),
    ///     ImmediateHigh::new(0).unwrap(),
    /// )));
    /// // Save current pc with immediate
    /// computer.queue(Instruction::Base(instructions::base::Auipc(
    ///     // pc: 0x14
    ///     RegisterIdx(11),
    ///     ImmediateHigh::new(0x0000_6).unwrap(),
    /// )));
    /// computer.run_all();
    ///
    /// assert_eq!(computer.get(RegisterIdx(10)), 0x0000_0010_u32.into());
    /// assert_eq!(computer.get(RegisterIdx(11)), 0x0000_6014_u32.into());
    /// ```
    ///
    #[instruction(long_add)]
    Auipc(RegisterIdx, ImmediateHigh),

    // Logical
    /// Bitwise XOR Instruction
    ///
    /// Instruction that performs the bitwise logical XOR operation on registers `rs1` and `rs2`.
    /// The result is written in `rd`.
    ///
    /// # Arguments
    ///
    /// * `rd` - Destination register specifier, where the result is stored.
    /// * `rs1` - 1st source register specifier, first operand.
    /// * `rs2` - 2nd source register specifier, second operand.
    ///
    /// # Examples
    ///
    /// ```
    /// # use asmv::{Computer, Register32Bit, NoMult, NoFloats, Instruction, RegisterIdx, ImmediateLow};
    /// # use asmv::instructions::{self, Executable};
    /// let mut computer = Computer::<Register32Bit, NoMult, NoFloats>::new(0);
    /// # computer.queue(Instruction::Base(instructions::base::Addi(
    /// #     // 12x = 0101
    /// #     RegisterIdx(12),
    /// #     RegisterIdx(0),
    /// #     ImmediateLow::new(0b0101).unwrap(),
    /// # )));
    /// # computer.queue(Instruction::Base(instructions::base::Addi(
    /// #     // 13x = 0011
    /// #     RegisterIdx(13),
    /// #     RegisterIdx(0),
    /// #     ImmediateLow::new(0b0011).unwrap(),
    /// # )));
    ///
    /// computer.queue(Instruction::Base(instructions::base::Xor(
    ///     RegisterIdx(10), // rd: 0101 ^ 0011
    ///     RegisterIdx(12), // rs1: 0101
    ///     RegisterIdx(13), // rs2: 0011
    /// )));
    /// computer.run_all();
    ///
    /// assert_eq!(computer.get(RegisterIdx(10)), 0b0110u32.into());
    /// ```
    ///
    #[instruction(registers)]
    Xor(RegisterIdx, RegisterIdx, RegisterIdx),

    /// Immadiate Bitwise XOR Instruction
    ///
    /// Instruction that performs the bitwise logical XOR operation on register `rs1` and the
    /// sign-extended 12-bits immediate integer `imm`. The result is written in `rd`.
    ///
    /// # Arguments
    ///
    /// * `rd` - Destination register specifier, where the result is stored.
    /// * `rs1` - 1st source register specifier, first operand.
    /// * `imm` - Immediate 12-bit integer value, second operand.
    ///
    /// # Examples
    ///
    /// ```
    /// # use asmv::{Computer, Register32Bit, NoMult, NoFloats, Instruction, RegisterIdx, ImmediateLow};
    /// # use asmv::instructions::{self, Executable};
    /// let mut computer = Computer::<Register32Bit, NoMult, NoFloats>::new(0);
    /// # computer.queue(Instruction::Base(instructions::base::Addi(
    /// #     // 12x = 0101
    /// #     RegisterIdx(12),
    /// #     RegisterIdx(0),
    /// #     ImmediateLow::new(0b0101).unwrap(),
    /// # )));
    ///
    /// computer.queue(Instruction::Base(instructions::base::Xori(
    ///     RegisterIdx(10), // rd: 0101 ^ 0011
    ///     RegisterIdx(12), // rs1: 0101
    ///     ImmediateLow::new(0b0011).unwrap(), // imm: 0011
    /// )));
    /// computer.run_all();
    ///
    /// assert_eq!(computer.get(RegisterIdx(10)), 0b0110u32.into());
    /// ```
    ///
    #[instruction(immediate)]
    Xori(RegisterIdx, RegisterIdx, ImmediateLow),

    /// Bitwise OR Instruction
    ///
    /// Instruction that performs the bitwise logical OR operation on registers `rs1` and `rs2`.
    /// The result is written in `rd`.
    ///
    /// # Arguments
    ///
    /// * `rd` - Destination register specifier, where the result is stored.
    /// * `rs1` - 1st source register specifier, first operand.
    /// * `rs2` - 2nd source register specifier, second operand.
    ///
    /// # Examples
    ///
    /// ```
    /// # use asmv::{Computer, Register32Bit, NoMult, NoFloats, Instruction, RegisterIdx, ImmediateLow};
    /// # use asmv::instructions::{self, Executable};
    /// let mut computer = Computer::<Register32Bit, NoMult, NoFloats>::new(0);
    /// # computer.queue(Instruction::Base(instructions::base::Addi(
    /// #     // 12x = 0101
    /// #     RegisterIdx(12),
    /// #     RegisterIdx(0),
    /// #     ImmediateLow::new(0b0101).unwrap(),
    /// # )));
    /// # computer.queue(Instruction::Base(instructions::base::Addi(
    /// #     // 13x = 0011
    /// #     RegisterIdx(13),
    /// #     RegisterIdx(0),
    /// #     ImmediateLow::new(0b0011).unwrap(),
    /// # )));
    ///
    /// computer.queue(Instruction::Base(instructions::base::Or(
    ///     RegisterIdx(10), // rd: 0101 | 0011
    ///     RegisterIdx(12), // rs1: 0101
    ///     RegisterIdx(13), // rs2: 0011
    /// )));
    /// computer.run_all();
    ///
    /// assert_eq!(computer.get(RegisterIdx(10)), 0b0111u32.into());
    /// ```
    ///
    #[instruction(registers)]
    Or(RegisterIdx, RegisterIdx, RegisterIdx),

    /// Immadiate Bitwise OR Instruction
    ///
    /// Instruction that performs the bitwise logical OR operation on register `rs1` and the
    /// sign-extended 12-bits immediate integer `imm`. The result is written in `rd`.
    ///
    /// # Arguments
    ///
    /// * `rd` - Destination register specifier, where the result is stored.
    /// * `rs1` - 1st source register specifier, first operand.
    /// * `imm` - Immediate 12-bit integer value, second operand.
    ///
    /// # Examples
    ///
    /// ```
    /// # use asmv::{Computer, Register32Bit, NoMult, NoFloats, Instruction, RegisterIdx, ImmediateLow};
    /// # use asmv::instructions::{self, Executable};
    /// let mut computer = Computer::<Register32Bit, NoMult, NoFloats>::new(0);
    /// # computer.queue(Instruction::Base(instructions::base::Addi(
    /// #     // 12x = 0101
    /// #     RegisterIdx(12),
    /// #     RegisterIdx(0),
    /// #     ImmediateLow::new(0b0101).unwrap(),
    /// # )));
    ///
    /// computer.queue(Instruction::Base(instructions::base::Ori(
    ///     RegisterIdx(10), // rd: 0101 | 0011
    ///     RegisterIdx(12), // rs1: 0101
    ///     ImmediateLow::new(0b0011).unwrap(), // imm: 0011
    /// )));
    /// computer.run_all();
    ///
    /// assert_eq!(computer.get(RegisterIdx(10)), 0b0111u32.into());
    /// ```
    ///
    #[instruction(immediate)]
    Ori(RegisterIdx, RegisterIdx, ImmediateLow),

    /// Bitwise AND Instruction
    ///
    /// Instruction that performs the bitwise logical AND operation on registers `rs1` and `rs2`.
    /// The result is written in `rd`.
    ///
    /// # Arguments
    ///
    /// * `rd` - Destination register specifier, where the result is stored.
    /// * `rs1` - 1st source register specifier, first operand.
    /// * `rs2` - 2nd source register specifier, second operand.
    ///
    /// # Examples
    ///
    /// ```
    /// # use asmv::{Computer, Register32Bit, NoMult, NoFloats, Instruction, RegisterIdx, ImmediateLow};
    /// # use asmv::instructions::{self, Executable};
    /// let mut computer = Computer::<Register32Bit, NoMult, NoFloats>::new(0);
    /// # computer.queue(Instruction::Base(instructions::base::Addi(
    /// #     // 12x = 0101
    /// #     RegisterIdx(12),
    /// #     RegisterIdx(0),
    /// #     ImmediateLow::new(0b0101).unwrap(),
    /// # )));
    /// # computer.queue(Instruction::Base(instructions::base::Addi(
    /// #     // 13x = 0011
    /// #     RegisterIdx(13),
    /// #     RegisterIdx(0),
    /// #     ImmediateLow::new(0b0011).unwrap(),
    /// # )));
    ///
    /// computer.queue(Instruction::Base(instructions::base::And(
    ///     RegisterIdx(10), // rd: 0101 & 0011
    ///     RegisterIdx(12), // rs1: 0101
    ///     RegisterIdx(13), // rs2: 0011
    /// )));
    /// computer.run_all();
    ///
    /// assert_eq!(computer.get(RegisterIdx(10)), 0b0001u32.into());
    /// ```
    ///
    #[instruction(registers)]
    And(RegisterIdx, RegisterIdx, RegisterIdx),

    /// Immadiate Bitwise AND Instruction
    ///
    /// Instruction that performs the bitwise logical AND operation on register `rs1` and the
    /// sign-extended 12-bits immediate integer `imm`. The result is written in `rd`.
    ///
    /// # Arguments
    ///
    /// * `rd` - Destination register specifier, where the result is stored.
    /// * `rs1` - 1st source register specifier, first operand.
    /// * `imm` - Immediate 12-bit integer value, second operand.
    ///
    /// # Examples
    ///
    /// ```
    /// # use asmv::{Computer, Register32Bit, NoMult, NoFloats, Instruction, RegisterIdx, ImmediateLow};
    /// # use asmv::instructions::{self, Executable};
    /// let mut computer = Computer::<Register32Bit, NoMult, NoFloats>::new(0);
    /// # computer.queue(Instruction::Base(instructions::base::Addi(
    /// #     // 12x = 0101
    /// #     RegisterIdx(12),
    /// #     RegisterIdx(0),
    /// #     ImmediateLow::new(0b0101).unwrap(),
    /// # )));
    ///
    /// computer.queue(Instruction::Base(instructions::base::Andi(
    ///     RegisterIdx(10), // rd: 0101 & 0011
    ///     RegisterIdx(12), // rs1: 0101
    ///     ImmediateLow::new(0b0011).unwrap(), // imm: 0011
    /// )));
    /// computer.run_all();
    ///
    /// assert_eq!(computer.get(RegisterIdx(10)), 0b0001u32.into());
    /// ```
    ///
    #[instruction(immediate)]
    Andi(RegisterIdx, RegisterIdx, ImmediateLow),

    // Compare
    /// Set Less Than Instruction
    ///
    /// Instruction that performs signed comparison of registers `rs1` and `rs2`. Writes the value 1
    /// to register `rd` if `rs1` is less than `rs2`, both treated as signed integers; writes 0
    /// otherwise.
    ///
    /// # Arguments
    ///
    /// * `rd` - Destination register specifier, where the result is stored.
    /// * `rs1` - 1st source register specifier, first operand.
    /// * `rs2` - 2nd source register specifier, second operand.
    ///
    /// # Examples
    ///
    /// ```
    /// # use asmv::{Computer, Register32Bit, NoMult, NoFloats, Instruction, RegisterIdx, ImmediateLow};
    /// # use asmv::instructions::{self, Executable};
    /// let mut computer = Computer::<Register32Bit, NoMult, NoFloats>::new(0);
    /// # computer.queue(Instruction::Base(instructions::base::Addi(
    /// #     // 12x = -2
    /// #     RegisterIdx(12),
    /// #     RegisterIdx(0),
    /// #     ImmediateLow::new(0xFFE).unwrap(),
    /// # )));
    /// # computer.queue(Instruction::Base(instructions::base::Addi(
    /// #     // 13x = 3
    /// #     RegisterIdx(13),
    /// #     RegisterIdx(0),
    /// #     ImmediateLow::new(0x003).unwrap(),
    /// # )));
    ///
    /// computer.queue(Instruction::Base(instructions::base::Slt(
    ///     RegisterIdx(10), // rd: -2 < 3
    ///     RegisterIdx(12), // rs1: -2
    ///     RegisterIdx(13), // rs2: 3
    /// )));
    /// computer.queue(Instruction::Base(instructions::base::Slt(
    ///     RegisterIdx(11), // rd: 3 < -2
    ///     RegisterIdx(13), // rs1: 3
    ///     RegisterIdx(12), // rs2: -2
    /// )));
    /// computer.run_all();
    ///
    /// assert_eq!(computer.get(RegisterIdx(10)), 1u32.into());
    /// assert_eq!(computer.get(RegisterIdx(11)), 0u32.into());
    /// ```
    ///
    #[instruction(registers)]
    Slt(RegisterIdx, RegisterIdx, RegisterIdx),

    /// Set Less Than Immediate Instruction
    ///
    /// Instruction that performs signed comparison of register `rs1` and the signed-extended
    /// 12-bits immediate integer `imm`. Writes the value 1 to register `rd` if `rs1` is less than
    /// `imm`, both treated as signed integers; writes 0 otherwise.
    ///
    /// # Arguments
    ///
    /// * `rd` - Destination register specifier, where the result is stored.
    /// * `rs1` - 1st source register specifier, first operand.
    /// * `imm` - Immediate 12-bit integer value, second operand.
    ///
    /// # Examples
    ///
    /// ```
    /// # use asmv::{Computer, Register32Bit, NoMult, NoFloats, Instruction, RegisterIdx, ImmediateLow};
    /// # use asmv::instructions::{self, Executable};
    /// let mut computer = Computer::<Register32Bit, NoMult, NoFloats>::new(0);
    /// # computer.queue(Instruction::Base(instructions::base::Addi(
    /// #     // 12x = -2
    /// #     RegisterIdx(12),
    /// #     RegisterIdx(0),
    /// #     ImmediateLow::new(0xFFE).unwrap(),
    /// # )));
    /// # computer.queue(Instruction::Base(instructions::base::Addi(
    /// #     // 13x = 3
    /// #     RegisterIdx(13),
    /// #     RegisterIdx(0),
    /// #     ImmediateLow::new(0x003).unwrap(),
    /// # )));
    ///
    /// computer.queue(Instruction::Base(instructions::base::Slti(
    ///     RegisterIdx(10), // rd: -2 < 3
    ///     RegisterIdx(12), // rs1: -2
    ///     ImmediateLow::new(0x003).unwrap(), // imm: 3
    /// )));
    /// computer.queue(Instruction::Base(instructions::base::Slti(
    ///     RegisterIdx(11), // rd: 3 < -2
    ///     RegisterIdx(13), // rs1: 3
    ///     ImmediateLow::new(0xFFE).unwrap(), // imm: -2
    /// )));
    /// computer.run_all();
    ///
    /// assert_eq!(computer.get(RegisterIdx(10)), 1u32.into());
    /// assert_eq!(computer.get(RegisterIdx(11)), 0u32.into());
    /// ```
    ///
    #[instruction(immediate)]
    Slti(RegisterIdx, RegisterIdx, ImmediateLow),

    /// Set Less Than Unsigned Instruction
    ///
    /// Instruction that performs unsigned comparison of registers `rs1` and `rs2`. Writes the value
    /// 1 to register `rd` if `rs1` is less than `rs2`, both treated as unsigned integers; writes 0
    /// otherwise.
    ///
    /// # Arguments
    ///
    /// * `rd` - Destination register specifier, where the result is stored.
    /// * `rs1` - 1st source register specifier, first operand.
    /// * `rs2` - 2nd source register specifier, second operand.
    ///
    /// # Examples
    ///
    /// ```
    /// # use asmv::{Computer, Register32Bit, NoMult, NoFloats, Instruction, RegisterIdx, ImmediateLow};
    /// # use asmv::instructions::{self, Executable};
    /// let mut computer = Computer::<Register32Bit, NoMult, NoFloats>::new(0);
    /// # computer.queue(Instruction::Base(instructions::base::Addi(
    /// #     // 12x = 0xFFFFFFFE
    /// #     RegisterIdx(12),
    /// #     RegisterIdx(0),
    /// #     ImmediateLow::new(0xFFE).unwrap(),
    /// # )));
    /// # computer.queue(Instruction::Base(instructions::base::Addi(
    /// #     // 13x = 3
    /// #     RegisterIdx(13),
    /// #     RegisterIdx(0),
    /// #     ImmediateLow::new(0x003).unwrap(),
    /// # )));
    ///
    /// computer.queue(Instruction::Base(instructions::base::Sltu(
    ///     RegisterIdx(10), // rd: 0xFFFFFFFE < 3
    ///     RegisterIdx(12), // rs1: 0xFFFFFFFE
    ///     RegisterIdx(13), // rs2: 3
    /// )));
    /// computer.queue(Instruction::Base(instructions::base::Sltu(
    ///     RegisterIdx(11), // rd: 3 < 0xFFFFFFFE
    ///     RegisterIdx(13), // rs1: 3
    ///     RegisterIdx(12), // rs2: 0xFFFFFFFE
    /// )));
    /// computer.run_all();
    ///
    /// assert_eq!(computer.get(RegisterIdx(10)), 0u32.into());
    /// assert_eq!(computer.get(RegisterIdx(11)), 1u32.into());
    /// ```
    ///
    #[instruction(registers)]
    Sltu(RegisterIdx, RegisterIdx, RegisterIdx),

    /// Immediate Unsigned Set Less Than Instruction
    ///
    /// Instruction that performs unsigned comparison of register `rs1` and the 12-bits
    /// sign-extended immediate integer `imm`, both treated as unsigned integers. Writes the value 1
    /// to register `rd` if `rs1` is less than `imm`, writes 0 otherwise.
    ///
    /// # Arguments
    ///
    /// * `rd` - Destination register specifier, where the result is stored.
    /// * `rs1` - 1st source register specifier, first operand.
    /// * `imm` - Immediate 12-bit integer value, second operand.
    ///
    /// # Examples
    ///
    /// ```
    /// # use asmv::{Computer, Register32Bit, NoMult, NoFloats, Instruction, RegisterIdx, ImmediateLow};
    /// # use asmv::instructions::{self, Executable};
    /// let mut computer = Computer::<Register32Bit, NoMult, NoFloats>::new(0);
    /// # computer.queue(Instruction::Base(instructions::base::Addi(
    /// #     // 12x = -2
    /// #     RegisterIdx(12),
    /// #     RegisterIdx(0),
    /// #     ImmediateLow::new(0xFFE).unwrap(),
    /// # )));
    /// # computer.queue(Instruction::Base(instructions::base::Addi(
    /// #     // 13x = 3
    /// #     RegisterIdx(13),
    /// #     RegisterIdx(0),
    /// #     ImmediateLow::new(0x003).unwrap(),
    /// # )));
    ///
    /// computer.queue(Instruction::Base(instructions::base::Sltui(
    ///     RegisterIdx(10), // rd: 0xFFFFFFFE < 3
    ///     RegisterIdx(12), // rs1: 0xFFFFFFFE
    ///     ImmediateLow::new(0x003).unwrap(), // imm: 3
    /// )));
    /// computer.queue(Instruction::Base(instructions::base::Sltui(
    ///     RegisterIdx(11), // rd: 3 < 0xFFFFFFFE
    ///     RegisterIdx(13), // rs1: 3
    ///     ImmediateLow::new(0xFFE).unwrap(), // imm: 0xFFFFFFFE
    /// )));
    /// computer.run_all();
    ///
    /// assert_eq!(computer.get(RegisterIdx(10)), 0u32.into());
    /// assert_eq!(computer.get(RegisterIdx(11)), 1u32.into());
    /// ```
    ///
    #[instruction(immediate)]
    Sltui(RegisterIdx, RegisterIdx, ImmediateLow),

    // Branching

    // TODO: add doc examples for branch instructions
    /// Branch if Equal Instruction
    ///
    /// Jumps to the targeted address if register `rs1` is equal to register `rs2`.
    /// The 12-bits immediate value `imm` encodes a signed offset in multiples of two bytes, ranging
    /// to 2^12 bytes forward or backward.
    /// The target address is obtained by adding the offset to the address of this instruction.
    ///
    /// # Arguments
    ///
    /// * `rs1` - 1st source register specifier, first operand.
    /// * `rs2` - 2nd source register specifier, second operand.
    /// * `imm` - immediate 12-bits value, offset to the target address.
    ///
    #[instruction(branch)]
    Beq(RegisterIdx, RegisterIdx, ImmediateLow),

    /// Branch if Not Equal Instruction
    ///
    /// Jumps to the targeted address if register `rs1` is not equal to register `rs2`.
    /// The 12-bits immediate value `imm` encodes a signed offset in multiples of two bytes, ranging
    /// to 2^12 bytes forward or backward.
    /// The target address is obtained by adding the offset to the address of this instruction.
    ///
    /// # Arguments
    ///
    /// * `rs1` - 1st source register specifier, first operand.
    /// * `rs2` - 2nd source register specifier, second operand.
    /// * `imm` - immediate 12-bits value, offset to the target address.
    ///
    #[instruction(branch)]
    Bne(RegisterIdx, RegisterIdx, ImmediateLow),

    /// Branch if Less Than Instruction
    ///
    /// Jumps to the targeted address if register `rs1` is less than register `rs2`,
    /// both value treated as signed integers.
    /// The 12-bits immediate value `imm` encodes a signed offset in multiples of two bytes, ranging
    /// to 2^12 bytes forward or backward.
    /// The target address is obtained by adding the offset to the address of this instruction.
    ///
    /// # Arguments
    ///
    /// * `rs1` - 1st source register specifier, first operand.
    /// * `rs2` - 2nd source register specifier, second operand.
    /// * `imm` - immediate 12-bits value, offset to the target address.
    ///
    #[instruction(branch)]
    Blt(RegisterIdx, RegisterIdx, ImmediateLow),

    /// Branch if Greater Than or Equal Instruction
    ///
    /// Jumps to the targeted address if register `rs1` is greater than or equal to register `rs2`,
    /// both value treated as signed integers.
    /// The 12-bits immediate value `imm` encodes a signed offset in multiples of two bytes, ranging
    /// to 2^12 bytes forward or backward.
    /// The target address is obtained by adding the offset to the address of this instruction.
    ///
    /// # Arguments
    ///
    /// * `rs1` - 1st source register specifier, first operand.
    /// * `rs2` - 2nd source register specifier, second operand.
    /// * `imm` - immediate 12-bits value, offset to the target address.
    ///
    #[instruction(branch)]
    Bge(RegisterIdx, RegisterIdx, ImmediateLow),

    /// Branch if Less Than Unsigned Instruction
    ///
    /// Jumps to the targeted address if register `rs1` is less than register `rs2`,
    /// both values treated as unsigned integers.
    /// The 12-bits immediate value `imm` encodes a signed offset in multiples of two bytes, ranging
    /// to 2^12 bytes forward or backward.
    /// The target address is obtained by adding the offset to the address of this instruction.
    ///
    /// # Arguments
    ///
    /// * `rs1` - 1st source register specifier, first operand.
    /// * `rs2` - 2nd source register specifier, second operand.
    /// * `imm` - immediate 12-bits value, offset to the target address.
    ///
    #[instruction(branch)]
    Bltu(RegisterIdx, RegisterIdx, ImmediateLow),

    /// Branch if Greater than or Equal Unsigned Instruction
    ///
    /// Jumps to the targeted address if register `rs1` is greater than or equal to register `rs2`,
    /// both values treated as unsigned integers.
    /// The 12-bits immediate value `imm` encodes a signed offset in multiples of two bytes, ranging
    /// to 2^12 bytes forward or backward.
    /// The target address is obtained by adding the offset to the address of this instruction.
    ///
    /// # Arguments
    ///
    /// * `rs1` - 1st source register specifier, first operand.
    /// * `rs2` - 2nd source register specifier, second operand.
    /// * `imm` - immediate 12-bits value, offset to the target address.
    ///
    #[instruction(branch)]
    Bgeu(RegisterIdx, RegisterIdx, ImmediateLow),

    // Jump & Link

    // TODO: add doc examples for jump instructions
    /// Jump And Link Instruction
    ///
    /// Jumps to the targeted address and stores the following instruction in `rd`.
    /// The 20-bits upper-immediate value `imm` encodes a signed offset in multiples of two bytes,
    /// ranging to 2^20 bytes forward or backward.
    /// The target address is obtained by adding the offset to the address of this instruction.
    /// The address of the following instruction (pc + 4 bytes) is written in rd.
    ///
    /// # Arguments
    ///
    /// * `rd` - Destination register specifier, where the result is stored.
    /// * `imm` - 20-bits upper-immediate value.
    ///
    #[instruction(long_jump)]
    Jal(RegisterIdx, ImmediateHigh),

    // TODO: fix broken doc links to Lui variant
    /// Jump And Link Register Instruction
    ///
    /// Jumps to the targeted address and stores the following instruction in `rd`.
    /// The target address is obtained by adding the sign-extended immediate value `imm` to the
    /// source register `rs1`, the least significant bit of the result set to zero. The address is
    /// thus absolute (non-relative to pc).
    /// The address of the following instruction (pc + 4 bytes) is written in rd.
    ///
    /// This instruction was designed to be used in pair with the [LUI](Instruction::Lui) instruction, allowing a jump
    /// to an absolute 32-bits address. A LUI instruction first load the upper 20 bits of the
    /// address to `rs1`, then the lower bits are added by JALR.
    ///
    /// # Arguments
    ///
    /// * `rd` - Destination register specifier, where the result is stored.
    /// * `rs1` - Source register specifier.
    /// * `imm` - 20-bits offset.
    ///
    #[instruction(short_jump)]
    Jalr(RegisterIdx, RegisterIdx, ImmediateLow),
}

impl<R, M, F> Computer<R, M, F>
where
    F: FRegister,
    M: ExtensionMult,
    R: AsPrimitive<u32>,
    R: Register,
    u32: AsPrimitive<R>,
{
    /// Execute a base instruction
    ///
    /// Panics if there is no more instructions to execute. Use at_end
    /// to avoid it
    pub fn execute_base(&mut self, instruction: Instruction) {
        use Instruction::*;
        match instruction {
            // Load
            Lb(result, target, offset) => {
                let val = self.get(target) + R::from_immediate_low(offset);
                self.set(result, self.load_mem(1, val, true));
            }
            Lbu(result, target, offset) => {
                let val = self.get(target) + R::from_immediate_low(offset);
                self.set(result, self.load_mem(1, val, false));
            }
            Lh(result, target, offset) => {
                let val = self.get(target) + R::from_immediate_low(offset);
                self.set(result, self.load_mem(2, val, true));
            }
            Lhu(result, target, offset) => {
                let val = self.get(target) + R::from_immediate_low(offset);
                self.set(result, self.load_mem(2, val, false));
            }
            Lw(result, target, offset) => {
                let val = self.get(target) + R::from_immediate_low(offset);
                self.set(result, self.load_mem(4, val, true));
            }
            // Stores
            Sb(value, target, offset) => self.set_mem(
                1,
                self.get(target) + R::from_immediate_low(offset),
                self.get(value),
            ),
            Sh(value, target, offset) => self.set_mem(
                2,
                self.get(target) + R::from_immediate_low(offset),
                self.get(value),
            ),
            Sw(value, target, offset) => self.set_mem(
                4,
                self.get(target) + R::from_immediate_low(offset),
                self.get(value),
            ),
            // Shifts
            Sll(result, a, b) => self.set(
                result,
                self.get(a)
                    .unsigned_shl(AsPrimitive::<u32>::as_(self.get(b))),
            ),
            Slli(result, a, b) => self.set(
                result,
                self.get(a)
                    .unsigned_shl(AsPrimitive::<u32>::as_(R::from_immediate_low(b))),
            ),
            Srl(result, a, b) => self.set(
                result,
                self.get(a)
                    .unsigned_shr(AsPrimitive::<u32>::as_(self.get(b))),
            ),
            Srli(result, a, b) => self.set(
                result,
                self.get(a)
                    .unsigned_shr(AsPrimitive::<u32>::as_(R::from_immediate_low(b))),
            ),
            Sra(result, a, b) => self.set(
                result,
                self.get(a).signed_shr(AsPrimitive::<u32>::as_(self.get(b))),
            ),
            Srai(result, a, b) => self.set(
                result,
                self.get(a)
                    .signed_shr(AsPrimitive::<u32>::as_(R::from_immediate_low(b))),
            ),
            // Arithmetic
            Add(result, a, b) => self.set(result, self.get(a).wrapping_add(&self.get(b))),
            Addi(result, a, b) => {
                self.set(result, self.get(a).wrapping_add(&R::from_immediate_low(b)))
            }
            Sub(result, a, b) => self.set(result, self.get(a).wrapping_sub(&self.get(b))),
            Lui(result, v) => self.set(result, R::from_immediate_high(v)),
            Auipc(result, a) => self.set(
                result,
                R::from_immediate_high(a).wrapping_add(&self.pos().to_address().into()),
            ),
            // Logical
            Xor(result, a, b) => self.set(result, self.get(a) ^ self.get(b)),
            Xori(result, a, b) => self.set(result, self.get(a) ^ R::from_immediate_low(b)),
            Or(result, a, b) => self.set(result, self.get(a) | self.get(b)),
            Ori(result, a, b) => self.set(result, self.get(a) | R::from_immediate_low(b)),
            And(result, a, b) => self.set(result, self.get(a) & self.get(b)),
            Andi(result, a, b) => self.set(result, self.get(a) & R::from_immediate_low(b)),
            // Compare
            Slt(result, a, b) => self.set(
                result,
                ((self.get_signed(a) < self.get_signed(b)) as u32).into(),
            ),
            Slti(result, a, b) => self.set(
                result,
                ((self.get_signed(a) < R::from_immediate_low(b).to_signed()) as u32).into(),
            ),
            Sltu(result, a, b) => self.set(result, ((self.get(a) < self.get(b)) as u32).into()),
            Sltui(result, a, b) => self.set(
                result,
                ((self.get(a) < R::from_immediate_low(b)) as u32).into(),
            ),
            // Branching
            Beq(a, b, mut j) => {
                if self.get(a) == self.get(b) {
                    j.0 *= 2u16;
                    self.jump(self.pos() + j.into());
                }
            }
            Bne(a, b, mut j) => {
                if self.get(a) != self.get(b) {
                    j.0 *= 2u16;
                    self.jump(self.pos() + j.into());
                }
            }
            Blt(a, b, mut j) => {
                if self.get_signed(a) < self.get_signed(b) {
                    j.0 *= 2u16;
                    self.jump(self.pos() + j.into());
                }
            }
            Bge(a, b, mut j) => {
                if self.get_signed(a) >= self.get_signed(b) {
                    j.0 *= 2u16;
                    self.jump(self.pos() + j.into());
                }
            }
            Bltu(a, b, mut j) => {
                if self.get(a) < self.get(b) {
                    j.0 *= 2u16;
                    self.jump(self.pos() + j.into());
                }
            }
            Bgeu(a, b, mut j) => {
                if self.get(a) >= self.get(b) {
                    j.0 *= 2u16;
                    self.jump(self.pos() + j.into());
                }
            }
            // Jump and link
            Jal(result, location) => {
                let pos = self.pos().to_address().into();
                self.set(result, pos);
                self.jump(CodeIdx::from_address(
                    R::from_immediate_high(location) * 2u32.into() + pos - (4u32).into(),
                ));
            }
            Jalr(result, a, location) => {
                self.set(result, self.pos().to_address().into());
                self.jump(CodeIdx::from_address(
                    (R::from_immediate_low(location) + self.get(a)) & (!1u32).into(),
                ));
            }
        }
    }
}
