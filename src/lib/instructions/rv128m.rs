#![warn(missing_docs)]
#![warn(missing_doc_code_examples)]
#![allow(unreachable_code)]
#![allow(unused_variables)]

//! RV128M Standard Extension
//!
//! This extension provides instructions for integer multiplication and division of double-word
//! (64-bits) values. This extension extends the RV32M and RV64M for 128-bits register size.
//!
//! See the RISC-V [ISA] specification chapter 7  for more information.
//!
//! [ISA]: https://riscv.org/specifications/isa-spec-pdf
//!

use crate::{Computer, FRegister, Mult, Register, RegisterIdx};

pub use Instruction::*;

use asmv_derive::ParsableInstruction;
use num_traits::{AsPrimitive, WrappingMul};
use std::ops;

/// Instructions for the [RV128M](index.html) extension.
///
/// Contains multiplication and signed and unsigned division and remainder operations on double-word
/// (64-bits) integers.
#[derive(Debug, Eq, PartialEq, Hash, Clone, Copy, ParsableInstruction)]
pub enum Instruction {
    /// Double-Word Multiplication Instruction
    ///
    /// Instruction that performs multiplication of the lower double-word (64 bits) of `rs1` by
    /// `rs2`, writing the lower double-word of the result into `rd`, sign-extending to fill the
    /// register.
    ///
    /// # Arguments
    ///
    /// * `rd` - Destination register specifier, where the result is stored.
    /// * `rs1` - 1st source register specifier, this is the first operand.
    /// * `rs2` - 2nd source register specifier, this is the second operant.
    ///
    /// [ISA]: https://riscv.org/specifications/isa-spec-pdf/
    ///
    /// # Examples
    ///
    /// ```
    /// # use asmv::{Computer, Register128Bit, Mult, NoFloats, Instruction, RegisterIdx, ImmediateLow, ImmediateHigh};
    /// # use asmv::instructions::{self, Executable};
    /// let mut computer = Computer::<Register128Bit, Mult, NoFloats>::new(0);
    /// # computer.queue(Instruction::Base(instructions::base::Addi(
    /// #     // 12x = 0x0000_0012
    /// #     RegisterIdx(12),
    /// #     RegisterIdx(0),
    /// #     ImmediateLow::new(0x012).unwrap(),
    /// # )));
    /// # computer.queue(Instruction::Base(instructions::base::Slli(
    /// #     // 12x = 0x0000_1200
    /// #     RegisterIdx(12),
    /// #     RegisterIdx(12),
    /// #     ImmediateLow::new(8).unwrap(),
    /// # )));
    /// # computer.queue(Instruction::Base(instructions::base::Addi(
    /// #     // 12x = 0x0000_1234
    /// #     RegisterIdx(12),
    /// #     RegisterIdx(12),
    /// #     ImmediateLow::new(0x034).unwrap(),
    /// # )));
    /// # computer.queue(Instruction::Base(instructions::base::Addi(
    /// #     // 13x = 1
    /// #     RegisterIdx(13),
    /// #     RegisterIdx(0),
    /// #     ImmediateLow::new(1).unwrap(),
    /// # )));
    /// # computer.queue(Instruction::Base(instructions::base::Slli(
    /// #     // 12x = 0x01000000_00000000
    /// #     RegisterIdx(13),
    /// #     RegisterIdx(13),
    /// #     ImmediateLow::new(56).unwrap(),
    /// # )));
    ///
    /// computer.queue(Instruction::RV128M(instructions::rv128m::Muld(
    ///     RegisterIdx(10), // rs1 * rs2: 0x00000000_00000012__34000000_00000000
    ///     RegisterIdx(12), // rs1: 0x00000000_00001234
    ///     RegisterIdx(13), // rs2: 0x01000000_00000000
    /// )));
    /// computer.run_all();
    ///
    /// assert_eq!(computer.get(RegisterIdx(10)), 0x34000000_00000000u128.into());
    /// ```
    ///
    #[instruction(registers)]
    Muld(RegisterIdx, RegisterIdx, RegisterIdx),

    /// Double-Word Signed Divisison Instruction
    ///
    /// Instruction that divides the lower 64 bits of `rs1` by the lower 64 bits of `rs2`, treating
    /// them as signed integers, placing the 64-bit quotient in `rd`, sign-extended to [XLEN] bits.
    ///
    /// # Arguments
    ///
    /// * `rd` - Destination register specifier, where the result is stored.
    /// * `rs1` - 1st source register specifier, this is the dividend.
    /// * `rs2` - 2nd source register specifier, this is the divisor.
    ///
    /// [XLEN]: ../../trait.Register.html
    ///
    /// The quotient of division by zero has all bits set.
    ///
    /// Signed division overflow occurs only when the most-negative integer is divided by -1. The
    /// quotient of a signed division with overflow is equal to the dividend.
    ///
    /// See the RISC-V [ISA] specification chapter 7  for more information.
    ///
    /// [ISA]: https://riscv.org/specifications/isa-spec-pdf/
    ///
    /// # Examples
    ///
    /// ```
    /// # use asmv::{Computer, Register128Bit, Mult, NoFloats, Instruction, RegisterIdx, ImmediateLow};
    /// # use asmv::instructions::{self, Executable};
    /// let mut computer = Computer::<Register128Bit, Mult, NoFloats>::new(0);
    /// # computer.queue(Instruction::Base(instructions::base::Addi(
    /// #     // 12x = 18
    /// #     RegisterIdx(12),
    /// #     RegisterIdx(0),
    /// #     ImmediateLow::new(0x012).unwrap(),
    /// # )));
    /// # computer.queue(Instruction::Base(instructions::base::Addi(
    /// #     // 13x = 5
    /// #     RegisterIdx(13),
    /// #     RegisterIdx(0),
    /// #     ImmediateLow::new(0x005).unwrap(),
    /// # )));
    ///
    /// computer.queue(Instruction::RV128M(instructions::rv128m::Divd(
    ///     RegisterIdx(10), // rd: 18 / 5
    ///     RegisterIdx(12), // rs1: 18
    ///     RegisterIdx(13), // rs2: 5
    /// )));
    /// computer.run_all();
    ///
    /// assert_eq!(computer.get(RegisterIdx(10)), 3u32.into());
    /// ```
    ///
    #[instruction(registers)]
    Divd(RegisterIdx, RegisterIdx, RegisterIdx),

    /// Double-Word Unsigned Divisison Instruction
    ///
    /// Instruction that divides the lower 64 bits of `rs1` by the lower 64 bits of `rs2`, treating
    /// them as unsigned integers, placing the 64-bit quotient in `rd`, sign-extended to [XLEN]
    /// bits.
    ///
    /// # Arguments
    ///
    /// * `rd` - Destination register specifier, where the result is stored.
    /// * `rs1` - 1st source register specifier, this is the dividend.
    /// * `rs2` - 2nd source register specifier, this is the divisor.
    ///
    /// [XLEN]: ../../trait.Register.html
    ///
    /// The quotient of division by zero has all bits set.
    ///
    /// Unsigned division overflow cannot occur.
    ///
    /// See the RISC-V [ISA] specification chapter 7  for more information.
    ///
    /// [ISA]: https://riscv.org/specifications/isa-spec-pdf/
    ///
    /// # Examples
    ///
    /// ```
    /// # use asmv::{Computer, Register128Bit, Mult, NoFloats, Instruction, RegisterIdx, ImmediateLow};
    /// # use asmv::instructions::{self, Executable};
    /// let mut computer = Computer::<Register128Bit, Mult, NoFloats>::new(0);
    /// # computer.queue(Instruction::Base(instructions::base::Addi(
    /// #     // 12x = 18
    /// #     RegisterIdx(12),
    /// #     RegisterIdx(0),
    /// #     ImmediateLow::new(0x012).unwrap(),
    /// # )));
    /// # computer.queue(Instruction::Base(instructions::base::Addi(
    /// #     // 13x = 5
    /// #     RegisterIdx(13),
    /// #     RegisterIdx(0),
    /// #     ImmediateLow::new(0x005).unwrap(),
    /// # )));
    ///
    /// computer.queue(Instruction::RV128M(instructions::rv128m::Divud(
    ///     RegisterIdx(10), // rd: 18 / 5
    ///     RegisterIdx(12), // rs1: 18
    ///     RegisterIdx(13), // rs2: 5
    /// )));
    /// computer.run_all();
    ///
    /// assert_eq!(computer.get(RegisterIdx(10)), 3u32.into());
    /// ```
    ///
    #[instruction(registers)]
    Divud(RegisterIdx, RegisterIdx, RegisterIdx),

    /// Double-Word Signed Remainder Instruction
    ///
    /// Instruction that provides the remainder of the lower 64 bits of `rs1` divided by the lower
    /// 64 bits of `rs2`, both treated as signed integers, placing the 64-bits result in `rd`,
    /// sign-extended to [XLEN].
    ///
    /// # Arguments
    ///
    /// * `rd` - Destination register specifier, where the result is stored.
    /// * `rs1` - 1st source register specifier, this is the dividend.
    /// * `rs2` - 2nd source register specifier, this is the divisor.
    ///
    /// [XLEN]: ../../trait.Register.html
    ///
    /// The remainder of division by zero equals the dividend, signed-extended to [XLEN].
    ///
    /// Signed division overflow occurs only when the most-negative integer is divided by -1. The
    /// remainder of a signed division with overflow equals zero.
    ///
    /// See the RISC-V [ISA] specification chapter 7  for more information.
    ///
    /// [ISA]: https://riscv.org/specifications/isa-spec-pdf/
    ///
    /// # Examples
    ///
    /// ```
    /// # use asmv::{Computer, Register128Bit, Mult, NoFloats, Instruction, RegisterIdx, ImmediateLow};
    /// # use asmv::instructions::{self, Executable};
    /// let mut computer = Computer::<Register128Bit, Mult, NoFloats>::new(0);
    /// # computer.queue(Instruction::Base(instructions::base::Addi(
    /// #     // 12x = 18
    /// #     RegisterIdx(12),
    /// #     RegisterIdx(0),
    /// #     ImmediateLow::new(0x012).unwrap(),
    /// # )));
    /// # computer.queue(Instruction::Base(instructions::base::Addi(
    /// #     // 13x = 5
    /// #     RegisterIdx(13),
    /// #     RegisterIdx(0),
    /// #     ImmediateLow::new(0x005).unwrap(),
    /// # )));
    ///
    /// computer.queue(Instruction::RV128M(instructions::rv128m::Remd(
    ///     RegisterIdx(10), // rd: 18 % 5
    ///     RegisterIdx(12), // rs1: 18
    ///     RegisterIdx(13), // rs2: 5
    /// )));
    /// computer.run_all();
    ///
    /// assert_eq!(computer.get(RegisterIdx(10)), 3u32.into());
    /// ```
    ///
    #[instruction(registers)]
    Remd(RegisterIdx, RegisterIdx, RegisterIdx),

    /// Double-Word Unsigned Remainder Instruction
    ///
    /// Instruction that provides the remainder of the lower 64 bits of `rs1` divided by the lower
    /// 64 bits of `rs2`, both treated as unsigned integers, placing the 64-bits result in `rd`,
    /// sign-extended to [XLEN].
    ///
    /// # Arguments
    ///
    /// * `rd` - Destination register specifier, where the result is stored.
    /// * `rs1` - 1st source register specifier, this is the dividend.
    /// * `rs2` - 2nd source register specifier, this is the divisor.
    ///
    /// [XLEN]: ../../trait.Register.html
    ///
    /// The remainder of division by zero equals the dividend, signed-extended to [XLEN].
    ///
    /// Unsigned division overflow cannot occur.
    ///
    /// See the RISC-V [ISA] specification chapter 7  for more information.
    ///
    /// [ISA]: https://riscv.org/specifications/isa-spec-pdf/
    ///
    /// # Examples
    ///
    /// ```
    /// # use asmv::{Computer, Register128Bit, Mult, NoFloats, Instruction, RegisterIdx, ImmediateLow};
    /// # use asmv::instructions::{self, Executable};
    /// let mut computer = Computer::<Register128Bit, Mult, NoFloats>::new(0);
    /// # computer.queue(Instruction::Base(instructions::base::Addi(
    /// #     // 12x = 18
    /// #     RegisterIdx(12),
    /// #     RegisterIdx(0),
    /// #     ImmediateLow::new(0x012).unwrap(),
    /// # )));
    /// # computer.queue(Instruction::Base(instructions::base::Addi(
    /// #     // 13x = 5
    /// #     RegisterIdx(13),
    /// #     RegisterIdx(0),
    /// #     ImmediateLow::new(0x005).unwrap(),
    /// # )));
    ///
    /// computer.queue(Instruction::RV128M(instructions::rv128m::Remud(
    ///     RegisterIdx(10), // rd: 18 % 5
    ///     RegisterIdx(12), // rs1: 18
    ///     RegisterIdx(13), // rs2: 5
    /// )));
    /// computer.run_all();
    ///
    /// assert_eq!(computer.get(RegisterIdx(10)), 3u32.into());
    /// ```
    ///
    #[instruction(registers)]
    Remud(RegisterIdx, RegisterIdx, RegisterIdx),
}

impl<R: Register, F> Computer<R, Mult, F>
where
    F: FRegister,
    R: AsPrimitive<u64>,
    R: WrappingMul,
    R: From<u64>,
    R::Signed: AsPrimitive<i64>,
{
    /// Executes an RV128M instruction
    ///
    /// # Panics
    ///
    /// Panics if there is no more instructions to execute. Use at_end
    /// to avoid it
    pub fn execute_rv128m(&mut self, instruction: Instruction) {
        use Instruction::*;
        match instruction {
            Muld(result, a, b) => self.set(
                result,
                R::sign_extend(
                    <R as AsPrimitive<u64>>::as_(self.get(a)).wrapping_mul(self.get(b).as_()),
                    64, // Sign-extend 64bits to 128bits
                ),
            ),
            Divd(result, a, b) => self.set(
                result,
                R::sign_extend::<u64>(
                    {
                        // Division with overflow => dividend (a)
                        if AsPrimitive::<i64>::as_(self.get_signed(a)) == std::i64::MIN
                            && AsPrimitive::<u64>::as_(self.get(b)).count_zeros() == 0
                        {
                            self.get(a).as_()

                        // Division by zero => all bits set (-1)
                        } else if AsPrimitive::<u64>::as_(self.get(b)).count_ones() == 0 {
                            R::max_value().as_()

                        // Division
                        } else {
                            ops::Div::div(
                                AsPrimitive::<i64>::as_(self.get_signed(a)),
                                AsPrimitive::<i64>::as_(self.get_signed(b)),
                            )
                            .as_()
                        }
                    },
                    64, // Sign-extend 64bits to 128bits
                ),
            ),
            Divud(result, a, b) => self.set(
                result,
                R::sign_extend::<u64>(
                    {
                        // Division by zero => all bits set (-1)
                        if AsPrimitive::<u64>::as_(self.get(b)).count_ones() == 0 {
                            R::max_value().as_()

                        // Division
                        } else {
                            ops::Div::div(
                                AsPrimitive::<u64>::as_(self.get(a)),
                                AsPrimitive::<u64>::as_(self.get(b)),
                            )
                        }
                    },
                    64, // Sign-extend 64bits to 128bits
                ),
            ),
            Remd(result, a, b) => self.set(
                result,
                R::sign_extend::<u64>(
                    {
                        // Remainder with overflow => 0
                        if AsPrimitive::<i64>::as_(self.get_signed(a)) == std::i64::MIN
                            && AsPrimitive::<u64>::as_(self.get(b)).count_zeros() == 0
                        {
                            R::min_value().as_()

                        // Remainder by zero => dividend (a)
                        } else if AsPrimitive::<u64>::as_(self.get(b)).count_ones() == 0 {
                            self.get(a).as_()

                        // Remainder
                        } else {
                            ops::Rem::rem(
                                AsPrimitive::<i64>::as_(self.get_signed(a)),
                                AsPrimitive::<i64>::as_(self.get_signed(b)),
                            )
                            .as_()
                        }
                    },
                    64, // Sign-extend 64bits to 128bits
                ),
            ),
            Remud(result, a, b) => self.set(
                result,
                R::sign_extend::<u64>(
                    {
                        // Remainder by zero => dividend (a)
                        if AsPrimitive::<u64>::as_(self.get(b)).count_ones() == 0 {
                            self.get(a).as_()

                        // Remainder
                        } else {
                            ops::Rem::rem(
                                AsPrimitive::<u64>::as_(self.get(a)),
                                AsPrimitive::<u64>::as_(self.get(b)),
                            )
                        }
                    },
                    64, // Sign-extend 64bits to 128bits
                ),
            ),
        }
    }
}
