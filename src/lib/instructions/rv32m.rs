#![warn(missing_docs)]
#![allow(unreachable_code)]
#![allow(unused_variables)]

//! RV32M Standard Extension
//!
//! This extension provides instructions for integer multiplication and division.
//!
//! See the RISC-V [ISA] specification chapter 7  for more information.
//!
//! [ISA]: https://riscv.org/specifications/isa-spec-pdf/
//!

use crate::{Computer, FRegister, Mult, Register, RegisterIdx};

pub use Instruction::*;

use asmv_derive::ParsableInstruction;
use num_traits::{bounds::Bounded, AsPrimitive, Signed, WrappingMul};
use std::convert::TryFrom;
use std::ops;

// TODO: Multiplications

/// Instructions for the [RV32M](index.html) extension.
///
/// Contains various multiplication operations, as well as signed and unsigned division and
/// remainder operations.
#[derive(Debug, Eq, PartialEq, Hash, Clone, Copy, ParsableInstruction)]
pub enum Instruction {
    /// Multiplication Instruction
    ///
    /// Instruction that performs multiplication of `rs1` by `rs2`, writing the lower [XLEN] bits of
    /// the result in `rd`.
    ///
    /// To get the higher half of the result, use one of the high multiplication instructions.
    ///
    /// # Arguments
    ///
    /// * `rd` - Destination register specifier, where the result is stored.
    /// * `rs1` - 1st source register specifier, this is the first operand.
    /// * `rs2` - 2nd source register specifier, this is the second operant.
    ///
    /// [XLEN]: ../../trait.Register.html
    ///
    /// [ISA]: https://riscv.org/specifications/isa-spec-pdf/
    ///
    /// # Examples
    ///
    /// ```
    /// # use asmv::{Computer, Register32Bit, Mult, NoFloats, Instruction, RegisterIdx, ImmediateLow, ImmediateHigh};
    /// # use asmv::instructions::{self, Executable};
    /// let mut computer = Computer::<Register32Bit, Mult, NoFloats>::new(0);
    /// # computer.queue(Instruction::Base(instructions::base::Addi(
    /// #     // 12x = 0x0000_0012
    /// #     RegisterIdx(12),
    /// #     RegisterIdx(0),
    /// #     ImmediateLow::new(0x012).unwrap(),
    /// # )));
    /// # computer.queue(Instruction::Base(instructions::base::Slli(
    /// #     // 12x = 0x0000_1200
    /// #     RegisterIdx(12),
    /// #     RegisterIdx(12),
    /// #     ImmediateLow::new(8).unwrap(),
    /// # )));
    /// # computer.queue(Instruction::Base(instructions::base::Addi(
    /// #     // 12x = 0x0000_1234
    /// #     RegisterIdx(12),
    /// #     RegisterIdx(12),
    /// #     ImmediateLow::new(0x034).unwrap(),
    /// # )));
    /// # computer.queue(Instruction::Base(instructions::base::Lui(
    /// #     // 13x = 0x0100_0000
    /// #     RegisterIdx(13),
    /// #     ImmediateHigh::new(0x0100_0).unwrap(),
    /// # )));
    ///
    /// computer.queue(Instruction::RV32M(instructions::rv32m::Mul(
    ///     RegisterIdx(10), // rs1 * rs2: 0x0000_0012__3400_0000
    ///     RegisterIdx(12), // rs1: 0x0000_1234
    ///     RegisterIdx(13), // rs2: 0x0100_0000
    /// )));
    /// computer.run_all();
    ///
    /// assert_eq!(computer.get(RegisterIdx(10)), 0x3400_0000u32.into());
    /// ```
    ///
    #[instruction(registers)]
    Mul(RegisterIdx, RegisterIdx, RegisterIdx),

    /// High Multiplication Instruction
    ///
    /// Instruction that performs multiplication of `rs1` by `rs2`, both treated as signed integers,
    /// writing the upper [XLEN] bits of the result in `rd`.
    ///
    /// # Arguments
    ///
    /// * `rd` - Destination register specifier, where the result is stored.
    /// * `rs1` - 1st source register specifier, this is the first operand.
    /// * `rs2` - 2nd source register specifier, this is the second operant.
    ///
    /// [XLEN]: ../../trait.Register.html
    ///
    /// [ISA]: https://riscv.org/specifications/isa-spec-pdf/
    ///
    /// # Examples
    ///
    /// ```
    /// # use asmv::{Computer, Register32Bit, Mult, NoFloats, Instruction, RegisterIdx, ImmediateLow, ImmediateHigh};
    /// # use asmv::instructions::{self, Executable};
    /// let mut computer = Computer::<Register32Bit, Mult, NoFloats>::new(0);
    /// # computer.queue(Instruction::Base(instructions::base::Addi(
    /// #     // 12x = 0x0000_0012
    /// #     RegisterIdx(12),
    /// #     RegisterIdx(0),
    /// #     ImmediateLow::new(0x012).unwrap(),
    /// # )));
    /// # computer.queue(Instruction::Base(instructions::base::Slli(
    /// #     // 12x = 0x0000_1200
    /// #     RegisterIdx(12),
    /// #     RegisterIdx(12),
    /// #     ImmediateLow::new(8).unwrap(),
    /// # )));
    /// # computer.queue(Instruction::Base(instructions::base::Addi(
    /// #     // 12x = 0x0000_1234
    /// #     RegisterIdx(12),
    /// #     RegisterIdx(12),
    /// #     ImmediateLow::new(0x034).unwrap(),
    /// # )));
    /// # computer.queue(Instruction::Base(instructions::base::Lui(
    /// #     // 13x = 0x0100_0000
    /// #     RegisterIdx(13),
    /// #     ImmediateHigh::new(0x0100_0).unwrap(),
    /// # )));
    ///
    /// computer.queue(Instruction::RV32M(instructions::rv32m::Mulh(
    ///     RegisterIdx(10), // rs1 * rs2: 0x0000_0012__3400_0000
    ///     RegisterIdx(12), // rs1: 0x0000_1234
    ///     RegisterIdx(13), // rs2: 0x0100_0000
    /// )));
    /// computer.run_all();
    ///
    /// assert_eq!(computer.get(RegisterIdx(10)), 0x0000_0012u32.into());
    /// ```
    ///
    #[instruction(registers)]
    Mulh(RegisterIdx, RegisterIdx, RegisterIdx),

    /// High Multiplication Instruction
    ///
    /// Instruction that performs multiplication of `rs1` treated as signed integer by `rs2` as an
    /// unsigned integer, writing the upper [XLEN] bits of the result in `rd`.
    ///
    /// # Arguments
    ///
    /// * `rd` - Destination register specifier, where the result is stored.
    /// * `rs1` - 1st source register specifier, this is the first operand.
    /// * `rs2` - 2nd source register specifier, this is the second operant.
    ///
    /// [XLEN]: ../../trait.Register.html
    ///
    /// [ISA]: https://riscv.org/specifications/isa-spec-pdf/
    ///
    /// # Examples
    ///
    /// ```
    /// # use asmv::{Computer, Register32Bit, Mult, NoFloats, Instruction, RegisterIdx, ImmediateLow, ImmediateHigh};
    /// # use asmv::instructions::{self, Executable};
    /// let mut computer = Computer::<Register32Bit, Mult, NoFloats>::new(0);
    /// # computer.queue(Instruction::Base(instructions::base::Addi(
    /// #     // 12x = 0x0000_0012
    /// #     RegisterIdx(12),
    /// #     RegisterIdx(0),
    /// #     ImmediateLow::new(0x012).unwrap(),
    /// # )));
    /// # computer.queue(Instruction::Base(instructions::base::Slli(
    /// #     // 12x = 0x0000_1200
    /// #     RegisterIdx(12),
    /// #     RegisterIdx(12),
    /// #     ImmediateLow::new(8).unwrap(),
    /// # )));
    /// # computer.queue(Instruction::Base(instructions::base::Addi(
    /// #     // 12x = 0x0000_1234
    /// #     RegisterIdx(12),
    /// #     RegisterIdx(12),
    /// #     ImmediateLow::new(0x034).unwrap(),
    /// # )));
    /// # computer.queue(Instruction::Base(instructions::base::Lui(
    /// #     // 13x = 0x0100_0000
    /// #     RegisterIdx(13),
    /// #     ImmediateHigh::new(0x0100_0).unwrap(),
    /// # )));
    ///
    /// computer.queue(Instruction::RV32M(instructions::rv32m::Mulhsu(
    ///     RegisterIdx(10), // rs1 * rs2: 0x0000_0012__3400_0000
    ///     RegisterIdx(12), // rs1: 0x0000_1234
    ///     RegisterIdx(13), // rs2: 0x0100_0000
    /// )));
    /// computer.run_all();
    ///
    /// assert_eq!(computer.get(RegisterIdx(10)), 0x0000_0012u32.into());
    /// ```
    ///
    #[instruction(registers)]
    Mulhsu(RegisterIdx, RegisterIdx, RegisterIdx),

    /// High Multiplication Instruction
    ///
    /// Instruction that performs multiplication of `rs1` by `rs2`, both treated as unsigned
    /// integers, writing the upper [XLEN] bits of the result in `rd`.
    ///
    /// # Arguments
    ///
    /// * `rd` - Destination register specifier, where the result is stored.
    /// * `rs1` - 1st source register specifier, this is the first operand.
    /// * `rs2` - 2nd source register specifier, this is the second operant.
    ///
    /// [XLEN]: ../../trait.Register.html
    ///
    /// [ISA]: https://riscv.org/specifications/isa-spec-pdf/
    ///
    /// # Examples
    ///
    /// ```
    /// # use asmv::{Computer, Register32Bit, Mult, NoFloats, Instruction, RegisterIdx, ImmediateLow, ImmediateHigh};
    /// # use asmv::instructions::{self, Executable};
    /// let mut computer = Computer::<Register32Bit, Mult, NoFloats>::new(0);
    /// # computer.queue(Instruction::Base(instructions::base::Addi(
    /// #     // 12x = 0x0000_0012
    /// #     RegisterIdx(12),
    /// #     RegisterIdx(0),
    /// #     ImmediateLow::new(0x012).unwrap(),
    /// # )));
    /// # computer.queue(Instruction::Base(instructions::base::Slli(
    /// #     // 12x = 0x0000_1200
    /// #     RegisterIdx(12),
    /// #     RegisterIdx(12),
    /// #     ImmediateLow::new(8).unwrap(),
    /// # )));
    /// # computer.queue(Instruction::Base(instructions::base::Addi(
    /// #     // 12x = 0x0000_1234
    /// #     RegisterIdx(12),
    /// #     RegisterIdx(12),
    /// #     ImmediateLow::new(0x034).unwrap(),
    /// # )));
    /// # computer.queue(Instruction::Base(instructions::base::Lui(
    /// #     // 13x = 0x0100_0000
    /// #     RegisterIdx(13),
    /// #     ImmediateHigh::new(0x0100_0).unwrap(),
    /// # )));
    ///
    /// computer.queue(Instruction::RV32M(instructions::rv32m::Mulhu(
    ///     RegisterIdx(10), // rs1 * rs2: 0x0000_0012__3400_0000
    ///     RegisterIdx(12), // rs1: 0x0000_1234
    ///     RegisterIdx(13), // rs2: 0x0100_0000
    /// )));
    /// computer.run_all();
    ///
    /// assert_eq!(computer.get(RegisterIdx(10)), 0x0000_0012u32.into());
    /// ```
    ///
    #[instruction(registers)]
    Mulhu(RegisterIdx, RegisterIdx, RegisterIdx),

    /// Signed Division Instruction
    ///
    /// Instruction that performs an [XLEN] bits by [XLEN] bits signed integer division of `rs1` by
    /// `rs2`, rounding towards zero.
    ///
    /// # Arguments
    ///
    /// * `rd` - Destination register specifier, where the result is stored.
    /// * `rs1` - 1st source register specifier, this is the dividend.
    /// * `rs2` - 2nd source register specifier, this is the divisor.
    ///
    /// [XLEN]: ../../trait.Register.html
    ///
    /// The quotient of division by zero has all bits set.
    ///
    /// Signed division overflow occurs only when the most-negative integer is divided by -1. The
    /// quotient of a signed division with overflow is equal to the dividend.
    ///
    /// See the RISC-V [ISA] specification chapter 7  for more information.
    ///
    /// [ISA]: https://riscv.org/specifications/isa-spec-pdf/
    ///
    /// # Examples
    ///
    /// ```
    /// # use asmv::{Computer, Register32Bit, Mult, NoFloats, Instruction, RegisterIdx, ImmediateLow};
    /// # use asmv::instructions::{self, Executable};
    /// let mut computer = Computer::<Register32Bit, Mult, NoFloats>::new(0);
    /// # computer.queue(Instruction::Base(instructions::base::Addi(
    /// #     // 12x = 18
    /// #     RegisterIdx(12),
    /// #     RegisterIdx(0),
    /// #     ImmediateLow::new(0x012).unwrap(),
    /// # )));
    /// # computer.queue(Instruction::Base(instructions::base::Addi(
    /// #     // 13x = 5
    /// #     RegisterIdx(13),
    /// #     RegisterIdx(0),
    /// #     ImmediateLow::new(0x005).unwrap(),
    /// # )));
    ///
    /// computer.queue(Instruction::RV32M(instructions::rv32m::Div(
    ///     RegisterIdx(10), // rd: 18 / 5
    ///     RegisterIdx(12), // rs1: 18
    ///     RegisterIdx(13), // rs2: 5
    /// )));
    /// computer.run_all();
    ///
    /// assert_eq!(computer.get(RegisterIdx(10)), 3u32.into());
    /// ```
    ///
    #[instruction(registers)]
    Div(RegisterIdx, RegisterIdx, RegisterIdx),

    /// Unsigned Division Instruction
    ///
    /// Instruction that performs an [XLEN] bits by [XLEN] bits unsigned integer division of `rs1`
    /// by `rs2`, rounding towards zero.
    ///
    /// # Arguments
    ///
    /// * `rd` - Destination register specifier, where the result is stored.
    /// * `rs1` - 1st source register specifier, this is the dividend.
    /// * `rs2` - 2nd source register specifier, this is the divisor.
    ///
    /// [XLEN]: ../../trait.Register.html
    ///
    /// The quotient of division by zero has all bits set.
    ///
    /// Unsigned division with overflow cannot occur.
    ///
    /// See the RISC-V [ISA] specification chapter 7  for more information.
    ///
    /// [ISA]: https://riscv.org/specifications/isa-spec-pdf/
    ///
    /// # Examples
    ///
    /// ```
    /// # use asmv::{Computer, Register32Bit, Mult, NoFloats, Instruction, RegisterIdx, ImmediateLow};
    /// # use asmv::instructions::{self, Executable};
    /// let mut computer = Computer::<Register32Bit, Mult, NoFloats>::new(0);
    /// # computer.queue(Instruction::Base(instructions::base::Addi(
    /// #     // 12x = 18
    /// #     RegisterIdx(12),
    /// #     RegisterIdx(0),
    /// #     ImmediateLow::new(0x012).unwrap(),
    /// # )));
    /// # computer.queue(Instruction::Base(instructions::base::Addi(
    /// #     // 13x = 5
    /// #     RegisterIdx(13),
    /// #     RegisterIdx(0),
    /// #     ImmediateLow::new(0x005).unwrap(),
    /// # )));
    ///
    /// computer.queue(Instruction::RV32M(instructions::rv32m::Divu(
    ///     RegisterIdx(10), // rd: 18 / 5
    ///     RegisterIdx(12), // rs1: 18
    ///     RegisterIdx(13), // rs2: 5
    /// )));
    /// computer.run_all();
    ///
    /// assert_eq!(computer.get(RegisterIdx(10)), 3u32.into());
    /// ```
    ///
    #[instruction(registers)]
    Divu(RegisterIdx, RegisterIdx, RegisterIdx),

    /// Signed Remainder Instruction
    ///
    /// Instruction that provides the remainder of an [XLEN] bits by [XLEN] bits signed integer
    /// division of `rs1` by `rs2`. The sign of the remainder equals the sign of the dividend.
    ///
    /// # Arguments
    ///
    /// * `rd` - Destination register specifier, where the result is stored.
    /// * `rs1` - 1st source register specifier, this is the dividend.
    /// * `rs2` - 2nd source register specifier, this is the divisor.
    ///
    /// [XLEN]: ../../trait.Register.html
    ///
    /// The remainder of division by zero equals the dividend.
    ///
    /// Signed division overflow occurs only when the most-negative integer is divided by -1. The
    /// remainder of a signed division with overflow equals zero.
    ///
    /// See the RISC-V [ISA] specification chapter 7  for more information.
    ///
    /// [ISA]: https://riscv.org/specifications/isa-spec-pdf/
    ///
    /// # Examples
    ///
    /// ```
    /// # use asmv::{Computer, Register32Bit, Mult, NoFloats, Instruction, RegisterIdx, ImmediateLow};
    /// # use asmv::instructions::{self, Executable};
    /// let mut computer = Computer::<Register32Bit, Mult, NoFloats>::new(0);
    /// # computer.queue(Instruction::Base(instructions::base::Addi(
    /// #     // 12x = 18
    /// #     RegisterIdx(12),
    /// #     RegisterIdx(0),
    /// #     ImmediateLow::new(0x012).unwrap(),
    /// # )));
    /// # computer.queue(Instruction::Base(instructions::base::Addi(
    /// #     // 13x = 5
    /// #     RegisterIdx(13),
    /// #     RegisterIdx(0),
    /// #     ImmediateLow::new(0x005).unwrap(),
    /// # )));
    ///
    /// computer.queue(Instruction::RV32M(instructions::rv32m::Rem(
    ///     RegisterIdx(10), // rd: 18 % 5
    ///     RegisterIdx(12), // rs1: 18
    ///     RegisterIdx(13), // rs2: 5
    /// )));
    /// computer.run_all();
    ///
    /// assert_eq!(computer.get(RegisterIdx(10)), 3u32.into());
    /// ```
    ///
    #[instruction(registers)]
    Rem(RegisterIdx, RegisterIdx, RegisterIdx),

    /// Unsigned Remainder Instruction
    ///
    /// Instruction that provides the remainder of an [XLEN] bits by [XLEN] bits unsigned integer
    /// division of `rs1` by `rs2`.
    ///
    /// # Arguments
    ///
    /// * `rd` - Destination register specifier, where the result is stored.
    /// * `rs1` - 1st source register specifier, this is the dividend.
    /// * `rs2` - 2nd source register specifier, this is the divisor.
    ///
    /// [XLEN]: ../../trait.Register.html
    ///
    /// The remainder of division by zero equals the dividend.
    ///
    /// Unsigned division overflow cannot occur.
    ///
    /// See the RISC-V [ISA] specification chapter 7  for more information.
    ///
    /// [ISA]: https://riscv.org/specifications/isa-spec-pdf/
    ///
    /// # Examples
    ///
    /// ```
    /// # use asmv::{Computer, Register32Bit, Mult, NoFloats, Instruction, RegisterIdx, ImmediateLow};
    /// # use asmv::instructions::{self, Executable};
    /// let mut computer = Computer::<Register32Bit, Mult, NoFloats>::new(0);
    /// # computer.queue(Instruction::Base(instructions::base::Addi(
    /// #     // 12x = 18
    /// #     RegisterIdx(12),
    /// #     RegisterIdx(0),
    /// #     ImmediateLow::new(0x012).unwrap(),
    /// # )));
    /// # computer.queue(Instruction::Base(instructions::base::Addi(
    /// #     // 13x = 5
    /// #     RegisterIdx(13),
    /// #     RegisterIdx(0),
    /// #     ImmediateLow::new(0x005).unwrap(),
    /// # )));
    ///
    /// computer.queue(Instruction::RV32M(instructions::rv32m::Remu(
    ///     RegisterIdx(10), // rd: 18 % 5
    ///     RegisterIdx(12), // rs1: 18
    ///     RegisterIdx(13), // rs2: 5
    /// )));
    /// computer.run_all();
    ///
    /// assert_eq!(computer.get(RegisterIdx(10)), 3u32.into());
    /// ```
    ///
    #[instruction(registers)]
    Remu(RegisterIdx, RegisterIdx, RegisterIdx),
}

impl<R: Register, F> Computer<R, Mult, F>
where
    F: FRegister,
    // R: AsPrimitive<u32>,
    R: WrappingMul,
    R::Signed: AsPrimitive<R>,
    R::HigherUnsigned: AsPrimitive<R>,
    R::HigherSigned: AsPrimitive<R>,
    <R::HigherUnsigned as TryFrom<R::HigherSigned>>::Error: std::fmt::Debug,
    <R::HigherSigned as TryFrom<R::HigherUnsigned>>::Error: std::fmt::Debug,
{
    /// Executes an RV32M instruction
    ///
    /// # Panics
    ///
    /// Panics if there is no more instructions to execute. Use at_end
    /// to avoid it
    pub fn execute_rv32m(&mut self, instruction: Instruction) {
        use Instruction::*;
        match instruction {
            Mul(result, a, b) => self.set(result, {
                num_traits::WrappingMul::wrapping_mul(&self.get(a), &self.get(b))
            }),
            Mulh(result, a, b) => self.set(result, {
                ops::Shr::shr(
                    ops::Mul::mul(self.get_higher_signed(a), self.get_higher_signed(b)),
                    std::mem::size_of::<R>() * 8,
                )
                .as_()
            }),
            Mulhsu(result, a, b) => self.set(result, {
                // Operands
                let a = self.get_higher_signed(a);
                let b = self.get_higher_unsigned(b);

                // Retain the original sign
                let neg = a.is_negative();

                // Absolute value to convert to unsigned
                let a = num_traits::abs(a);

                // Convert to unsigned for multiplication
                let a = R::HigherUnsigned::try_from(a).unwrap();

                // Actual multiplication
                let value = a * b;

                // Convert to signed for original sign
                let value = R::HigherSigned::try_from(value).unwrap();

                // Return original sign
                let value = if neg { -value } else { value };

                // Right shift
                let value = value >> (std::mem::size_of::<R>() * 8);

                // Final convert to Register
                value.as_()
            }),
            Mulhu(result, a, b) => self.set(result, {
                ops::Shr::shr(
                    ops::Mul::mul(self.get_higher_unsigned(a), self.get_higher_unsigned(b)),
                    std::mem::size_of::<R>() * 8,
                )
                .as_()
            }),
            Div(result, a, b) => self.set(result, {
                // Division by zero => all bits set (-1)
                if self.get(b).as_() == 0 {
                    R::max_value()

                // Division with overflow => dividend (a)
                } else if self.get_signed(a) == <R::Signed as Bounded>::min_value()
                    && self.get(b).count_zeros() == 0
                {
                    self.get(a)

                // Division
                } else {
                    ops::Div::div(self.get_signed(a), self.get_signed(b)).as_()
                }
            }),
            Divu(result, a, b) => self.set(result, {
                // Division by zero => all bits set (-1)
                if self.get(b).as_() == 0 {
                    R::max_value()

                // Division
                } else {
                    ops::Div::div(self.get(a), self.get(b))
                }
            }),
            Rem(result, a, b) => self.set(result, {
                // Remainder by zero => Dividend (a)
                if self.get(b).as_() == 0 {
                    self.get(a)

                // Remainder with overflow => 0
                } else if self.get_signed(a) == <R::Signed as Bounded>::min_value()
                    && self.get(b).count_zeros() == 0
                {
                    R::min_value()

                // Remainder
                } else {
                    ops::Rem::rem(self.get_signed(a), self.get_signed(b)).as_()
                }
            }),
            Remu(result, a, b) => self.set(result, {
                // Remainder by zero => Dividend (a)
                if self.get(b).as_() == 0 {
                    self.get(a)

                // Remainder
                } else {
                    ops::Rem::rem(self.get(a), self.get(b))
                }
            }),
        }
    }
}
