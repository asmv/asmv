use crate::{Computer, ExtensionMult, FRegister, ImmediateLow, Register, RegisterIdx, F32};

pub use Instruction::*;

use asmv_derive::ParsableInstruction;
use core::num::FpCategory;
use num_traits::{AsPrimitive, Signed};

#[derive(Debug, Eq, PartialEq, Hash, Clone, Copy, ParsableInstruction)]
pub enum Instruction {
    // Move
    #[instruction(registers_2)]
    Fmvsw(RegisterIdx, RegisterIdx),
    #[instruction(registers_2)]
    Fmvws(RegisterIdx, RegisterIdx),
    // Convert
    #[instruction(registers_2)]
    Fcvtws(RegisterIdx, RegisterIdx),
    #[instruction(registers_2)]
    Fcvtsw(RegisterIdx, RegisterIdx),
    // Load
    #[instruction(store_load)]
    Flw(RegisterIdx, RegisterIdx, ImmediateLow),
    // Stores
    #[instruction(store_load)]
    Fsw(RegisterIdx, RegisterIdx, ImmediateLow),
    // Arithmetic (s = f32)
    #[instruction(registers)]
    Fadds(RegisterIdx, RegisterIdx, RegisterIdx),
    #[instruction(registers)]
    Fsubs(RegisterIdx, RegisterIdx, RegisterIdx),
    #[instruction(registers)]
    Fmuls(RegisterIdx, RegisterIdx, RegisterIdx),
    #[instruction(registers)]
    Fdivs(RegisterIdx, RegisterIdx, RegisterIdx),
    #[instruction(registers_2)]
    Fsqrts(RegisterIdx, RegisterIdx),
    // Mul-Add
    #[instruction(muladd)]
    Fmadds(RegisterIdx, RegisterIdx, RegisterIdx, RegisterIdx),
    #[instruction(muladd)]
    Fmsubs(RegisterIdx, RegisterIdx, RegisterIdx, RegisterIdx),
    #[instruction(muladd)]
    Fnmadds(RegisterIdx, RegisterIdx, RegisterIdx, RegisterIdx),
    #[instruction(muladd)]
    Fnmsubs(RegisterIdx, RegisterIdx, RegisterIdx, RegisterIdx),
    // Sign Inject
    #[instruction(registers)]
    Fsgnjs(RegisterIdx, RegisterIdx, RegisterIdx),
    #[instruction(registers)]
    Fsgnjns(RegisterIdx, RegisterIdx, RegisterIdx),
    #[instruction(registers)]
    Fsgnjxs(RegisterIdx, RegisterIdx, RegisterIdx),
    // Min/Max
    #[instruction(registers)]
    Fmins(RegisterIdx, RegisterIdx, RegisterIdx),
    #[instruction(registers)]
    Fmaxs(RegisterIdx, RegisterIdx, RegisterIdx),
    // Compare
    #[instruction(registers)]
    Feqs(RegisterIdx, RegisterIdx, RegisterIdx),
    #[instruction(registers)]
    Flts(RegisterIdx, RegisterIdx, RegisterIdx),
    #[instruction(registers)]
    Fles(RegisterIdx, RegisterIdx, RegisterIdx),
    // Categorization
    #[instruction(registers_2)]
    Fclasss(RegisterIdx, RegisterIdx),
    // Configuration
}

impl<R, M, F> Computer<R, M, F>
where
    F: AsPrimitive<F32> + FRegister + Signed,
    F32: AsPrimitive<F>,
    F: AsPrimitive<R>,
    R: AsPrimitive<F>,
    M: ExtensionMult,
    u32: AsPrimitive<R>,
    R: AsPrimitive<u32>,
    R: Register,
    bool: AsPrimitive<R>,
{
    /// Execute an instruction
    ///
    /// Panics if there is no more instructions to execute. Use at_end
    /// to avoid it
    pub fn execute_rv32f(&mut self, instruction: Instruction) {
        use Instruction::*;
        match instruction {
            // Move
            Fmvsw(target, source) => self.set(
                target,
                R::sign_extend(
                    AsPrimitive::<f32>::as_(self.get_f(source)).to_bits(),
                    32,
                ),
            ),
            Fmvws(target, source) => self.set_f(target, {
                f32::from_bits(self.get(source).as_()).as_()
            }),
            // Convert
            Fcvtws(target, source) => self.set(
                target,
                R::sign_extend(
                    AsPrimitive::<u32>::as_(AsPrimitive::<R>::as_(self.get_f(source))),
                    32,
                ),
            ),
            Fcvtsw(target, source) => self.set_f(
                target,
                AsPrimitive::<F>::as_(AsPrimitive::<R>::as_(AsPrimitive::<u32>::as_(
                    self.get(source),
                ))),
            ),
            // Load
            Flw(result, target, offset) => {
                let val = self.get(target) + R::from_immediate_low(offset);
                self.set_f(result, self.load_mem_f::<f32>(val));
            }
            // Stores
            Fsw(value, target, offset) => self.set_mem_f::<f32>(
                self.get(target) + R::from_immediate_low(offset),
                self.get_f(value).as_(),
            ),
            // Arithmetic
            Fadds(target, a, b) => self.set_f(
                target,
                AsPrimitive::<F>::as_(
                    AsPrimitive::<f32>::as_(self.get_f(a)) + AsPrimitive::<f32>::as_(self.get_f(b)),
                ),
            ),
            Fsubs(target, a, b) => self.set_f(
                target,
                AsPrimitive::<F>::as_(
                    AsPrimitive::<f32>::as_(self.get_f(a)) - AsPrimitive::<f32>::as_(self.get_f(b)),
                ),
            ),
            Fmuls(target, a, b) => self.set_f(
                target,
                AsPrimitive::<F>::as_(
                    AsPrimitive::<f32>::as_(self.get_f(a)) * AsPrimitive::<f32>::as_(self.get_f(b)),
                ),
            ),
            Fdivs(target, a, b) => self.set_f(
                target,
                AsPrimitive::<F>::as_(
                    AsPrimitive::<f32>::as_(self.get_f(a)) / AsPrimitive::<f32>::as_(self.get_f(b)),
                ),
            ),
            Fsqrts(target, a) => self.set_f(
                target,
                AsPrimitive::<F>::as_(AsPrimitive::<f32>::as_(self.get_f(a)).sqrt()),
            ),
            // Mul-Add
            Fmadds(target, a, b, c) => self.set_f(
                target,
                AsPrimitive::<F>::as_(AsPrimitive::<f32>::as_(self.get_f(a)).mul_add(
                    AsPrimitive::<f32>::as_(self.get_f(b)),
                    AsPrimitive::<f32>::as_(self.get_f(c)),
                )),
            ),
            Fmsubs(target, a, b, c) => self.set_f(
                target,
                AsPrimitive::<F>::as_(AsPrimitive::<f32>::as_(self.get_f(a)).mul_add(
                    AsPrimitive::<f32>::as_(self.get_f(b)),
                    AsPrimitive::<f32>::as_(self.get_f(c)),
                )),
            ),
            Fnmadds(target, a, b, c) => self.set_f(
                target,
                AsPrimitive::<F>::as_(-AsPrimitive::<f32>::as_(self.get_f(a)).mul_add(
                    AsPrimitive::<f32>::as_(self.get_f(b)),
                    AsPrimitive::<f32>::as_(self.get_f(c)),
                )),
            ),
            Fnmsubs(target, a, b, c) => self.set_f(
                target,
                AsPrimitive::<F>::as_(-AsPrimitive::<f32>::as_(self.get_f(a)).mul_add(
                    AsPrimitive::<f32>::as_(self.get_f(b)),
                    -AsPrimitive::<f32>::as_(self.get_f(c)),
                )),
            ),
            // Sign Inject
            Fsgnjs(target, a, b) => self.set_f(
                target,
                AsPrimitive::<F>::as_(
                    AsPrimitive::<f32>::as_(self.get_f(a)).abs()
                        * AsPrimitive::<f32>::as_(self.get_f(b)).signum(),
                ),
            ),
            Fsgnjns(target, a, b) => self.set_f(
                target,
                AsPrimitive::<F>::as_(
                    AsPrimitive::<f32>::as_(self.get_f(a)).abs()
                        * -AsPrimitive::<f32>::as_(self.get_f(b)).signum(),
                ),
            ),
            Fsgnjxs(target, a, b) => self.set_f(
                target,
                AsPrimitive::<F>::as_(
                    AsPrimitive::<f32>::as_(self.get_f(a))
                        * AsPrimitive::<f32>::as_(self.get_f(b)).signum(),
                ),
            ),
            // Min/Max
            Fmins(target, a, b) => self.set_f(
                target,
                AsPrimitive::<F>::as_(f32::min(
                    AsPrimitive::<f32>::as_(self.get_f(a)),
                    AsPrimitive::<f32>::as_(self.get_f(b)),
                )),
            ),
            Fmaxs(target, a, b) => self.set_f(
                target,
                AsPrimitive::<F>::as_(f32::max(
                    AsPrimitive::<f32>::as_(self.get_f(a)),
                    AsPrimitive::<f32>::as_(self.get_f(b)),
                )),
            ),
            // Compare
            Feqs(target, a, b) => self.set(
                target,
                AsPrimitive::<R>::as_(
                    AsPrimitive::<f32>::as_(self.get_f(a))
                        == AsPrimitive::<f32>::as_(self.get_f(b)),
                ),
            ),
            Flts(target, a, b) => self.set(
                target,
                AsPrimitive::<R>::as_(
                    AsPrimitive::<f32>::as_(self.get_f(a)) < AsPrimitive::<f32>::as_(self.get_f(b)),
                ),
            ),
            Fles(target, a, b) => self.set(
                target,
                AsPrimitive::<R>::as_(
                    AsPrimitive::<f32>::as_(self.get_f(a))
                        <= AsPrimitive::<f32>::as_(self.get_f(b)),
                ),
            ),
            // Categorization
            Fclasss(target, a) => {
                let a = AsPrimitive::<f32>::as_(self.get_f(a));
                let v: u32 = match (a.classify(), a.is_positive()) {
                    (FpCategory::Infinite, false) => 0b00_0000_0001,
                    (FpCategory::Normal, false) => 0b00_0000_0010,
                    (FpCategory::Subnormal, false) => 0b00_0000_0100,
                    (FpCategory::Zero, false) => 0b00_0000_1000,
                    (FpCategory::Zero, true) => 0b00_0001_0000,
                    (FpCategory::Subnormal, true) => 0b00_0010_0000,
                    (FpCategory::Normal, true) => 0b00_0100_0000,
                    (FpCategory::Infinite, true) => 0b00_1000_0000,
                    (FpCategory::Nan, _) => 0b10_0000_0000,
                    // No signaling NaN in Rust AFAIK
                };
                self.set(target, AsPrimitive::<R>::as_(v));
            } // Configuration
        }
    }
}
