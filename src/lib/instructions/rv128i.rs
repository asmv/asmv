#![warn(missing_docs)]

//! RV128I Base Integer Instruction Set
//!
//! This instruction set extends the base RV32I, providing instructions that operate on double-word
//! (64-bits) integer when working with larger registers. These word instructions operates on the
//! lower 64 bits of the integers, sign-extending all the other bits to [XLEN].
//!
//! [XLEN]: ../../trait.Register.html
//!

use crate::{Computer, ExtensionMult, FRegister, ImmediateLow, Register, RegisterIdx};

pub use Instruction::*;

use asmv_derive::ParsableInstruction;
use num_traits::{AsPrimitive, PrimInt, WrappingAdd, WrappingSub};

/// Instructions for the [RV128I](index.html) extension.
///
/// Contains various operations (memory manipulation, arithmetic, boolean) that operate on
/// double-word (64-bits) integers.
#[derive(Debug, Eq, PartialEq, Hash, Clone, Copy, ParsableInstruction)]
pub enum Instruction {
    // Load
    /// Load Quad-Word Instruction
    ///
    /// Loads a Quad-Word (128 bits, 16 bytes) value from memory to `rd`. The address in memory can
    /// be obtained by adding the value in `rs1` to the sign-extended `imm` offset.
    ///
    /// # Arguments
    ///
    /// * `rd` - Destination register specifier, where the result is loaded.
    /// * `rs1` - Base memory address, in bytes, to which is added the offset.
    /// * `imm` - Offset, in bytes, to add to `rs1` to obtain the memory address.
    ///
    /// # Examples
    ///
    /// ```
    /// # use asmv::{Computer, Register128Bit, NoMult, NoFloats, Instruction, RegisterIdx, ImmediateLow, ImmediateHigh, ExtensionMult, FRegister, Register};
    /// # use asmv::instructions::{self, Executable};
    /// let mut computer = Computer::<Register128Bit, NoMult, NoFloats>::new(20);
    /// # use std::marker::PhantomData;
    /// # use asmv::CodeIdx;
    /// # struct PubComputer<R: Register, M: ExtensionMult, F: FRegister> {
    /// #     extensions: PhantomData<M>, registers: [R; 32], f_registers: [F; 32], pc: CodeIdx<R>,
    /// #     memory: Box<[u8]>, instructions: Vec<Instruction<R>>,
    /// # }
    /// # let mut computer = unsafe {
    /// #     let mut computer: PubComputer::<Register128Bit, NoMult, NoFloats> = std::mem::transmute(computer);
    /// #     computer.registers[12] = 0x8F8E8D8C_8B8A8988_87868584_83828180;
    /// #     let mut computer: Computer::<Register128Bit, NoMult, NoFloats> = std::mem::transmute(computer);
    /// #     computer
    /// # };
    ///
    /// // 12x = 0x8F_8E_8D_8C__8B_8A_89_88__87_86_85_84__83_82_81_80
    /// computer.queue(Instruction::RV128I(instructions::rv128i::Sq(
    ///     // Stores 80, 81, 82, 83, 84, 85, 86, 87, 88, 89, 8A, 8B, 8C, 8D, 8E, 8F from 12x
    ///     RegisterIdx(12),
    ///     RegisterIdx(0),
    ///     ImmediateLow::new(0x000).unwrap(),
    /// )));
    /// computer.queue(Instruction::RV128I(instructions::rv128i::Lq(
    ///     // Loads 0x80_81_82_83_84_85_86_87_88_89_8A_8B_8C_8D_8E_8F into 10x
    ///     RegisterIdx(10),
    ///     RegisterIdx(0),
    ///     ImmediateLow::new(0x000).unwrap(),
    /// )));
    /// computer.run_all();
    ///
    /// assert_eq!(computer.get(RegisterIdx(10)), 0x80818283_84858687_88898A8B_8C8D8E8Fu128.to_be());
    /// ```
    ///
    #[instruction(store_load)]
    Lq(RegisterIdx, RegisterIdx, ImmediateLow),

    /// Load Word Unsigned Instruction
    ///
    /// Loads a Double-Word (64 bits, 8 bytes) value, zero-extended, from memory to `rd`. The
    /// address in memory can be obtained by adding the value in `rs1` to the sign-extended `imm` offset.
    ///
    /// # Arguments
    ///
    /// * `rd` - Destination register specifier, where the result is loaded.
    /// * `rs1` - Base memory address, in bytes, to which is added the offset.
    /// * `imm` - Offset, in bytes, to add to `rs1` to obtain the memory address.
    ///
    /// # Examples
    ///
    /// ```
    /// # use asmv::{Computer, Register128Bit, NoMult, NoFloats, Instruction, RegisterIdx, ImmediateLow, ImmediateHigh, ExtensionMult, FRegister, Register};
    /// # use asmv::instructions::{self, Executable};
    /// let mut computer = Computer::<Register128Bit, NoMult, NoFloats>::new(20);
    /// # use std::marker::PhantomData;
    /// # use asmv::CodeIdx;
    /// # struct PubComputer<R: Register, M: ExtensionMult, F: FRegister> {
    /// #     extensions: PhantomData<M>, registers: [R; 32], f_registers: [F; 32], pc: CodeIdx<R>,
    /// #     memory: Box<[u8]>, instructions: Vec<Instruction<R>>,
    /// # }
    /// # let mut computer = unsafe {
    /// #     let mut computer: PubComputer::<Register128Bit, NoMult, NoFloats> = std::mem::transmute(computer);
    /// #     computer.registers[12] = 0x8F8E8D8C_8B8A8988_87868584_83828180;
    /// #     let mut computer: Computer::<Register128Bit, NoMult, NoFloats> = std::mem::transmute(computer);
    /// #     computer
    /// # };
    ///
    /// // 12x = 0x8F_8E_8D_8C__8B_8A_89_88__87_86_85_84__83_82_81_80
    /// computer.queue(Instruction::RV128I(instructions::rv128i::Sq(
    ///     // Stores 80, 81, 82, 83, 84, 85, 86, 87, 88, 89, 8A, 8B, 8C, 8D, 8E, 8F from 12x
    ///     RegisterIdx(12),
    ///     RegisterIdx(0),
    ///     ImmediateLow::new(0x000).unwrap(),
    /// )));
    /// computer.queue(Instruction::RV128I(instructions::rv128i::Ldu(
    ///     // Loads 0x80_81_82_83_84_85_86_87 into 10x
    ///     RegisterIdx(10),
    ///     RegisterIdx(0),
    ///     ImmediateLow::new(0x000).unwrap(),
    /// )));
    /// computer.run_all();
    ///
    /// assert_eq!(computer.get(RegisterIdx(10)), 0x00000000_00000000_80818283_84858687u128.to_be());
    /// ```
    ///
    #[instruction(store_load)]
    Ldu(RegisterIdx, RegisterIdx, ImmediateLow),

    // Store
    /// Store Quad-Word Instruction
    ///
    /// Stores a Quad-Word (128 bits, 16 bytes) value to memory, value taken from `rs2`. The address
    /// in memory can be obtained by adding the value in `rs1` to the sign-extended `imm` offset.
    ///
    /// # Arguments
    ///
    /// * `rd` - Source register, of which the result is stored.
    /// * `rs1` - Base memory address, in bytes, to which is added the offset.
    /// * `imm` - Offset, in bytes, to add to `rs1` to obtain the memory address.
    ///
    /// # Examples
    ///
    /// ```
    /// # use asmv::{Computer, Register128Bit, NoMult, NoFloats, Instruction, RegisterIdx, ImmediateLow, ImmediateHigh, ExtensionMult, FRegister, Register};
    /// # use asmv::instructions::{self, Executable};
    /// let mut computer = Computer::<Register128Bit, NoMult, NoFloats>::new(20);
    /// # use std::marker::PhantomData;
    /// # use asmv::CodeIdx;
    /// # struct PubComputer<R: Register, M: ExtensionMult, F: FRegister> {
    /// #     extensions: PhantomData<M>, registers: [R; 32], f_registers: [F; 32], pc: CodeIdx<R>,
    /// #     memory: Box<[u8]>, instructions: Vec<Instruction<R>>,
    /// # }
    /// # let mut computer = unsafe {
    /// #     let mut computer: PubComputer::<Register128Bit, NoMult, NoFloats> = std::mem::transmute(computer);
    /// #     computer.registers[12] = 0x8F8E8D8C_8B8A8988_87868584_83828180;
    /// #     let mut computer: Computer::<Register128Bit, NoMult, NoFloats> = std::mem::transmute(computer);
    /// #     computer
    /// # };
    ///
    /// // 12x = 0x8F_8E_8D_8C__8B_8A_89_88__87_86_85_84__83_82_81_80
    /// computer.queue(Instruction::RV128I(instructions::rv128i::Sq(
    ///     // Stores 80, 81, 82, 83, 84, 85, 86, 87, 88, 89, 8A, 8B, 8C, 8D, 8E, 8F from 12x
    ///     RegisterIdx(12),
    ///     RegisterIdx(0),
    ///     ImmediateLow::new(0x000).unwrap(),
    /// )));
    /// computer.queue(Instruction::RV128I(instructions::rv128i::Lq(
    ///     // Loads 0x80_81_82_83_84_85_86_87_88_89_8A_8B_8C_8D_8E_8F into 10x
    ///     RegisterIdx(10),
    ///     RegisterIdx(0),
    ///     ImmediateLow::new(0x000).unwrap(),
    /// )));
    /// computer.run_all();
    ///
    /// assert_eq!(computer.get(RegisterIdx(10)), 0x80818283_84858687_88898A8B_8C8D8E8Fu128.to_be());
    /// ```
    ///
    #[instruction(store_load)]
    Sq(RegisterIdx, RegisterIdx, ImmediateLow),

    // Shift
    /// Double-Word Logical Left Shift Instruction
    ///
    /// Instruction that performs the bitwise left shift operation of the lower 64 bits of `rs1` by
    /// the number of bits specified in the lower 7 bits of `rs2`. The result is sign-extended and
    /// written in `rd`.
    ///
    /// # Arguments
    ///
    /// * `rd` - Destination register specifier, where the result is stored.
    /// * `rs1` - 1st source register specifier, first operand.
    /// * `rs2` - 2nd source register specifier, shift amount.
    ///
    /// # Examples
    ///
    /// ```
    /// # use asmv::{Computer, Register128Bit, NoMult, NoFloats, Instruction, RegisterIdx, ImmediateLow};
    /// # use asmv::instructions::{self, Executable};
    /// let mut computer = Computer::<Register128Bit, NoMult, NoFloats>::new(0);
    /// # computer.queue(Instruction::Base(instructions::base::Addi(
    /// #     // 12x = 18
    /// #     RegisterIdx(12),
    /// #     RegisterIdx(0),
    /// #     ImmediateLow::new(0x012).unwrap(),
    /// # )));
    /// # computer.queue(Instruction::Base(instructions::base::Addi(
    /// #     // 13x = 2
    /// #     RegisterIdx(13),
    /// #     RegisterIdx(0),
    /// #     ImmediateLow::new(0x002).unwrap(),
    /// # )));
    ///
    /// computer.queue(Instruction::RV128I(instructions::rv128i::Slld(
    ///     RegisterIdx(10), // rd: 18 << 2
    ///     RegisterIdx(12), // rs1: 18
    ///     RegisterIdx(13), // rs2: 2
    /// )));
    /// computer.run_all();
    ///
    /// assert_eq!(computer.get(RegisterIdx(10)), 72u32.into());
    /// ```
    ///
    #[instruction(registers)]
    Slld(RegisterIdx, RegisterIdx, RegisterIdx),

    /// Double-Word Immediate Logical Left Shift Instruction
    ///
    /// Instruction that performs the bitwise left shift operation of the lower 64 bits of `rs1` by
    /// the number of bits specified in `imm`. The result is sign-extended and written in `rd`.
    ///
    /// # Arguments
    ///
    /// * `rd` - Destination register specifier, where the result is stored.
    /// * `rs1` - 1st source register specifier, first operand.
    /// * `imm` - Immediate 12-bit integer value, shift amount.
    ///
    /// # Examples
    ///
    /// ```
    /// # use asmv::{Computer, Register128Bit, NoMult, NoFloats, Instruction, RegisterIdx, ImmediateLow};
    /// # use asmv::instructions::{self, Executable};
    /// let mut computer = Computer::<Register128Bit, NoMult, NoFloats>::new(0);
    /// # computer.queue(Instruction::Base(instructions::base::Addi(
    /// #     // 12x = 18
    /// #     RegisterIdx(12),
    /// #     RegisterIdx(0),
    /// #     ImmediateLow::new(0x012).unwrap(),
    /// # )));
    ///
    /// computer.queue(Instruction::RV128I(instructions::rv128i::Sllid(
    ///     RegisterIdx(10), // rd: 18 << 2
    ///     RegisterIdx(12), // rs1: 18
    ///     ImmediateLow::new(0x002).unwrap(), // imm: 2
    /// )));
    /// computer.run_all();
    ///
    /// assert_eq!(computer.get(RegisterIdx(10)), 72u32.into());
    /// ```
    ///
    #[instruction(immediate)]
    Sllid(RegisterIdx, RegisterIdx, ImmediateLow),

    /// Double-Word Logical Right Shift Instruction
    ///
    /// Instruction that performs the bitwise logical right shift operation of the lower 64 bits of
    /// `rs1` by the number of bits specified in the lower 7 bits of `rs2`, 0-filled on the left.
    /// The result is sign-extended and written in `rd`.
    ///
    /// # Arguments
    ///
    /// * `rd` - Destination register specifier, where the result is stored.
    /// * `rs1` - 1st source register specifier, first operand.
    /// * `rs2` - 2nd source register specifier, shift amount.
    ///
    /// # Examples
    ///
    /// ```
    /// # use asmv::{Computer, Register128Bit, NoMult, NoFloats, Instruction, RegisterIdx, ImmediateLow};
    /// # use asmv::instructions::{self, Executable};
    /// let mut computer = Computer::<Register128Bit, NoMult, NoFloats>::new(0);
    /// # computer.queue(Instruction::Base(instructions::base::Addi(
    /// #     // 12x = 18
    /// #     RegisterIdx(12),
    /// #     RegisterIdx(0),
    /// #     ImmediateLow::new(0x012).unwrap(),
    /// # )));
    /// # computer.queue(Instruction::Base(instructions::base::Addi(
    /// #     // 13x = 2
    /// #     RegisterIdx(13),
    /// #     RegisterIdx(0),
    /// #     ImmediateLow::new(0x002).unwrap(),
    /// # )));
    ///
    /// computer.queue(Instruction::RV128I(instructions::rv128i::Srld(
    ///     RegisterIdx(10), // rd: 18 >> 2
    ///     RegisterIdx(12), // rs1: 18
    ///     RegisterIdx(13), // rs2: 2
    /// )));
    /// computer.run_all();
    ///
    /// assert_eq!(computer.get(RegisterIdx(10)), 4u32.into());
    /// ```
    ///
    #[instruction(registers)]
    Srld(RegisterIdx, RegisterIdx, RegisterIdx),

    /// Double-Word Immediate Logical Right Shift Instruction
    ///
    /// Instruction that performs the bitwise logical right shift operation of the lower 64 bits of
    /// `rs1` by the number of bits specified in the lower 7 bits of `imm`, 0-filled on the left.
    /// The result is sign-extended and written in `rd`.
    ///
    /// # Arguments
    ///
    /// * `rd` - Destination register specifier, where the result is stored.
    /// * `rs1` - 1st source register specifier, first operand.
    /// * `imm` - Immediate 12-bit integer value, the shift amount.
    ///
    /// # Examples
    ///
    /// ```
    /// # use asmv::{Computer, Register128Bit, NoMult, NoFloats, Instruction, RegisterIdx, ImmediateLow};
    /// # use asmv::instructions::{self, Executable};
    /// let mut computer = Computer::<Register128Bit, NoMult, NoFloats>::new(0);
    /// # computer.queue(Instruction::Base(instructions::base::Addi(
    /// #     // 12x = 18
    /// #     RegisterIdx(12),
    /// #     RegisterIdx(0),
    /// #     ImmediateLow::new(0x012).unwrap(),
    /// # )));
    /// # computer.queue(Instruction::Base(instructions::base::Addi(
    /// #     // 13x = 2
    /// #     RegisterIdx(13),
    /// #     RegisterIdx(0),
    /// #     ImmediateLow::new(0x002).unwrap(),
    /// # )));
    ///
    /// computer.queue(Instruction::RV128I(instructions::rv128i::Srlid(
    ///     RegisterIdx(10), // rd: 18 >> 2
    ///     RegisterIdx(12), // rs1: 18
    ///     ImmediateLow::new(0x002).unwrap(), // imm: 2
    /// )));
    /// computer.run_all();
    ///
    /// assert_eq!(computer.get(RegisterIdx(10)), 4u32.into());
    /// ```
    ///
    #[instruction(immediate)]
    Srlid(RegisterIdx, RegisterIdx, ImmediateLow),

    /// Double-Word Arithmetic Right Shift Instruction
    ///
    /// Instruction that performs the bitwise arithmetic (also called algebraic) right shift
    /// operation of the lower 64 bits of `rs1` by the number of bits specified in the lower 7 bits
    /// of `rs2`, copying the sign on the left. The result is sign-extended and written in `rd`.
    ///
    /// # Arguments
    ///
    /// * `rd` - Destination register specifier, where the result is stored.
    /// * `rs1` - 1st source register specifier, first operand.
    /// * `rs2` - 2nd source register specifier, shift amount.
    ///
    /// # Examples
    ///
    /// ```
    /// # use asmv::{Computer, Register128Bit, NoMult, NoFloats, Instruction, RegisterIdx, ImmediateLow};
    /// # use asmv::instructions::{self, Executable};
    /// let mut computer = Computer::<Register128Bit, NoMult, NoFloats>::new(0);
    /// # computer.queue(Instruction::Base(instructions::base::Addi(
    /// #     // 12x = 18
    /// #     RegisterIdx(12),
    /// #     RegisterIdx(0),
    /// #     ImmediateLow::new(0x012).unwrap(),
    /// # )));
    /// # computer.queue(Instruction::Base(instructions::base::Addi(
    /// #     // 13x = 2
    /// #     RegisterIdx(13),
    /// #     RegisterIdx(0),
    /// #     ImmediateLow::new(0x002).unwrap(),
    /// # )));
    ///
    /// computer.queue(Instruction::RV128I(instructions::rv128i::Srad(
    ///     RegisterIdx(10), // rd: 18 >> 2
    ///     RegisterIdx(12), // rs1: 18
    ///     RegisterIdx(13), // rs2: 2
    /// )));
    /// computer.run_all();
    ///
    /// assert_eq!(computer.get(RegisterIdx(10)), 4u32.into());
    /// ```
    ///
    #[instruction(registers)]
    Srad(RegisterIdx, RegisterIdx, RegisterIdx),

    /// Double-Word Immediate Arithmetic Right Shift Instruction
    ///
    /// Instruction that performs the bitwise arithmetic (also called algebraic) right shift
    /// operation of the lower 64 bits of `rs1` by the number of bits specified in the lower 7 bits
    /// of `imm`, copying the sign on the left. The result is sign-extended and written in `rd`.
    ///
    /// # Arguments
    ///
    /// * `rd` - Destination register specifier, where the result is stored.
    /// * `rs1` - 1st source register specifier, first operand.
    /// * `imm` - Immediate 12-bit integer value, the shift amount.
    ///
    /// # Examples
    ///
    /// ```
    /// # use asmv::{Computer, Register128Bit, NoMult, NoFloats, Instruction, RegisterIdx, ImmediateLow};
    /// # use asmv::instructions::{self, Executable};
    /// let mut computer = Computer::<Register128Bit, NoMult, NoFloats>::new(0);
    /// # computer.queue(Instruction::Base(instructions::base::Addi(
    /// #     // 12x = 18
    /// #     RegisterIdx(12),
    /// #     RegisterIdx(0),
    /// #     ImmediateLow::new(0x012).unwrap(),
    /// # )));
    ///
    /// computer.queue(Instruction::RV128I(instructions::rv128i::Sraid(
    ///     RegisterIdx(10), // rd: 18 >> 2
    ///     RegisterIdx(12), // rs1: 18
    ///     ImmediateLow::new(0x002).unwrap(), // imm: 2
    /// )));
    /// computer.run_all();
    ///
    /// assert_eq!(computer.get(RegisterIdx(10)), 4u32.into());
    /// ```
    ///
    #[instruction(immediate)]
    Sraid(RegisterIdx, RegisterIdx, ImmediateLow),

    // Arithmetic
    /// Double-Word Addition Instruction
    ///
    /// Instruction that performs the addition of the lower 64 bits of `rs1` and `rs2`, wrapping
    /// around the overflow. The result is sign-extended and written in `rd`.
    ///
    /// # Arguments
    ///
    /// * `rd` - Destination register specifier, where the result is stored.
    /// * `rs1` - 1st source register specifier, first operand.
    /// * `rs2` - 2nd source register specifier, second operand.
    ///
    /// # Examples
    ///
    /// ```
    /// # use asmv::{Computer, Register128Bit, NoMult, NoFloats, Instruction, RegisterIdx, ImmediateLow};
    /// # use asmv::instructions::{self, Executable};
    /// let mut computer = Computer::<Register128Bit, NoMult, NoFloats>::new(0);
    /// # computer.queue(Instruction::Base(instructions::base::Addi(
    /// #     // 12x = 18
    /// #     RegisterIdx(12),
    /// #     RegisterIdx(0),
    /// #     ImmediateLow::new(0x012).unwrap(),
    /// # )));
    /// # computer.queue(Instruction::Base(instructions::base::Addi(
    /// #     // 13x = 5
    /// #     RegisterIdx(13),
    /// #     RegisterIdx(0),
    /// #     ImmediateLow::new(0x005).unwrap(),
    /// # )));
    ///
    /// computer.queue(Instruction::RV128I(instructions::rv128i::Addd(
    ///     RegisterIdx(10), // rd: 18 + 5
    ///     RegisterIdx(12), // rs1: 18
    ///     RegisterIdx(13), // rs2: 5
    /// )));
    /// computer.run_all();
    ///
    /// assert_eq!(computer.get(RegisterIdx(10)), 23u32.into());
    /// ```
    ///
    #[instruction(registers)]
    Addd(RegisterIdx, RegisterIdx, RegisterIdx),

    /// Double-Word Immediate Addition Instruction
    ///
    /// Instruction that performs the addition of the lower 64 bits of `rs1` and the sign-extended
    /// 12-bits immediate integer `imm`. The overflow is ignored, and the sign-extended result is
    /// written in `rd`.
    ///
    /// # Arguments
    ///
    /// * `rd` - Destination register specifier, where the result is stored.
    /// * `rs1` - 1st source register specifier, first operand.
    /// * `imm` - Immediate 12-bit integer value, the second operand.
    ///
    /// # Examples
    ///
    /// ```
    /// # use asmv::{Computer, Register128Bit, NoMult, NoFloats, Instruction, RegisterIdx, ImmediateLow};
    /// # use asmv::instructions::{self, Executable};
    /// let mut computer = Computer::<Register128Bit, NoMult, NoFloats>::new(0);
    /// # computer.queue(Instruction::Base(instructions::base::Addi(
    /// #     // 12x = 18
    /// #     RegisterIdx(12),
    /// #     RegisterIdx(0),
    /// #     ImmediateLow::new(0x012).unwrap(),
    /// # )));
    ///
    /// computer.queue(Instruction::RV128I(instructions::rv128i::Addid(
    ///     RegisterIdx(10), // rd: 18 + 5
    ///     RegisterIdx(12), // rs1: 18
    ///     ImmediateLow::new(0x005).unwrap(), // imm: 5
    /// )));
    /// computer.run_all();
    ///
    /// assert_eq!(computer.get(RegisterIdx(10)), 23u32.into());
    /// ```
    ///
    #[instruction(immediate)]
    Addid(RegisterIdx, RegisterIdx, ImmediateLow),

    /// Double-Word Subtraction Instruction
    ///
    /// Instruction that performs the subtraction of the lower 64 bits of `rs1` by the corresponding
    /// bits of `rs2`, ignoring the overflow. The sign-extended result is written in `rd`.
    ///
    /// # Arguments
    ///
    /// * `rd` - Destination register specifier, where the result is stored.
    /// * `rs1` - 1st source register specifier, first operand.
    /// * `rs2` - 2nd source register specifier, second operand.
    ///
    /// # Examples
    ///
    /// ```
    /// # use asmv::{Computer, Register128Bit, NoMult, NoFloats, Instruction, RegisterIdx, ImmediateLow};
    /// # use asmv::instructions::{self, Executable};
    /// let mut computer = Computer::<Register128Bit, NoMult, NoFloats>::new(0);
    /// # computer.queue(Instruction::Base(instructions::base::Addi(
    /// #     // 12x = 18
    /// #     RegisterIdx(12),
    /// #     RegisterIdx(0),
    /// #     ImmediateLow::new(0x012).unwrap(),
    /// # )));
    /// # computer.queue(Instruction::Base(instructions::base::Addi(
    /// #     // 13x = 5
    /// #     RegisterIdx(13),
    /// #     RegisterIdx(0),
    /// #     ImmediateLow::new(0x005).unwrap(),
    /// # )));
    ///
    /// computer.queue(Instruction::RV128I(instructions::rv128i::Subd(
    ///     RegisterIdx(10), // rd: 18 - 5
    ///     RegisterIdx(12), // rs1: 18
    ///     RegisterIdx(13), // rs2: 5
    /// )));
    /// computer.run_all();
    ///
    /// assert_eq!(computer.get(RegisterIdx(10)), 13u32.into());
    /// ```
    ///
    #[instruction(registers)]
    Subd(RegisterIdx, RegisterIdx, RegisterIdx),
}

impl<R, M, F> Computer<R, M, F>
where
    F: FRegister,
    M: ExtensionMult,
    R: AsPrimitive<u32>,
    R: AsPrimitive<u64>,
    R: From<u64>,
    R: Register,
{
    /// Execute an instruction
    ///
    /// Panics if there is no more instructions to execute. Use at_end
    /// to avoid it
    pub fn execute_rv128i(&mut self, instruction: Instruction) {
        use Instruction::*;
        match instruction {
            // Load
            Lq(result, target, offset) => {
                let val = self.get(target) + R::from_immediate_low(offset);
                self.set(result, self.load_mem(16, val, false));
            }
            Ldu(result, target, offset) => {
                let val = self.get(target) + R::from_immediate_low(offset);
                self.set(result, self.load_mem(8, val, false));
            }
            // Stores
            Sq(value, target, offset) => self.set_mem(
                16,
                self.get(target) + R::from_immediate_low(offset),
                self.get(value),
            ),
            // Shifts
            Slld(result, a, b) => self.set(
                result,
                R::sign_extend::<u64>(
                    PrimInt::unsigned_shl(
                        AsPrimitive::<u64>::as_(self.get(a)),
                        AsPrimitive::<_>::as_(self.get(b)),
                    ),
                    64, // Sign extend 64its to 128bits
                ),
            ),
            Sllid(result, a, b) => self.set(
                result,
                R::sign_extend::<u64>(
                    PrimInt::signed_shl(
                        AsPrimitive::<u64>::as_(self.get(a)),
                        AsPrimitive::<_>::as_(R::from_immediate_low(b)),
                    ),
                    64, // Sign extend 64its to 128bits
                ),
            ),
            Srld(result, a, b) => self.set(
                result,
                PrimInt::unsigned_shr(
                    AsPrimitive::<u64>::as_(self.get(a)),
                    AsPrimitive::<_>::as_(self.get(b)),
                )
                .into(),
            ),
            Srlid(result, a, b) => self.set(
                result,
                PrimInt::unsigned_shr(
                    AsPrimitive::<u64>::as_(self.get(a)),
                    AsPrimitive::<_>::as_(R::from_immediate_low(b)),
                )
                .into(),
            ),
            Srad(result, a, b) => self.set(
                result,
                R::sign_extend::<u64>(
                    PrimInt::signed_shr(
                        AsPrimitive::<u64>::as_(self.get(a)),
                        AsPrimitive::<_>::as_(self.get(b)),
                    ),
                    64, // Sign extend 64its to 128bits
                ),
            ),
            Sraid(result, a, b) => self.set(
                result,
                R::sign_extend::<u64>(
                    PrimInt::signed_shr(
                        AsPrimitive::<u64>::as_(self.get(a)),
                        AsPrimitive::<_>::as_(R::from_immediate_low(b)),
                    ),
                    64, // Sign extend 64its to 128bits
                ),
            ),
            // Arithmetic
            Addd(result, a, b) => self.set(
                result,
                R::sign_extend::<u64>(
                    WrappingAdd::wrapping_add(
                        &AsPrimitive::<u64>::as_(self.get(a)),
                        &AsPrimitive::<u64>::as_(self.get(b)),
                    ),
                    64, // Sign extend 64its to 128bits
                ),
            ),
            Addid(result, a, b) => self.set(
                result,
                R::sign_extend::<u64>(
                    WrappingAdd::wrapping_add(
                        &AsPrimitive::<u64>::as_(self.get(a)),
                        &AsPrimitive::<u64>::as_(R::from_immediate_low(b)),
                    ),
                    64, // Sign extend 64its to 128bits
                ),
            ),
            Subd(result, a, b) => self.set(
                result,
                R::sign_extend::<u64>(
                    WrappingSub::wrapping_sub(
                        &AsPrimitive::<u64>::as_(self.get(a)),
                        &AsPrimitive::<u64>::as_(self.get(b)),
                    ),
                    64, // Sign extend 64its to 128bits
                ),
            ),
        }
    }
}
