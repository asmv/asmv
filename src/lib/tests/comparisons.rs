use super::extend;
use crate::{Computer, ImmediateLow, Instruction, RegisterIdx};

test_all! { comparison_basic, || {
    let mut computer = Computer::<R, M, F>::new(0);
    computer.queue(Instruction::Base(base::Addi(
        RegisterIdx(1),
        RegisterIdx(0),
        ImmediateLow::new(2).unwrap(),
    )));
    computer.queue(Instruction::Base(base::Addi(
        RegisterIdx(2),
        RegisterIdx(0),
        ImmediateLow::new(1).unwrap(),
    )));
    computer.queue(Instruction::Base(base::Slt(
        RegisterIdx(3),
        RegisterIdx(1),
        RegisterIdx(1),
    )));
    computer.queue(Instruction::Base(base::Slt(
        RegisterIdx(4),
        RegisterIdx(1),
        RegisterIdx(2),
    )));
    computer.queue(Instruction::Base(base::Slt(
        RegisterIdx(5),
        RegisterIdx(2),
        RegisterIdx(1),
    )));
    computer.run_all();
    assert_eq!(computer.get(RegisterIdx(3)), extend(0));
    assert_eq!(computer.get(RegisterIdx(4)), extend(0));
    assert_eq!(computer.get(RegisterIdx(5)), extend(1));
}}

test_all! { comparison_immediate, || {
    let mut computer = Computer::<R, M, F>::new(0);
    computer.queue(Instruction::Base(base::Addi(
        RegisterIdx(1),
        RegisterIdx(0),
        ImmediateLow::new(2).unwrap(),
    )));
    computer.queue(Instruction::Base(base::Slti(
        RegisterIdx(3),
        RegisterIdx(1),
        ImmediateLow::new(2).unwrap(),
    )));
    computer.queue(Instruction::Base(base::Slti(
        RegisterIdx(4),
        RegisterIdx(1),
        ImmediateLow::new(1).unwrap(),
    )));
    computer.queue(Instruction::Base(base::Slti(
        RegisterIdx(5),
        RegisterIdx(2),
        ImmediateLow::new(3).unwrap(),
    )));
    computer.run_all();
    assert_eq!(computer.get(RegisterIdx(3)), extend(0));
    assert_eq!(computer.get(RegisterIdx(4)), extend(0));
    assert_eq!(computer.get(RegisterIdx(5)), extend(1));
}}

test_all! { comparison_signed, || {
    let mut computer = Computer::<R, M, F>::new(0);
    computer.queue(Instruction::Base(base::Addi(
        RegisterIdx(1),
        RegisterIdx(0),
        ImmediateLow::new(2).unwrap(),
    )));
    computer.queue(Instruction::Base(base::Addi(
        RegisterIdx(2),
        RegisterIdx(0),
        ImmediateLow::new(0xFFE).unwrap(),
    )));
    computer.queue(Instruction::Base(base::Slt(
        RegisterIdx(3),
        RegisterIdx(1),
        RegisterIdx(1),
    )));
    computer.queue(Instruction::Base(base::Slt(
        RegisterIdx(4),
        RegisterIdx(1),
        RegisterIdx(2),
    )));
    computer.queue(Instruction::Base(base::Slt(
        RegisterIdx(5),
        RegisterIdx(2),
        RegisterIdx(1),
    )));
    computer.run_all();
    assert_eq!(computer.get(RegisterIdx(3)), extend(0));
    assert_eq!(computer.get(RegisterIdx(4)), extend(0));
    assert_eq!(computer.get(RegisterIdx(5)), extend(1));
}}

test_all! { comparison_basic_unsigned, || {
    let mut computer = Computer::<R, M, F>::new(0);
    computer.queue(Instruction::Base(base::Addi(
        RegisterIdx(1),
        RegisterIdx(0),
        ImmediateLow::new(2).unwrap(),
    )));
    computer.queue(Instruction::Base(base::Addi(
        RegisterIdx(2),
        RegisterIdx(0),
        ImmediateLow::new(1).unwrap(),
    )));
    computer.queue(Instruction::Base(base::Sltu(
        RegisterIdx(3),
        RegisterIdx(1),
        RegisterIdx(1),
    )));
    computer.queue(Instruction::Base(base::Sltu(
        RegisterIdx(4),
        RegisterIdx(1),
        RegisterIdx(2),
    )));
    computer.queue(Instruction::Base(base::Sltu(
        RegisterIdx(5),
        RegisterIdx(2),
        RegisterIdx(1),
    )));
    computer.run_all();
    assert_eq!(computer.get(RegisterIdx(3)), extend(0));
    assert_eq!(computer.get(RegisterIdx(4)), extend(0));
    assert_eq!(computer.get(RegisterIdx(5)), extend(1));
}}

test_all! { comparison_immediate_unsigned, || {
    let mut computer = Computer::<R, M, F>::new(0);
    computer.queue(Instruction::Base(base::Addi(
        RegisterIdx(1),
        RegisterIdx(0),
        ImmediateLow::new(2).unwrap(),
    )));
    computer.queue(Instruction::Base(base::Sltui(
        RegisterIdx(3),
        RegisterIdx(1),
        ImmediateLow::new(2).unwrap(),
    )));
    computer.queue(Instruction::Base(base::Sltui(
        RegisterIdx(4),
        RegisterIdx(1),
        ImmediateLow::new(1).unwrap(),
    )));
    computer.queue(Instruction::Base(base::Sltui(
        RegisterIdx(5),
        RegisterIdx(2),
        ImmediateLow::new(3).unwrap(),
    )));
    computer.run_all();
    assert_eq!(computer.get(RegisterIdx(3)), extend(0));
    assert_eq!(computer.get(RegisterIdx(4)), extend(0));
    assert_eq!(computer.get(RegisterIdx(5)), extend(1));
}}

test_all! { comparison_unsigned, || {
    let mut computer = Computer::<R, M, F>::new(0);
    computer.queue(Instruction::Base(base::Addi(
        RegisterIdx(1),
        RegisterIdx(0),
        ImmediateLow::new(2).unwrap(),
    )));
    computer.queue(Instruction::Base(base::Addi(
        RegisterIdx(2),
        RegisterIdx(0),
        ImmediateLow::new(0xFFE).unwrap(),
    )));
    computer.queue(Instruction::Base(base::Sltu(
        RegisterIdx(3),
        RegisterIdx(1),
        RegisterIdx(1),
    )));
    computer.queue(Instruction::Base(base::Sltu(
        RegisterIdx(4),
        RegisterIdx(1),
        RegisterIdx(2),
    )));
    computer.queue(Instruction::Base(base::Sltu(
        RegisterIdx(5),
        RegisterIdx(2),
        RegisterIdx(1),
    )));
    computer.run_all();
    assert_eq!(computer.get(RegisterIdx(3)), extend(0));
    assert_eq!(computer.get(RegisterIdx(4)), extend(1));
    assert_eq!(computer.get(RegisterIdx(5)), extend(0));
}}
