use super::extend;
use crate::{Computer, ImmediateHigh, ImmediateLow, Instruction, RegisterIdx};

test_all! { pc, || {
    let mut computer = Computer::<R, M, F>::new(0);

    // Increment pc with dumb operations
    computer.queue(Instruction::Base(base::Addi(
        RegisterIdx(0),
        RegisterIdx(0),
        ImmediateLow::new(0x2).unwrap(),
    )));
    computer.queue(Instruction::Base(base::Addi(
        RegisterIdx(0),
        RegisterIdx(0),
        ImmediateLow::new(0x2).unwrap(),
    )));
    computer.queue(Instruction::Base(base::Addi(
        RegisterIdx(0),
        RegisterIdx(0),
        ImmediateLow::new(0x2).unwrap(),
    )));

    // Save position as is
    computer.queue(Instruction::Base(base::Auipc(
        RegisterIdx(3),
        ImmediateHigh::new(0).unwrap(),
    )));
    // Save position with immediate
    computer.queue(Instruction::Base(base::Auipc(
        RegisterIdx(1),
        ImmediateHigh::new(0x6).unwrap(),
    )));

    // Overflow immediate
    computer.queue(Instruction::Base(base::Lui(
        RegisterIdx(2),
        ImmediateHigh::new(0xFFFFA).unwrap(),
    )));
    computer.queue(Instruction::Base(base::Add(
        RegisterIdx(2),
        RegisterIdx(2),
        RegisterIdx(1),
    )));

    // Jump over next instruction
    computer.queue(Instruction::Base(base::Jalr(
        RegisterIdx(0),
        RegisterIdx(3),
        ImmediateLow::new(6 * 4).unwrap(),
    )));

    // Should be skipped
    computer.queue(Instruction::Base(base::Add(
        RegisterIdx(4),
        RegisterIdx(0),
        RegisterIdx(1),
    )));

    // Arrival
    computer.queue(Instruction::Base(base::Add(
        RegisterIdx(0),
        RegisterIdx(0),
        RegisterIdx(0),
    )));
    computer.run_all();
    assert_eq!(computer.get(RegisterIdx(1)), extend(0x0000_6014));
    assert_eq!(computer.get(RegisterIdx(2)), extend(0x0000_0014));
    assert_eq!(computer.get(RegisterIdx(3)), extend(0x0000_0010));
    assert_eq!(computer.get(RegisterIdx(4)), extend(0x0000_0000));
}}
