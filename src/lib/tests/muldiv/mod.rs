use crate::{Computer, ImmediateLow, Instruction, RegisterIdx};
// use num_traits::AsPrimitive;
use super::extend;

macro_rules! test_all_m {
    ($id:ident, $f:expr) => {
        paste::item! {
            fn [<$id _test>]<R: $crate::Register, M: $crate::ExtensionMult, F: $crate::FRegister>()
            where
                Computer<R, M, F>: $crate::instructions::Executable
            {
                #[allow(unused_imports)]
                use crate::{instructions::{base, rv32m, Executable}, Mult, NoMult};
                ($f)()
            }

            #[test]
            fn [<$id _rv32m_32>]() {
                [<$id _test>]::<$crate::Register32Bit, $crate::Mult, $crate::NoFloats>();
            }
            #[test]
            fn [<$id _rv32m_64>]() {
                [<$id _test>]::<$crate::Register64Bit, $crate::Mult, $crate::NoFloats>();
            }
            #[test]
            fn [<$id _rv32m_128>]() {
                [<$id _test>]::<$crate::Register128Bit, $crate::Mult, $crate::NoFloats>();
            }
        }
    };
}

macro_rules! test_rv64m {
    ($id:ident, $f:expr) => {
        paste::item! {
            fn [<$id _rv64m_test>]<R: $crate::Register+std::convert::From<u64>, M: $crate::ExtensionMult, F: $crate::FRegister>()
            where
                Computer<R, M, F>: $crate::instructions::Executable
            {
                #[allow(unused_imports)]
                use crate::{instructions::{base, rv64i, rv32m, rv64m, Executable}, Mult, NoMult};
                ($f)()
            }

            #[test]
            fn [<$id _rv64m_64>]() {
                [<$id _rv64m_test>]::<$crate::Register64Bit, $crate::Mult, $crate::NoFloats>();
            }
            #[test]
            fn [<$id _rv64m_128>]() {
                [<$id _rv64m_test>]::<$crate::Register128Bit, $crate::Mult, $crate::NoFloats>();
            }
        }
    };
}

macro_rules! test_rv128m {
    ($id:ident, $f:expr) => {
        paste::item! {
            fn [<$id _rv128m_test>]<R: $crate::Register+std::convert::From<u128>, M: $crate::ExtensionMult, F: $crate::FRegister>()
            where
                Computer<R, M, F>: $crate::instructions::Executable
            {
                #[allow(unused_imports)]
                use crate::{instructions::{base, rv64i, rv128i, rv32m, rv64m, rv128m, Executable}, Mult, NoMult};
                ($f)()
            }

            #[test]
            fn [<$id _rv128m_128>]() {
                [<$id _rv128m_test>]::<$crate::Register128Bit, $crate::Mult, $crate::NoFloats>();
            }
        }
    };
}

mod divisions;
mod multiplications;
mod remainders;

test_all_m! { compiles, || {
    let mut computer = Computer::<R, M, F>::new(0);
    computer.queue(Instruction::Base(base::Instruction::Addi(
        RegisterIdx(0),
        RegisterIdx(0),
        ImmediateLow::new(0).unwrap(),
    )));
    computer.run_all();
    assert_eq!(computer.get(RegisterIdx(0)), 0_u32.into());
}}

test_rv64m! { compiles, || {
    let mut computer = Computer::<R, M, F>::new(0);
    computer.queue(Instruction::Base(base::Instruction::Addi(
        RegisterIdx(0),
        RegisterIdx(0),
        ImmediateLow::new(0).unwrap(),
    )));
    computer.run_all();
    assert_eq!(computer.get(RegisterIdx(0)), 0_u32.into());
}}

test_rv128m! { compiles, || {
    let mut computer = Computer::<R, M, F>::new(0);
    computer.queue(Instruction::Base(base::Instruction::Addi(
        RegisterIdx(0),
        RegisterIdx(0),
        ImmediateLow::new(0).unwrap(),
    )));
    computer.run_all();
    assert_eq!(computer.get(RegisterIdx(0)), 0_u32.into());
}}
