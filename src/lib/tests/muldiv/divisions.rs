use super::extend;
use crate::{Computer, ImmediateLow, Instruction, RegisterIdx};

// Basic
test_all_m! { basic, || {
    let mut computer = Computer::new(0);
    computer.queue(Instruction::Base(base::Addi(
        // 10x = 18
        RegisterIdx(10),
        RegisterIdx(0),
        ImmediateLow::new(0x12).unwrap(),
    )));
    computer.queue(Instruction::Base(base::Addi(
        // 11x = 5
        RegisterIdx(11),
        RegisterIdx(0),
        ImmediateLow::new(0x5).unwrap(),
    )));
    computer.queue(Instruction::Base(base::Addi(
        // 12x = 6
        RegisterIdx(12),
        RegisterIdx(0),
        ImmediateLow::new(0x6).unwrap(),
    )));

    computer.queue(Instruction::RV32M(rv32m::Divu(
        // 1x = 18 / 5
        RegisterIdx(1),
        RegisterIdx(10),
        RegisterIdx(11),
    )));
    computer.queue(Instruction::RV32M(rv32m::Divu(
        // 2x = 18 / 6
        RegisterIdx(2),
        RegisterIdx(10),
        RegisterIdx(12),
    )));
    computer.queue(Instruction::RV32M(rv32m::Div(
        // 3x = 18 / 5
        RegisterIdx(3),
        RegisterIdx(10),
        RegisterIdx(11),
    )));
    computer.queue(Instruction::RV32M(rv32m::Div(
        // 4x = 18 / 6
        RegisterIdx(4),
        RegisterIdx(10),
        RegisterIdx(12),
    )));
    computer.run_all();

    assert_eq!(computer.get(RegisterIdx(1)), extend(3)); // DIVU 18, 5
    assert_eq!(computer.get(RegisterIdx(2)), extend(3)); // DIVU 18, 6
    assert_eq!(computer.get(RegisterIdx(3)), extend(3)); // DIV 18, 5
    assert_eq!(computer.get(RegisterIdx(4)), extend(3)); // DIV 18, 6
}}

// Basic
test_rv64m! { basic, || {
    let mut computer = Computer::new(0);
    computer.queue(Instruction::Base(base::Addi(
        // 10x = 18
        RegisterIdx(10),
        RegisterIdx(0),
        ImmediateLow::new(0x12).unwrap(),
    )));
    computer.queue(Instruction::Base(base::Addi(
        // 11x = 5
        RegisterIdx(11),
        RegisterIdx(0),
        ImmediateLow::new(0x5).unwrap(),
    )));
    computer.queue(Instruction::Base(base::Addi(
        // 12x = 6
        RegisterIdx(12),
        RegisterIdx(0),
        ImmediateLow::new(0x6).unwrap(),
    )));

    computer.queue(Instruction::RV64M(rv64m::Divuw(
        // 1x = 18 % 5
        RegisterIdx(1),
        RegisterIdx(10),
        RegisterIdx(11),
    )));
    computer.queue(Instruction::RV64M(rv64m::Divuw(
        // 2x = 18 % 6
        RegisterIdx(2),
        RegisterIdx(10),
        RegisterIdx(12),
    )));
    computer.queue(Instruction::RV64M(rv64m::Divw(
        // 3x = 18 % 5
        RegisterIdx(3),
        RegisterIdx(10),
        RegisterIdx(11),
    )));
    computer.queue(Instruction::RV64M(rv64m::Divw(
        // 4x = 18 % 6
        RegisterIdx(4),
        RegisterIdx(10),
        RegisterIdx(12),
    )));
    computer.run_all();

    assert_eq!(computer.get(RegisterIdx(1)), extend(3)); // DIVUW 18, 5
    assert_eq!(computer.get(RegisterIdx(2)), extend(3)); // DIVUW 18, 6
    assert_eq!(computer.get(RegisterIdx(3)), extend(3)); // DIVW 18, 5
    assert_eq!(computer.get(RegisterIdx(4)), extend(3)); // DIVW 18, 6
}}

// Basic
test_rv128m! { basic, || {
    let mut computer = Computer::new(0);
    computer.queue(Instruction::Base(base::Addi(
        // 10x = 18
        RegisterIdx(10),
        RegisterIdx(0),
        ImmediateLow::new(0x12).unwrap(),
    )));
    computer.queue(Instruction::Base(base::Addi(
        // 11x = 5
        RegisterIdx(11),
        RegisterIdx(0),
        ImmediateLow::new(0x5).unwrap(),
    )));
    computer.queue(Instruction::Base(base::Addi(
        // 12x = 6
        RegisterIdx(12),
        RegisterIdx(0),
        ImmediateLow::new(0x6).unwrap(),
    )));

    computer.queue(Instruction::RV128M(rv128m::Divud(
        // 1x = 18 % 5
        RegisterIdx(1),
        RegisterIdx(10),
        RegisterIdx(11),
    )));
    computer.queue(Instruction::RV128M(rv128m::Divud(
        // 2x = 18 % 6
        RegisterIdx(2),
        RegisterIdx(10),
        RegisterIdx(12),
    )));
    computer.queue(Instruction::RV128M(rv128m::Divd(
        // 3x = 18 % 5
        RegisterIdx(3),
        RegisterIdx(10),
        RegisterIdx(11),
    )));
    computer.queue(Instruction::RV128M(rv128m::Divd(
        // 4x = 18 % 6
        RegisterIdx(4),
        RegisterIdx(10),
        RegisterIdx(12),
    )));
    computer.run_all();

    assert_eq!(computer.get(RegisterIdx(1)), extend(3)); // DIVUD 18, 5
    assert_eq!(computer.get(RegisterIdx(2)), extend(3)); // DIVUD 18, 6
    assert_eq!(computer.get(RegisterIdx(3)), extend(3)); // DIVD 18, 5
    assert_eq!(computer.get(RegisterIdx(4)), extend(3)); // DIVD 18, 6
}}

// Signed
test_all_m! { signed, || {

    /*
    x10 = 0b...0000_1110 (14)
    x11 = 0b...1111_0010 (-14 | 242)
    x12 = 0b...0000_0011 (3)
    x13 = 0b...1111_1101 (-3 | 253)

    REM
    x01: 0000_1110 / 0000_0011 = 0000_0100  ::  14 / 3 = 4
    x02: 0000_1110 / 1111_1101 = 1111_1100  ::  14 / -3 = -4
    x03: 1111_0010 / 0000_0011 = 1111_1100  ::  -14 / 3 = -4
    x04: 1111_0010 / 1111_1101 = 0000_0100  ::  -14 / -3 = 4

    REMU
    x05: 0000_1110 / 0000_0011 = 0000_0100  ::  14 / 3 = 4
    x06: 0000_1110 / 1111_1101 = 0000_0000  ::  14 / 253 = 0
    x07: 1111_0010 / 0000_0011 = 0101_0000  ::  242 / 3 = ???
    x08: 1111_0010 / 1111_1101 = 0000_0000  ::  242 / 253 = 0
    */

    let mut computer = Computer::new(0);
    computer.queue(Instruction::Base(base::Addi(
        // 10x = ...0000_1110
        RegisterIdx(10),
        RegisterIdx(0),
        ImmediateLow::new(0xE).unwrap(),
    )));
    computer.queue(Instruction::Base(base::Sub(
        // 11x = ...1111_0010
        RegisterIdx(11),
        RegisterIdx(0),
        RegisterIdx(10),
    )));
    computer.queue(Instruction::Base(base::Addi(
        // 12x = ...0000_0011
        RegisterIdx(12),
        RegisterIdx(0),
        ImmediateLow::new(0x3).unwrap(),
    )));
    computer.queue(Instruction::Base(base::Sub(
        // 13x = ...1111_1101
        RegisterIdx(13),
        RegisterIdx(0),
        RegisterIdx(12),
    )));

    computer.queue(Instruction::RV32M(rv32m::Div(
        // x01: 0000_1110 / 0000_0011 = 0000_0100  ::  14 / 9 = 5
        RegisterIdx(1),
        RegisterIdx(10),
        RegisterIdx(12),
    )));
    computer.queue(Instruction::RV32M(rv32m::Div(
        // x02: 0000_1110 / 1111_1101 = 1111_1100  ::  14 / -9 = 5
        RegisterIdx(2),
        RegisterIdx(10),
        RegisterIdx(13),
    )));
    computer.queue(Instruction::RV32M(rv32m::Div(
        // x03: 1111_0010 / 0000_0011 = 1111_1100  ::  -14 / 9 = -5
        RegisterIdx(3),
        RegisterIdx(11),
        RegisterIdx(12),
    )));
    computer.queue(Instruction::RV32M(rv32m::Div(
        // x04: 1111_0010 / 1111_1101 = 0000_0100  ::  -14 / -9 = -5
        RegisterIdx(4),
        RegisterIdx(11),
        RegisterIdx(13),
    )));
    computer.queue(Instruction::RV32M(rv32m::Divu(
        // x05: 0000_1110 / 0000_0011 = 0000_0100  ::  14 / 9 = 5
        RegisterIdx(5),
        RegisterIdx(10),
        RegisterIdx(12),
    )));
    computer.queue(Instruction::RV32M(rv32m::Divu(
        // x06: 0000_1110 / 1111_1101 = 0000_1110  ::  14 / 247 = 14
        RegisterIdx(6),
        RegisterIdx(10),
        RegisterIdx(13),
    )));
    computer.queue(Instruction::RV32M(rv32m::Divu(
        // x07: 1111_0010 / 0000_0011 = 0000_1000  ::  242 / 9 = depends
        RegisterIdx(7),
        RegisterIdx(11),
        RegisterIdx(12),
    )));
    computer.queue(Instruction::RV32M(rv32m::Divu(
        // x08: 1111_0010 / 1111_1101 = 1111_0010  ::  242 / 247 = 242
        RegisterIdx(8),
        RegisterIdx(11),
        RegisterIdx(13),
    )));
    computer.run_all();

    println!("x10: {:0128b}", computer.registers[10]);
    println!("x11: {:0128b}", computer.registers[11]);
    println!("x12: {:0128b}", computer.registers[12]);
    println!("x13: {:0128b}", computer.registers[13]);
    println!();
    println!("x01: {:0128b}", computer.registers[1]);
    println!("x02: {:0128b}", computer.registers[2]);
    println!("x03: {:0128b}", computer.registers[3]);
    println!("x04: {:0128b}", computer.registers[4]);
    println!("x05: {:0128b}", computer.registers[5]);
    println!("x06: {:0128b}", computer.registers[6]);
    println!("x07: {:0128b}", computer.registers[7]);
    println!("x08: {:0128b}", computer.registers[8]);

    // Result depends on the length of the registers
    let lhs_idx7 = format!("{:0128b}", computer.get(RegisterIdx(7)));
    let rhs_idx7 = match std::mem::size_of::<R>() {
        4 => format!("{:0128b}", ((-14_i32) as u32) / 3),
        8 => format!("{:0128b}", ((-14_i64) as u64) / 3),
        16 => format!("{:0128b}", ((-14_i64) as u128) / 3),
        _ => panic!(),
    };

    assert_eq!(computer.get(RegisterIdx(1)), extend(4));
    assert_eq!(computer.get(RegisterIdx(2)), extend(-4 as _));
    assert_eq!(computer.get(RegisterIdx(3)), extend(-4 as _));
    assert_eq!(computer.get(RegisterIdx(4)), extend(4));
    assert_eq!(computer.get(RegisterIdx(5)), extend(4));
    assert_eq!(computer.get(RegisterIdx(6)), extend(0));
    assert_eq!(lhs_idx7, rhs_idx7);
    assert_eq!(computer.get(RegisterIdx(8)), extend(0));
}}

// Signed
test_rv64m! { signed, || {

    let mut computer = Computer::new(0);
    computer.queue(Instruction::Base(base::Addi(
        // 10x = ...0000_1110
        RegisterIdx(20),
        RegisterIdx(0),
        ImmediateLow::new(0xE).unwrap(),
    )));
    computer.queue(Instruction::Base(base::Sub(
        // 11x = ...1111_1101
        RegisterIdx(21),
        RegisterIdx(0),
        RegisterIdx(20),
    )));
    computer.registers[22] = 0x00000000_FFFFFFF2u64.into();
    computer.queue(Instruction::Base(base::Addi(
        // 12x = ...0000_0011
        RegisterIdx(23),
        RegisterIdx(0),
        ImmediateLow::new(0x3).unwrap(),
    )));
    computer.queue(Instruction::Base(base::Sub(
        // 13x = ...1111_1101
        RegisterIdx(24),
        RegisterIdx(0),
        RegisterIdx(23),
    )));
    computer.registers[25] = 0x00000000_FFFFFFFDu64.into();

    for rd in 1..10 {
        let rs1 = ((rd - 1) / 3) + 20;
        let rs2 = ((rd - 1) % 3) + 23;
        computer.queue(Instruction::RV64M(rv64m::Divw(
            RegisterIdx(rd),
            RegisterIdx(rs1),
            RegisterIdx(rs2),
        )));
        computer.queue(Instruction::RV64M(rv64m::Divuw(
            RegisterIdx(rd + 9),
            RegisterIdx(rs1),
            RegisterIdx(rs2),
        )));
    }
    computer.run_all();

    println!("x20: {:0128b}", computer.registers[20]);
    println!("x21: {:0128b}", computer.registers[21]);
    println!("x22: {:0128b}", computer.registers[22]);
    println!("x23: {:0128b}", computer.registers[23]);
    println!("x24: {:0128b}", computer.registers[24]);
    println!("x25: {:0128b}", computer.registers[25]);
    println!();
    println!("x01: {:0128b}", computer.registers[1]);
    println!("x02: {:0128b}", computer.registers[2]);
    println!("x03: {:0128b}", computer.registers[3]);
    println!("x04: {:0128b}", computer.registers[4]);
    println!("x05: {:0128b}", computer.registers[5]);
    println!("x06: {:0128b}", computer.registers[6]);
    println!("x07: {:0128b}", computer.registers[7]);
    println!("x08: {:0128b}", computer.registers[8]);
    println!("x09: {:0128b}", computer.registers[9]);
    println!("x10: {:0128b}", computer.registers[10]);
    println!("x11: {:0128b}", computer.registers[11]);
    println!("x12: {:0128b}", computer.registers[12]);
    println!("x13: {:0128b}", computer.registers[13]);
    println!("x14: {:0128b}", computer.registers[14]);
    println!("x15: {:0128b}", computer.registers[15]);
    println!("x16: {:0128b}", computer.registers[16]);
    println!("x17: {:0128b}", computer.registers[17]);
    println!("x18: {:0128b}", computer.registers[18]);

    let lhs_idx13 = format!("{:0128b}", computer.get(RegisterIdx(13)));
    let rhs_idx13 = format!("{:0128b}", ((-14_i32) as u32) / 3);
    let lhs_idx16 = format!("{:0128b}", computer.get(RegisterIdx(16)));
    let rhs_idx16 = format!("{:0128b}", ((-14_i32) as u32) / 3);
    println!();
    println!("lhs: {}", lhs_idx13);
    println!("rhs: {}", rhs_idx13);
    println!("lhs: {}", lhs_idx16);
    println!("rhs: {}", rhs_idx16);

    assert_eq!(computer.get(RegisterIdx(1)), extend(4));
    assert_eq!(computer.get(RegisterIdx(2)), extend(-4i32 as u32));
    // assert_eq!(computer.get(RegisterIdx(3)), extend(-4i32 as _));
    // assert_eq!(computer.get(RegisterIdx(4)), extend(-4i32 as _));
    assert_eq!(computer.get(RegisterIdx(5)), extend(4));
    assert_eq!(computer.get(RegisterIdx(6)), extend(4));
    // assert_eq!(computer.get(RegisterIdx(7)), extend(-4i32 as _));
    assert_eq!(computer.get(RegisterIdx(8)), extend(4));
    assert_eq!(computer.get(RegisterIdx(9)), extend(4));
    assert_eq!(computer.get(RegisterIdx(10)), extend(4));
    assert_eq!(computer.get(RegisterIdx(11)), extend(0));
    assert_eq!(computer.get(RegisterIdx(12)), extend(0));
    assert_eq!(lhs_idx13, rhs_idx13);
    assert_eq!(computer.get(RegisterIdx(14)), extend(0));
    assert_eq!(computer.get(RegisterIdx(15)), extend(0));
    assert_eq!(lhs_idx16, rhs_idx16);
    assert_eq!(computer.get(RegisterIdx(17)), extend(0));
    assert_eq!(computer.get(RegisterIdx(18)), extend(0));
}}

// Signed
test_rv128m! { signed, || {

    let mut computer = Computer::new(0);
    computer.queue(Instruction::Base(base::Addi(
        // 10x = ...0000_1110
        RegisterIdx(20),
        RegisterIdx(0),
        ImmediateLow::new(0xE).unwrap(),
    )));
    computer.queue(Instruction::Base(base::Sub(
        // 11x = ...1111_1101
        RegisterIdx(21),
        RegisterIdx(0),
        RegisterIdx(20),
    )));
    computer.registers[22] = 0x00000000_00000000_FFFFFFFF_FFFFFFF2u128.into();
    computer.queue(Instruction::Base(base::Addi(
        // 12x = ...0000_0011
        RegisterIdx(23),
        RegisterIdx(0),
        ImmediateLow::new(0x3).unwrap(),
    )));
    computer.queue(Instruction::Base(base::Sub(
        // 13x = ...1111_1101
        RegisterIdx(24),
        RegisterIdx(0),
        RegisterIdx(23),
    )));
    computer.registers[25] = 0x00000000_00000000_FFFFFFFF_FFFFFFFDu128.into();

    for rd in 1..10 {
        let rs1 = ((rd - 1) / 3) + 20;
        let rs2 = ((rd - 1) % 3) + 23;
        computer.queue(Instruction::RV128M(rv128m::Divd(
            RegisterIdx(rd),
            RegisterIdx(rs1),
            RegisterIdx(rs2),
        )));
        computer.queue(Instruction::RV128M(rv128m::Divud(
            RegisterIdx(rd + 9),
            RegisterIdx(rs1),
            RegisterIdx(rs2),
        )));
    }
    computer.run_all();

    println!("x20: {:0128b}", computer.registers[20]);
    println!("x21: {:0128b}", computer.registers[21]);
    println!("x22: {:0128b}", computer.registers[22]);
    println!("x23: {:0128b}", computer.registers[23]);
    println!("x24: {:0128b}", computer.registers[24]);
    println!("x25: {:0128b}", computer.registers[25]);
    println!();
    println!("x01: {:0128b}", computer.registers[1]);
    println!("x02: {:0128b}", computer.registers[2]);
    println!("x03: {:0128b}", computer.registers[3]);
    println!("x04: {:0128b}", computer.registers[4]);
    println!("x05: {:0128b}", computer.registers[5]);
    println!("x06: {:0128b}", computer.registers[6]);
    println!("x07: {:0128b}", computer.registers[7]);
    println!("x08: {:0128b}", computer.registers[8]);
    println!("x09: {:0128b}", computer.registers[9]);
    println!("x10: {:0128b}", computer.registers[10]);
    println!("x11: {:0128b}", computer.registers[11]);
    println!("x12: {:0128b}", computer.registers[12]);
    println!("x13: {:0128b}", computer.registers[13]);
    println!("x14: {:0128b}", computer.registers[14]);
    println!("x15: {:0128b}", computer.registers[15]);
    println!("x16: {:0128b}", computer.registers[16]);
    println!("x17: {:0128b}", computer.registers[17]);
    println!("x18: {:0128b}", computer.registers[18]);

    let lhs_idx13 = format!("{:0128b}", computer.get(RegisterIdx(13)));
    let rhs_idx13 = format!("{:0128b}", ((-14_i32) as u64) / 3);
    let lhs_idx16 = format!("{:0128b}", computer.get(RegisterIdx(16)));
    let rhs_idx16 = format!("{:0128b}", ((-14_i32) as u64) / 3);

    assert_eq!(computer.get(RegisterIdx(1)), extend(4));
    assert_eq!(computer.get(RegisterIdx(2)), extend(-4 as _));
    assert_eq!(computer.get(RegisterIdx(3)), extend(-4 as _));
    assert_eq!(computer.get(RegisterIdx(4)), extend(-4 as _));
    assert_eq!(computer.get(RegisterIdx(5)), extend(4));
    assert_eq!(computer.get(RegisterIdx(6)), extend(4));
    assert_eq!(computer.get(RegisterIdx(7)), extend(-4 as _));
    assert_eq!(computer.get(RegisterIdx(8)), extend(4));
    assert_eq!(computer.get(RegisterIdx(9)), extend(4));
    assert_eq!(computer.get(RegisterIdx(10)), extend(4));
    assert_eq!(computer.get(RegisterIdx(11)), extend(0));
    assert_eq!(computer.get(RegisterIdx(12)), extend(0));
    assert_eq!(lhs_idx13, rhs_idx13);
    assert_eq!(computer.get(RegisterIdx(14)), extend(0));
    assert_eq!(computer.get(RegisterIdx(15)), extend(0));
    assert_eq!(lhs_idx16, rhs_idx16);
    assert_eq!(computer.get(RegisterIdx(17)), extend(0));
    assert_eq!(computer.get(RegisterIdx(18)), extend(0));
}}

// By zero
test_all_m! { by_zero, || {
    let mut computer = Computer::new(0);
    computer.queue(Instruction::Base(base::Addi(
        // x10 = 17
        RegisterIdx(10),
        RegisterIdx(0),
        ImmediateLow::new(0x11).unwrap(),
    )));
    computer.queue(Instruction::Base(base::Sub(
        // x11 = -17
        RegisterIdx(11),
        RegisterIdx(0),
        RegisterIdx(10),
    )));

    computer.queue(Instruction::RV32M(rv32m::Div(
        // 1x = 17 / 0
        RegisterIdx(1),
        RegisterIdx(10),
        RegisterIdx(0),
    )));
    computer.queue(Instruction::RV32M(rv32m::Divu(
        // 2x = 17 / 0
        RegisterIdx(2),
        RegisterIdx(10),
        RegisterIdx(0),
    )));
    computer.queue(Instruction::RV32M(rv32m::Div(
        // 3x = -17 / 0
        RegisterIdx(3),
        RegisterIdx(11),
        RegisterIdx(0),
    )));
    computer.queue(Instruction::RV32M(rv32m::Divu(
        // 4x = ...1110_1111 / 0
        RegisterIdx(4),
        RegisterIdx(11),
        RegisterIdx(0),
    )));

    computer.run_all();

    println!("x10: {:0128b}", computer.registers[10]);
    println!("x11: {:0128b}", computer.registers[11]);
    println!();
    println!("x01: {:0128b}", computer.registers[1]);
    println!("x02: {:0128b}", computer.registers[2]);
    println!("x03: {:0128b}", computer.registers[3]);
    println!("x04: {:0128b}", computer.registers[4]);

    // All bits set
    assert_eq!(computer.get(RegisterIdx(1)), extend(-1 as _));
    assert_eq!(computer.get(RegisterIdx(2)), extend(-1 as _));
    assert_eq!(computer.get(RegisterIdx(3)), extend(-1 as _));
    assert_eq!(computer.get(RegisterIdx(4)), extend(-1 as _));
}}

// By zero
test_rv64m! { by_zero, || {
    let mut computer = Computer::new(0);
    computer.queue(Instruction::Base(base::Addi(
        // x10 = 17
        RegisterIdx(10),
        RegisterIdx(0),
        ImmediateLow::new(0x11).unwrap(),
    )));
    computer.queue(Instruction::Base(base::Sub(
        // x11 = -17
        RegisterIdx(11),
        RegisterIdx(0),
        RegisterIdx(10),
    )));
    // x12 = -17 as word
    computer.registers[12] = 0x00000000_FFFFFFEFu64.into();

    computer.queue(Instruction::RV64M(rv64m::Divw(
        // 1x = 17 / 0
        RegisterIdx(1),
        RegisterIdx(10),
        RegisterIdx(0),
    )));
    computer.queue(Instruction::RV64M(rv64m::Divw(
        // 2x = -17 / 0
        RegisterIdx(2),
        RegisterIdx(11),
        RegisterIdx(0),
    )));
    computer.queue(Instruction::RV64M(rv64m::Divw(
        // 3x = -17 / 0
        RegisterIdx(3),
        RegisterIdx(12),
        RegisterIdx(0),
    )));
    computer.queue(Instruction::RV64M(rv64m::Divuw(
        // 4x = 17 / 0
        RegisterIdx(4),
        RegisterIdx(10),
        RegisterIdx(0),
    )));
    computer.queue(Instruction::RV64M(rv64m::Divuw(
        // 5x = ...1110_1111 / 0
        RegisterIdx(5),
        RegisterIdx(11),
        RegisterIdx(0),
    )));
    computer.queue(Instruction::RV64M(rv64m::Divw(
        // 6x = ...1110_1111 / 0
        RegisterIdx(6),
        RegisterIdx(12),
        RegisterIdx(0),
    )));

    computer.run_all();

    println!("x10: {:0128b}", computer.registers[10]);
    println!("x11: {:0128b}", computer.registers[11]);
    println!("x12: {:0128b}", computer.registers[12]);
    println!();
    println!("x01: {:0128b}", computer.registers[1]);
    println!("x02: {:0128b}", computer.registers[2]);
    println!("x03: {:0128b}", computer.registers[3]);
    println!("x04: {:0128b}", computer.registers[4]);
    println!("x05: {:0128b}", computer.registers[5]);
    println!("x06: {:0128b}", computer.registers[6]);

    assert_eq!(computer.get(RegisterIdx(1)), extend(-1 as _));
    assert_eq!(computer.get(RegisterIdx(2)), extend(-1 as _));
    assert_eq!(computer.get(RegisterIdx(3)), extend(-1 as _));
    assert_eq!(computer.get(RegisterIdx(4)), extend(-1 as _));
    assert_eq!(computer.get(RegisterIdx(5)), extend(-1 as _));
    assert_eq!(computer.get(RegisterIdx(6)), extend(-1 as _));
}}

// By zero
test_rv128m! { by_zero, || {
    let mut computer = Computer::new(0);
    computer.queue(Instruction::Base(base::Addi(
        // x10 = 17
        RegisterIdx(10),
        RegisterIdx(0),
        ImmediateLow::new(0x11).unwrap(),
    )));
    computer.queue(Instruction::Base(base::Sub(
        // x11 = -17
        RegisterIdx(11),
        RegisterIdx(0),
        RegisterIdx(10),
    )));
    // x12 = -17 as double word
    computer.registers[12] = 0x00000000_00000000_FFFFFFFF_FFFFFFEFu128.into();

    computer.queue(Instruction::RV128M(rv128m::Divd(
        // 1x = 17 / 0
        RegisterIdx(1),
        RegisterIdx(10),
        RegisterIdx(0),
    )));
    computer.queue(Instruction::RV128M(rv128m::Divd(
        // 2x = -17 / 0
        RegisterIdx(2),
        RegisterIdx(11),
        RegisterIdx(0),
    )));
    computer.queue(Instruction::RV128M(rv128m::Divd(
        // 3x = -17 / 0
        RegisterIdx(3),
        RegisterIdx(12),
        RegisterIdx(0),
    )));
    computer.queue(Instruction::RV128M(rv128m::Divud(
        // 4x = 17 / 0
        RegisterIdx(4),
        RegisterIdx(10),
        RegisterIdx(0),
    )));
    computer.queue(Instruction::RV128M(rv128m::Divud(
        // 5x = ...1110_1111 / 0
        RegisterIdx(5),
        RegisterIdx(11),
        RegisterIdx(0),
    )));
    computer.queue(Instruction::RV128M(rv128m::Divd(
        // 6x = ...1110_1111 / 0
        RegisterIdx(6),
        RegisterIdx(12),
        RegisterIdx(0),
    )));

    computer.run_all();

    println!("x10: {:0128b}", computer.registers[10]);
    println!("x11: {:0128b}", computer.registers[11]);
    println!("x12: {:0128b}", computer.registers[12]);
    println!();
    println!("x01: {:0128b}", computer.registers[1]);
    println!("x02: {:0128b}", computer.registers[2]);
    println!("x03: {:0128b}", computer.registers[3]);
    println!("x04: {:0128b}", computer.registers[4]);
    println!("x05: {:0128b}", computer.registers[5]);
    println!("x06: {:0128b}", computer.registers[6]);

    assert_eq!(computer.get(RegisterIdx(1)), extend(-1 as _));
    assert_eq!(computer.get(RegisterIdx(2)), extend(-1 as _));
    assert_eq!(computer.get(RegisterIdx(3)), extend(-1 as _));
    assert_eq!(computer.get(RegisterIdx(4)), extend(-1 as _));
    assert_eq!(computer.get(RegisterIdx(5)), extend(-1 as _));
    assert_eq!(computer.get(RegisterIdx(6)), extend(-1 as _));
}}

// Overflow
test_all_m! { overflow, || {
    let mut computer = Computer::new(0);
    computer.queue(Instruction::Base(base::Addi(
        // 10x = 0 + 1
        RegisterIdx(10),
        RegisterIdx(0),
        ImmediateLow::new(0x1).unwrap(),
    )));
    let shift = (std::mem::size_of::<R>() * 8 - 1) as u16;
    computer.queue(Instruction::Base(base::Slli(
        // 10x = 0b_1000... (most negative value)
        RegisterIdx(10),
        RegisterIdx(10),
        ImmediateLow::new(shift).unwrap(),
    )));
    computer.queue(Instruction::Base(base::Addi(
        // 11x = 0 + 1
        RegisterIdx(11),
        RegisterIdx(0),
        ImmediateLow::new(0x1).unwrap(),
    )));
    computer.queue(Instruction::Base(base::Sub(
        // 11x = 0 - 1
        RegisterIdx(11),
        RegisterIdx(0),
        RegisterIdx(11),
    )));

    computer.queue(Instruction::RV32M(rv32m::Div(
        // 1x = 0b_1000... / 0b_1111...
        RegisterIdx(1),
        RegisterIdx(10),
        RegisterIdx(11),
    )));
    computer.run_all();

    println!("x10: {:0128b}", computer.registers[10]);
    println!("x11: {:0128b}", computer.registers[11]);
    println!("x01: {:0128b}", computer.registers[01]);

    assert_eq!(computer.get(RegisterIdx(1)), computer.get(RegisterIdx(10)));
}}

// Overflow
test_rv64m! { overflow, || {
    let mut computer = Computer::new(0);
    computer.queue(Instruction::Base(base::Addi(
        // 10x = 0 + 1
        RegisterIdx(10),
        RegisterIdx(0),
        ImmediateLow::new(0x1).unwrap(),
    )));
    computer.queue(Instruction::Base(base::Slli(
        // 10x = 0b_1000... (most negative value)
        RegisterIdx(10),
        RegisterIdx(10),
        ImmediateLow::new(31).unwrap(),
    )));
    // most negative sign extended
    computer.registers[11] = extend(0x8000_0000u32);
    computer.queue(Instruction::Base(base::Addi(
        // 12x = 0 + 1
        RegisterIdx(12),
        RegisterIdx(0),
        ImmediateLow::new(0x1).unwrap(),
    )));
    computer.queue(Instruction::Base(base::Sub(
        // 12x = -1
        RegisterIdx(12),
        RegisterIdx(0),
        RegisterIdx(12),
    )));

    computer.queue(Instruction::RV64M(rv64m::Divw(
        RegisterIdx(1),
        RegisterIdx(10),
        RegisterIdx(12),
    )));
    computer.queue(Instruction::RV64M(rv64m::Divw(
        RegisterIdx(2),
        RegisterIdx(11),
        RegisterIdx(12),
    )));
    computer.run_all();

    println!("x10: {:0128b}", computer.registers[10]);
    println!("x11: {:0128b}", computer.registers[11]);
    println!("x12: {:0128b}", computer.registers[12]);
    println!();
    println!("x01: {:0128b}", computer.registers[01]);
    println!("x02: {:0128b}", computer.registers[02]);

    assert_eq!(computer.get(RegisterIdx(1)), extend(std::i32::MIN as _));
    assert_eq!(computer.get(RegisterIdx(2)), extend(std::i32::MIN as _));
}}

// Overflow
test_rv128m! { overflow, || {
    let mut computer = Computer::new(0);
    computer.queue(Instruction::Base(base::Addi(
        // 10x = 0 + 1
        RegisterIdx(10),
        RegisterIdx(0),
        ImmediateLow::new(0x1).unwrap(),
    )));
    computer.queue(Instruction::Base(base::Slli(
        // 10x = 0b_1000... (most negative value)
        RegisterIdx(10),
        RegisterIdx(10),
        ImmediateLow::new(63).unwrap(),
    )));
    // most negative sign extended
    computer.registers[11] = 0xFFFFFFFF_FFFFFFFF_80000000_00000000u128.into();
    computer.queue(Instruction::Base(base::Addi(
        // 12x = 0 + 1
        RegisterIdx(12),
        RegisterIdx(0),
        ImmediateLow::new(0x1).unwrap(),
    )));
    computer.queue(Instruction::Base(base::Sub(
        // 12x = -1
        RegisterIdx(12),
        RegisterIdx(0),
        RegisterIdx(12),
    )));

    computer.queue(Instruction::RV128M(rv128m::Divd(
        RegisterIdx(1),
        RegisterIdx(10),
        RegisterIdx(12),
    )));
    computer.queue(Instruction::RV128M(rv128m::Divd(
        RegisterIdx(2),
        RegisterIdx(11),
        RegisterIdx(12),
    )));
    computer.run_all();

    println!("x10: {:0128b}", computer.registers[10]);
    println!("x11: {:0128b}", computer.registers[11]);
    println!("x12: {:0128b}", computer.registers[12]);
    println!();
    println!("x01: {:0128b}", computer.registers[01]);
    println!("x02: {:0128b}", computer.registers[02]);

    assert_eq!(computer.get(RegisterIdx(1)), 0xFFFFFFFF_FFFFFFFF_80000000_00000000u128.into());
    assert_eq!(computer.get(RegisterIdx(2)), 0xFFFFFFFF_FFFFFFFF_80000000_00000000u128.into());
}}

// Length
test_rv64m! { length, || {
    let mut computer = Computer::<R, M, F>::new(0);
    computer.queue(Instruction::Base(base::Addi(
        // 10x = 10
        RegisterIdx(10),
        RegisterIdx(0),
        ImmediateLow::new(0xA).unwrap(),
    )));
    computer.queue(Instruction::Base(base::Slli(
        // 10x = 10 << 32
        RegisterIdx(10),
        RegisterIdx(10),
        ImmediateLow::new(0x20).unwrap(),
    )));
    computer.queue(Instruction::Base(base::Addi(
        // 10x = 10x + 15
        RegisterIdx(10),
        RegisterIdx(10),
        ImmediateLow::new(0xF).unwrap(),
    )));
    computer.queue(Instruction::Base(base::Addi(
        // 11x = 5
        RegisterIdx(11),
        RegisterIdx(0),
        ImmediateLow::new(0x5).unwrap(),
    )));

    computer.queue(Instruction::RV32M(rv32m::Divu(
        // 1x = 0xA_0000000F / 5 = 0x2_00000003
        RegisterIdx(1),
        RegisterIdx(10),
        RegisterIdx(11),
    )));
    computer.queue(Instruction::RV64M(rv64m::Divuw(
        // 2x = 0xA_0000000F / 5 = 3
        RegisterIdx(2),
        RegisterIdx(10),
        RegisterIdx(11),
    )));
    computer.queue(Instruction::RV32M(rv32m::Div(
        // 3x = 0xA_0000000F / 5 = 0x2_00000003
        RegisterIdx(3),
        RegisterIdx(10),
        RegisterIdx(11),
    )));
    computer.queue(Instruction::RV64M(rv64m::Divw(
        // 4x = 0xA_0000000F / 5 = 3
        RegisterIdx(4),
        RegisterIdx(10),
        RegisterIdx(11),
    )));
    computer.run_all();

    println!("x10: {:032X}", computer.registers[10]);
    println!("x11: {:032X}", computer.registers[11]);
    println!();
    println!("x01: {:032X}", computer.registers[1]);
    println!("x02: {:032X}", computer.registers[2]);
    println!("x03: {:032X}", computer.registers[3]);
    println!("x04: {:032X}", computer.registers[4]);

    assert_eq!(computer.get(RegisterIdx(1)), 0x2_00000003u64.into()); // Divu
    assert_eq!(computer.get(RegisterIdx(2)), extend(3)); // Divuw
    assert_eq!(computer.get(RegisterIdx(3)), 0x2_00000003u64.into()); // Div
    assert_eq!(computer.get(RegisterIdx(4)), extend(3)); // Divw
}}

// Length
test_rv128m! { length, || {
    let mut computer = Computer::<R, M, F>::new(0);
    computer.queue(Instruction::Base(base::Addi(
        // 10x = 10
        RegisterIdx(10),
        RegisterIdx(0),
        ImmediateLow::new(0xA).unwrap(),
    )));
    computer.queue(Instruction::Base(base::Slli(
        // 10x = 10 << 64
        RegisterIdx(10),
        RegisterIdx(10),
        ImmediateLow::new(0x40).unwrap(),
    )));
    computer.queue(Instruction::Base(base::Addi(
        // 10x = 10x + 15
        RegisterIdx(10),
        RegisterIdx(10),
        ImmediateLow::new(0xF).unwrap(),
    )));
    computer.queue(Instruction::Base(base::Addi(
        // 11x = 5
        RegisterIdx(11),
        RegisterIdx(0),
        ImmediateLow::new(0x5).unwrap(),
    )));

    computer.queue(Instruction::RV32M(rv32m::Divu(
        // 1x = 0xA_00000000_0000000F / 5 = 0x2_00000000_00000003
        RegisterIdx(1),
        RegisterIdx(10),
        RegisterIdx(11),
    )));
    computer.queue(Instruction::RV128M(rv128m::Divud(
        // 2x = 0xA_00000000_0000000F / 5 = 3
        RegisterIdx(2),
        RegisterIdx(10),
        RegisterIdx(11),
    )));
    computer.queue(Instruction::RV32M(rv32m::Div(
        // 3x = 0xA_00000000_0000000F / 5 = 0x2_00000000_00000003
        RegisterIdx(3),
        RegisterIdx(10),
        RegisterIdx(11),
    )));
    computer.queue(Instruction::RV128M(rv128m::Divd(
        // 4x = 0xA_00000000_0000000F / 5 = 3
        RegisterIdx(4),
        RegisterIdx(10),
        RegisterIdx(11),
    )));
    computer.run_all();

    println!("x10: {:032X}", computer.registers[10]);
    println!("x11: {:032X}", computer.registers[11]);
    println!();
    println!("x01: {:032X}", computer.registers[1]);
    println!("x02: {:032X}", computer.registers[2]);
    println!("x03: {:032X}", computer.registers[3]);
    println!("x04: {:032X}", computer.registers[4]);

    assert_eq!(computer.get(RegisterIdx(1)), 0x2_00000000_00000003u128.into()); // Divu
    assert_eq!(computer.get(RegisterIdx(2)), extend(3)); // Divud
    assert_eq!(computer.get(RegisterIdx(3)), 0x2_00000000_00000003u128.into()); // Div
    assert_eq!(computer.get(RegisterIdx(4)), extend(3)); // Divd
}}
