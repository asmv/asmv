use super::extend;
use crate::{Computer, ImmediateLow, Instruction, RegisterIdx};

// Basic
test_all_m! { basic, || {
    let mut computer = Computer::new(0);
    computer.queue(Instruction::Base(base::Addi(
        // 10x = 18
        RegisterIdx(10),
        RegisterIdx(0),
        ImmediateLow::new(0x12).unwrap(),
    )));
    computer.queue(Instruction::Base(base::Addi(
        // 11x = 5
        RegisterIdx(11),
        RegisterIdx(0),
        ImmediateLow::new(0x5).unwrap(),
    )));
    computer.queue(Instruction::Base(base::Addi(
        // 12x = 6
        RegisterIdx(12),
        RegisterIdx(0),
        ImmediateLow::new(0x6).unwrap(),
    )));

    computer.queue(Instruction::RV32M(rv32m::Remu(
        // 1x = 18 % 5
        RegisterIdx(1),
        RegisterIdx(10),
        RegisterIdx(11),
    )));
    computer.queue(Instruction::RV32M(rv32m::Remu(
        // 2x = 18 % 6
        RegisterIdx(2),
        RegisterIdx(10),
        RegisterIdx(12),
    )));
    computer.queue(Instruction::RV32M(rv32m::Rem(
        // 3x = 18 % 5
        RegisterIdx(3),
        RegisterIdx(10),
        RegisterIdx(11),
    )));
    computer.queue(Instruction::RV32M(rv32m::Rem(
        // 4x = 18 % 6
        RegisterIdx(4),
        RegisterIdx(10),
        RegisterIdx(12),
    )));
    computer.run_all();

    assert_eq!(computer.get(RegisterIdx(1)), extend(3)); // REMU 18, 5
    assert_eq!(computer.get(RegisterIdx(2)), extend(0)); // REMU 18, 6
    assert_eq!(computer.get(RegisterIdx(3)), extend(3)); // REM 18, 5
    assert_eq!(computer.get(RegisterIdx(4)), extend(0)); // REM 18, 6
}}

// Basic
test_rv64m! { basic, || {
    let mut computer = Computer::new(0);
    computer.queue(Instruction::Base(base::Addi(
        // 10x = 18
        RegisterIdx(10),
        RegisterIdx(0),
        ImmediateLow::new(0x12).unwrap(),
    )));
    computer.queue(Instruction::Base(base::Addi(
        // 11x = 5
        RegisterIdx(11),
        RegisterIdx(0),
        ImmediateLow::new(0x5).unwrap(),
    )));
    computer.queue(Instruction::Base(base::Addi(
        // 12x = 6
        RegisterIdx(12),
        RegisterIdx(0),
        ImmediateLow::new(0x6).unwrap(),
    )));

    computer.queue(Instruction::RV64M(rv64m::Remuw(
        // 1x = 18 % 5
        RegisterIdx(1),
        RegisterIdx(10),
        RegisterIdx(11),
    )));
    computer.queue(Instruction::RV64M(rv64m::Remuw(
        // 2x = 18 % 6
        RegisterIdx(2),
        RegisterIdx(10),
        RegisterIdx(12),
    )));
    computer.queue(Instruction::RV64M(rv64m::Remw(
        // 3x = 18 % 5
        RegisterIdx(3),
        RegisterIdx(10),
        RegisterIdx(11),
    )));
    computer.queue(Instruction::RV64M(rv64m::Remw(
        // 4x = 18 % 6
        RegisterIdx(4),
        RegisterIdx(10),
        RegisterIdx(12),
    )));
    computer.run_all();

    assert_eq!(computer.get(RegisterIdx(1)), extend(3)); // REMUW 18, 5
    assert_eq!(computer.get(RegisterIdx(2)), extend(0)); // REMUW 18, 6
    assert_eq!(computer.get(RegisterIdx(3)), extend(3)); // REMW 18, 5
    assert_eq!(computer.get(RegisterIdx(4)), extend(0)); // REMW 18, 6
}}

// Basic
test_rv128m! { basic, || {
    let mut computer = Computer::new(0);
    computer.queue(Instruction::Base(base::Addi(
        // 10x = 18
        RegisterIdx(10),
        RegisterIdx(0),
        ImmediateLow::new(0x12).unwrap(),
    )));
    computer.queue(Instruction::Base(base::Addi(
        // 11x = 5
        RegisterIdx(11),
        RegisterIdx(0),
        ImmediateLow::new(0x5).unwrap(),
    )));
    computer.queue(Instruction::Base(base::Addi(
        // 12x = 6
        RegisterIdx(12),
        RegisterIdx(0),
        ImmediateLow::new(0x6).unwrap(),
    )));

    computer.queue(Instruction::RV128M(rv128m::Remud(
        // 1x = 18 % 5
        RegisterIdx(1),
        RegisterIdx(10),
        RegisterIdx(11),
    )));
    computer.queue(Instruction::RV128M(rv128m::Remud(
        // 2x = 18 % 6
        RegisterIdx(2),
        RegisterIdx(10),
        RegisterIdx(12),
    )));
    computer.queue(Instruction::RV128M(rv128m::Remd(
        // 3x = 18 % 5
        RegisterIdx(3),
        RegisterIdx(10),
        RegisterIdx(11),
    )));
    computer.queue(Instruction::RV128M(rv128m::Remd(
        // 4x = 18 % 6
        RegisterIdx(4),
        RegisterIdx(10),
        RegisterIdx(12),
    )));
    computer.run_all();

    assert_eq!(computer.get(RegisterIdx(1)), extend(3)); // REMUD 18, 5
    assert_eq!(computer.get(RegisterIdx(2)), extend(0)); // REMUD 18, 6
    assert_eq!(computer.get(RegisterIdx(3)), extend(3)); // REMD 18, 5
    assert_eq!(computer.get(RegisterIdx(4)), extend(0)); // REMD 18, 6
}}

// Signed
test_all_m! { signed, || {

    /*
    x10 = 0b...0000_1110 (14)
    x11 = 0b...1111_0010 (-14 | 242)
    x12 = 0b...0000_1001 (9)
    x13 = 0b...1111_0111 (-9 | 247)

    REM
    x01: 0000_1110 % 0000_1001 = 0000_0101  ::  14 % 9 = 5
    x02: 0000_1110 % 1111_0111 = 0000_0101  ::  14 % -9 = 5
    x03: 1111_0010 % 0000_1001 = 1111_1011  ::  -14 % 9 = -5
    x04: 1111_0010 % 1111_0111 = 1111_1011  ::  -14 % -9 = -5

    REMU
    x05: 0000_1110 % 0000_1001 = 0000_0101  ::  14 % 9 = 5
    x06: 0000_1110 % 1111_0111 = 0000_1110  ::  14 % 247 = 14
    x07: 1111_0010 % 0000_1001 = 0000_1000  ::  242 % 9 = 8
    x08: 1111_0010 % 1111_0111 = 1111_0010  ::  242 % 247 = 242
    */

    let mut computer = Computer::new(0);
    computer.queue(Instruction::Base(base::Addi(
        // 10x = ...0000_1110
        RegisterIdx(10),
        RegisterIdx(0),
        ImmediateLow::new(0xE).unwrap(),
    )));
    computer.queue(Instruction::Base(base::Sub(
        // 11x = ...1111_0010
        RegisterIdx(11),
        RegisterIdx(0),
        RegisterIdx(10),
    )));
    computer.queue(Instruction::Base(base::Addi(
        // 12x = ...0000_1001
        RegisterIdx(12),
        RegisterIdx(0),
        ImmediateLow::new(0x9).unwrap(),
    )));
    computer.queue(Instruction::Base(base::Sub(
        // 13x = ...1111_0111
        RegisterIdx(13),
        RegisterIdx(0),
        RegisterIdx(12),
    )));

    computer.queue(Instruction::RV32M(rv32m::Rem(
        // x01: 0000_1110 % 0000_1001 = 0000_0101  ::  14 % 9 = 5
        RegisterIdx(1),
        RegisterIdx(10),
        RegisterIdx(12),
    )));
    computer.queue(Instruction::RV32M(rv32m::Rem(
        // x02: 0000_1110 % 1111_0111 = 0000_0101  ::  14 % -9 = 5
        RegisterIdx(2),
        RegisterIdx(10),
        RegisterIdx(13),
    )));
    computer.queue(Instruction::RV32M(rv32m::Rem(
        // x03: 1111_0010 % 0000_1001 = 1111_1011  ::  -14 % 9 = -5
        RegisterIdx(3),
        RegisterIdx(11),
        RegisterIdx(12),
    )));
    computer.queue(Instruction::RV32M(rv32m::Rem(
        // x04: 1111_0010 % 1111_0111 = 1111_1011  ::  -14 % -9 = -5
        RegisterIdx(4),
        RegisterIdx(11),
        RegisterIdx(13),
    )));
    computer.queue(Instruction::RV32M(rv32m::Remu(
        // x05: 0000_1110 % 0000_1001 = 0000_0101  ::  14 % 9 = 5
        RegisterIdx(5),
        RegisterIdx(10),
        RegisterIdx(12),
    )));
    computer.queue(Instruction::RV32M(rv32m::Remu(
        // x06: 0000_1110 % 1111_0111 = 0000_1110  ::  14 % 247 = 14
        RegisterIdx(6),
        RegisterIdx(10),
        RegisterIdx(13),
    )));
    computer.queue(Instruction::RV32M(rv32m::Remu(
        // x07: 1111_0010 % 0000_1001 = 0000_1000  ::  242 % 9 = depends
        RegisterIdx(7),
        RegisterIdx(11),
        RegisterIdx(12),
    )));
    computer.queue(Instruction::RV32M(rv32m::Remu(
        // x08: 1111_0010 % 1111_0111 = 1111_0010  ::  242 % 247 = 242
        RegisterIdx(8),
        RegisterIdx(11),
        RegisterIdx(13),
    )));
    computer.run_all();

    println!("x10: {:0128b}", computer.registers[10]);
    println!("x11: {:0128b}", computer.registers[11]);
    println!("x12: {:0128b}", computer.registers[12]);
    println!("x13: {:0128b}", computer.registers[13]);
    println!();
    println!("x01: {:0128b}", computer.registers[1]);
    println!("x02: {:0128b}", computer.registers[2]);
    println!("x03: {:0128b}", computer.registers[3]);
    println!("x04: {:0128b}", computer.registers[4]);
    println!("x05: {:0128b}", computer.registers[5]);
    println!("x06: {:0128b}", computer.registers[6]);
    println!("x07: {:0128b}", computer.registers[7]);
    println!("x08: {:0128b}", computer.registers[8]);

    // Result depends on the length of the registers
    let rhs_idx7 = match std::mem::size_of::<R>() {
        4 | 16 => 8,
        8 => 2,
        _ => panic!(),
    };

    assert_eq!(computer.get(RegisterIdx(1)), extend(5));
    assert_eq!(computer.get(RegisterIdx(2)), extend(5));
    assert_eq!(computer.get(RegisterIdx(3)), extend(-5 as _));
    assert_eq!(computer.get(RegisterIdx(4)), extend(-5 as _));
    assert_eq!(computer.get(RegisterIdx(5)), extend(5));
    assert_eq!(computer.get(RegisterIdx(6)), extend(14));
    assert_eq!(computer.get(RegisterIdx(7)), extend(rhs_idx7));
    assert_eq!(computer.get(RegisterIdx(8)), extend(-14 as _));
}}

// Signed
test_rv64m! { signed, || {

    let mut computer = Computer::new(0);
    computer.queue(Instruction::Base(base::Addi(
        // 10x = ...0000_1110
        RegisterIdx(20),
        RegisterIdx(0),
        ImmediateLow::new(0xE).unwrap(),
    )));
    computer.queue(Instruction::Base(base::Sub(
        // 11x = ...1111_0010
        RegisterIdx(21),
        RegisterIdx(0),
        RegisterIdx(20),
    )));
    computer.registers[22] = 0x00000000_FFFFFFF2u64.into();
    computer.queue(Instruction::Base(base::Addi(
        // 12x = ...0000_1001
        RegisterIdx(23),
        RegisterIdx(0),
        ImmediateLow::new(0x9).unwrap(),
    )));
    computer.queue(Instruction::Base(base::Sub(
        // 13x = ...1111_0111
        RegisterIdx(24),
        RegisterIdx(0),
        RegisterIdx(23),
    )));
    computer.registers[25] = 0x00000000_FFFFFFF7u64.into();

    for rd in 1..10 {
        let rs1 = ((rd - 1) / 3) + 20;
        let rs2 = ((rd - 1) % 3) + 23;
        computer.queue(Instruction::RV64M(rv64m::Remw(
            RegisterIdx(rd),
            RegisterIdx(rs1),
            RegisterIdx(rs2),
        )));
        computer.queue(Instruction::RV64M(rv64m::Remuw(
            RegisterIdx(rd + 9),
            RegisterIdx(rs1),
            RegisterIdx(rs2),
        )));
    }
    computer.run_all();

    println!("x20: {:0128b}", computer.registers[20]);
    println!("x21: {:0128b}", computer.registers[21]);
    println!("x22: {:0128b}", computer.registers[22]);
    println!("x23: {:0128b}", computer.registers[23]);
    println!("x24: {:0128b}", computer.registers[24]);
    println!("x25: {:0128b}", computer.registers[25]);
    println!();
    println!("x01: {:0128b}", computer.registers[1]);
    println!("x02: {:0128b}", computer.registers[2]);
    println!("x03: {:0128b}", computer.registers[3]);
    println!("x04: {:0128b}", computer.registers[4]);
    println!("x05: {:0128b}", computer.registers[5]);
    println!("x06: {:0128b}", computer.registers[6]);
    println!("x07: {:0128b}", computer.registers[7]);
    println!("x08: {:0128b}", computer.registers[8]);
    println!("x09: {:0128b}", computer.registers[9]);
    println!("x10: {:0128b}", computer.registers[10]);
    println!("x11: {:0128b}", computer.registers[11]);
    println!("x12: {:0128b}", computer.registers[12]);
    println!("x13: {:0128b}", computer.registers[13]);
    println!("x14: {:0128b}", computer.registers[14]);
    println!("x15: {:0128b}", computer.registers[15]);
    println!("x16: {:0128b}", computer.registers[16]);
    println!("x17: {:0128b}", computer.registers[17]);
    println!("x18: {:0128b}", computer.registers[18]);

    assert_eq!(computer.get(RegisterIdx(1)), extend(5));
    assert_eq!(computer.get(RegisterIdx(2)), extend(5));
    assert_eq!(computer.get(RegisterIdx(3)), extend(5));
    assert_eq!(computer.get(RegisterIdx(4)), extend(-5 as _));
    assert_eq!(computer.get(RegisterIdx(5)), extend(-5 as _));
    assert_eq!(computer.get(RegisterIdx(6)), extend(-5 as _));
    assert_eq!(computer.get(RegisterIdx(7)), extend(-5 as _));
    assert_eq!(computer.get(RegisterIdx(8)), extend(-5 as _));
    assert_eq!(computer.get(RegisterIdx(9)), extend(-5 as _));
    assert_eq!(computer.get(RegisterIdx(10)), extend(5));
    assert_eq!(computer.get(RegisterIdx(11)), extend(14));
    assert_eq!(computer.get(RegisterIdx(12)), extend(14));
    assert_eq!(computer.get(RegisterIdx(13)), extend(8));
    assert_eq!(computer.get(RegisterIdx(14)), extend(-14 as _));
    assert_eq!(computer.get(RegisterIdx(15)), extend(-14 as _));
    assert_eq!(computer.get(RegisterIdx(16)), extend(8));
    assert_eq!(computer.get(RegisterIdx(17)), extend(-14 as _));
    assert_eq!(computer.get(RegisterIdx(18)), extend(-14 as _));
}}

// Signed
test_rv128m! { signed, || {

    let mut computer = Computer::new(0);
    computer.queue(Instruction::Base(base::Addi(
        // 10x = ...0000_1110
        RegisterIdx(20),
        RegisterIdx(0),
        ImmediateLow::new(0xE).unwrap(),
    )));
    computer.queue(Instruction::Base(base::Sub(
        // 11x = ...1111_0010
        RegisterIdx(21),
        RegisterIdx(0),
        RegisterIdx(20),
    )));
    computer.registers[22] = 0x00000000_00000000_FFFFFFFF_FFFFFFF2u128.into();
    computer.queue(Instruction::Base(base::Addi(
        // 12x = ...0000_1001
        RegisterIdx(23),
        RegisterIdx(0),
        ImmediateLow::new(0x9).unwrap(),
    )));
    computer.queue(Instruction::Base(base::Sub(
        // 13x = ...1111_0111
        RegisterIdx(24),
        RegisterIdx(0),
        RegisterIdx(23),
    )));
    computer.registers[25] = 0x00000000_00000000_FFFFFFFF_FFFFFFF7u128.into();

    for rd in 1..10 {
        let rs1 = ((rd - 1) / 3) + 20;
        let rs2 = ((rd - 1) % 3) + 23;
        computer.queue(Instruction::RV128M(rv128m::Remd(
            RegisterIdx(rd),
            RegisterIdx(rs1),
            RegisterIdx(rs2),
        )));
        computer.queue(Instruction::RV128M(rv128m::Remud(
            RegisterIdx(rd + 9),
            RegisterIdx(rs1),
            RegisterIdx(rs2),
        )));
    }
    computer.run_all();

    println!("x20: {:0128b}", computer.registers[20]);
    println!("x21: {:0128b}", computer.registers[21]);
    println!("x22: {:0128b}", computer.registers[22]);
    println!("x23: {:0128b}", computer.registers[23]);
    println!("x24: {:0128b}", computer.registers[24]);
    println!("x25: {:0128b}", computer.registers[25]);
    println!();
    println!("x01: {:0128b}", computer.registers[1]);
    println!("x02: {:0128b}", computer.registers[2]);
    println!("x03: {:0128b}", computer.registers[3]);
    println!("x04: {:0128b}", computer.registers[4]);
    println!("x05: {:0128b}", computer.registers[5]);
    println!("x06: {:0128b}", computer.registers[6]);
    println!("x07: {:0128b}", computer.registers[7]);
    println!("x08: {:0128b}", computer.registers[8]);
    println!("x09: {:0128b}", computer.registers[9]);
    println!("x10: {:0128b}", computer.registers[10]);
    println!("x11: {:0128b}", computer.registers[11]);
    println!("x12: {:0128b}", computer.registers[12]);
    println!("x13: {:0128b}", computer.registers[13]);
    println!("x14: {:0128b}", computer.registers[14]);
    println!("x15: {:0128b}", computer.registers[15]);
    println!("x16: {:0128b}", computer.registers[16]);
    println!("x17: {:0128b}", computer.registers[17]);
    println!("x18: {:0128b}", computer.registers[18]);

    assert_eq!(computer.get(RegisterIdx(1)), extend(5));
    assert_eq!(computer.get(RegisterIdx(2)), extend(5));
    assert_eq!(computer.get(RegisterIdx(3)), extend(5));
    assert_eq!(computer.get(RegisterIdx(4)), extend(-5 as _));
    assert_eq!(computer.get(RegisterIdx(5)), extend(-5 as _));
    assert_eq!(computer.get(RegisterIdx(6)), extend(-5 as _));
    assert_eq!(computer.get(RegisterIdx(7)), extend(-5 as _));
    assert_eq!(computer.get(RegisterIdx(8)), extend(-5 as _));
    assert_eq!(computer.get(RegisterIdx(9)), extend(-5 as _));
    assert_eq!(computer.get(RegisterIdx(10)), extend(5));
    assert_eq!(computer.get(RegisterIdx(11)), extend(14));
    assert_eq!(computer.get(RegisterIdx(12)), extend(14));
    assert_eq!(computer.get(RegisterIdx(13)), extend(2));
    assert_eq!(computer.get(RegisterIdx(14)), extend(-14 as _));
    assert_eq!(computer.get(RegisterIdx(15)), extend(-14 as _));
    assert_eq!(computer.get(RegisterIdx(16)), extend(2));
    assert_eq!(computer.get(RegisterIdx(17)), extend(-14 as _));
    assert_eq!(computer.get(RegisterIdx(18)), extend(-14 as _));
}}

// By zero
test_all_m! { by_zero, || {
    let mut computer = Computer::new(0);
    computer.queue(Instruction::Base(base::Addi(
        // x10 = 17
        RegisterIdx(10),
        RegisterIdx(0),
        ImmediateLow::new(0x11).unwrap(),
    )));
    computer.queue(Instruction::Base(base::Sub(
        // x11 = -17
        RegisterIdx(11),
        RegisterIdx(0),
        RegisterIdx(10),
    )));

    computer.queue(Instruction::RV32M(rv32m::Rem(
        // 1x = 17 % 0
        RegisterIdx(1),
        RegisterIdx(10),
        RegisterIdx(0),
    )));
    computer.queue(Instruction::RV32M(rv32m::Remu(
        // 2x = 17 % 0
        RegisterIdx(2),
        RegisterIdx(10),
        RegisterIdx(0),
    )));
    computer.queue(Instruction::RV32M(rv32m::Rem(
        // 3x = -17 % 0
        RegisterIdx(3),
        RegisterIdx(11),
        RegisterIdx(0),
    )));
    computer.queue(Instruction::RV32M(rv32m::Remu(
        // 4x = ...1110_1111 % 0
        RegisterIdx(4),
        RegisterIdx(11),
        RegisterIdx(0),
    )));

    computer.run_all();

    println!("x10: {:0128b}", computer.registers[10]);
    println!("x11: {:0128b}", computer.registers[11]);
    println!();
    println!("x01: {:0128b}", computer.registers[1]);
    println!("x02: {:0128b}", computer.registers[2]);
    println!("x03: {:0128b}", computer.registers[3]);
    println!("x04: {:0128b}", computer.registers[4]);

    // All bits set
    assert_eq!(computer.get(RegisterIdx(1)), extend(17));
    assert_eq!(computer.get(RegisterIdx(2)), extend(17));
    assert_eq!(computer.get(RegisterIdx(3)), extend(-17 as _));
    assert_eq!(computer.get(RegisterIdx(4)), extend(-17 as _));
}}

// By zero
test_rv64m! { by_zero, || {
    let mut computer = Computer::new(0);
    computer.queue(Instruction::Base(base::Addi(
        // x10 = 17
        RegisterIdx(10),
        RegisterIdx(0),
        ImmediateLow::new(0x11).unwrap(),
    )));
    computer.queue(Instruction::Base(base::Sub(
        // x11 = -17
        RegisterIdx(11),
        RegisterIdx(0),
        RegisterIdx(10),
    )));
    // x12 = -17 as word
    computer.registers[12] = 0x00000000_FFFFFFEFu64.into();

    computer.queue(Instruction::RV64M(rv64m::Remw(
        // 1x = 17 % 0
        RegisterIdx(1),
        RegisterIdx(10),
        RegisterIdx(0),
    )));
    computer.queue(Instruction::RV64M(rv64m::Remw(
        // 1x = 17 % 0
        RegisterIdx(2),
        RegisterIdx(11),
        RegisterIdx(0),
    )));
    computer.queue(Instruction::RV64M(rv64m::Remw(
        // 2x = 17 % 0
        RegisterIdx(3),
        RegisterIdx(12),
        RegisterIdx(0),
    )));
    computer.queue(Instruction::RV64M(rv64m::Remuw(
        // 3x = -17 % 0
        RegisterIdx(4),
        RegisterIdx(10),
        RegisterIdx(0),
    )));
    computer.queue(Instruction::RV64M(rv64m::Remuw(
        // 4x = ...1110_1111 % 0
        RegisterIdx(5),
        RegisterIdx(11),
        RegisterIdx(0),
    )));
    computer.queue(Instruction::RV64M(rv64m::Remuw(
        // 1x = 17 % 0
        RegisterIdx(6),
        RegisterIdx(12),
        RegisterIdx(0),
    )));

    computer.run_all();

    println!("x10: {:0128b}", computer.registers[10]);
    println!("x11: {:0128b}", computer.registers[11]);
    println!("x12: {:0128b}", computer.registers[12]);
    println!();
    println!("x01: {:0128b}", computer.registers[1]);
    println!("x02: {:0128b}", computer.registers[2]);
    println!("x03: {:0128b}", computer.registers[3]);
    println!("x04: {:0128b}", computer.registers[4]);
    println!("x05: {:0128b}", computer.registers[5]);
    println!("x06: {:0128b}", computer.registers[6]);

    assert_eq!(computer.get(RegisterIdx(1)), extend(17));
    assert_eq!(computer.get(RegisterIdx(2)), extend(-17 as _));
    assert_eq!(computer.get(RegisterIdx(3)), extend(-17 as _));
    assert_eq!(computer.get(RegisterIdx(4)), extend(17));
    assert_eq!(computer.get(RegisterIdx(5)), extend(-17 as _));
    assert_eq!(computer.get(RegisterIdx(6)), extend(-17 as _));
}}

// By zero
test_rv128m! { by_zero, || {
    let mut computer = Computer::new(0);
    computer.queue(Instruction::Base(base::Addi(
        // x10 = 17
        RegisterIdx(10),
        RegisterIdx(0),
        ImmediateLow::new(0x11).unwrap(),
    )));
    computer.queue(Instruction::Base(base::Sub(
        // x11 = -17
        RegisterIdx(11),
        RegisterIdx(0),
        RegisterIdx(10),
    )));
    // x12 = -17 as double word
    computer.registers[12] = 0x00000000_00000000_FFFFFFFF_FFFFFFEFu128.into();

    computer.queue(Instruction::RV128M(rv128m::Remd(
        // 1x = 17 % 0
        RegisterIdx(1),
        RegisterIdx(10),
        RegisterIdx(0),
    )));
    computer.queue(Instruction::RV128M(rv128m::Remd(
        // 2x = -17 % 0
        RegisterIdx(2),
        RegisterIdx(11),
        RegisterIdx(0),
    )));
    computer.queue(Instruction::RV128M(rv128m::Remd(
        // 3x = -17 % 0
        RegisterIdx(3),
        RegisterIdx(12),
        RegisterIdx(0),
    )));
    computer.queue(Instruction::RV128M(rv128m::Remud(
        // 4x = 17 % 0
        RegisterIdx(4),
        RegisterIdx(10),
        RegisterIdx(0),
    )));
    computer.queue(Instruction::RV128M(rv128m::Remud(
        // 5x = ...1110_1111 % 0
        RegisterIdx(5),
        RegisterIdx(11),
        RegisterIdx(0),
    )));
    computer.queue(Instruction::RV128M(rv128m::Remd(
        // 6x = ...1110_1111 % 0
        RegisterIdx(6),
        RegisterIdx(12),
        RegisterIdx(0),
    )));

    computer.run_all();

    println!("x10: {:0128b}", computer.registers[10]);
    println!("x11: {:0128b}", computer.registers[11]);
    println!("x12: {:0128b}", computer.registers[12]);
    println!();
    println!("x01: {:0128b}", computer.registers[1]);
    println!("x02: {:0128b}", computer.registers[2]);
    println!("x03: {:0128b}", computer.registers[3]);
    println!("x04: {:0128b}", computer.registers[4]);
    println!("x05: {:0128b}", computer.registers[5]);
    println!("x06: {:0128b}", computer.registers[6]);

    assert_eq!(computer.get(RegisterIdx(1)), extend(17));
    assert_eq!(computer.get(RegisterIdx(2)), extend(-17 as _));
    assert_eq!(computer.get(RegisterIdx(3)), extend(-17 as _));
    assert_eq!(computer.get(RegisterIdx(4)), extend(17));
    assert_eq!(computer.get(RegisterIdx(5)), extend(-17 as _));
    assert_eq!(computer.get(RegisterIdx(6)), extend(-17 as _));
}}

#[test]
fn overflow_rv32m_32() {
    use crate::{
        instructions::{base, rv32m, Executable},
        Mult, NoFloats, Register32Bit,
    };

    let mut computer = Computer::<Register32Bit, Mult, NoFloats>::new(0);
    computer.queue(Instruction::Base(base::Addi(
        // 10x = 0 + 1
        RegisterIdx(10),
        RegisterIdx(0),
        ImmediateLow::new(0x1).unwrap(),
    )));
    computer.queue(Instruction::Base(base::Slli(
        // 10x = 0b_1000... (most negative value)
        RegisterIdx(10),
        RegisterIdx(10),
        ImmediateLow::new(31).unwrap(),
    )));
    computer.queue(Instruction::Base(base::Addi(
        // 11x = 0 + 1
        RegisterIdx(11),
        RegisterIdx(0),
        ImmediateLow::new(0x1).unwrap(),
    )));
    computer.queue(Instruction::Base(base::Sub(
        // 11x = 0 - 1
        RegisterIdx(11),
        RegisterIdx(0),
        RegisterIdx(11),
    )));

    computer.queue(Instruction::RV32M(rv32m::Rem(
        // 1x = 0b_1000... % 0b_1111...
        RegisterIdx(1),
        RegisterIdx(10),
        RegisterIdx(11),
    )));
    computer.run_all();

    assert_eq!(computer.get(RegisterIdx(1)), extend(0));
}

#[test]
fn overflow_rv64m_64() {
    use crate::{
        instructions::{base, rv64m, Executable},
        Mult, NoFloats, Register64Bit,
    };

    let mut computer = Computer::<Register64Bit, Mult, NoFloats>::new(0);
    computer.queue(Instruction::Base(base::Addi(
        // 10x = 0 + 1
        RegisterIdx(10),
        RegisterIdx(0),
        ImmediateLow::new(0x1).unwrap(),
    )));
    computer.queue(Instruction::Base(base::Slli(
        // 10x = 0b_1000... (most negative value)
        RegisterIdx(10),
        RegisterIdx(10),
        ImmediateLow::new(31).unwrap(),
    )));
    computer.queue(Instruction::Base(base::Addi(
        // 11x = 0 + 1
        RegisterIdx(11),
        RegisterIdx(0),
        ImmediateLow::new(0x1).unwrap(),
    )));
    computer.queue(Instruction::Base(base::Sub(
        // 11x = 0 - 1
        RegisterIdx(11),
        RegisterIdx(0),
        RegisterIdx(11),
    )));

    computer.queue(Instruction::RV64M(rv64m::Remw(
        // 1x = 0b_1000... % 0b_1111...
        RegisterIdx(1),
        RegisterIdx(10),
        RegisterIdx(11),
    )));
    computer.run_all();

    assert_eq!(computer.get(RegisterIdx(1)), extend(0));
}

#[test]
fn overflow_rv64m_128() {
    use crate::{
        instructions::{base, rv64m, Executable},
        Mult, NoFloats, Register128Bit,
    };

    let mut computer = Computer::<Register128Bit, Mult, NoFloats>::new(0);
    computer.queue(Instruction::Base(base::Addi(
        // 10x = 0 + 1
        RegisterIdx(10),
        RegisterIdx(0),
        ImmediateLow::new(0x1).unwrap(),
    )));
    computer.queue(Instruction::Base(base::Slli(
        // 10x = 0b_1000... (most negative value)
        RegisterIdx(10),
        RegisterIdx(10),
        ImmediateLow::new(31).unwrap(),
    )));
    computer.queue(Instruction::Base(base::Addi(
        // 11x = 0 + 1
        RegisterIdx(11),
        RegisterIdx(0),
        ImmediateLow::new(0x1).unwrap(),
    )));
    computer.queue(Instruction::Base(base::Sub(
        // 11x = 0 - 1
        RegisterIdx(11),
        RegisterIdx(0),
        RegisterIdx(11),
    )));

    computer.queue(Instruction::RV64M(rv64m::Remw(
        // 1x = 0b_1000... % 0b_1111...
        RegisterIdx(1),
        RegisterIdx(10),
        RegisterIdx(11),
    )));
    computer.run_all();

    assert_eq!(computer.get(RegisterIdx(1)), extend(0));
}

#[test]
fn overflow_rv32m_64() {
    use crate::{
        instructions::{base, rv32m, Executable},
        Mult, NoFloats, Register64Bit,
    };

    let mut computer = Computer::<Register64Bit, Mult, NoFloats>::new(0);
    computer.queue(Instruction::Base(base::Addi(
        // 10x = 0 + 1
        RegisterIdx(10),
        RegisterIdx(0),
        ImmediateLow::new(0x1).unwrap(),
    )));
    computer.queue(Instruction::Base(base::Slli(
        // 10x = 0b_1000... (most negative value)
        RegisterIdx(10),
        RegisterIdx(10),
        ImmediateLow::new(63).unwrap(),
    )));
    computer.queue(Instruction::Base(base::Addi(
        // 11x = 0 + 1
        RegisterIdx(11),
        RegisterIdx(0),
        ImmediateLow::new(0x1).unwrap(),
    )));
    computer.queue(Instruction::Base(base::Sub(
        // 11x = 0 - 1
        RegisterIdx(11),
        RegisterIdx(0),
        RegisterIdx(11),
    )));

    computer.queue(Instruction::RV32M(rv32m::Rem(
        // 1x = 0b_1000... % 0b_1111...
        RegisterIdx(1),
        RegisterIdx(10),
        RegisterIdx(11),
    )));
    computer.run_all();

    assert_eq!(computer.get(RegisterIdx(1)), extend(0));
}

#[test]
fn overflow_rv128m_128() {
    use crate::{
        instructions::{base, rv128m, Executable},
        Mult, NoFloats, Register128Bit,
    };

    let mut computer = Computer::<Register128Bit, Mult, NoFloats>::new(0);
    computer.queue(Instruction::Base(base::Addi(
        // 10x = 0 + 1
        RegisterIdx(10),
        RegisterIdx(0),
        ImmediateLow::new(0x1).unwrap(),
    )));
    computer.queue(Instruction::Base(base::Slli(
        // 10x = 0b_1000... (most negative value)
        RegisterIdx(10),
        RegisterIdx(10),
        ImmediateLow::new(63).unwrap(),
    )));
    computer.queue(Instruction::Base(base::Addi(
        // 11x = 0 + 1
        RegisterIdx(11),
        RegisterIdx(0),
        ImmediateLow::new(0x1).unwrap(),
    )));
    computer.queue(Instruction::Base(base::Sub(
        // 11x = 0 - 1
        RegisterIdx(11),
        RegisterIdx(0),
        RegisterIdx(11),
    )));

    computer.queue(Instruction::RV128M(rv128m::Remd(
        // 1x = 0b_1000... % 0b_1111...
        RegisterIdx(1),
        RegisterIdx(10),
        RegisterIdx(11),
    )));
    computer.run_all();

    assert_eq!(computer.get(RegisterIdx(1)), extend(0));
}

#[test]
fn overflow_rv32m_128() {
    use crate::{
        instructions::{base, rv32m, Executable},
        Mult, NoFloats, Register128Bit,
    };

    let mut computer = Computer::<Register128Bit, Mult, NoFloats>::new(0);
    computer.queue(Instruction::Base(base::Addi(
        // 10x = 0 + 1
        RegisterIdx(10),
        RegisterIdx(0),
        ImmediateLow::new(0x1).unwrap(),
    )));
    computer.queue(Instruction::Base(base::Slli(
        // 10x = 0b_1000... (most negative value)
        RegisterIdx(10),
        RegisterIdx(10),
        ImmediateLow::new(127).unwrap(),
    )));
    computer.queue(Instruction::Base(base::Addi(
        // 11x = 0 + 1
        RegisterIdx(11),
        RegisterIdx(0),
        ImmediateLow::new(0x1).unwrap(),
    )));
    computer.queue(Instruction::Base(base::Sub(
        // 11x = 0 - 1
        RegisterIdx(11),
        RegisterIdx(0),
        RegisterIdx(11),
    )));

    computer.queue(Instruction::RV32M(rv32m::Rem(
        // 1x = 0b_1000... % 0b_1111...
        RegisterIdx(1),
        RegisterIdx(10),
        RegisterIdx(11),
    )));
    computer.run_all();

    assert_eq!(computer.get(RegisterIdx(1)), extend(0));
}

// Length
test_rv64m! { length, || {
    let mut computer = Computer::<R, M, F>::new(0);
    computer.queue(Instruction::Base(base::Addi(
        // 10x = 1
        RegisterIdx(10),
        RegisterIdx(0),
        ImmediateLow::new(0x1).unwrap(),
    )));
    computer.queue(Instruction::Base(base::Slli(
        // 10x = 1 << 32
        RegisterIdx(10),
        RegisterIdx(10),
        ImmediateLow::new(0x20).unwrap(),
    )));
    computer.queue(Instruction::Base(base::Addi(
        // 11x = 5
        RegisterIdx(11),
        RegisterIdx(0),
        ImmediateLow::new(0x5).unwrap(),
    )));

    computer.queue(Instruction::RV32M(rv32m::Remu(
        // 1x = 0x1_0000_0000 % 5 = 1
        RegisterIdx(1),
        RegisterIdx(10),
        RegisterIdx(11),
    )));
    computer.queue(Instruction::RV64M(rv64m::Remuw(
        // 1x = 0x1_0000_0000 % 5 = 0
        RegisterIdx(2),
        RegisterIdx(10),
        RegisterIdx(11),
    )));
    computer.queue(Instruction::RV32M(rv32m::Rem(
        // 1x = 0x1_0000_0000 % 5 = 1
        RegisterIdx(3),
        RegisterIdx(10),
        RegisterIdx(11),
    )));
    computer.queue(Instruction::RV64M(rv64m::Remw(
        // 1x = 0x1_0000_0000 % 5 = 0
        RegisterIdx(4),
        RegisterIdx(10),
        RegisterIdx(11),
    )));
    computer.run_all();

    assert_eq!(computer.get(RegisterIdx(1)), extend(1)); // Remu
    assert_eq!(computer.get(RegisterIdx(2)), extend(0)); // Remuw
    assert_eq!(computer.get(RegisterIdx(3)), extend(1)); // Rem
    assert_eq!(computer.get(RegisterIdx(4)), extend(0)); // Remw
}}

// Length
test_rv128m! { length, || {
    let mut computer = Computer::<R, M, F>::new(0);
    computer.queue(Instruction::Base(base::Addi(
        // 10x = 1
        RegisterIdx(10),
        RegisterIdx(0),
        ImmediateLow::new(0x1).unwrap(),
    )));
    computer.queue(Instruction::Base(base::Slli(
        // 10x = 1 << 32
        RegisterIdx(10),
        RegisterIdx(10),
        ImmediateLow::new(0x40).unwrap(),
    )));
    computer.queue(Instruction::Base(base::Addi(
        // 11x = 5
        RegisterIdx(11),
        RegisterIdx(0),
        ImmediateLow::new(0x5).unwrap(),
    )));

    computer.queue(Instruction::RV32M(rv32m::Remu(
        // 1x = 0x1_0000_0000__0000_0000 % 5 = 1
        RegisterIdx(1),
        RegisterIdx(10),
        RegisterIdx(11),
    )));
    computer.queue(Instruction::RV128M(rv128m::Remud(
        // 2x = 0x1_0000_0000__0000_0000 % 5 = 0
        RegisterIdx(2),
        RegisterIdx(10),
        RegisterIdx(11),
    )));
    computer.queue(Instruction::RV32M(rv32m::Rem(
        // 3x = 0x1_0000_0000__0000_0000 % 5 = 1
        RegisterIdx(3),
        RegisterIdx(10),
        RegisterIdx(11),
    )));
    computer.queue(Instruction::RV128M(rv128m::Remd(
        // 4x = 0x1_0000_0000__0000_0000 % 5 = 0
        RegisterIdx(4),
        RegisterIdx(10),
        RegisterIdx(11),
    )));
    computer.run_all();

    assert_eq!(computer.get(RegisterIdx(1)), extend(1)); // Remu
    assert_eq!(computer.get(RegisterIdx(2)), extend(0)); // Remud
    assert_eq!(computer.get(RegisterIdx(3)), extend(1)); // Rem
    assert_eq!(computer.get(RegisterIdx(4)), extend(0)); // Remd
}}
