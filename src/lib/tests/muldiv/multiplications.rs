use super::extend;
use crate::{Computer, ImmediateLow, Instruction, RegisterIdx};

// Basic Overflow
test_all_m! { basic_overflow, || {
    let mut computer = Computer::new(0);
    computer.queue(Instruction::Base(base::Instruction::Addi(
        // 10x = 18
        RegisterIdx(10),
        RegisterIdx(0),
        ImmediateLow::new(0x12).unwrap(),
    )));
    computer.queue(Instruction::Base(base::Instruction::Addi(
        // 11x = 5
        RegisterIdx(11),
        RegisterIdx(0),
        ImmediateLow::new(0x5).unwrap(),
    )));
    computer.queue(Instruction::Base(base::Instruction::Addi(
        // 12x = -2
        RegisterIdx(12),
        RegisterIdx(0),
        ImmediateLow::new(0xFFE).unwrap(),
    )));
    computer.queue(Instruction::Base(base::Instruction::Srli(
        // 13x = 0x7FFF...
        RegisterIdx(13),
        RegisterIdx(12),
        ImmediateLow::new(0x1).unwrap(),
    )));

    computer.queue(Instruction::RV32M(rv32m::Instruction::Mul(
        // 1x = 18 * 5
        RegisterIdx(1),
        RegisterIdx(10),
        RegisterIdx(11),
    )));
    computer.queue(Instruction::RV32M(rv32m::Instruction::Mul(
        // 2x = 18 * -2
        RegisterIdx(2),
        RegisterIdx(10),
        RegisterIdx(12),
    )));
    computer.queue(Instruction::RV32M(rv32m::Instruction::Mul(
        // 3x = -2 * 0x7FFF...
        RegisterIdx(3),
        RegisterIdx(12),
        RegisterIdx(13),
    )));
    computer.run_all();

    println!("x10: {:0128b}", computer.registers[10]);
    println!("x11: {:0128b}", computer.registers[11]);
    println!("x12: {:0128b}", computer.registers[12]);
    println!("x13: {:0128b}", computer.registers[13]);
    println!();
    println!("x01: {:0128b}", computer.registers[1]);
    println!("x02: {:0128b}", computer.registers[2]);
    println!("x03: {:0128b}", computer.registers[3]);

    assert_eq!(computer.get(RegisterIdx(1)), extend(90)); // MUL 18, 5
    assert_eq!(computer.get(RegisterIdx(2)), extend(-36 as _)); // MUL 18, -2
    assert_eq!(computer.get(RegisterIdx(3)), extend(2)); // MUL -2, 0x7FFF...
}}

// Basic Overflow
test_rv64m! { basic_overflow, || {
    let mut computer = Computer::new(0);
    computer.queue(Instruction::Base(base::Instruction::Addi(
        // 10x = 18
        RegisterIdx(10),
        RegisterIdx(0),
        ImmediateLow::new(0x12).unwrap(),
    )));
    computer.queue(Instruction::Base(base::Instruction::Addi(
        // 11x = 5
        RegisterIdx(11),
        RegisterIdx(0),
        ImmediateLow::new(0x5).unwrap(),
    )));
    computer.queue(Instruction::Base(base::Instruction::Addi(
        // 12x = -2
        RegisterIdx(12),
        RegisterIdx(0),
        ImmediateLow::new(0xFFE).unwrap(),
    )));
    computer.registers[13] = 0x7FFFFFFFu64.into();

    computer.queue(Instruction::RV64M(rv64m::Instruction::Mulw(
        // 1x = 18 * 5
        RegisterIdx(1),
        RegisterIdx(10),
        RegisterIdx(11),
    )));
    computer.queue(Instruction::RV64M(rv64m::Instruction::Mulw(
        // 2x = 18 * -2
        RegisterIdx(2),
        RegisterIdx(10),
        RegisterIdx(12),
    )));
    computer.queue(Instruction::RV64M(rv64m::Instruction::Mulw(
        // 3x = -2 * 0x7FFF...
        RegisterIdx(3),
        RegisterIdx(12),
        RegisterIdx(13),
    )));
    computer.run_all();

    println!("x10: {:0128b}", computer.registers[10]);
    println!("x11: {:0128b}", computer.registers[11]);
    println!("x12: {:0128b}", computer.registers[12]);
    println!("x13: {:0128b}", computer.registers[13]);
    println!();
    println!("x01: {:0128b}", computer.registers[1]);
    println!("x02: {:0128b}", computer.registers[2]);
    println!("x03: {:0128b}", computer.registers[3]);

    assert_eq!(computer.get(RegisterIdx(1)), extend(90)); // MUL 18, 5
    assert_eq!(computer.get(RegisterIdx(2)), extend(-36 as _)); // MUL 18, -2
    assert_eq!(computer.get(RegisterIdx(3)), extend(2)); // MUL -2, 0x7FFF...
}}

// Basic Overflow
test_rv128m! { basic_overflow, || {
    let mut computer = Computer::new(0);
    computer.queue(Instruction::Base(base::Instruction::Addi(
        // 10x = 18
        RegisterIdx(10),
        RegisterIdx(0),
        ImmediateLow::new(0x12).unwrap(),
    )));
    computer.queue(Instruction::Base(base::Instruction::Addi(
        // 11x = 5
        RegisterIdx(11),
        RegisterIdx(0),
        ImmediateLow::new(0x5).unwrap(),
    )));
    computer.queue(Instruction::Base(base::Instruction::Addi(
        // 12x = -2
        RegisterIdx(12),
        RegisterIdx(0),
        ImmediateLow::new(0xFFE).unwrap(),
    )));
    computer.registers[13] = 0x7FFFFFFF_FFFFFFFFu128.into();

    computer.queue(Instruction::RV128M(rv128m::Instruction::Muld(
        // 1x = 18 * 5
        RegisterIdx(1),
        RegisterIdx(10),
        RegisterIdx(11),
    )));
    computer.queue(Instruction::RV128M(rv128m::Instruction::Muld(
        // 2x = 18 * -2
        RegisterIdx(2),
        RegisterIdx(10),
        RegisterIdx(12),
    )));
    computer.queue(Instruction::RV128M(rv128m::Instruction::Muld(
        // 3x = -2 * 0x7FFF...
        RegisterIdx(3),
        RegisterIdx(12),
        RegisterIdx(13),
    )));
    computer.run_all();

    println!("x10: {:0128b}", computer.registers[10]);
    println!("x11: {:0128b}", computer.registers[11]);
    println!("x12: {:0128b}", computer.registers[12]);
    println!("x13: {:0128b}", computer.registers[13]);
    println!();
    println!("x01: {:0128b}", computer.registers[1]);
    println!("x02: {:0128b}", computer.registers[2]);
    println!("x03: {:0128b}", computer.registers[3]);

    assert_eq!(computer.get(RegisterIdx(1)), extend(90)); // MUL 18, 5
    assert_eq!(computer.get(RegisterIdx(2)), extend(-36 as _)); // MUL 18, -2
    assert_eq!(computer.get(RegisterIdx(3)), extend(2)); // MUL -2, 0x7FFF...
}}

#[test]
fn mulh_32() {
    // Test inspired from https://github.com/riscv/riscv-tests/blob/master/isa/rv32um/mulh.S

    use crate::{
        instructions::{rv32m, Executable},
        Mult, NoFloats, Register32Bit,
    };

    let mut computer = Computer::<Register32Bit, Mult, NoFloats>::new(0);

    /*Initialization*/
    {
        computer.registers[1] = 0x00000001;
        computer.registers[2] = 0x00000003;
        computer.registers[3] = 0x00000007;

        computer.registers[4] = 0xFFFF8000;
        computer.registers[5] = 0x80000000;

        computer.registers[6] = 0xAAAAAAAB;
        computer.registers[7] = 0x0002FE7D;

        computer.registers[8] = 0xFF000000;

        computer.registers[9] = 0xFFFFFFFF;
    }

    /*Execution*/
    {
        computer.queue(Instruction::RV32M(rv32m::Instruction::Mulh(
            // 0x00000000 = 0x00000000 * 0x00000000
            RegisterIdx(10),
            RegisterIdx(0),
            RegisterIdx(0),
        )));
        computer.queue(Instruction::RV32M(rv32m::Instruction::Mulh(
            // 0x00000000 = 0x00000001 * 0x00000001
            RegisterIdx(11),
            RegisterIdx(1),
            RegisterIdx(1),
        )));
        computer.queue(Instruction::RV32M(rv32m::Instruction::Mulh(
            // 0x00000000 = 0x00000003 * 0x00000007
            RegisterIdx(12),
            RegisterIdx(2),
            RegisterIdx(3),
        )));

        computer.queue(Instruction::RV32M(rv32m::Instruction::Mulh(
            // 0x00000000 = 0x00000000 * 0xFFFF8000
            RegisterIdx(13),
            RegisterIdx(0),
            RegisterIdx(4),
        )));
        computer.queue(Instruction::RV32M(rv32m::Instruction::Mulh(
            // 0x00000000 = 0x80000000 * 0x00000000
            RegisterIdx(14),
            RegisterIdx(5),
            RegisterIdx(0),
        )));
        computer.queue(Instruction::RV32M(rv32m::Instruction::Mulh(
            // 0x00004000 = 0xFFFF8000 * 0x80000000
            RegisterIdx(15),
            RegisterIdx(4),
            RegisterIdx(5),
        )));
        computer.queue(Instruction::RV32M(rv32m::Instruction::Mulh(
            // 0x00004000 = 0x80000000 * 0xFFFF8000
            RegisterIdx(16),
            RegisterIdx(5),
            RegisterIdx(4),
        )));

        computer.queue(Instruction::RV32M(rv32m::Instruction::Mulh(
            // 0xFFFF0081 = 0xAAAAAAAB * 0x0002FE7D
            RegisterIdx(17),
            RegisterIdx(6),
            RegisterIdx(7),
        )));
        computer.queue(Instruction::RV32M(rv32m::Instruction::Mulh(
            // 0xFFFF0081 = 0x0002FE7D * 0xAAAAAAAB
            RegisterIdx(18),
            RegisterIdx(7),
            RegisterIdx(6),
        )));

        computer.queue(Instruction::RV32M(rv32m::Instruction::Mulh(
            // 0x00010000 = 0xFF000000 * 0xFF000000
            RegisterIdx(19),
            RegisterIdx(8),
            RegisterIdx(8),
        )));

        computer.queue(Instruction::RV32M(rv32m::Instruction::Mulh(
            // 0x00000000 = 0xFFFFFFFF * 0xFFFFFFFF
            RegisterIdx(20),
            RegisterIdx(9),
            RegisterIdx(9),
        )));
        computer.queue(Instruction::RV32M(rv32m::Instruction::Mulh(
            // 0xFFFFFFFE = 0xFFFFFFFF * 0x00000001
            RegisterIdx(21),
            RegisterIdx(9),
            RegisterIdx(1),
        )));
        computer.queue(Instruction::RV32M(rv32m::Instruction::Mulh(
            // 0xFFFFFFFE = 0x00000001 * 0xFFFFFFFF
            RegisterIdx(22),
            RegisterIdx(1),
            RegisterIdx(9),
        )));

        computer.run_all();
    }

    /*Printing*/
    {
        println!("x10: {:032X}", computer.registers[10]);
        println!("x11: {:032X}", computer.registers[11]);
        println!("x12: {:032X}", computer.registers[12]);
        println!();
        println!("x13: {:032X}", computer.registers[13]);
        println!("x14: {:032X}", computer.registers[14]);
        println!("x15: {:032X}", computer.registers[15]);
        println!("x16: {:032X}", computer.registers[16]);
        println!();
        println!("x17: {:032X}", computer.registers[17]);
        println!("x18: {:032X}", computer.registers[18]);
        println!();
        println!("x19: {:032X}", computer.registers[19]);
        println!();
        println!("x20: {:032X}", computer.registers[20]);
        println!("x21: {:032X}", computer.registers[21]);
        println!("x22: {:032X}", computer.registers[22]);
    }

    /*Assertions*/
    {
        assert_eq!(computer.get(RegisterIdx(10)), 0x00000000u32.into());
        assert_eq!(computer.get(RegisterIdx(11)), 0x00000000u32.into());
        assert_eq!(computer.get(RegisterIdx(12)), 0x00000000u32.into());

        assert_eq!(computer.get(RegisterIdx(13)), 0x00000000u32.into());
        assert_eq!(computer.get(RegisterIdx(14)), 0x00000000u32.into());
        assert_eq!(computer.get(RegisterIdx(15)), 0x00004000u32.into());
        assert_eq!(computer.get(RegisterIdx(16)), 0x00004000u32.into());

        assert_eq!(computer.get(RegisterIdx(17)), 0xFFFF0081u32.into());
        assert_eq!(computer.get(RegisterIdx(18)), 0xFFFF0081u32.into());

        assert_eq!(computer.get(RegisterIdx(19)), 0x00010000u32.into());

        assert_eq!(computer.get(RegisterIdx(20)), 0x00000000u32.into());
        assert_eq!(computer.get(RegisterIdx(21)), 0xFFFFFFFFu32.into());
        assert_eq!(computer.get(RegisterIdx(22)), 0xFFFFFFFFu32.into());
    }
}

#[test]
fn mulhsu_32() {
    // Test inspired from https://github.com/riscv/riscv-tests/blob/master/isa/rv32um/mulh.S

    use crate::{
        instructions::{rv32m, Executable},
        Mult, NoFloats, Register32Bit,
    };

    let mut computer = Computer::<Register32Bit, Mult, NoFloats>::new(0);

    /*Initialization*/
    {
        computer.registers[1] = 0x00000001;
        computer.registers[2] = 0x00000003;
        computer.registers[3] = 0x00000007;

        computer.registers[4] = 0xFFFF8000;
        computer.registers[5] = 0x80000000;

        computer.registers[6] = 0xAAAAAAAB;
        computer.registers[7] = 0x0002FE7D;

        computer.registers[8] = 0xFF000000;

        computer.registers[9] = 0xFFFFFFFF;
    }

    /*Execution*/
    {
        computer.queue(Instruction::RV32M(rv32m::Instruction::Mulhsu(
            // 0x00000000 = 0x00000000 * 0x00000000
            RegisterIdx(10),
            RegisterIdx(0),
            RegisterIdx(0),
        )));
        computer.queue(Instruction::RV32M(rv32m::Instruction::Mulhsu(
            // 0x00000000 = 0x00000001 * 0x00000001
            RegisterIdx(11),
            RegisterIdx(1),
            RegisterIdx(1),
        )));
        computer.queue(Instruction::RV32M(rv32m::Instruction::Mulhsu(
            // 0x00000000 = 0x00000003 * 0x00000007
            RegisterIdx(12),
            RegisterIdx(2),
            RegisterIdx(3),
        )));

        computer.queue(Instruction::RV32M(rv32m::Instruction::Mulhsu(
            // 0x00000000 = 0x00000000 * 0xFFFF8000
            RegisterIdx(13),
            RegisterIdx(0),
            RegisterIdx(4),
        )));
        computer.queue(Instruction::RV32M(rv32m::Instruction::Mulhsu(
            // 0x00000000 = 0x80000000 * 0x00000000
            RegisterIdx(14),
            RegisterIdx(5),
            RegisterIdx(0),
        )));
        computer.queue(Instruction::RV32M(rv32m::Instruction::Mulhsu(
            // 0xFFFFC000 = 0xFFFF8000 * 0x80000000
            RegisterIdx(15),
            RegisterIdx(4),
            RegisterIdx(5),
        )));
        computer.queue(Instruction::RV32M(rv32m::Instruction::Mulhsu(
            // 0x80004000 = 0x80000000 * 0xFFFF8000
            RegisterIdx(16),
            RegisterIdx(5),
            RegisterIdx(4),
        )));

        computer.queue(Instruction::RV32M(rv32m::Instruction::Mulhsu(
            // 0xFFFF0081 = 0xAAAAAAAB * 0x0002FE7D
            RegisterIdx(17),
            RegisterIdx(6),
            RegisterIdx(7),
        )));
        computer.queue(Instruction::RV32M(rv32m::Instruction::Mulhsu(
            // 0x0001FEFE = 0x0002FE7D * 0xAAAAAAAB
            RegisterIdx(18),
            RegisterIdx(7),
            RegisterIdx(6),
        )));

        computer.queue(Instruction::RV32M(rv32m::Instruction::Mulhsu(
            // 0xFF010000 = 0xFF000000 * 0xFF000000
            RegisterIdx(19),
            RegisterIdx(8),
            RegisterIdx(8),
        )));

        computer.queue(Instruction::RV32M(rv32m::Instruction::Mulhsu(
            // 0xFFFFFFFF = 0xFFFFFFFF * 0xFFFFFFFF
            RegisterIdx(20),
            RegisterIdx(9),
            RegisterIdx(9),
        )));
        computer.queue(Instruction::RV32M(rv32m::Instruction::Mulhsu(
            // 0xFFFFFFFF = 0xFFFFFFFF * 0x00000001
            RegisterIdx(21),
            RegisterIdx(9),
            RegisterIdx(1),
        )));
        computer.queue(Instruction::RV32M(rv32m::Instruction::Mulhsu(
            // 0x00000000 = 0x00000001 * 0xFFFFFFFF
            RegisterIdx(22),
            RegisterIdx(1),
            RegisterIdx(9),
        )));

        computer.run_all();
    }

    /*Printing*/
    {
        println!("x10: {:032X}", computer.registers[10]);
        println!("x11: {:032X}", computer.registers[11]);
        println!("x12: {:032X}", computer.registers[12]);
        println!();
        println!("x13: {:032X}", computer.registers[13]);
        println!("x14: {:032X}", computer.registers[14]);
        println!("x15: {:032X}", computer.registers[15]);
        println!("x16: {:032X}", computer.registers[16]);
        println!();
        println!("x17: {:032X}", computer.registers[17]);
        println!("x18: {:032X}", computer.registers[18]);
        println!();
        println!("x19: {:032X}", computer.registers[19]);
        println!();
        println!("x20: {:032X}", computer.registers[20]);
        println!("x21: {:032X}", computer.registers[21]);
        println!("x22: {:032X}", computer.registers[22]);
    }

    /*Assertions*/
    {
        assert_eq!(computer.get(RegisterIdx(10)), 0x00000000u32.into(), "x10");
        assert_eq!(computer.get(RegisterIdx(11)), 0x00000000u32.into(), "x11");
        assert_eq!(computer.get(RegisterIdx(12)), 0x00000000u32.into(), "x12");

        assert_eq!(computer.get(RegisterIdx(13)), 0x00000000u32.into(), "x13");
        assert_eq!(computer.get(RegisterIdx(14)), 0x00000000u32.into(), "x14");
        assert_eq!(computer.get(RegisterIdx(15)), 0xFFFFC000u32.into(), "x15");
        assert_eq!(computer.get(RegisterIdx(16)), 0x80004000u32.into(), "x16");

        assert_eq!(computer.get(RegisterIdx(17)), 0xFFFF0081u32.into(), "x17");
        assert_eq!(computer.get(RegisterIdx(18)), 0x0001FEFEu32.into(), "x18");

        assert_eq!(computer.get(RegisterIdx(19)), 0xFF010000u32.into(), "x19");

        assert_eq!(computer.get(RegisterIdx(20)), 0xFFFFFFFFu32.into(), "x20");
        assert_eq!(computer.get(RegisterIdx(21)), 0xFFFFFFFFu32.into(), "x21");
        assert_eq!(computer.get(RegisterIdx(22)), 0x00000000u32.into(), "x22");
    }
}

#[test]
fn mulhu_32() {
    // Test inspired from https://github.com/riscv/riscv-tests/blob/master/isa/rv32um/mulh.S

    use crate::{
        instructions::{rv32m, Executable},
        Mult, NoFloats, Register32Bit,
    };

    let mut computer = Computer::<Register32Bit, Mult, NoFloats>::new(0);

    /*Initialization*/
    {
        computer.registers[1] = 0x00000001;
        computer.registers[2] = 0x00000003;
        computer.registers[3] = 0x00000007;

        computer.registers[4] = 0xFFFF8000;
        computer.registers[5] = 0x80000000;

        computer.registers[6] = 0xAAAAAAAB;
        computer.registers[7] = 0x0002FE7D;

        computer.registers[8] = 0xFF000000;

        computer.registers[9] = 0xFFFFFFFF;
    }

    /*Execution*/
    {
        computer.queue(Instruction::RV32M(rv32m::Instruction::Mulhu(
            // 0x00000000 = 0x00000000 * 0x00000000
            RegisterIdx(10),
            RegisterIdx(0),
            RegisterIdx(0),
        )));
        computer.queue(Instruction::RV32M(rv32m::Instruction::Mulhu(
            // 0x00000000 = 0x00000001 * 0x00000001
            RegisterIdx(11),
            RegisterIdx(1),
            RegisterIdx(1),
        )));
        computer.queue(Instruction::RV32M(rv32m::Instruction::Mulhu(
            // 0x00000000 = 0x00000003 * 0x00000007
            RegisterIdx(12),
            RegisterIdx(2),
            RegisterIdx(3),
        )));

        computer.queue(Instruction::RV32M(rv32m::Instruction::Mulhu(
            // 0x00000000 = 0x00000000 * 0xFFFF8000
            RegisterIdx(13),
            RegisterIdx(0),
            RegisterIdx(4),
        )));
        computer.queue(Instruction::RV32M(rv32m::Instruction::Mulhu(
            // 0x00000000 = 0x80000000 * 0x00000000
            RegisterIdx(14),
            RegisterIdx(5),
            RegisterIdx(0),
        )));
        computer.queue(Instruction::RV32M(rv32m::Instruction::Mulhu(
            // 0x7FFFC000 = 0xFFFF8000 * 0x80000000
            RegisterIdx(15),
            RegisterIdx(4),
            RegisterIdx(5),
        )));
        computer.queue(Instruction::RV32M(rv32m::Instruction::Mulhu(
            // 0x7FFFC000 = 0x80000000 * 0xFFFF8000
            RegisterIdx(16),
            RegisterIdx(5),
            RegisterIdx(4),
        )));

        computer.queue(Instruction::RV32M(rv32m::Instruction::Mulhu(
            // 0x0001FEFE = 0xAAAAAAAB * 0x0002FE7D
            RegisterIdx(17),
            RegisterIdx(6),
            RegisterIdx(7),
        )));
        computer.queue(Instruction::RV32M(rv32m::Instruction::Mulhu(
            // 0x0001FEFE = 0x0002FE7D * 0xAAAAAAAB
            RegisterIdx(18),
            RegisterIdx(7),
            RegisterIdx(6),
        )));

        computer.queue(Instruction::RV32M(rv32m::Instruction::Mulhu(
            // 0xFE010000 = 0xFF000000 * 0xFF000000
            RegisterIdx(19),
            RegisterIdx(8),
            RegisterIdx(8),
        )));

        computer.queue(Instruction::RV32M(rv32m::Instruction::Mulhu(
            // 0xFFFFFFFE = 0xFFFFFFFF * 0xFFFFFFFF
            RegisterIdx(20),
            RegisterIdx(9),
            RegisterIdx(9),
        )));
        computer.queue(Instruction::RV32M(rv32m::Instruction::Mulhu(
            // 0x00000000 = 0xFFFFFFFF * 0x00000001
            RegisterIdx(21),
            RegisterIdx(9),
            RegisterIdx(1),
        )));
        computer.queue(Instruction::RV32M(rv32m::Instruction::Mulhu(
            // 0x00000000 = 0x00000001 * 0xFFFFFFFF
            RegisterIdx(22),
            RegisterIdx(1),
            RegisterIdx(9),
        )));

        computer.run_all();
    }

    /*Printing*/
    {
        println!("x10: {:032X}", computer.registers[10]);
        println!("x11: {:032X}", computer.registers[11]);
        println!("x12: {:032X}", computer.registers[12]);
        println!();
        println!("x13: {:032X}", computer.registers[13]);
        println!("x14: {:032X}", computer.registers[14]);
        println!("x15: {:032X}", computer.registers[15]);
        println!("x16: {:032X}", computer.registers[16]);
        println!();
        println!("x17: {:032X}", computer.registers[17]);
        println!("x18: {:032X}", computer.registers[18]);
        println!();
        println!("x19: {:032X}", computer.registers[19]);
        println!();
        println!("x20: {:032X}", computer.registers[20]);
        println!("x21: {:032X}", computer.registers[21]);
        println!("x22: {:032X}", computer.registers[22]);
    }

    /*Assertions*/
    {
        assert_eq!(computer.get(RegisterIdx(10)), 0x00000000u32.into());
        assert_eq!(computer.get(RegisterIdx(11)), 0x00000000u32.into());
        assert_eq!(computer.get(RegisterIdx(12)), 0x00000000u32.into());

        assert_eq!(computer.get(RegisterIdx(13)), 0x00000000u32.into());
        assert_eq!(computer.get(RegisterIdx(14)), 0x00000000u32.into());
        assert_eq!(computer.get(RegisterIdx(15)), 0x7FFFC000u32.into());
        assert_eq!(computer.get(RegisterIdx(16)), 0x7FFFC000u32.into());

        assert_eq!(computer.get(RegisterIdx(17)), 0x0001FEFEu32.into());
        assert_eq!(computer.get(RegisterIdx(18)), 0x0001FEFEu32.into());

        assert_eq!(computer.get(RegisterIdx(19)), 0xFE010000u32.into());

        assert_eq!(computer.get(RegisterIdx(20)), 0xFFFFFFFEu32.into());
        assert_eq!(computer.get(RegisterIdx(21)), 0x00000000u32.into());
        assert_eq!(computer.get(RegisterIdx(22)), 0x00000000u32.into());
    }
}

#[test]
fn mulh_64() {
    // Test inspired from https://github.com/riscv/riscv-tests/blob/master/isa/rv32um/mulh.S

    use crate::{
        instructions::{rv32m, Executable},
        Mult, NoFloats, Register64Bit,
    };

    let mut computer = Computer::<Register64Bit, Mult, NoFloats>::new(0);

    /*Initialization*/
    {
        computer.registers[1] = 0x00000000_00000001;
        computer.registers[2] = 0x00000000_00000003;
        computer.registers[3] = 0x00000000_00000007;

        computer.registers[4] = 0xFFFFFFFF_80000000;
        computer.registers[5] = 0x80000000_00000000;

        computer.registers[6] = 0xAAAAAAAA_AAAAAAAB;
        computer.registers[7] = 0x00000000_0002FE7D;

        computer.registers[8] = 0xFF000000_00000000;

        computer.registers[9] = 0xFFFFFFFF_FFFFFFFF;
    }

    /*Execution*/
    {
        computer.queue(Instruction::RV32M(rv32m::Instruction::Mulh(
            // 0x00000000_00000000 = 0x00000000_00000000 * 0x00000000_00000000
            RegisterIdx(10),
            RegisterIdx(0),
            RegisterIdx(0),
        )));
        computer.queue(Instruction::RV32M(rv32m::Instruction::Mulh(
            // 0x00000000_00000000 = 0x00000000_00000001 * 0x00000000_00000001
            RegisterIdx(11),
            RegisterIdx(1),
            RegisterIdx(1),
        )));
        computer.queue(Instruction::RV32M(rv32m::Instruction::Mulh(
            // 0x00000000_00000000 = 0x00000000_00000003 * 0x00000000_00000007
            RegisterIdx(12),
            RegisterIdx(2),
            RegisterIdx(3),
        )));

        computer.queue(Instruction::RV32M(rv32m::Instruction::Mulh(
            // 0x00000000_00000000 = 0x00000000_00000000 * 0xFFFFFFFF_FFFF8000
            RegisterIdx(13),
            RegisterIdx(0),
            RegisterIdx(4),
        )));
        computer.queue(Instruction::RV32M(rv32m::Instruction::Mulh(
            // 0x00000000_00000000 = 0xFFFFFFFF_80000000 * 0x00000000_00000000
            RegisterIdx(14),
            RegisterIdx(5),
            RegisterIdx(0),
        )));
        computer.queue(Instruction::RV32M(rv32m::Instruction::Mulh(
            // 0x00000000_40000000 = 0xFFFFFFFF_80000000 * 0x80000000_00000000
            RegisterIdx(15),
            RegisterIdx(4),
            RegisterIdx(5),
        )));
        computer.queue(Instruction::RV32M(rv32m::Instruction::Mulh(
            // 0x00000000_40000000 = 0x80000000_00000000 * 0xFFFFFFFF_80000000
            RegisterIdx(16),
            RegisterIdx(5),
            RegisterIdx(4),
        )));

        computer.queue(Instruction::RV32M(rv32m::Instruction::Mulh(
            // 0xFFFFFFFF_FFFF0081 = 0xAAAAAAAA_AAAAAAAB * 0x00000000_0002FE7D
            RegisterIdx(17),
            RegisterIdx(6),
            RegisterIdx(7),
        )));
        computer.queue(Instruction::RV32M(rv32m::Instruction::Mulh(
            // 0xFFFFFFFF_FFFF0081 = 0x00000000_0002FE7D * 0xAAAAAAAA_AAAAAAAB
            RegisterIdx(18),
            RegisterIdx(7),
            RegisterIdx(6),
        )));

        computer.queue(Instruction::RV32M(rv32m::Instruction::Mulh(
            // 0x00010000_00000000 = 0xFF000000_00000000 * 0xFF000000_00000000
            RegisterIdx(19),
            RegisterIdx(8),
            RegisterIdx(8),
        )));

        computer.queue(Instruction::RV32M(rv32m::Instruction::Mulh(
            // 0x00000000_00000000 = 0xFFFFFFFF_FFFFFFFF * 0xFFFFFFFF_FFFFFFFF
            RegisterIdx(20),
            RegisterIdx(9),
            RegisterIdx(9),
        )));
        computer.queue(Instruction::RV32M(rv32m::Instruction::Mulh(
            // 0xFFFFFFFF_FFFFFFFF = 0xFFFFFFFF_FFFFFFFF * 0x00000000_00000001
            RegisterIdx(21),
            RegisterIdx(9),
            RegisterIdx(1),
        )));
        computer.queue(Instruction::RV32M(rv32m::Instruction::Mulh(
            // 0xFFFFFFFF_FFFFFFFF = 0x00000000_00000001 * 0xFFFFFFFF_FFFFFFFF
            RegisterIdx(22),
            RegisterIdx(1),
            RegisterIdx(9),
        )));

        computer.run_all();
    }

    /*Printing*/
    {
        println!("x10: {:032X}", computer.registers[10]);
        println!("x11: {:032X}", computer.registers[11]);
        println!("x12: {:032X}", computer.registers[12]);
        println!();
        println!("x13: {:032X}", computer.registers[13]);
        println!("x14: {:032X}", computer.registers[14]);
        println!("x15: {:032X}", computer.registers[15]);
        println!("x16: {:032X}", computer.registers[16]);
        println!();
        println!("x17: {:032X}", computer.registers[17]);
        println!("x18: {:032X}", computer.registers[18]);
        println!();
        println!("x19: {:032X}", computer.registers[19]);
        println!();
        println!("x20: {:032X}", computer.registers[20]);
        println!("x21: {:032X}", computer.registers[21]);
        println!("x22: {:032X}", computer.registers[22]);
    }

    /*Assertions*/
    {
        assert_eq!(computer.get(RegisterIdx(10)), 0x00000000_00000000u64.into());
        assert_eq!(computer.get(RegisterIdx(11)), 0x00000000_00000000u64.into());
        assert_eq!(computer.get(RegisterIdx(12)), 0x00000000_00000000u64.into());

        assert_eq!(computer.get(RegisterIdx(13)), 0x00000000_00000000u64.into());
        assert_eq!(computer.get(RegisterIdx(14)), 0x00000000_00000000u64.into());
        assert_eq!(computer.get(RegisterIdx(15)), 0x00000000_40000000u64.into());
        assert_eq!(computer.get(RegisterIdx(16)), 0x00000000_40000000u64.into());

        assert_eq!(computer.get(RegisterIdx(17)), 0xFFFFFFFF_FFFF0081u64.into());
        assert_eq!(computer.get(RegisterIdx(18)), 0xFFFFFFFF_FFFF0081u64.into());

        assert_eq!(computer.get(RegisterIdx(19)), 0x00010000_00000000u64.into());

        assert_eq!(computer.get(RegisterIdx(20)), 0x00000000_00000000u64.into());
        assert_eq!(computer.get(RegisterIdx(21)), 0xFFFFFFFF_FFFFFFFFu64.into());
        assert_eq!(computer.get(RegisterIdx(22)), 0xFFFFFFFF_FFFFFFFFu64.into());
    }
}

#[test]
fn mulhsu_64() {
    // Test inspired from https://github.com/riscv/riscv-tests/blob/master/isa/rv32um/mulh.S

    use crate::{
        instructions::{rv32m, Executable},
        Mult, NoFloats, Register64Bit,
    };

    let mut computer = Computer::<Register64Bit, Mult, NoFloats>::new(0);

    /*Initialization*/
    {
        computer.registers[1] = 0x00000000_00000001;
        computer.registers[2] = 0x00000000_00000003;
        computer.registers[3] = 0x00000000_00000007;

        computer.registers[4] = 0xFFFFFFFF_80000000;
        computer.registers[5] = 0x80000000_00000000;

        computer.registers[6] = 0xAAAAAAAA_AAAAAAAB;
        computer.registers[7] = 0x00000000_0002FE7D;

        computer.registers[8] = 0xFF000000_00000000;

        computer.registers[9] = 0xFFFFFFFF_FFFFFFFF;
    }

    /*Execution*/
    {
        computer.queue(Instruction::RV32M(rv32m::Instruction::Mulhsu(
            // 0x00000000_00000000 = 0x00000000_00000000 * 0x00000000_00000000
            RegisterIdx(10),
            RegisterIdx(0),
            RegisterIdx(0),
        )));
        computer.queue(Instruction::RV32M(rv32m::Instruction::Mulhsu(
            // 0x00000000_00000000 = 0x00000000_00000001 * 0x00000000_00000001
            RegisterIdx(11),
            RegisterIdx(1),
            RegisterIdx(1),
        )));
        computer.queue(Instruction::RV32M(rv32m::Instruction::Mulhsu(
            // 0x00000000_00000000 = 0x00000000_00000003 * 0x00000000_00000007
            RegisterIdx(12),
            RegisterIdx(2),
            RegisterIdx(3),
        )));

        computer.queue(Instruction::RV32M(rv32m::Instruction::Mulhsu(
            // 0x00000000_00000000 = 0x00000000_00000000 * 0xFFFFFFFF_FFFF8000
            RegisterIdx(13),
            RegisterIdx(0),
            RegisterIdx(4),
        )));
        computer.queue(Instruction::RV32M(rv32m::Instruction::Mulhsu(
            // 0x00000000_00000000 = 0xFFFFFFFF_80000000 * 0x00000000_00000000
            RegisterIdx(14),
            RegisterIdx(5),
            RegisterIdx(0),
        )));
        computer.queue(Instruction::RV32M(rv32m::Instruction::Mulhsu(
            // 0xFFFFFFFF_C0000000 = 0xFFFFFFFF_80000000 * 0x80000000_00000000
            RegisterIdx(15),
            RegisterIdx(4),
            RegisterIdx(5),
        )));
        computer.queue(Instruction::RV32M(rv32m::Instruction::Mulhsu(
            // 0x80000000_40000000 = 0x80000000_00000000 * 0xFFFFFFFF_80000000
            RegisterIdx(16),
            RegisterIdx(5),
            RegisterIdx(4),
        )));

        computer.queue(Instruction::RV32M(rv32m::Instruction::Mulhsu(
            // 0x00000000_0001FEFE = 0xAAAAAAAA_AAAAAAAB * 0x00000000_0002FE7D
            RegisterIdx(17),
            RegisterIdx(6),
            RegisterIdx(7),
        )));
        computer.queue(Instruction::RV32M(rv32m::Instruction::Mulhsu(
            // 0x00000000_0001FEFE = 0x00000000_0002FE7D * 0xAAAAAAAA_AAAAAAAB
            RegisterIdx(18),
            RegisterIdx(7),
            RegisterIdx(6),
        )));

        computer.queue(Instruction::RV32M(rv32m::Instruction::Mulhsu(
            // 0xFF010000_00000000 = 0xFF000000_00000000 * 0xFF000000_00000000
            RegisterIdx(19),
            RegisterIdx(8),
            RegisterIdx(8),
        )));

        computer.queue(Instruction::RV32M(rv32m::Instruction::Mulhsu(
            // 0xFFFFFFFF_FFFFFFFf = 0xFFFFFFFF_FFFFFFFF * 0xFFFFFFFF_FFFFFFFF
            RegisterIdx(20),
            RegisterIdx(9),
            RegisterIdx(9),
        )));
        computer.queue(Instruction::RV32M(rv32m::Instruction::Mulhsu(
            // 0xFFFFFFFF_FFFFFFFF = 0xFFFFFFFF_FFFFFFFF * 0x00000000_00000001
            RegisterIdx(21),
            RegisterIdx(9),
            RegisterIdx(1),
        )));
        computer.queue(Instruction::RV32M(rv32m::Instruction::Mulhsu(
            // 0x00000000_00000000 = 0x00000000_00000001 * 0xFFFFFFFF_FFFFFFFF
            RegisterIdx(22),
            RegisterIdx(1),
            RegisterIdx(9),
        )));

        computer.run_all();
    }

    /*Printing*/
    {
        println!("x10: {:032X}", computer.registers[10]);
        println!("x11: {:032X}", computer.registers[11]);
        println!("x12: {:032X}", computer.registers[12]);
        println!();
        println!("x13: {:032X}", computer.registers[13]);
        println!("x14: {:032X}", computer.registers[14]);
        println!("x15: {:032X}", computer.registers[15]);
        println!("x16: {:032X}", computer.registers[16]);
        println!();
        println!("x17: {:032X}", computer.registers[17]);
        println!("x18: {:032X}", computer.registers[18]);
        println!();
        println!("x19: {:032X}", computer.registers[19]);
        println!();
        println!("x20: {:032X}", computer.registers[20]);
        println!("x21: {:032X}", computer.registers[21]);
        println!("x22: {:032X}", computer.registers[22]);
    }

    /*Assertions*/
    {
        assert_eq!(
            computer.get(RegisterIdx(10)),
            0x00000000_00000000u64.into(),
            "x10"
        );
        assert_eq!(
            computer.get(RegisterIdx(11)),
            0x00000000_00000000u64.into(),
            "x11"
        );
        assert_eq!(
            computer.get(RegisterIdx(12)),
            0x00000000_00000000u64.into(),
            "x12"
        );

        assert_eq!(
            computer.get(RegisterIdx(13)),
            0x00000000_00000000u64.into(),
            "x13"
        );
        assert_eq!(
            computer.get(RegisterIdx(14)),
            0x00000000_00000000u64.into(),
            "x14"
        );
        assert_eq!(
            computer.get(RegisterIdx(15)),
            0xFFFFFFFF_C0000000u64.into(),
            "x15"
        );
        assert_eq!(
            computer.get(RegisterIdx(16)),
            0x80000000_40000000u64.into(),
            "x16"
        );

        assert_eq!(
            computer.get(RegisterIdx(17)),
            0xFFFFFFFF_FFFF0081u64.into(),
            "x17"
        );
        assert_eq!(
            computer.get(RegisterIdx(18)),
            0x00000000_0001FEFEu64.into(),
            "x18"
        );

        assert_eq!(
            computer.get(RegisterIdx(19)),
            0xFF010000_00000000u64.into(),
            "x19"
        );

        assert_eq!(
            computer.get(RegisterIdx(20)),
            0xFFFFFFFF_FFFFFFFfu64.into(),
            "x20"
        );
        assert_eq!(
            computer.get(RegisterIdx(21)),
            0xFFFFFFFF_FFFFFFFfu64.into(),
            "x21"
        );
        assert_eq!(
            computer.get(RegisterIdx(22)),
            0x00000000_00000000u64.into(),
            "x22"
        );
    }
}

#[test]
fn mulhu_64() {
    // Test inspired from https://github.com/riscv/riscv-tests/blob/master/isa/rv32um/mulh.S

    use crate::{
        instructions::{rv32m, Executable},
        Mult, NoFloats, Register64Bit,
    };

    let mut computer = Computer::<Register64Bit, Mult, NoFloats>::new(0);

    /*Initialization*/
    {
        computer.registers[1] = 0x00000000_00000001;
        computer.registers[2] = 0x00000000_00000003;
        computer.registers[3] = 0x00000000_00000007;

        computer.registers[4] = 0xFFFFFFFF_80000000;
        computer.registers[5] = 0x80000000_00000000;

        computer.registers[6] = 0xAAAAAAAA_AAAAAAAB;
        computer.registers[7] = 0x00000000_0002FE7D;

        computer.registers[8] = 0xFF000000_00000000;

        computer.registers[9] = 0xFFFFFFFF_FFFFFFFF;
    }

    /*Execution*/
    {
        computer.queue(Instruction::RV32M(rv32m::Instruction::Mulhu(
            // 0x00000000_00000000 = 0x00000000_00000000 * 0x00000000_00000000
            RegisterIdx(10),
            RegisterIdx(0),
            RegisterIdx(0),
        )));
        computer.queue(Instruction::RV32M(rv32m::Instruction::Mulhu(
            // 0x00000000_00000000 = 0x00000000_00000001 * 0x00000000_00000001
            RegisterIdx(11),
            RegisterIdx(1),
            RegisterIdx(1),
        )));
        computer.queue(Instruction::RV32M(rv32m::Instruction::Mulhu(
            // 0x00000000_00000000 = 0x00000000_00000003 * 0x00000000_00000007
            RegisterIdx(12),
            RegisterIdx(2),
            RegisterIdx(3),
        )));

        computer.queue(Instruction::RV32M(rv32m::Instruction::Mulhu(
            // 0x00000000_00000000 = 0x00000000_00000000 * 0xFFFFFFFF_FFFF8000
            RegisterIdx(13),
            RegisterIdx(0),
            RegisterIdx(4),
        )));
        computer.queue(Instruction::RV32M(rv32m::Instruction::Mulhu(
            // 0x00000000_00000000 = 0xFFFFFFFF_80000000 * 0x00000000_00000000
            RegisterIdx(14),
            RegisterIdx(5),
            RegisterIdx(0),
        )));
        computer.queue(Instruction::RV32M(rv32m::Instruction::Mulhu(
            // 0x7FFFFFFF_C0000000 = 0xFFFFFFFF_80000000 * 0x80000000_00000000
            RegisterIdx(15),
            RegisterIdx(4),
            RegisterIdx(5),
        )));
        computer.queue(Instruction::RV32M(rv32m::Instruction::Mulhu(
            // 0x7FFFFFFF_C0000000 = 0x80000000_00000000 * 0xFFFFFFFF_80000000
            RegisterIdx(16),
            RegisterIdx(5),
            RegisterIdx(4),
        )));

        computer.queue(Instruction::RV32M(rv32m::Instruction::Mulhu(
            // 0x00000000_0001FEFE = 0xAAAAAAAA_AAAAAAAB * 0x00000000_0002FE7D
            RegisterIdx(17),
            RegisterIdx(6),
            RegisterIdx(7),
        )));
        computer.queue(Instruction::RV32M(rv32m::Instruction::Mulhu(
            // 0x00000000_0001FEFE = 0x00000000_0002FE7D * 0xAAAAAAAA_AAAAAAAB
            RegisterIdx(18),
            RegisterIdx(7),
            RegisterIdx(6),
        )));

        computer.queue(Instruction::RV32M(rv32m::Instruction::Mulhu(
            // 0xFE010000_00000000 = 0xFF000000_00000000 * 0xFF000000_00000000
            RegisterIdx(19),
            RegisterIdx(8),
            RegisterIdx(8),
        )));

        computer.queue(Instruction::RV32M(rv32m::Instruction::Mulhu(
            // 0xFFFFFFFF_FFFFFFFE = 0xFFFFFFFF_FFFFFFFF * 0xFFFFFFFF_FFFFFFFF
            RegisterIdx(20),
            RegisterIdx(9),
            RegisterIdx(9),
        )));
        computer.queue(Instruction::RV32M(rv32m::Instruction::Mulhu(
            // 0x00000000_00000000 = 0xFFFFFFFF_FFFFFFFF * 0x00000000_00000001
            RegisterIdx(21),
            RegisterIdx(9),
            RegisterIdx(1),
        )));
        computer.queue(Instruction::RV32M(rv32m::Instruction::Mulhu(
            // 0x00000000_00000000 = 0x00000000_00000001 * 0xFFFFFFFF_FFFFFFFF
            RegisterIdx(22),
            RegisterIdx(1),
            RegisterIdx(9),
        )));

        computer.run_all();
    }

    /*Printing*/
    {
        println!("x10: {:032X}", computer.registers[10]);
        println!("x11: {:032X}", computer.registers[11]);
        println!("x12: {:032X}", computer.registers[12]);
        println!();
        println!("x13: {:032X}", computer.registers[13]);
        println!("x14: {:032X}", computer.registers[14]);
        println!("x15: {:032X}", computer.registers[15]);
        println!("x16: {:032X}", computer.registers[16]);
        println!();
        println!("x17: {:032X}", computer.registers[17]);
        println!("x18: {:032X}", computer.registers[18]);
        println!();
        println!("x19: {:032X}", computer.registers[19]);
        println!();
        println!("x20: {:032X}", computer.registers[20]);
        println!("x21: {:032X}", computer.registers[21]);
        println!("x22: {:032X}", computer.registers[22]);
    }

    /*Assertions*/
    {
        assert_eq!(computer.get(RegisterIdx(10)), 0x00000000_00000000u64.into());
        assert_eq!(computer.get(RegisterIdx(11)), 0x00000000_00000000u64.into());
        assert_eq!(computer.get(RegisterIdx(12)), 0x00000000_00000000u64.into());

        assert_eq!(computer.get(RegisterIdx(13)), 0x00000000_00000000u64.into());
        assert_eq!(computer.get(RegisterIdx(14)), 0x00000000_00000000u64.into());
        assert_eq!(computer.get(RegisterIdx(15)), 0x7FFFFFFF_C0000000u64.into());
        assert_eq!(computer.get(RegisterIdx(16)), 0x7FFFFFFF_C0000000u64.into());

        assert_eq!(computer.get(RegisterIdx(17)), 0x00000000_0001FEFEu64.into());
        assert_eq!(computer.get(RegisterIdx(18)), 0x00000000_0001FEFEu64.into());

        assert_eq!(computer.get(RegisterIdx(19)), 0xFE010000_00000000u64.into());

        assert_eq!(computer.get(RegisterIdx(20)), 0xFFFFFFFF_FFFFFFFEu64.into());
        assert_eq!(computer.get(RegisterIdx(21)), 0x00000000_00000000u64.into());
        assert_eq!(computer.get(RegisterIdx(22)), 0x00000000_00000000u64.into());
    }
}

#[test]
fn mulh_128() {
    // Test inspired from https://github.com/riscv/riscv-tests/blob/master/isa/rv32um/mulh.S

    use crate::{
        instructions::{rv32m, Executable},
        Mult, NoFloats, Register128Bit,
    };

    let mut computer = Computer::<Register128Bit, Mult, NoFloats>::new(0);

    /*Initialization*/
    {
        computer.registers[1] = 0x00000001;
        computer.registers[2] = 0x00000003;
        computer.registers[3] = 0x00000007;

        computer.registers[4] = 0xFFFFFFFF_FFFFFFFF_80000000_00000000;
        computer.registers[5] = 0x80000000_00000000_00000000_00000000;

        computer.registers[6] = 0xAAAAAAAA_AAAAAAAA_AAAAAAAA_AAAAAAAB;
        computer.registers[7] = 0x0002FE7D;

        computer.registers[8] = 0xFF000000_00000000_00000000_00000000;

        computer.registers[9] = 0xFFFFFFFF_FFFFFFFF_FFFFFFFF_FFFFFFFF;
    }

    /*Execution*/
    {
        computer.queue(Instruction::RV32M(rv32m::Instruction::Mulh(
            // 0x00000000 = 0x00000000 * 0x00000000
            RegisterIdx(10),
            RegisterIdx(0),
            RegisterIdx(0),
        )));
        computer.queue(Instruction::RV32M(rv32m::Instruction::Mulh(
            // 0x00000000 = 0x00000001 * 0x00000001
            RegisterIdx(11),
            RegisterIdx(1),
            RegisterIdx(1),
        )));
        computer.queue(Instruction::RV32M(rv32m::Instruction::Mulh(
            // 0x00000000 = 0x00000003 * 0x00000007
            RegisterIdx(12),
            RegisterIdx(2),
            RegisterIdx(3),
        )));

        computer.queue(Instruction::RV32M(rv32m::Instruction::Mulh(
            // 0x00000000 = 0x00000000 * 0xFFFFFFFF_FFFFFFFF_80000000_00000000
            RegisterIdx(13),
            RegisterIdx(0),
            RegisterIdx(4),
        )));
        computer.queue(Instruction::RV32M(rv32m::Instruction::Mulh(
            // 0x00000000 = 0xFFFFFFFF_FFFFFFFF_80000000_00000000 * 0x00000000
            RegisterIdx(14),
            RegisterIdx(5),
            RegisterIdx(0),
        )));
        computer.queue(Instruction::RV32M(rv32m::Instruction::Mulh(
            // 0x00000000_00000000_40000000_00000000 = 0xFFFFFFFF_FFFFFFFF_80000000_00000000 * 0x80000000_00000000_00000000_00000000
            RegisterIdx(15),
            RegisterIdx(4),
            RegisterIdx(5),
        )));
        computer.queue(Instruction::RV32M(rv32m::Instruction::Mulh(
            // 0x00000000_00000000_40000000_00000000 = 0x80000000_00000000_00000000_00000000 * 0xFFFFFFFF_FFFFFFFF_80000000_00000000
            RegisterIdx(16),
            RegisterIdx(5),
            RegisterIdx(4),
        )));

        computer.queue(Instruction::RV32M(rv32m::Instruction::Mulh(
            // 0xFFFFFFFF_FFFFFFFF_FFFFFFFF_FFFF0081 = 0xAAAAAAAA_AAAAAAAA_AAAAAAAA_AAAAAAAB * 0x0002FE7D
            RegisterIdx(17),
            RegisterIdx(6),
            RegisterIdx(7),
        )));
        computer.queue(Instruction::RV32M(rv32m::Instruction::Mulh(
            // 0xFFFFFFFF_FFFFFFFF_FFFFFFFF_FFFF0081 = 0x0002FE7D * 0xAAAAAAAA_AAAAAAAA_AAAAAAAA_AAAAAAAB
            RegisterIdx(18),
            RegisterIdx(7),
            RegisterIdx(6),
        )));

        computer.queue(Instruction::RV32M(rv32m::Instruction::Mulh(
            // 0x00010000_00000000_00000000_00000000 = 0xFF000000_00000000_00000000_00000000 * 0xFF000000_00000000_00000000_00000000
            RegisterIdx(19),
            RegisterIdx(8),
            RegisterIdx(8),
        )));

        computer.queue(Instruction::RV32M(rv32m::Instruction::Mulh(
            // 0x00000000 = 0xFFFFFFFF_FFFFFFFF_FFFFFFFF_FFFFFFFF * 0xFFFFFFFF_FFFFFFFF_FFFFFFFF_FFFFFFFF
            RegisterIdx(20),
            RegisterIdx(9),
            RegisterIdx(9),
        )));
        computer.queue(Instruction::RV32M(rv32m::Instruction::Mulh(
            // 0xFFFFFFFF_FFFFFFFF_FFFFFFFF_FFFFFFFF = 0xFFFFFFFF_FFFFFFFF_FFFFFFFF_FFFFFFFF * 0x00000001
            RegisterIdx(21),
            RegisterIdx(9),
            RegisterIdx(1),
        )));
        computer.queue(Instruction::RV32M(rv32m::Instruction::Mulh(
            // 0xFFFFFFFF_FFFFFFFF_FFFFFFFF_FFFFFFFF = 0x00000001 * 0xFFFFFFFF_FFFFFFFF_FFFFFFFF_FFFFFFFF
            RegisterIdx(22),
            RegisterIdx(1),
            RegisterIdx(9),
        )));

        computer.run_all();
    }

    /*Printing*/
    {
        println!("x10: {:032X}", computer.registers[10]);
        println!("x11: {:032X}", computer.registers[11]);
        println!("x12: {:032X}", computer.registers[12]);
        println!();
        println!("x13: {:032X}", computer.registers[13]);
        println!("x14: {:032X}", computer.registers[14]);
        println!("x15: {:032X}", computer.registers[15]);
        println!("x16: {:032X}", computer.registers[16]);
        println!();
        println!("x17: {:032X}", computer.registers[17]);
        println!("x18: {:032X}", computer.registers[18]);
        println!();
        println!("x19: {:032X}", computer.registers[19]);
        println!();
        println!("x20: {:032X}", computer.registers[20]);
        println!("x21: {:032X}", computer.registers[21]);
        println!("x22: {:032X}", computer.registers[22]);
    }

    /*Assertions*/
    {
        assert_eq!(computer.get(RegisterIdx(10)), 0x00000000u128.into(), "x10");
        assert_eq!(computer.get(RegisterIdx(11)), 0x00000000u128.into(), "x11");
        assert_eq!(computer.get(RegisterIdx(12)), 0x00000000u128.into(), "x12");

        assert_eq!(computer.get(RegisterIdx(13)), 0x00000000u128.into(), "x13");
        assert_eq!(computer.get(RegisterIdx(14)), 0x00000000u128.into(), "x14");
        assert_eq!(
            computer.get(RegisterIdx(15)),
            0x00000000_00000000_40000000_00000000u128.into(),
            "x15"
        );
        assert_eq!(
            computer.get(RegisterIdx(16)),
            0x00000000_00000000_40000000_00000000u128.into(),
            "x16"
        );

        assert_eq!(
            computer.get(RegisterIdx(17)),
            0xFFFFFFFF_FFFFFFFF_FFFFFFFF_FFFF0081u128.into(),
            "x17"
        );
        assert_eq!(
            computer.get(RegisterIdx(18)),
            0xFFFFFFFF_FFFFFFFF_FFFFFFFF_FFFF0081u128.into(),
            "x18"
        );

        assert_eq!(
            computer.get(RegisterIdx(19)),
            0x00010000_00000000_00000000_00000000u128.into(),
            "x19"
        );

        assert_eq!(computer.get(RegisterIdx(20)), 0x00000000u128.into(), "x20");
        assert_eq!(
            computer.get(RegisterIdx(21)),
            0xFFFFFFFF_FFFFFFFF_FFFFFFFF_FFFFFFFFu128.into(),
            "x21"
        );
        assert_eq!(
            computer.get(RegisterIdx(22)),
            0xFFFFFFFF_FFFFFFFF_FFFFFFFF_FFFFFFFFu128.into(),
            "x22"
        );
    }
}

#[test]
fn mulhsu_128() {
    // Test inspired from https://github.com/riscv/riscv-tests/blob/master/isa/rv32um/mulh.S

    use crate::{
        instructions::{rv32m, Executable},
        Mult, NoFloats, Register128Bit,
    };

    let mut computer = Computer::<Register128Bit, Mult, NoFloats>::new(0);

    /*Initialization*/
    {
        computer.registers[1] = 0x00000001;
        computer.registers[2] = 0x00000003;
        computer.registers[3] = 0x00000007;

        computer.registers[4] = 0xFFFFFFFF_FFFFFFFF_80000000_00000000;
        computer.registers[5] = 0x80000000_00000000_00000000_00000000;

        computer.registers[6] = 0xAAAAAAAA_AAAAAAAA_AAAAAAAA_AAAAAAAB;
        computer.registers[7] = 0x0002FE7D;

        computer.registers[8] = 0xFF000000_00000000_00000000_00000000;

        computer.registers[9] = 0xFFFFFFFF_FFFFFFFF_FFFFFFFF_FFFFFFFF;
    }

    /*Execution*/
    {
        computer.queue(Instruction::RV32M(rv32m::Instruction::Mulhsu(
            // 0x00000000 = 0x00000000 * 0x00000000
            RegisterIdx(10),
            RegisterIdx(0),
            RegisterIdx(0),
        )));
        computer.queue(Instruction::RV32M(rv32m::Instruction::Mulhsu(
            // 0x00000000 = 0x00000001 * 0x00000001
            RegisterIdx(11),
            RegisterIdx(1),
            RegisterIdx(1),
        )));
        computer.queue(Instruction::RV32M(rv32m::Instruction::Mulhsu(
            // 0x00000000 = 0x00000003 * 0x00000007
            RegisterIdx(12),
            RegisterIdx(2),
            RegisterIdx(3),
        )));

        computer.queue(Instruction::RV32M(rv32m::Instruction::Mulhsu(
            // 0x00000000 = 0x00000000 * 0xFFFFFFFF_FFFFFFFF_80000000_00000000
            RegisterIdx(13),
            RegisterIdx(0),
            RegisterIdx(4),
        )));
        computer.queue(Instruction::RV32M(rv32m::Instruction::Mulhsu(
            // 0x00000000 = 0x80000000_00000000_00000000_00000000 * 0x00000000
            RegisterIdx(14),
            RegisterIdx(5),
            RegisterIdx(0),
        )));
        computer.queue(Instruction::RV32M(rv32m::Instruction::Mulhsu(
            // 0xFFFFFFFF_C0000000 = 0xFFFFFFFF_FFFFFFFF_80000000_00000000 * 0x80000000_00000000_00000000_00000000
            RegisterIdx(15),
            RegisterIdx(4),
            RegisterIdx(5),
        )));
        computer.queue(Instruction::RV32M(rv32m::Instruction::Mulhsu(
            // 0x80000000_40000000 = 0x80000000_00000000_00000000_00000000 * 0xFFFFFFFF_FFFFFFFF_80000000_00000000
            RegisterIdx(16),
            RegisterIdx(5),
            RegisterIdx(4),
        )));

        computer.queue(Instruction::RV32M(rv32m::Instruction::Mulhsu(
            // 0x0001FEFE = 0xAAAAAAAA_AAAAAAAA_AAAAAAAA_AAAAAAAB * 0x0002FE7D
            RegisterIdx(17),
            RegisterIdx(6),
            RegisterIdx(7),
        )));
        computer.queue(Instruction::RV32M(rv32m::Instruction::Mulhsu(
            // 0x0001FEFE = 0x0002FE7D * 0xAAAAAAAA_AAAAAAAA_AAAAAAAA_AAAAAAAB
            RegisterIdx(18),
            RegisterIdx(7),
            RegisterIdx(6),
        )));

        computer.queue(Instruction::RV32M(rv32m::Instruction::Mulhsu(
            // 0xFF010000_00000000_00000000_00000000 = 0xFF000000_00000000_00000000_00000000 * 0xFF000000_00000000_00000000_00000000
            RegisterIdx(19),
            RegisterIdx(8),
            RegisterIdx(8),
        )));

        computer.queue(Instruction::RV32M(rv32m::Instruction::Mulhsu(
            // 0xFFFFFFFF_FFFFFFFF_FFFFFFFF_FFFFFFFF = 0xFFFFFFFF_FFFFFFFF_FFFFFFFF_FFFFFFFF * 0xFFFFFFFF_FFFFFFFF_FFFFFFFF_FFFFFFFF
            RegisterIdx(20),
            RegisterIdx(9),
            RegisterIdx(9),
        )));
        computer.queue(Instruction::RV32M(rv32m::Instruction::Mulhsu(
            // 0xFFFFFFFF_FFFFFFFF_FFFFFFFF_FFFFFFFF = 0xFFFFFFFF_FFFFFFFF_FFFFFFFF_FFFFFFFF * 0x00000001
            RegisterIdx(21),
            RegisterIdx(9),
            RegisterIdx(1),
        )));
        computer.queue(Instruction::RV32M(rv32m::Instruction::Mulhsu(
            // 0x00000000 = 0x00000001 * 0xFFFFFFFF_FFFFFFFF_FFFFFFFF_FFFFFFFF
            RegisterIdx(22),
            RegisterIdx(1),
            RegisterIdx(9),
        )));

        computer.run_all();
    }

    /*Printing*/
    {
        println!("x10: {:032X}", computer.registers[10]);
        println!("x11: {:032X}", computer.registers[11]);
        println!("x12: {:032X}", computer.registers[12]);
        println!();
        println!("x13: {:032X}", computer.registers[13]);
        println!("x14: {:032X}", computer.registers[14]);
        println!("x15: {:032X}", computer.registers[15]);
        println!("x16: {:032X}", computer.registers[16]);
        println!();
        println!("x17: {:032X}", computer.registers[17]);
        println!("x18: {:032X}", computer.registers[18]);
        println!();
        println!("x19: {:032X}", computer.registers[19]);
        println!();
        println!("x20: {:032X}", computer.registers[20]);
        println!("x21: {:032X}", computer.registers[21]);
        println!("x22: {:032X}", computer.registers[22]);
    }

    /*Assertions*/
    {
        assert_eq!(computer.get(RegisterIdx(10)), 0x00000000u128.into(), "x10");
        assert_eq!(computer.get(RegisterIdx(11)), 0x00000000u128.into(), "x11");
        assert_eq!(computer.get(RegisterIdx(12)), 0x00000000u128.into(), "x12");

        assert_eq!(computer.get(RegisterIdx(13)), 0x00000000u128.into(), "x13");
        assert_eq!(computer.get(RegisterIdx(14)), 0x00000000u128.into(), "x14");
        assert_eq!(
            computer.get(RegisterIdx(15)),
            0xFFFFFFFF_FFFFFFFF_C0000000_00000000u128.into(),
            "x15"
        );
        assert_eq!(
            computer.get(RegisterIdx(16)),
            0x80000000_00000000_40000000_00000000u128.into(),
            "x16"
        );

        assert_eq!(
            computer.get(RegisterIdx(17)),
            0xFFFFFFFF_FFFFFFFF_FFFFFFFF_FFFF0081u128.into(),
            "x17"
        );
        assert_eq!(computer.get(RegisterIdx(18)), 0x0001FEFEu128.into(), "x18");

        assert_eq!(
            computer.get(RegisterIdx(19)),
            0xFF010000_00000000_00000000_00000000u128.into(),
            "x19"
        );

        assert_eq!(
            computer.get(RegisterIdx(20)),
            0xFFFFFFFF_FFFFFFFF_FFFFFFFF_FFFFFFFFu128.into(),
            "x20"
        );
        assert_eq!(
            computer.get(RegisterIdx(21)),
            0xFFFFFFFF_FFFFFFFF_FFFFFFFF_FFFFFFFFu128.into(),
            "x21"
        );
        assert_eq!(computer.get(RegisterIdx(22)), 0x00000000u128.into(), "x22");
    }
}

#[test]
fn mulhu_128() {
    // Test inspired from https://github.com/riscv/riscv-tests/blob/master/isa/rv32um/mulh.S

    use crate::{
        instructions::{rv32m, Executable},
        Mult, NoFloats, Register128Bit,
    };

    let mut computer = Computer::<Register128Bit, Mult, NoFloats>::new(0);

    /*Initialization*/
    {
        computer.registers[1] = 0x00000001;
        computer.registers[2] = 0x00000003;
        computer.registers[3] = 0x00000007;

        computer.registers[4] = 0xFFFFFFFF_FFFFFFFF_80000000_00000000;
        computer.registers[5] = 0x80000000_00000000_00000000_00000000;

        computer.registers[6] = 0xAAAAAAAA_AAAAAAAA_AAAAAAAA_AAAAAAAB;
        computer.registers[7] = 0x0002FE7D;

        computer.registers[8] = 0xFF000000_00000000_00000000_00000000;

        computer.registers[9] = 0xFFFFFFFF_FFFFFFFF_FFFFFFFF_FFFFFFFF;
    }

    /*Execution*/
    {
        computer.queue(Instruction::RV32M(rv32m::Instruction::Mulhu(
            // 0x00000000 = 0x00000000 * 0x00000000
            RegisterIdx(10),
            RegisterIdx(0),
            RegisterIdx(0),
        )));
        computer.queue(Instruction::RV32M(rv32m::Instruction::Mulhu(
            // 0x00000000 = 0x00000001 * 0x00000001
            RegisterIdx(11),
            RegisterIdx(1),
            RegisterIdx(1),
        )));
        computer.queue(Instruction::RV32M(rv32m::Instruction::Mulhu(
            // 0x00000000 = 0x00000003 * 0x00000007
            RegisterIdx(12),
            RegisterIdx(2),
            RegisterIdx(3),
        )));

        computer.queue(Instruction::RV32M(rv32m::Instruction::Mulhu(
            // 0x00000000 = 0x00000000 * 0xFFFFFFFF_FFFF8000
            RegisterIdx(13),
            RegisterIdx(0),
            RegisterIdx(4),
        )));
        computer.queue(Instruction::RV32M(rv32m::Instruction::Mulhu(
            // 0x00000000 = 0xFFFFFFFF_80000000 * 0x00000000
            RegisterIdx(14),
            RegisterIdx(5),
            RegisterIdx(0),
        )));
        computer.queue(Instruction::RV32M(rv32m::Instruction::Mulhu(
            // 0x7FFFFFFF_FFFFFFFF_C0000000_00000000 = 0xFFFFFFFF_FFFFFFFF_80000000_00000000 * 0x80000000_00000000_00000000_00000000
            RegisterIdx(15),
            RegisterIdx(4),
            RegisterIdx(5),
        )));
        computer.queue(Instruction::RV32M(rv32m::Instruction::Mulhu(
            // 0x7FFFFFFF_FFFFFFFF_C0000000_00000000 = 0x80000000_00000000_00000000_00000000 * 0xFFFFFFFF_FFFFFFFF_80000000_00000000
            RegisterIdx(16),
            RegisterIdx(5),
            RegisterIdx(4),
        )));

        computer.queue(Instruction::RV32M(rv32m::Instruction::Mulhu(
            // 0x0001FEFE = 0xAAAAAAAA_AAAAAAAA_AAAAAAAA_AAAAAAAB * 0x0002FE7D
            RegisterIdx(17),
            RegisterIdx(6),
            RegisterIdx(7),
        )));
        computer.queue(Instruction::RV32M(rv32m::Instruction::Mulhu(
            // 0x0001FEFE = 0x0002FE7D * 0xAAAAAAAA_AAAAAAAA_AAAAAAAA_AAAAAAAB
            RegisterIdx(18),
            RegisterIdx(7),
            RegisterIdx(6),
        )));

        computer.queue(Instruction::RV32M(rv32m::Instruction::Mulhu(
            // 0xFE010000_00000000_00000000_00000000 = 0xFF000000_00000000_00000000_00000000 * 0xFF000000_00000000_00000000_00000000
            RegisterIdx(19),
            RegisterIdx(8),
            RegisterIdx(8),
        )));

        computer.queue(Instruction::RV32M(rv32m::Instruction::Mulhu(
            // 0xFFFFFFFF_FFFFFFFF_FFFFFFFF_FFFFFFFE = 0xFFFFFFFF_FFFFFFFF_FFFFFFFF_FFFFFFFF * 0xFFFFFFFF_FFFFFFFF_FFFFFFFF_FFFFFFFF
            RegisterIdx(20),
            RegisterIdx(9),
            RegisterIdx(9),
        )));
        computer.queue(Instruction::RV32M(rv32m::Instruction::Mulhu(
            // 0x00000000 = 0xFFFFFFFF_FFFFFFFF_FFFFFFFF_FFFFFFFF * 0x00000001
            RegisterIdx(21),
            RegisterIdx(9),
            RegisterIdx(1),
        )));
        computer.queue(Instruction::RV32M(rv32m::Instruction::Mulhu(
            // 0x00000000 = 0x00000001 * 0xFFFFFFFF_FFFFFFFF_FFFFFFFF_FFFFFFFF
            RegisterIdx(22),
            RegisterIdx(1),
            RegisterIdx(9),
        )));

        computer.run_all();
    }

    /*Printing*/
    {
        println!("x10: {:032X}", computer.registers[10]);
        println!("x11: {:032X}", computer.registers[11]);
        println!("x12: {:032X}", computer.registers[12]);
        println!();
        println!("x13: {:032X}", computer.registers[13]);
        println!("x14: {:032X}", computer.registers[14]);
        println!("x15: {:032X}", computer.registers[15]);
        println!("x16: {:032X}", computer.registers[16]);
        println!();
        println!("x17: {:032X}", computer.registers[17]);
        println!("x18: {:032X}", computer.registers[18]);
        println!();
        println!("x19: {:032X}", computer.registers[19]);
        println!();
        println!("x20: {:032X}", computer.registers[20]);
        println!("x21: {:032X}", computer.registers[21]);
        println!("x22: {:032X}", computer.registers[22]);
    }

    /*Assertions*/
    {
        assert_eq!(computer.get(RegisterIdx(10)), 0x00000000u128.into());
        assert_eq!(computer.get(RegisterIdx(11)), 0x00000000u128.into());
        assert_eq!(computer.get(RegisterIdx(12)), 0x00000000u128.into());

        assert_eq!(computer.get(RegisterIdx(13)), 0x00000000u128.into());
        assert_eq!(computer.get(RegisterIdx(14)), 0x00000000u128.into());
        assert_eq!(
            computer.get(RegisterIdx(15)),
            0x7FFFFFFF_FFFFFFFF_C0000000_00000000u128.into()
        );
        assert_eq!(
            computer.get(RegisterIdx(16)),
            0x7FFFFFFF_FFFFFFFF_C0000000_00000000u128.into()
        );

        assert_eq!(computer.get(RegisterIdx(17)), 0x0001FEFEu128.into());
        assert_eq!(computer.get(RegisterIdx(18)), 0x0001FEFEu128.into());

        assert_eq!(
            computer.get(RegisterIdx(19)),
            0xFE010000_00000000_00000000_00000000u128.into()
        );

        assert_eq!(
            computer.get(RegisterIdx(20)),
            0xFFFFFFFF_FFFFFFFF_FFFFFFFF_FFFFFFFEu128.into()
        );
        assert_eq!(computer.get(RegisterIdx(21)), 0x00000000u128.into());
        assert_eq!(computer.get(RegisterIdx(22)), 0x00000000u128.into());
    }
}
