use super::extend;
use crate::{Computer, ImmediateLow, Instruction, RegisterIdx};

test_all! { xor, || {
    let mut computer = Computer::<R, M, F>::new(0);
    computer.queue(Instruction::Base(base::Addi(
        RegisterIdx(1),
        RegisterIdx(0),
        ImmediateLow::new(0xAAA).unwrap(),
    )));
    computer.queue(Instruction::Base(base::Addi(
        RegisterIdx(2),
        RegisterIdx(0),
        ImmediateLow::new(0x555).unwrap(),
    )));
    computer.queue(Instruction::Base(base::Addi(
        RegisterIdx(3),
        RegisterIdx(0),
        ImmediateLow::new(0x666).unwrap(),
    )));
    computer.queue(Instruction::Base(base::Xor(
        RegisterIdx(4),
        RegisterIdx(1),
        RegisterIdx(2),
    )));
    computer.queue(Instruction::Base(base::Xor(
        RegisterIdx(5),
        RegisterIdx(1),
        RegisterIdx(3),
    )));
    computer.run_all();
    assert_eq!(computer.get(RegisterIdx(4)), extend(0xFFFF_FFFF));
    assert_eq!(computer.get(RegisterIdx(5)), extend(0xFFFF_FCCC));
}}

test_all! { xori, || {
    let mut computer = Computer::<R, M, F>::new(0);
    computer.queue(Instruction::Base(base::Addi(
        RegisterIdx(1),
        RegisterIdx(0),
        ImmediateLow::new(0xAAA).unwrap(),
    )));
    computer.queue(Instruction::Base(base::Xori(
        RegisterIdx(3),
        RegisterIdx(1),
        ImmediateLow::new(0x555).unwrap(),
    )));
    computer.run_all();
    assert_eq!(computer.get(RegisterIdx(3)), extend(0xFFFF_FFFF));
}}

test_all! { or, || {
    let mut computer = Computer::<R, M, F>::new(0);
    computer.queue(Instruction::Base(base::Addi(
        RegisterIdx(1),
        RegisterIdx(0),
        ImmediateLow::new(0xAAA).unwrap(),
    )));
    computer.queue(Instruction::Base(base::Addi(
        RegisterIdx(2),
        RegisterIdx(0),
        ImmediateLow::new(0x555).unwrap(),
    )));
    computer.queue(Instruction::Base(base::Or(
        RegisterIdx(3),
        RegisterIdx(1),
        RegisterIdx(2),
    )));
    computer.run_all();
    assert_eq!(computer.get(RegisterIdx(3)), extend(0xFFFF_FFFF));
}}

test_all! { ori, || {
    let mut computer = Computer::<R, M, F>::new(0);
    computer.queue(Instruction::Base(base::Addi(
        RegisterIdx(1),
        RegisterIdx(0),
        ImmediateLow::new(0xAAA).unwrap(),
    )));
    computer.queue(Instruction::Base(base::Ori(
        RegisterIdx(3),
        RegisterIdx(1),
        ImmediateLow::new(0x555).unwrap(),
    )));
    computer.run_all();
    assert_eq!(computer.get(RegisterIdx(3)), extend(0xFFFF_FFFF));
}}

test_all! { and, || {
    let mut computer = Computer::<R, M, F>::new(0);
    computer.queue(Instruction::Base(base::Addi(
        RegisterIdx(1),
        RegisterIdx(0),
        ImmediateLow::new(0xAAA).unwrap(),
    )));
    computer.queue(Instruction::Base(base::Addi(
        RegisterIdx(2),
        RegisterIdx(0),
        ImmediateLow::new(0x666).unwrap(),
    )));
    computer.queue(Instruction::Base(base::And(
        RegisterIdx(3),
        RegisterIdx(1),
        RegisterIdx(2),
    )));
    computer.run_all();
    assert_eq!(computer.get(RegisterIdx(3)), extend(0x0000_0222));
}}

test_all! { andi, || {
    let mut computer = Computer::<R, M, F>::new(0);
    computer.queue(Instruction::Base(base::Addi(
        RegisterIdx(1),
        RegisterIdx(0),
        ImmediateLow::new(0xAAA).unwrap(),
    )));
    computer.queue(Instruction::Base(base::Andi(
        RegisterIdx(3),
        RegisterIdx(1),
        ImmediateLow::new(0x666).unwrap(),
    )));
    computer.run_all();
    assert_eq!(computer.get(RegisterIdx(3)), extend(0x0000_0222));
}}
