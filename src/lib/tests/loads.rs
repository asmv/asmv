use crate::{Computer, ImmediateLow, Instruction, RegisterIdx};

use super::{extend, endian};

test_all! { loads, || {
    let mut mem = [0; 20];
    for (i, m) in mem.iter_mut().enumerate() {
        *m = i as u8;
    }
    let mut computer = Computer::<R, M, F>::with_mem(&mem[..]);
    computer.queue(Instruction::Base(base::Lb(
        RegisterIdx(1),
        RegisterIdx(0),
        ImmediateLow::new(0x000).unwrap(),
    )));
    computer.queue(Instruction::Base(base::Lbu(
        RegisterIdx(2),
        RegisterIdx(0),
        ImmediateLow::new(0x000).unwrap(),
    )));
    computer.queue(Instruction::Base(base::Lh(
        RegisterIdx(3),
        RegisterIdx(0),
        ImmediateLow::new(0x004).unwrap(),
    )));
    computer.queue(Instruction::Base(base::Lhu(
        RegisterIdx(4),
        RegisterIdx(0),
        ImmediateLow::new(0x004).unwrap(),
    )));
    computer.queue(Instruction::Base(base::Lw(
        RegisterIdx(5),
        RegisterIdx(0),
        ImmediateLow::new(0x008).unwrap(),
    )));
    computer.run_all();
    assert_eq!(computer.get(RegisterIdx(1)), endian(extend(0x0000_0000)));
    assert_eq!(computer.get(RegisterIdx(2)), endian(extend(0x0000_0000)));
    assert_eq!(computer.get(RegisterIdx(3)), endian(extend(0x0000_0405)));
    assert_eq!(computer.get(RegisterIdx(4)), endian(extend(0x0000_0405)));
    assert_eq!(computer.get(RegisterIdx(5)), endian(extend(0x0809_0A0B)));
}}

test_all! { loads_signed, || {
    let mut mem = [0; 20];
    for (i, m) in mem.iter_mut().enumerate() {
        *m = 0x80 + i as u8;
    }
    let mut computer = Computer::<R, M, F>::with_mem(&mem[..]);
    computer.queue(Instruction::Base(base::Lb(
        RegisterIdx(1),
        RegisterIdx(0),
        ImmediateLow::new(0x000).unwrap(),
    )));
    computer.queue(Instruction::Base(base::Lbu(
        RegisterIdx(2),
        RegisterIdx(0),
        ImmediateLow::new(0x000).unwrap(),
    )));
    computer.queue(Instruction::Base(base::Lh(
        RegisterIdx(3),
        RegisterIdx(0),
        ImmediateLow::new(0x004).unwrap(),
    )));
    computer.queue(Instruction::Base(base::Lhu(
        RegisterIdx(4),
        RegisterIdx(0),
        ImmediateLow::new(0x004).unwrap(),
    )));
    computer.queue(Instruction::Base(base::Lw(
        RegisterIdx(5),
        RegisterIdx(0),
        ImmediateLow::new(0x008).unwrap(),
    )));
    computer.run_all();
    assert_eq!(computer.get(RegisterIdx(1)), endian(extend(0xFFFF_FF80)));
    assert_eq!(computer.get(RegisterIdx(2)), endian(extend(0x0000_0080)));
    assert_eq!(computer.get(RegisterIdx(3)), endian(extend(0xFFFF_8485)));
    assert_eq!(computer.get(RegisterIdx(4)), endian(extend(0x0000_8485)));
    assert_eq!(computer.get(RegisterIdx(5)), endian(extend(0x8889_8A8B)));
}}
