use crate::{Computer, ImmediateLow, Instruction, Register, RegisterIdx};
use num_traits::AsPrimitive;

fn negative<R: Register>() -> R::Signed {
    R::Signed::from(0xFFFF_FFE0_u32 as i32)
}

// Shift left

test_all! { shift_left, || {
    let mut computer = Computer::<R, M, F>::new(0);
    computer.queue(Instruction::Base(base::Addi(
        RegisterIdx(1),
        RegisterIdx(0),
        ImmediateLow::new(0x2).unwrap(),
    )));
    computer.queue(Instruction::Base(base::Addi(
        RegisterIdx(2),
        RegisterIdx(0),
        ImmediateLow::new(0xFE0).unwrap(),
    )));
    computer.queue(Instruction::Base(base::Sll(
        RegisterIdx(3),
        RegisterIdx(1),
        RegisterIdx(1),
    )));
    computer.queue(Instruction::Base(base::Sll(
        RegisterIdx(4),
        RegisterIdx(2),
        RegisterIdx(1),
    )));
    computer.queue(Instruction::Base(base::Slli(
        RegisterIdx(5),
        RegisterIdx(2),
        ImmediateLow::new(2).unwrap(),
    )));
    computer.run_all();

    assert_eq!(computer.get(RegisterIdx(3)), (2_u32 << 2).into());
    assert_eq!(computer.get(RegisterIdx(4)), negative::<R>().as_() << 2);
    assert_eq!(computer.get(RegisterIdx(5)), negative::<R>().as_() << 2);
}}

test_rv64i! { shift_left, || {
    let mut computer = Computer::<R, M, F>::new(0);
    computer.queue(Instruction::Base(base::Addi(
        RegisterIdx(1),
        RegisterIdx(0),
        ImmediateLow::new(0x2).unwrap(),
    )));
    computer.queue(Instruction::Base(base::Addi(
        RegisterIdx(2),
        RegisterIdx(0),
        ImmediateLow::new(0xFE0).unwrap(),
    )));
    computer.queue(Instruction::RV64I(rv64i::Sllw(
        RegisterIdx(3),
        RegisterIdx(1),
        RegisterIdx(1),
    )));
    computer.queue(Instruction::RV64I(rv64i::Sllw(
        RegisterIdx(4),
        RegisterIdx(2),
        RegisterIdx(1),
    )));
    computer.queue(Instruction::RV64I(rv64i::Slliw(
        RegisterIdx(5),
        RegisterIdx(2),
        ImmediateLow::new(2).unwrap(),
    )));
    computer.run_all();

    assert_eq!(computer.get(RegisterIdx(3)), (2_u32 << 2).into());
    assert_eq!(computer.get(RegisterIdx(4)), negative::<R>().as_() << 2);
    assert_eq!(computer.get(RegisterIdx(5)), negative::<R>().as_() << 2);
}}

test_rv128i! { shift_left, || {
    let mut computer = Computer::<R, M, F>::new(0);
    computer.queue(Instruction::Base(base::Addi(
        RegisterIdx(1),
        RegisterIdx(0),
        ImmediateLow::new(0x2).unwrap(),
    )));
    computer.queue(Instruction::Base(base::Addi(
        RegisterIdx(2),
        RegisterIdx(0),
        ImmediateLow::new(0xFE0).unwrap(),
    )));
    computer.queue(Instruction::RV128I(rv128i::Slld(
        RegisterIdx(3),
        RegisterIdx(1),
        RegisterIdx(1),
    )));
    computer.queue(Instruction::RV128I(rv128i::Slld(
        RegisterIdx(4),
        RegisterIdx(2),
        RegisterIdx(1),
    )));
    computer.queue(Instruction::RV128I(rv128i::Sllid(
        RegisterIdx(5),
        RegisterIdx(2),
        ImmediateLow::new(2).unwrap(),
    )));
    computer.run_all();

    assert_eq!(computer.get(RegisterIdx(3)), (2_u32 << 2).into());
    assert_eq!(computer.get(RegisterIdx(4)), negative::<R>().as_() << 2);
    assert_eq!(computer.get(RegisterIdx(5)), negative::<R>().as_() << 2);
}}

// Shift right logical

test_all! { shift_right_logical, || {
    let mut computer = Computer::<R, M, F>::new(0);
    computer.queue(Instruction::Base(base::Addi(
        RegisterIdx(1),
        RegisterIdx(0),
        ImmediateLow::new(0x2).unwrap(),
    )));
    computer.queue(Instruction::Base(base::Addi(
        RegisterIdx(2),
        RegisterIdx(0),
        ImmediateLow::new(0xFE0).unwrap(),
    )));
    computer.queue(Instruction::Base(base::Addi(
        RegisterIdx(3),
        RegisterIdx(0),
        ImmediateLow::new(0x20).unwrap(),
    )));

    computer.queue(Instruction::Base(base::Srl(
        RegisterIdx(10),
        RegisterIdx(3),
        RegisterIdx(1),
    )));
    computer.queue(Instruction::Base(base::Srl(
        RegisterIdx(11),
        RegisterIdx(2),
        RegisterIdx(1),
    )));
    computer.queue(Instruction::Base(base::Srli(
        RegisterIdx(12),
        RegisterIdx(2),
        ImmediateLow::new(2).unwrap(),
    )));
    computer.run_all();

    assert_eq!(computer.get(RegisterIdx(10)), 0x8u32.into());
    assert_eq!(computer.get(RegisterIdx(11)), negative::<R>().as_() >> 2);
    assert_eq!(computer.get(RegisterIdx(12)), negative::<R>().as_() >> 2);
}}

test_rv64i! { shift_right_logical, || {
    let mut computer = Computer::<R, M, F>::new(0);
    computer.queue(Instruction::Base(base::Addi(
        RegisterIdx(1),
        RegisterIdx(0),
        ImmediateLow::new(0x2).unwrap(),
    )));
    computer.queue(Instruction::Base(base::Addi(
        RegisterIdx(2),
        RegisterIdx(0),
        ImmediateLow::new(0xFE0).unwrap(),
    )));
    computer.queue(Instruction::Base(base::Addi(
        RegisterIdx(3),
        RegisterIdx(0),
        ImmediateLow::new(0x20).unwrap(),
    )));

    computer.queue(Instruction::RV64I(rv64i::Srlw(
        RegisterIdx(10),
        RegisterIdx(3),
        RegisterIdx(1),
    )));
    computer.queue(Instruction::RV64I(rv64i::Srlw(
        RegisterIdx(11),
        RegisterIdx(2),
        RegisterIdx(1),
    )));
    computer.queue(Instruction::RV64I(rv64i::Srliw(
        RegisterIdx(12),
        RegisterIdx(2),
        ImmediateLow::new(2).unwrap(),
    )));
    computer.run_all();

    assert_eq!(computer.get(RegisterIdx(10)), 0x8u32.into());
    assert_eq!(computer.get(RegisterIdx(11)), (-31i32 as u32 >> 2).into());
    assert_eq!(computer.get(RegisterIdx(12)), (-31i32 as u32 >> 2).into());
}}

test_rv128i! { shift_right_logical, || {
    let mut computer = Computer::<R, M, F>::new(0);
    computer.queue(Instruction::Base(base::Addi(
        RegisterIdx(1),
        RegisterIdx(0),
        ImmediateLow::new(0x2).unwrap(),
    )));
    computer.queue(Instruction::Base(base::Addi(
        RegisterIdx(2),
        RegisterIdx(0),
        ImmediateLow::new(0xFE0).unwrap(),
    )));
    computer.queue(Instruction::Base(base::Addi(
        RegisterIdx(3),
        RegisterIdx(0),
        ImmediateLow::new(0x20).unwrap(),
    )));

    computer.queue(Instruction::RV128I(rv128i::Srld(
        RegisterIdx(10),
        RegisterIdx(3),
        RegisterIdx(1),
    )));
    computer.queue(Instruction::RV128I(rv128i::Srld(
        RegisterIdx(11),
        RegisterIdx(2),
        RegisterIdx(1),
    )));
    computer.queue(Instruction::RV128I(rv128i::Srlid(
        RegisterIdx(12),
        RegisterIdx(2),
        ImmediateLow::new(2).unwrap(),
    )));
    computer.run_all();

    assert_eq!(computer.get(RegisterIdx(10)), 0x8u32.into());
    assert_eq!(computer.get(RegisterIdx(11)), (-31i64 as u64 >> 2).into());
    assert_eq!(computer.get(RegisterIdx(12)), (-31i64 as u64 >> 2).into());
}}

// Shift right arithmetic

test_all! { shift_right_arithmetic, || {
    let mut computer = Computer::<R, M, F>::new(0);
    computer.queue(Instruction::Base(base::Addi(
        RegisterIdx(1),
        RegisterIdx(0),
        ImmediateLow::new(0x2).unwrap(),
    )));
    computer.queue(Instruction::Base(base::Addi(
        RegisterIdx(2),
        RegisterIdx(0),
        ImmediateLow::new(0xFE0).unwrap(),
    )));
    computer.queue(Instruction::Base(base::Addi(
        RegisterIdx(3),
        RegisterIdx(0),
        ImmediateLow::new(0x20).unwrap(),
    )));

    computer.queue(Instruction::Base(base::Sra(
        RegisterIdx(10),
        RegisterIdx(3),
        RegisterIdx(1),
    )));
    computer.queue(Instruction::Base(base::Sra(
        RegisterIdx(11),
        RegisterIdx(2),
        RegisterIdx(1),
    )));
    computer.queue(Instruction::Base(base::Srai(
        RegisterIdx(12),
        RegisterIdx(2),
        ImmediateLow::new(2).unwrap(),
    )));
    computer.run_all();

    assert_eq!(computer.get(RegisterIdx(10)), 0x8u32.into());
    assert_eq!(computer.get(RegisterIdx(11)), (negative::<R>() >> 2).as_());
    assert_eq!(computer.get(RegisterIdx(12)), (negative::<R>() >> 2).as_());
}}

test_rv64i! { shift_right_arithmetic, || {
    let mut computer = Computer::<R, M, F>::new(0);
    computer.queue(Instruction::Base(base::Addi(
        RegisterIdx(1),
        RegisterIdx(0),
        ImmediateLow::new(0x2).unwrap(),
    )));
    computer.queue(Instruction::Base(base::Addi(
        RegisterIdx(2),
        RegisterIdx(0),
        ImmediateLow::new(0xFE0).unwrap(),
    )));
    computer.queue(Instruction::Base(base::Addi(
        RegisterIdx(3),
        RegisterIdx(0),
        ImmediateLow::new(0x20).unwrap(),
    )));

    computer.queue(Instruction::RV64I(rv64i::Sraw(
        RegisterIdx(10),
        RegisterIdx(3),
        RegisterIdx(1),
    )));
    computer.queue(Instruction::RV64I(rv64i::Sraw(
        RegisterIdx(11),
        RegisterIdx(2),
        RegisterIdx(1),
    )));
    computer.queue(Instruction::RV64I(rv64i::Sraiw(
        RegisterIdx(12),
        RegisterIdx(2),
        ImmediateLow::new(2).unwrap(),
    )));
    computer.run_all();

    assert_eq!(computer.get(RegisterIdx(10)), 0x8u32.into());
    assert_eq!(computer.get(RegisterIdx(11)), (negative::<R>() >> 2).as_());
    assert_eq!(computer.get(RegisterIdx(12)), (negative::<R>() >> 2).as_());
}}

test_rv128i! { shift_right_arithmetic, || {
    let mut computer = Computer::<R, M, F>::new(0);
    computer.queue(Instruction::Base(base::Addi(
        RegisterIdx(1),
        RegisterIdx(0),
        ImmediateLow::new(0x2).unwrap(),
    )));
    computer.queue(Instruction::Base(base::Addi(
        RegisterIdx(2),
        RegisterIdx(0),
        ImmediateLow::new(0xFE0).unwrap(),
    )));
    computer.queue(Instruction::Base(base::Addi(
        RegisterIdx(3),
        RegisterIdx(0),
        ImmediateLow::new(0x20).unwrap(),
    )));

    computer.queue(Instruction::RV128I(rv128i::Srad(
        RegisterIdx(10),
        RegisterIdx(3),
        RegisterIdx(1),
    )));
    computer.queue(Instruction::RV128I(rv128i::Srad(
        RegisterIdx(11),
        RegisterIdx(2),
        RegisterIdx(1),
    )));
    computer.queue(Instruction::RV128I(rv128i::Sraid(
        RegisterIdx(12),
        RegisterIdx(2),
        ImmediateLow::new(2).unwrap(),
    )));
    computer.run_all();

    assert_eq!(computer.get(RegisterIdx(10)), 0x8u32.into());
    assert_eq!(computer.get(RegisterIdx(11)), (negative::<R>() >> 2).as_());
    assert_eq!(computer.get(RegisterIdx(12)), (negative::<R>() >> 2).as_());
}}
