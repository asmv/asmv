use crate::{Computer, ImmediateLow, Instruction, Register, RegisterIdx};
use num_traits::AsPrimitive;

macro_rules! test_all {
    ($id:ident, $f:expr) => {
        paste::item! {
            fn [<$id _test>]<R: $crate::Register, M: $crate::ExtensionMult, F: $crate::FRegister>()
            where
                Computer<R, M, F>: $crate::instructions::Executable
            {
                #[allow(unused_imports)]
                use crate::{instructions::{base, Executable}, Mult, NoMult};
                ($f)()
            }

            #[test]
            fn [<$id _rv32i_32>]() {
                [<$id _test>]::<$crate::Register32Bit, $crate::NoMult, $crate::NoFloats>();
            }
            #[test]
            fn [<$id _rv32i_64>]() {
                [<$id _test>]::<$crate::Register64Bit, $crate::NoMult, $crate::NoFloats>();
            }
            #[test]
            fn [<$id _rv32i_128>]() {
                [<$id _test>]::<$crate::Register128Bit, $crate::NoMult, $crate::NoFloats>();
            }
        }
    };
}

macro_rules! test_rv64i {
    ($id:ident, $f:expr) => {
        paste::item! {
            fn [<$id _rv64i_test>]<R: $crate::Register, M: $crate::ExtensionMult, F: $crate::FRegister>()
            where
                Computer<R, M, F>: $crate::instructions::Executable
            {
                #[allow(unused_imports)]
                use crate::{instructions::{base, rv64i, Executable}, Mult, NoMult};
                ($f)()
            }

            #[test]
            fn [<$id _rv64i_64>]() {
                [<$id _rv64i_test>]::<$crate::Register64Bit, $crate::NoMult, $crate::NoFloats>();
            }
            #[test]
            fn [<$id _rv64i_128>]() {
                [<$id _rv64i_test>]::<$crate::Register128Bit, $crate::NoMult, $crate::NoFloats>();
            }
        }
    };
}

macro_rules! test_rv128i {
    ($id:ident, $f:expr) => {
        paste::item! {
            fn [<$id _rv128i_test>]<R: $crate::Register+std::convert::From<u64>, M: $crate::ExtensionMult, F: $crate::FRegister>()
            where
                Computer<R, M, F>: $crate::instructions::Executable
            {
                #[allow(unused_imports)]
                use crate::{instructions::{base, rv64i, rv128i, Executable}, Mult, NoMult};
                ($f)()
            }

            #[test]
            fn [<$id _rv128i_128>]() {
                [<$id _rv128i_test>]::<$crate::Register128Bit, $crate::NoMult, $crate::NoFloats>();
            }
        }
    };
}

mod arithmetic;
mod comparisons;
mod loads;
mod logical;
mod muldiv;
mod pc;
mod shifts;
mod stores;

/*
Test file template
******************

use super::extend;
use crate::{Computer, ImmediateLow, Instruction, RegisterIdx};

test_all! { identifier, || {
    let mut computer = Computer::new(0);
    computer.queue(Instruction::Base(base::Instruction::Addi(
        RegisterIdx(10),
        RegisterIdx(0),
        ImmediateLow::new(0).unwrap(),
    )));

    computer.queue(Instruction::Base(base::Instruction::Add(
        RegisterIdx(1),
        RegisterIdx(0),
        RegisterIdx(0),
    )));
    computer.run_all();

    assert_eq!(computer.get(RegisterIdx(1)), extend(0));
}}

*/

fn extend<R: Register>(v: u32) -> R {
    R::Signed::from(v as i32).as_()
}

fn endian<T: num_traits::PrimInt>(v: T) -> T {
    T::from_be(v)
}

test_all! { compiles, || {
    let mut computer = Computer::<R, M, F>::new(0);
    computer.queue(Instruction::Base(base::Instruction::Addi(
        RegisterIdx(0),
        RegisterIdx(0),
        ImmediateLow::new(0).unwrap(),
    )));
    computer.run_all();
    assert_eq!(computer.get(RegisterIdx(0)), 0_u32.into());
}}

test_rv64i! { compiles, || {
    let mut computer = Computer::<R, M, F>::new(0);
    computer.queue(Instruction::Base(base::Instruction::Addi(
        RegisterIdx(0),
        RegisterIdx(0),
        ImmediateLow::new(0).unwrap(),
    )));
    computer.run_all();
    assert_eq!(computer.get(RegisterIdx(0)), 0_u32.into());
}}

test_rv128i! { compiles, || {
    let mut computer = Computer::<R, M, F>::new(0);
    computer.queue(Instruction::Base(base::Instruction::Addi(
        RegisterIdx(0),
        RegisterIdx(0),
        ImmediateLow::new(0).unwrap(),
    )));
    computer.run_all();
    assert_eq!(computer.get(RegisterIdx(0)), 0_u32.into());
}}
