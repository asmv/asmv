use super::extend;
use crate::{Computer, ImmediateHigh, ImmediateLow, Instruction, RegisterIdx};

test_all! { set_constant, || {
    let mut computer = Computer::<R, M, F>::new(0);
    computer.queue(Instruction::Base(base::Lui(
        RegisterIdx(1),
        ImmediateHigh::new(0xDEADC).unwrap(),
    )));
    computer.queue(Instruction::Base(base::Addi(
        RegisterIdx(1),
        RegisterIdx(1),
        ImmediateLow::new(0xEEF).unwrap(),
    )));
    computer.queue(Instruction::Base(base::Addi(
        RegisterIdx(2),
        RegisterIdx(2),
        ImmediateLow::new(0xEEF).unwrap(),
    )));
    computer.queue(Instruction::Base(base::Addi(
        RegisterIdx(3),
        RegisterIdx(3),
        ImmediateLow::new(0x0EF).unwrap(),
    )));
    computer.queue(Instruction::Base(base::Lui(
        RegisterIdx(4),
        ImmediateHigh::new(0xDEADC).unwrap(),
    )));
    computer.queue(Instruction::Base(base::Lui(
        RegisterIdx(5),
        ImmediateHigh::new(0xDEADC).unwrap(),
    )));
    computer.queue(Instruction::Base(base::Addi(
        RegisterIdx(5),
        RegisterIdx(5),
        ImmediateLow::new(0x0EF).unwrap(),
    )));
    computer.run_all();
    assert_eq!(computer.get(RegisterIdx(1)), extend(0xDEADBEEF));
    assert_eq!(computer.get(RegisterIdx(2)), extend(0xFFFFFEEF));
    assert_eq!(computer.get(RegisterIdx(3)), extend(0x000000EF));
    assert_eq!(computer.get(RegisterIdx(4)), extend(0xDEADC000));
    assert_eq!(computer.get(RegisterIdx(5)), extend(0xDEADC0EF));
}}

test_all! { arithmetic, || {
    let mut computer = Computer::<R, M, F>::new(0);

    computer.queue(Instruction::Base(base::Addi(
        RegisterIdx(1),
        RegisterIdx(0),
        ImmediateLow::new(0x2).unwrap(),
    )));
    computer.queue(Instruction::Base(base::Addi(
        RegisterIdx(2),
        RegisterIdx(1),
        ImmediateLow::new(0xFFE).unwrap(),
    )));
    computer.queue(Instruction::Base(base::Add(
        RegisterIdx(3),
        RegisterIdx(1),
        RegisterIdx(1),
    )));
    computer.queue(Instruction::Base(base::Sub(
        RegisterIdx(4),
        RegisterIdx(1),
        RegisterIdx(1),
    )));
    computer.queue(Instruction::Base(base::Sub(
        RegisterIdx(5),
        RegisterIdx(1),
        RegisterIdx(3),
    )));
    computer.run_all();
    assert_eq!(computer.get(RegisterIdx(1)), extend(2));
    assert_eq!(computer.get(RegisterIdx(2)), extend(0));
    assert_eq!(computer.get(RegisterIdx(3)), extend(4));
    assert_eq!(computer.get(RegisterIdx(4)), extend(0));
    assert_eq!(computer.get(RegisterIdx(5)), extend(-2 as _));
}}

test_rv64i! { arithmetic, || {
    let mut computer = Computer::<R, M, F>::new(0);

    computer.queue(Instruction::Base(base::Addi(
        RegisterIdx(1),
        RegisterIdx(0),
        ImmediateLow::new(0x2).unwrap(),
    )));
    computer.queue(Instruction::Base(base::Addi(
        RegisterIdx(2),
        RegisterIdx(1),
        ImmediateLow::new(0xFFE).unwrap(),
    )));
    computer.queue(Instruction::RV64I(rv64i::Addw(
        RegisterIdx(3),
        RegisterIdx(1),
        RegisterIdx(1),
    )));
    computer.queue(Instruction::RV64I(rv64i::Subw(
        RegisterIdx(4),
        RegisterIdx(1),
        RegisterIdx(1),
    )));
    computer.queue(Instruction::RV64I(rv64i::Subw(
        RegisterIdx(5),
        RegisterIdx(1),
        RegisterIdx(3),
    )));
    computer.run_all();
    assert_eq!(computer.get(RegisterIdx(1)), extend(2));
    assert_eq!(computer.get(RegisterIdx(2)), extend(0));
    assert_eq!(computer.get(RegisterIdx(3)), extend(4));
    assert_eq!(computer.get(RegisterIdx(4)), extend(0));
    assert_eq!(computer.get(RegisterIdx(5)), extend(-2 as _));
}}

test_rv128i! { arithmetic, || {
    let mut computer = Computer::<R, M, F>::new(0);

    computer.queue(Instruction::Base(base::Addi(
        RegisterIdx(1),
        RegisterIdx(0),
        ImmediateLow::new(0x2).unwrap(),
    )));
    computer.queue(Instruction::Base(base::Addi(
        RegisterIdx(2),
        RegisterIdx(1),
        ImmediateLow::new(0xFFE).unwrap(),
    )));
    computer.queue(Instruction::RV128I(rv128i::Addd(
        RegisterIdx(3),
        RegisterIdx(1),
        RegisterIdx(1),
    )));
    computer.queue(Instruction::RV128I(rv128i::Subd(
        RegisterIdx(4),
        RegisterIdx(1),
        RegisterIdx(1),
    )));
    computer.queue(Instruction::RV128I(rv128i::Subd(
        RegisterIdx(5),
        RegisterIdx(1),
        RegisterIdx(3),
    )));
    computer.run_all();
    assert_eq!(computer.get(RegisterIdx(1)), extend(2));
    assert_eq!(computer.get(RegisterIdx(2)), extend(0));
    assert_eq!(computer.get(RegisterIdx(3)), extend(4));
    assert_eq!(computer.get(RegisterIdx(4)), extend(0));
    assert_eq!(computer.get(RegisterIdx(5)), extend(-2 as _));
}}

test_rv64i! { arithmetic_immediate, || {
    let mut computer = Computer::<R, M, F>::new(0);

    computer.queue(Instruction::RV64I(rv64i::Addiw(
        RegisterIdx(1),
        RegisterIdx(0),
        ImmediateLow::new(0x2).unwrap(),
    )));
    computer.queue(Instruction::RV64I(rv64i::Addiw(
        RegisterIdx(2),
        RegisterIdx(1),
        ImmediateLow::new(0xFFE).unwrap(),
    )));
    computer.queue(Instruction::RV64I(rv64i::Addiw(
        RegisterIdx(3),
        RegisterIdx(0),
        ImmediateLow::new(0xFFF).unwrap(),
    )));
    computer.queue(Instruction::RV64I(rv64i::Addiw(
        RegisterIdx(4),
        RegisterIdx(3),
        ImmediateLow::new(0x1).unwrap(),
    )));
    computer.run_all();
    assert_eq!(computer.get(RegisterIdx(1)), extend(2));
    assert_eq!(computer.get(RegisterIdx(2)), extend(0));
    assert_eq!(computer.get(RegisterIdx(3)), extend(0xFFFF_FFFF));
    assert_eq!(computer.get(RegisterIdx(4)), extend(0));
}}

test_rv128i! { arithmetic_immediate, || {
    let mut computer = Computer::<R, M, F>::new(0);

    computer.queue(Instruction::RV128I(rv128i::Addid(
        RegisterIdx(1),
        RegisterIdx(0),
        ImmediateLow::new(0x2).unwrap(),
    )));
    computer.queue(Instruction::RV128I(rv128i::Addid(
        RegisterIdx(2),
        RegisterIdx(1),
        ImmediateLow::new(0xFFE).unwrap(),
    )));
    computer.queue(Instruction::RV128I(rv128i::Addid(
        RegisterIdx(3),
        RegisterIdx(0),
        ImmediateLow::new(0xFFF).unwrap(),
    )));
    computer.queue(Instruction::RV128I(rv128i::Addid(
        RegisterIdx(4),
        RegisterIdx(3),
        ImmediateLow::new(0x1).unwrap(),
    )));
    computer.run_all();
    assert_eq!(computer.get(RegisterIdx(1)), extend(2));
    assert_eq!(computer.get(RegisterIdx(2)), extend(0));
    assert_eq!(computer.get(RegisterIdx(3)), extend(0xFFFF_FFFF));
    assert_eq!(computer.get(RegisterIdx(4)), extend(0));
}}

test_all! { ignore_write_zero, || {
    let mut computer = Computer::<R, M, F>::new(0);

    computer.queue(Instruction::Base(base::Addi(
        RegisterIdx(0),
        RegisterIdx(0),
        ImmediateLow::new(0x2).unwrap(),
    )));
    computer.run_all();
    assert_eq!(computer.get(RegisterIdx(0)), extend(0));
}}
