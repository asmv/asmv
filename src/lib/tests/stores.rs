use super::extend;
use crate::{Computer, ImmediateLow, Instruction, RegisterIdx};

test_all! { store_byte, || {
    let mut computer = Computer::<R, M, F>::with_mem(&[0; 12][..]);
    computer.queue(Instruction::Base(base::Addi(
        RegisterIdx(1),
        RegisterIdx(0),
        ImmediateLow::new(0xEEF).unwrap(),
    )));
    computer.queue(Instruction::Base(base::Sb(
        RegisterIdx(1),
        RegisterIdx(0),
        ImmediateLow::new(0x000).unwrap(),
    )));
    computer.queue(Instruction::Base(base::Sh(
        RegisterIdx(1),
        RegisterIdx(0),
        ImmediateLow::new(0x004).unwrap(),
    )));
    computer.queue(Instruction::Base(base::Sw(
        RegisterIdx(1),
        RegisterIdx(0),
        ImmediateLow::new(0x008).unwrap(),
    )));
    computer.run_all();
    assert_eq!(computer.get(RegisterIdx(1)), extend(0xFFFF_FEEF));
    assert_eq!(
        computer.mem(),
        &[0xEF, 0, 0, 0, 0xEF, 0xFE, 0, 0, 0xEF, 0xFE, 0xFF, 0xFF]
    );
}}

test_rv64i! { store_byte, || {
    let mut computer = Computer::<R, M, F>::with_mem(&[0; 8][..]);
    computer.queue(Instruction::Base(base::Addi(
        RegisterIdx(1),
        RegisterIdx(0),
        ImmediateLow::new(0xEEF).unwrap(),
    )));
    computer.queue(Instruction::RV64I(rv64i::Sd(
        RegisterIdx(1),
        RegisterIdx(0),
        ImmediateLow::new(0x000).unwrap(),
    )));
    computer.run_all();
    assert_eq!(computer.get(RegisterIdx(1)), extend(0xFFFF_FEEF));
    assert_eq!(
        computer.mem(),
        &[0xEF, 0xFE, 0xFF, 0xFF, 0xFF, 0xFF, 0xFF, 0xFF]
    );
}}

test_rv128i! { store_byte, || {
    let mut computer = Computer::<R, M, F>::with_mem(&[0; 16][..]);
    computer.queue(Instruction::Base(base::Addi(
        RegisterIdx(1),
        RegisterIdx(0),
        ImmediateLow::new(0xEEF).unwrap(),
    )));
    computer.queue(Instruction::RV128I(rv128i::Sq(
        RegisterIdx(1),
        RegisterIdx(0),
        ImmediateLow::new(0x000).unwrap(),
    )));
    computer.run_all();
    assert_eq!(computer.get(RegisterIdx(1)), extend(0xFFFF_FEEF));
    assert_eq!(
        computer.mem(),
        &[0xEF, 0xFE, 0xFF, 0xFF, 0xFF, 0xFF, 0xFF, 0xFF, 0xFF, 0xFF, 0xFF, 0xFF, 0xFF, 0xFF, 0xFF, 0xFF]
    );
}}
