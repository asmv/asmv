#![allow(unused_variables)]

use super::I256;
use num_traits::Zero;
use std::convert::TryFrom;
use std::fmt;
use std::ops;

type Data = primitive_types::U256;

#[derive(Debug, PartialEq, Eq, PartialOrd, Ord, Copy, Clone)]
#[allow(non_camel_case_types)]
pub struct U256 {
    data: Data,
}

impl_u256_from!(u8);
impl_u256_from!(u16);
impl_u256_from!(u32);
impl_u256_from!(u64);
impl_u256_from!(u128);
impl_u256_from!(usize);

impl_u256_try_from!(i8);
impl_u256_try_from!(i16);
impl_u256_try_from!(i32);
impl_u256_try_from!(i64);
impl_u256_try_from!(i128);
impl_u256_try_from!(isize);

impl From<Data> for U256 {
    fn from(value: Data) -> Self {
        Self { data: value }
    }
}

impl Into<Data> for U256 {
    fn into(self) -> Data {
        self.data
    }
}

impl From<[u64; 4]> for U256 {
    fn from(value: [u64; 4]) -> Self {
        Self {
            data: primitive_types::U256(value),
        }
    }
}

impl From<U256> for [u64; 4] {
    fn from(value: U256) -> [u64; 4] {
        value.data.0
    }
}

impl TryFrom<I256> for U256 {
    type Error = &'static str;

    fn try_from(value: I256) -> Result<Self, Self::Error> {
        let value = primitive_types::U256(value.into());

        match Data::try_from(value) {
            Ok(data) => Ok(Self { data }),
            Err(_) => Err("Attempt to convert a negative value"),
        }
    }
}

// std::fmt

impl fmt::LowerHex for U256 {
    fn fmt(&self, f: &mut fmt::Formatter<'_>) -> fmt::Result {
        write!(f, "{:x}", self.data)
    }
}

impl fmt::UpperHex for U256 {
    fn fmt(&self, f: &mut fmt::Formatter<'_>) -> fmt::Result {
        write!(f, "{}", format!("{:x}", self.data).to_uppercase())
    }
}

impl fmt::Binary for U256 {
    fn fmt(&self, f: &mut fmt::Formatter<'_>) -> fmt::Result {
        for word in &self.data.0 {
            write!(f, "{}", word)?
        }
        Ok(())
    }
}

// std::ops

impl ops::Add for U256 {
    type Output = Self;

    fn add(self, rhs: Self) -> Self::Output {
        Self::Output {
            data: self.data + rhs.data,
        }
    }
}

impl ops::Sub for U256 {
    type Output = Self;

    fn sub(self, rhs: Self) -> Self::Output {
        Self {
            data: self.data - rhs.data,
        }
    }
}

impl ops::Mul for U256 {
    type Output = Self;

    fn mul(self, rhs: Self) -> Self::Output {
        Self {
            data: self.data * rhs.data,
        }
    }
}

impl ops::Div for U256 {
    type Output = Self;

    fn div(self, rhs: Self) -> Self::Output {
        Self {
            data: self.data / rhs.data,
        }
    }
}

impl ops::Rem for U256 {
    type Output = Self;

    fn rem(self, rhs: Self) -> Self::Output {
        Self {
            data: self.data % rhs.data,
        }
    }
}

impl ops::BitAnd for U256 {
    type Output = Self;

    fn bitand(self, rhs: Self) -> Self::Output {
        Self {
            data: self.data & rhs.data,
        }
    }
}

impl ops::BitOr for U256 {
    type Output = Self;

    fn bitor(self, rhs: Self) -> Self::Output {
        Self {
            data: self.data | rhs.data,
        }
    }
}

impl ops::BitXor for U256 {
    type Output = Self;

    fn bitxor(self, rhs: Self) -> Self::Output {
        Self {
            data: self.data ^ rhs.data,
        }
    }
}

impl ops::Not for U256 {
    type Output = Self;

    fn not(self) -> Self::Output {
        Self { data: !self.data }
    }
}

impl ops::Shl<usize> for U256 {
    type Output = Self;

    fn shl(self, rhs: usize) -> Self::Output {
        Self {
            data: self.data << rhs,
        }
    }
}

impl ops::Shr<usize> for U256 {
    type Output = Self;

    fn shr(self, rhs: usize) -> Self::Output {
        Self::Output {
            data: self.data >> rhs,
        }
    }
}

// num_traits

impl num_traits::PrimInt for U256 {
    fn count_ones(self) -> u32 {
        self.data.0.iter().map(|w| w.count_ones()).sum()
    }

    fn count_zeros(self) -> u32 {
        self.data.0.iter().map(|w| w.count_zeros()).sum()
    }

    fn leading_zeros(self) -> u32 {
        self.data.leading_zeros()
    }

    fn trailing_zeros(self) -> u32 {
        self.data.trailing_zeros()
    }

    fn rotate_left(self, n: u32) -> Self {
        unimplemented!()
    }

    fn rotate_right(self, n: u32) -> Self {
        unimplemented!()
    }

    fn signed_shl(self, n: u32) -> Self {
        unimplemented!()
    }

    fn signed_shr(self, n: u32) -> Self {
        unimplemented!()
    }

    fn unsigned_shl(self, n: u32) -> Self {
        unimplemented!()
    }

    fn unsigned_shr(self, n: u32) -> Self {
        unimplemented!()
    }

    fn swap_bytes(self) -> Self {
        unimplemented!()
    }

    fn from_be(x: Self) -> Self {
        unimplemented!()
    }

    fn from_le(x: Self) -> Self {
        unimplemented!()
    }

    fn to_be(self) -> Self {
        unimplemented!()
    }

    fn to_le(self) -> Self {
        unimplemented!()
    }

    fn pow(self, exp: u32) -> Self {
        Self {
            data: self.data.pow(exp.into()),
        }
    }
}

impl num_traits::Bounded for U256 {
    fn min_value() -> Self {
        Self { data: Data::zero() }
    }

    fn max_value() -> Self {
        Self {
            data: Data::max_value(),
        }
    }
}

impl num_traits::Saturating for U256 {
    fn saturating_add(self, v: Self) -> Self {
        Self {
            data: self.data.saturating_add(v.data),
        }
    }

    fn saturating_sub(self, v: Self) -> Self {
        Self {
            data: self.data.saturating_sub(v.data),
        }
    }
}

impl num_traits::CheckedAdd for U256 {
    fn checked_add(&self, v: &Self) -> Option<Self> {
        match self.data.checked_add(v.data) {
            Some(data) => Some(Self { data }),
            None => None,
        }
    }
}

impl num_traits::CheckedSub for U256 {
    fn checked_sub(&self, v: &Self) -> Option<Self> {
        match self.data.checked_sub(v.data) {
            Some(data) => Some(Self { data }),
            None => None,
        }
    }
}

impl num_traits::CheckedMul for U256 {
    fn checked_mul(&self, v: &Self) -> Option<Self> {
        match self.data.checked_mul(v.data) {
            Some(data) => Some(Self { data }),
            None => None,
        }
    }
}

impl num_traits::CheckedDiv for U256 {
    fn checked_div(&self, v: &Self) -> Option<Self> {
        match self.data.checked_div(v.data) {
            Some(data) => Some(Self { data }),
            None => None,
        }
    }
}

impl num_traits::CheckedRem for U256 {
    fn checked_rem(&self, v: &Self) -> Option<Self> {
        match self.data.checked_rem(v.data) {
            Some(data) => Some(Self { data }),
            None => None,
        }
    }
}

impl num_traits::CheckedNeg for U256 {
    fn checked_neg(&self) -> Option<Self> {
        if self.is_zero() {
            Some(*self)
        } else {
            None
        }
    }
}

// num_traits::Num

impl num_traits::Num for U256 {
    type FromStrRadixErr = std::num::ParseIntError;

    fn from_str_radix(str: &str, radix: u32) -> Result<Self, Self::FromStrRadixErr> {
        unimplemented!()
    }
}

impl num_traits::Zero for U256 {
    fn zero() -> Self {
        Self { data: Data::zero() }
    }

    fn is_zero(&self) -> bool {
        self.data.is_zero()
    }
}

impl num_traits::One for U256 {
    fn one() -> Self {
        Self { data: Data::one() }
    }
}

impl num_traits::ToPrimitive for U256 {
    fn to_i64(&self) -> Option<i64> {
        if self.data.bits() < 32 {
            Some(self.data.low_u64() as _)
        } else {
            None
        }
    }

    fn to_u64(&self) -> Option<u64> {
        if self.data.bits() <= 64 {
            Some(self.data.low_u64())
        } else {
            None
        }
    }
}

impl num_traits::NumCast for U256 {
    fn from<T: num_traits::ToPrimitive>(n: T) -> Option<Self> {
        unimplemented!()
    }
}

impl num_traits::AsPrimitive<u8> for U256 {
    fn as_(self) -> u8 {
        Data::low_u32(&self.data) as _
    }
}

impl num_traits::AsPrimitive<u16> for U256 {
    fn as_(self) -> u16 {
        Data::low_u32(&self.data) as _
    }
}

impl num_traits::AsPrimitive<u32> for U256 {
    fn as_(self) -> u32 {
        Data::low_u32(&self.data)
    }
}

impl num_traits::AsPrimitive<u64> for U256 {
    fn as_(self) -> u64 {
        Data::low_u64(&self.data)
    }
}

impl num_traits::AsPrimitive<u128> for U256 {
    fn as_(self) -> u128 {
        Data::low_u128(&self.data)
    }
}

impl num_traits::AsPrimitive<usize> for U256 {
    fn as_(self) -> usize {
        Data::low_u64(&self.data) as _
    }
}

impl num_traits::AsPrimitive<i8> for U256 {
    fn as_(self) -> i8 {
        Data::low_u32(&self.data) as _
    }
}

impl num_traits::AsPrimitive<i16> for U256 {
    fn as_(self) -> i16 {
        Data::low_u32(&self.data) as _
    }
}

impl num_traits::AsPrimitive<i32> for U256 {
    fn as_(self) -> i32 {
        Data::low_u32(&self.data) as _
    }
}

impl num_traits::AsPrimitive<i64> for U256 {
    fn as_(self) -> i64 {
        Data::low_u64(&self.data) as _
    }
}

impl num_traits::AsPrimitive<i128> for U256 {
    fn as_(self) -> i128 {
        Data::low_u128(&self.data) as _
    }
}

impl num_traits::AsPrimitive<isize> for U256 {
    fn as_(self) -> isize {
        Data::low_u64(&self.data) as _
    }
}
