#[macro_export]
macro_rules! impl_u256_from {
    ( $type: ty ) => {
        impl From<$type> for U256 {
            fn from(value: $type) -> Self {
                Self { data: value.into() }
            }
        }
    };
}

#[macro_export]
macro_rules! impl_u256_try_from {
    ( $type: ty ) => {
        impl TryFrom<$type> for U256 {
            type Error = &'static str;

            fn try_from(value: $type) -> Result<Self, Self::Error> {
                if !value.is_negative() {
                    Ok(Self { data: value.into() })
                } else {
                    Err("Negative value")
                }
            }
        }
    };
}

#[macro_export]
macro_rules! impl_i256_from {
    ( $type: ty ) => {
        impl From<$type> for U256 {
            fn from(value: $type) -> Self {
                Self {
                    sign: sign(value),
                    data: value.into(),
                }
            }
        }
    };
}
