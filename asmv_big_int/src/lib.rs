pub use self::i256::I256;
pub use self::u256::U256;

#[macro_use]
mod macros;

mod i256;
mod u256;

#[cfg(test)]
mod tests {
    use super::*;
    use num_traits::One;

    mod i256_convert {
        use super::*;
        use i256::Sign;

        #[test]
        fn no_sign() {
            assert_eq!(Sign::NoSign, I256::from(0u8).sign());
            assert_eq!(Sign::NoSign, I256::from(0u16).sign());
            assert_eq!(Sign::NoSign, I256::from(0u32).sign());
            assert_eq!(Sign::NoSign, I256::from(0u64).sign());
            assert_eq!(Sign::NoSign, I256::from(0u128).sign());
            assert_eq!(Sign::NoSign, I256::from(0usize).sign());

            assert_eq!(Sign::NoSign, I256::from(0i8).sign());
            assert_eq!(Sign::NoSign, I256::from(0i16).sign());
            assert_eq!(Sign::NoSign, I256::from(0i32).sign());
            assert_eq!(Sign::NoSign, I256::from(0i64).sign());
            assert_eq!(Sign::NoSign, I256::from(0i128).sign());
            assert_eq!(Sign::NoSign, I256::from(0isize).sign());
        }

        #[test]
        fn sign_plus() {
            assert_eq!(Sign::Plus, I256::from(1u8).sign());
            assert_eq!(Sign::Plus, I256::from(1u16).sign());
            assert_eq!(Sign::Plus, I256::from(1u32).sign());
            assert_eq!(Sign::Plus, I256::from(1u64).sign());
            assert_eq!(Sign::Plus, I256::from(1u128).sign());
            assert_eq!(Sign::Plus, I256::from(1usize).sign());

            assert_eq!(Sign::Plus, I256::from(1i8).sign());
            assert_eq!(Sign::Plus, I256::from(1i16).sign());
            assert_eq!(Sign::Plus, I256::from(1i32).sign());
            assert_eq!(Sign::Plus, I256::from(1i64).sign());
            assert_eq!(Sign::Plus, I256::from(1i128).sign());
            assert_eq!(Sign::Plus, I256::from(1isize).sign());
        }

        #[test]
        fn sign_minus() {
            assert_eq!(Sign::Minus, I256::from(-1i8).sign());
            assert_eq!(Sign::Minus, I256::from(-1i16).sign());
            assert_eq!(Sign::Minus, I256::from(-1i32).sign());
            assert_eq!(Sign::Minus, I256::from(-1i64).sign());
            assert_eq!(Sign::Minus, I256::from(-1i128).sign());
            assert_eq!(Sign::Minus, I256::from(-1isize).sign());
        }

        #[test]
        fn minus() {
            assert_eq!(I256::from(-1i8), -I256::from(1u8));
            assert_eq!(I256::from(-1i16), -I256::from(1u16));
            assert_eq!(I256::from(-1i32), -I256::from(1u32));
            assert_eq!(I256::from(-1i64), -I256::from(1u64));
            assert_eq!(I256::from(-1i128), -I256::from(1u128));
            assert_eq!(I256::from(-1isize), -I256::from(1usize));

            assert_eq!(I256::from(1i8), -I256::from(-1i8));
            assert_eq!(I256::from(1i16), -I256::from(-1i16));
            assert_eq!(I256::from(1i32), -I256::from(-1i32));
            assert_eq!(I256::from(1i64), -I256::from(-1i64));
            assert_eq!(I256::from(1i128), -I256::from(-1i128));
            assert_eq!(I256::from(1isize), -I256::from(-1isize));
        }
    }

    mod i256_shr {
        use super::*;
        use i256::Sign;
        use std::ops::Shr;

        #[test]
        fn shr_plus() {
            assert_eq!(Sign::Plus, I256::from(100u64).shr(2).sign());
            assert_eq!(Sign::Plus, I256::from(100i64).shr(2).sign());

            assert_eq!(I256::from(100u64) >> 2, I256::from(25u64));
            assert_eq!(I256::from(100i64) >> 2, I256::from(25i64));
        }

        #[test]
        fn shr_no_sign() {
            assert_eq!(Sign::NoSign, I256::from(0u64).sign());
            assert_eq!(Sign::NoSign, I256::from(0i64).sign());
            assert_eq!(Sign::NoSign, I256::from(0u64).shr(2).sign());
            assert_eq!(Sign::NoSign, I256::from(0i64).shr(2).sign());

            assert_eq!(Sign::Plus, I256::from(1u64).sign());
            assert_eq!(Sign::Plus, I256::from(1i64).sign());
            assert_eq!(Sign::NoSign, I256::from(1u64).shr(2).sign());
            assert_eq!(Sign::NoSign, I256::from(1i64).shr(2).sign());

            assert_eq!(I256::from(0u64) >> 2, I256::from(0u64));
            assert_eq!(I256::from(0i64) >> 2, I256::from(0i64));
            assert_eq!(I256::from(1u64) >> 2, I256::from(0u64));
            assert_eq!(I256::from(1i64) >> 2, I256::from(0i64));
            assert_eq!(I256::from(100u64) >> 10, I256::from(0u64));
            assert_eq!(I256::from(100i64) >> 10, I256::from(0i64));
        }

        #[test]
        fn shr_minus() {
            assert_eq!(Sign::NoSign, I256::from(0i64).sign());
            assert_eq!(Sign::NoSign, I256::from(0i64).shr(2).sign());
            assert_eq!(Sign::NoSign, I256::from(-0i64).sign());
            assert_eq!(Sign::NoSign, I256::from(-0i64).shr(2).sign());

            assert_eq!(Sign::Minus, I256::from(-1i64).sign());
            assert_eq!(Sign::Minus, I256::from(-1i64).shr(2).sign());

            assert_eq!(I256::from(-100i64) >> 2, I256::from(-25i64));
            assert_eq!(I256::from(-100i64) >> 3, I256::from(-13i64));
            assert_eq!(I256::from(-100i64) >> 10, I256::from(-1i64));
        }
    }

    #[test]
    fn convert_u256() {
        use std::convert::TryFrom;

        assert!(U256::from(1u8).is_one());
        assert!(U256::from(1u16).is_one());
        assert!(U256::from(1u32).is_one());
        assert!(U256::from(1u64).is_one());
        assert!(U256::from(1u128).is_one());
        assert!(U256::from(1usize).is_one());

        assert_eq!(U256::from(1u64), U256::try_from(1i8).unwrap());
        assert_eq!(U256::from(1u64), U256::try_from(1i16).unwrap());
        assert_eq!(U256::from(1u64), U256::try_from(1i32).unwrap());
        assert_eq!(U256::from(1u64), U256::try_from(1i64).unwrap());
        assert_eq!(U256::from(1u64), U256::try_from(1i128).unwrap());
        assert_eq!(U256::from(1u64), U256::try_from(1isize).unwrap());

        assert!(U256::try_from(-1i8).is_err());
        assert!(U256::try_from(-1i16).is_err());
        assert!(U256::try_from(-1i32).is_err());
        assert!(U256::try_from(-1i64).is_err());
        assert!(U256::try_from(-1i128).is_err());
        assert!(U256::try_from(-1isize).is_err());
    }
}
