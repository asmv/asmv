#![allow(unused_variables)]

use super::U256;
use num_traits::{Signed, Zero};
use std::convert::TryFrom;
use std::fmt;
use std::ops;
use std::ops::BitAnd;

type Data = primitive_types::U256;
pub type Sign = num_bigint::Sign;

#[derive(Debug, PartialEq, Eq, PartialOrd, Ord, Copy, Clone)]
#[allow(non_camel_case_types)]
pub struct I256 {
    pub sign: Sign,
    pub data: Data,
}

impl I256 {
    pub fn sign(&self) -> Sign {
        self.sign
    }
}

impl From<u8> for I256 {
    fn from(value: u8) -> Self {
        Self {
            sign: sign(value),
            data: value.into(),
        }
    }
}

impl From<u16> for I256 {
    fn from(value: u16) -> Self {
        Self {
            sign: sign(value),
            data: value.into(),
        }
    }
}

impl From<u32> for I256 {
    fn from(value: u32) -> Self {
        Self {
            sign: sign(value),
            data: value.into(),
        }
    }
}

impl From<u64> for I256 {
    fn from(value: u64) -> Self {
        Self {
            sign: sign(value),
            data: value.into(),
        }
    }
}

impl From<u128> for I256 {
    fn from(value: u128) -> Self {
        Self {
            sign: sign(value),
            data: value.into(),
        }
    }
}

impl From<usize> for I256 {
    fn from(value: usize) -> Self {
        Self {
            sign: sign(value),
            data: value.into(),
        }
    }
}

impl From<[u64; 4]> for I256 {
    fn from(value: [u64; 4]) -> Self {
        let data = primitive_types::U256(value);
        // if should be negative
        if data.bit(255) {
            Self {
                sign: Sign::Minus,
                data: twos_complement(data),
            }
        } else {
            Self {
                sign: sign_data(data),
                data,
            }
        }
    }
}

impl From<I256> for [u64; 4] {
    fn from(value: I256) -> [u64; 4] {
        match value.sign {
            Sign::Plus | Sign::NoSign => value.data.0,
            Sign::Minus => twos_complement(value.data).0,
        }
    }
}

impl TryFrom<U256> for I256 {
    type Error = &'static str;

    fn try_from(value: U256) -> Result<Self, Self::Error> {
        let value: [u64; 4] = value.into();
        let value = primitive_types::U256(value);

        match Data::try_from(value) {
            Ok(data) => {
                Ok(
                    // if should be negative
                    if data.bit(255) {
                        Self {
                            sign: Sign::Minus,
                            data: twos_complement(data),
                        }
                    } else {
                        Self {
                            sign: sign_data(data),
                            data,
                        }
                    },
                )
            }
            Err(_) => Err("Overflow"),
        }
    }
}

impl From<isize> for I256 {
    fn from(value: isize) -> Self {
        Self {
            sign: sign(value),
            data: (value.abs() as usize).into(),
        }
    }
}

impl From<i8> for I256 {
    fn from(value: i8) -> Self {
        Self {
            sign: sign(value),
            data: (value.abs() as u8).into(),
        }
    }
}

impl From<i16> for I256 {
    fn from(value: i16) -> Self {
        Self {
            sign: sign(value),
            data: (value.abs() as u16).into(),
        }
    }
}

impl From<i32> for I256 {
    fn from(value: i32) -> Self {
        Self {
            sign: sign(value),
            data: (value.abs() as u32).into(),
        }
    }
}

impl From<i64> for I256 {
    fn from(value: i64) -> Self {
        Self {
            sign: sign(value),
            data: (value.abs() as u64).into(),
        }
    }
}

impl From<i128> for I256 {
    fn from(value: i128) -> Self {
        Self {
            sign: sign(value),
            data: (value.abs() as u128).into(),
        }
    }
}

// std::fmt

impl fmt::UpperHex for I256 {
    fn fmt(&self, f: &mut fmt::Formatter<'_>) -> fmt::Result {
        if self.is_negative() {
            write!(
                f,
                "{}",
                format!("{:064x}", twos_complement(self.data)).to_uppercase()
            )
        } else {
            write!(f, "{}", format!("{:064x}", self.data).to_uppercase())
        }
    }
}

impl fmt::Binary for I256 {
    fn fmt(&self, f: &mut fmt::Formatter<'_>) -> fmt::Result {
        unimplemented!()
    }
}

// std::ops

impl ops::Add for I256 {
    type Output = Self;

    fn add(self, rhs: Self) -> Self::Output {
        unimplemented!()
    }
}

impl ops::Sub for I256 {
    type Output = Self;

    fn sub(self, rhs: Self) -> Self::Output {
        unimplemented!()
    }
}

impl ops::Mul for I256 {
    type Output = Self;

    fn mul(self, rhs: Self) -> Self::Output {
        Self {
            sign: self.sign * rhs.sign,
            data: self.data * rhs.data,
        }
    }
}

impl ops::Div for I256 {
    type Output = Self;

    fn div(self, rhs: Self) -> Self::Output {
        unimplemented!()
    }
}

impl ops::Rem for I256 {
    type Output = Self;

    fn rem(self, rhs: Self) -> Self::Output {
        unimplemented!()
    }
}

impl ops::BitAnd for I256 {
    type Output = Self;

    fn bitand(self, rhs: Self) -> Self::Output {
        unimplemented!()
    }
}

impl ops::BitOr for I256 {
    type Output = Self;

    fn bitor(self, rhs: Self) -> Self::Output {
        unimplemented!()
    }
}

impl ops::BitXor for I256 {
    type Output = Self;

    fn bitxor(self, rhs: Self) -> Self::Output {
        unimplemented!()
    }
}

impl ops::Not for I256 {
    type Output = Self;

    fn not(self) -> Self::Output {
        unimplemented!()
    }
}

impl ops::Shl<usize> for I256 {
    type Output = Self;

    fn shl(self, rhs: usize) -> Self::Output {
        let data = match self.sign {
            Sign::NoSign => Data::zero(),
            Sign::Plus => self.data << rhs,
            Sign::Minus => twos_complement(twos_complement(self.data) << rhs),
        };
        Self {
            sign: sign_data(data),
            data,
        }
    }
}

impl ops::Shr<usize> for I256 {
    type Output = Self;

    fn shr(self, rhs: usize) -> Self::Output {
        match self.sign {
            // 0 => 0
            Sign::NoSign => self,
            // Positive => simple rhs
            Sign::Plus => {
                let data = self.data >> rhs;
                Self {
                    sign: sign_data(data),
                    data,
                }
            }
            // Negative => signed rhs
            Sign::Minus => {
                // Convert to neg
                let data = twos_complement(self.data);
                // Actual shift
                let data = data >> rhs;
                // Simulates signed shift (sets shifted to 1)
                let mask = Data::max_value() << (256 - rhs);
                let data = data | mask;
                // Convert back to positive
                let data = twos_complement(data);

                Self {
                    sign: Sign::Minus,
                    data,
                }
            }
        }
    }
}

// num_traits

impl num_traits::PrimInt for I256 {
    fn count_ones(self) -> u32 {
        twos_complement(self.data)
            .0
            .iter()
            .map(|w| w.count_ones())
            .sum()
    }

    fn count_zeros(self) -> u32 {
        twos_complement(self.data)
            .0
            .iter()
            .map(|w| w.count_zeros())
            .sum()
    }

    fn leading_zeros(self) -> u32 {
        twos_complement(self.data).leading_zeros()
    }

    fn trailing_zeros(self) -> u32 {
        twos_complement(self.data).trailing_zeros()
    }

    fn rotate_left(self, n: u32) -> Self {
        unimplemented!()
    }

    fn rotate_right(self, n: u32) -> Self {
        unimplemented!()
    }

    fn signed_shl(self, n: u32) -> Self {
        unimplemented!()
    }

    fn signed_shr(self, n: u32) -> Self {
        unimplemented!()
    }

    fn unsigned_shl(self, n: u32) -> Self {
        unimplemented!()
    }

    fn unsigned_shr(self, n: u32) -> Self {
        unimplemented!()
    }

    fn swap_bytes(self) -> Self {
        unimplemented!()
    }

    fn from_be(x: Self) -> Self {
        unimplemented!()
    }

    fn from_le(x: Self) -> Self {
        unimplemented!()
    }

    fn to_be(self) -> Self {
        unimplemented!()
    }

    fn to_le(self) -> Self {
        unimplemented!()
    }

    fn pow(self, exp: u32) -> Self {
        unimplemented!()
    }
}

impl num_traits::Bounded for I256 {
    fn min_value() -> Self {
        Self {
            sign: Sign::Minus,
            data: (Data::max_value() >> 1) + Data::one(),
        }
    }

    fn max_value() -> Self {
        Self {
            sign: Sign::Plus,
            data: Data::max_value() >> 1,
        }
    }
}

impl num_traits::Saturating for I256 {
    fn saturating_add(self, v: Self) -> Self {
        unimplemented!()
    }

    fn saturating_sub(self, v: Self) -> Self {
        unimplemented!()
    }
}

impl num_traits::CheckedAdd for I256 {
    fn checked_add(&self, v: &Self) -> Option<Self> {
        unimplemented!()
    }
}

impl num_traits::CheckedSub for I256 {
    fn checked_sub(&self, v: &Self) -> Option<Self> {
        unimplemented!()
    }
}

impl num_traits::CheckedMul for I256 {
    fn checked_mul(&self, v: &Self) -> Option<Self> {
        unimplemented!()
    }
}

impl num_traits::CheckedDiv for I256 {
    fn checked_div(&self, v: &Self) -> Option<Self> {
        unimplemented!()
    }
}

// num_traits::Num

impl num_traits::Num for I256 {
    type FromStrRadixErr = std::num::ParseIntError;

    fn from_str_radix(str: &str, radix: u32) -> Result<Self, Self::FromStrRadixErr> {
        unimplemented!()
    }
}

impl num_traits::Zero for I256 {
    fn zero() -> Self {
        Self {
            sign: Sign::NoSign,
            data: Data::zero(),
        }
    }

    fn is_zero(&self) -> bool {
        self.sign == Sign::NoSign
    }
}

impl num_traits::One for I256 {
    fn one() -> Self {
        Self {
            sign: Sign::Plus,
            data: Data::one(),
        }
    }
}

impl num_traits::ToPrimitive for I256 {
    fn to_i64(&self) -> Option<i64> {
        unimplemented!()
    }

    fn to_u64(&self) -> Option<u64> {
        if self.data.bits() <= 64 {
            Some(self.data.low_u64())
        } else {
            None
        }
    }
}

impl num_traits::NumCast for I256 {
    fn from<T: num_traits::ToPrimitive>(n: T) -> Option<Self> {
        unimplemented!()
    }
}

impl num_traits::AsPrimitive<u128> for I256 {
    fn as_(self) -> u128 {
        match self.sign {
            Sign::NoSign => 0u128,
            Sign::Plus => self.data.low_u128(),
            Sign::Minus => twos_complement(self.data).low_u128(),
        }
    }
}

impl num_traits::AsPrimitive<I256> for u128 {
    fn as_(self) -> I256 {
        // if most significant bit is 0 (not negative)
        if self.bitand(0x80000000_00000000_00000000_00000000) == 0 {
            I256 {
                sign: sign(self),
                data: self.into(),
            }
        } else {
            I256 {
                sign: Sign::Minus,
                // two's complement
                data: (!self + 1).into(),
            }
        }
    }
}

impl num_traits::Signed for I256 {
    fn abs(&self) -> Self {
        use num_traits::Bounded;

        // if min -> min
        if *self == Self::min_value() {
            return *self;
        }

        match self.sign {
            Sign::Plus => *self,
            Sign::Minus => -*self,
            Sign::NoSign => *self,
        }
    }

    fn abs_sub(&self, other: &Self) -> Self {
        if self <= other {
            Self::zero()
        } else {
            *self - *other
        }
    }

    fn signum(&self) -> Self {
        match self.sign {
            Sign::Plus => Self {
                sign: self.sign,
                data: Data::one(),
            },
            Sign::Minus => Self {
                sign: self.sign,
                data: Data::zero(),
            },
            Sign::NoSign => Self {
                sign: self.sign,
                data: Data::one(),
            },
        }
    }

    fn is_positive(&self) -> bool {
        self.sign == Sign::Plus
    }

    fn is_negative(&self) -> bool {
        self.sign == Sign::Minus
    }
}

impl ops::Neg for I256 {
    type Output = Self;

    fn neg(self) -> Self::Output {
        Self {
            sign: -self.sign,
            ..self
        }
    }
}

/// Returns the sign (`Plus`, `Minus`, `NoSign`) of the given value.
fn sign<T: Ord + num_traits::Zero>(num: T) -> Sign {
    use std::cmp::Ordering;
    match num.cmp(&T::zero()) {
        Ordering::Greater => Sign::Plus,
        Ordering::Less => Sign::Minus,
        Ordering::Equal => Sign::NoSign,
    }
}

/// Returns the sign (`Plus`, `Minus`, `NoSign`) of the given value.
fn sign_data(data: Data) -> Sign {
    use std::cmp::Ordering;
    match data.cmp(&Data::zero()) {
        Ordering::Greater => Sign::Plus,
        Ordering::Less => Sign::Minus,
        Ordering::Equal => Sign::NoSign,
    }
}

/// Two's Complement
///
/// Returns the two's complement of the given value.
///
/// Uses the flip + 1 formula.
fn twos_complement(data: Data) -> Data {
    !data + Data::one()
}
