#![allow(unused_variables)]

use std::convert::TryFrom;
use std::ops;
use std::fmt;
use num_bigint::Sign;
use super::U256;

#[derive(Debug, PartialEq, Eq, PartialOrd, Ord, Copy, Clone)]
#[allow(non_camel_case_types)]
pub struct I256Old {
    pub sign: Sign,
    pub data: U256,
}

impl From<u8> for I256Old {
    fn from(value: u8) -> Self {
        Self {
            sign: sign(value),
            data: value.into(),
        }
    }
}

impl From<u16> for I256Old {
    fn from(value: u16) -> Self {
        Self {
            sign: sign(value),
            data: value.into(),
        }
    }
}

impl From<u32> for I256Old {
    fn from(value: u32) -> Self {
        Self {
            sign: sign(value),
            data: value.into(),
        }
    }
}

impl From<u64> for I256Old {
    fn from(value: u64) -> Self {
        Self {
            sign: sign(value),
            data: value.into(),
        }
    }
}

impl From<u128> for I256Old {
    fn from(value: u128) -> Self {
        println!("I256.from::<u128>()");
        print!("> ");
        Self {
            sign: sign(value),
            data: value.into(),
        }
    }
}

impl TryFrom<U256> for I256Old {
    type Error = ();

    fn try_from(value: U256) -> Result<Self, Self::Error> {
        // use num_traits::Bounded;
        // use std::convert::TryInto;
        // print!("I256::try_from::<U256>() before\n> ");
        // let res = if value <= Self::max_value().try_into().unwrap() {
        //     print!("I256::try_from::<U256>() smaller\n> ");
        //     let res =
        //         Self {
        //             sign: sign(value),
        //             data: value,
        //         };
        //     Ok(res)
        // } else {
        //     print!("I256::try_from::<U256>() larger\n> ");
        //     Err(())
        // };
        // println!("I256::try_from::<U256>() after");
        // res
        unimplemented!()
    }
}

impl From<isize> for I256Old {
    fn from(value: isize) -> Self {
        Self {
            sign: sign(value),
            data: (value.abs() as usize).into(),
        }
    }
}

impl From<i8> for I256Old {
    fn from(value: i8) -> Self {
        Self {
            sign: sign(value),
            data: (value.abs() as u8).into(),
        }
    }
}

impl From<i16> for I256Old {
    fn from(value: i16) -> Self {
        Self {
            sign: sign(value),
            data: (value.abs() as u16).into(),
        }
    }
}

impl From<i32> for I256Old {
    fn from(value: i32) -> Self {
        Self {
            sign: sign(value),
            data: (value.abs() as u32).into(),
        }
    }
}

impl From<i64> for I256Old {
    fn from(value: i64) -> Self {
        Self {
            sign: sign(value),
            data: (value.abs() as u64).into(),
        }
    }
}

impl From<i128> for I256Old {
    fn from(value: i128) -> Self {
        Self {
            sign: sign(value),
            data: (value.abs() as u128).into(),
        }
    }
}

// std::fmt

impl fmt::UpperHex for I256Old {
    fn fmt(&self, f: &mut fmt::Formatter<'_>) -> fmt::Result {
        unimplemented!()
    }
}

impl fmt::Binary for I256Old {
    fn fmt(&self, f: &mut fmt::Formatter<'_>) -> fmt::Result {
        unimplemented!()
    }
}

// std::ops

impl ops::Add for I256Old {
    type Output = Self;

    fn add(self, rhs: Self) -> Self::Output {
        unimplemented!()
    }
}

impl ops::Sub for I256Old {
    type Output = Self;

    fn sub(self, rhs: Self) -> Self::Output {
        unimplemented!()
    }
}

impl ops::Mul for I256Old {
    type Output = Self;

    fn mul(self, rhs: Self) -> Self::Output {
        Self {
            sign: self.sign * rhs.sign,
            data: self.data * rhs.data,
        }
    }
}

impl ops::Div for I256Old {
    type Output = Self;

    fn div(self, rhs: Self) -> Self::Output {
        unimplemented!()
    }
}

impl ops::Rem for I256Old {
    type Output = Self;

    fn rem(self, rhs: Self) -> Self::Output {
        unimplemented!()
    }
}

impl ops::BitAnd for I256Old {
    type Output = Self;

    fn bitand(self, rhs: Self) -> Self::Output {
        unimplemented!()
    }
}

impl ops::BitOr for I256Old {
    type Output = Self;

    fn bitor(self, rhs: Self) -> Self::Output {
        unimplemented!()
    }
}

impl ops::BitXor for I256Old {
    type Output = Self;

    fn bitxor(self, rhs: Self) -> Self::Output {
        unimplemented!()
    }
}

impl ops::Not for I256Old {
    type Output = Self;

    fn not(self) -> Self::Output {
        unimplemented!()
    }
}

impl ops::Shl<usize> for I256Old {
    type Output = Self;

    fn shl(self, rhs: usize) -> Self::Output {
        unimplemented!()
    }
}

impl ops::Shr<usize> for I256Old {
    type Output = Self;

    fn shr(self, rhs: usize) -> Self::Output {
        Self {
            sign: self.sign,
            data: self.data >> rhs,
        }
    }
}

// num_traits

impl num_traits::PrimInt for I256Old {
    fn count_ones(self) -> u32 {
        unimplemented!()
    }

    fn count_zeros(self) -> u32 {
        unimplemented!()
    }

    fn leading_zeros(self) -> u32 {
        unimplemented!()
    }

    fn trailing_zeros(self) -> u32 {
        unimplemented!()
    }

    fn rotate_left(self, n: u32) -> Self {
        unimplemented!()
    }

    fn rotate_right(self, n: u32) -> Self {
        unimplemented!()
    }

    fn signed_shl(self, n: u32) -> Self {
        unimplemented!()
    }

    fn signed_shr(self, n: u32) -> Self {
        unimplemented!()
    }

    fn unsigned_shl(self, n: u32) -> Self {
        unimplemented!()
    }

    fn unsigned_shr(self, n: u32) -> Self {
        unimplemented!()
    }

    fn swap_bytes(self) -> Self {
        unimplemented!()
    }

    fn from_be(x: Self) -> Self {
        unimplemented!()
    }

    fn from_le(x: Self) -> Self {
        unimplemented!()
    }

    fn to_be(self) -> Self {
        unimplemented!()
    }

    fn to_le(self) -> Self {
        unimplemented!()
    }

    fn pow(self, exp: u32) -> Self {
        unimplemented!()
    }
}

impl num_traits::Bounded for I256Old {
    fn min_value() -> Self {
        print!("I256::min_value() before\n> ");
        let res = Self {
            sign: Sign::Minus,
            data: U256::max_value() / 2u64.into() + 1u64.into(),
        };
        println!("I256::min_value() after");
        res
    }

    fn max_value() -> Self {
        print!("I256::max_value() before\n> ");
        let sign = Sign::Plus;
        println!(">> after sign");
        let max = U256::max_value();
        println!(">> after max");
        let data = max / 2u64.into();
        println!(">> after data");
        let res = Self {
            sign, data,
        };
        println!("I256::max_value() after");
        res
    }
}

impl num_traits::Saturating for I256Old {
    fn saturating_add(self, v: Self) -> Self {
        unimplemented!()
    }

    fn saturating_sub(self, v: Self) -> Self {
        unimplemented!()
    }
}

impl num_traits::CheckedAdd for I256Old {
    fn checked_add(&self, v: &Self) -> Option<Self> {
        unimplemented!()
    }
}

impl num_traits::CheckedSub for I256Old {
    fn checked_sub(&self, v: &Self) -> Option<Self> {
        unimplemented!()
    }
}

impl num_traits::CheckedMul for I256Old {
    fn checked_mul(&self, v: &Self) -> Option<Self> {
        unimplemented!()
    }
}

impl num_traits::CheckedDiv for I256Old {
    fn checked_div(&self, v: &Self) -> Option<Self> {
        unimplemented!()
    }
}

// num_traits::Num

impl num_traits::Num for I256Old {
    type FromStrRadixErr = std::num::ParseIntError;

    fn from_str_radix(str: &str, radix: u32) -> Result<Self, Self::FromStrRadixErr> {
        unimplemented!()
    }
}

impl num_traits::Zero for I256Old {
    fn zero() -> Self {
        Self {
            sign: Sign::NoSign,
            data: 0u64.into(),
        }
    }

    fn is_zero(&self) -> bool {
        *self == Self::zero()
    }
}

impl num_traits::One for I256Old {
    fn one() -> Self {
        Self {
            sign: Sign::Plus,
            data: 1u64.into(),
        }
    }
}

impl num_traits::ToPrimitive for I256Old {
    fn to_i64(&self) -> Option<i64> {
        unimplemented!()
    }

    fn to_u64(&self) -> Option<u64> {
        unimplemented!()
    }
}

impl num_traits::NumCast for I256Old {
    fn from<T: num_traits::ToPrimitive>(n: T) -> Option<Self> {
        unimplemented!()
    }
}

// TODO
impl num_traits::AsPrimitive<u128> for I256Old {
    fn as_(self) -> u128 {
        use num_traits::Bounded;
        println!("I256.as_::<u128>()");
        let data = match self.sign {
            Sign::NoSign => return 0u128,
            Sign::Plus => self.data,
            Sign::Minus => U256::max_value() - self.data + 1u64.into(),
        };

        unsafe {
            let [low64, high64, _, _]: [u64; 4] = std::mem::transmute(data);
            std::mem::transmute([low64, high64])
        }
    }
}

impl num_traits::Signed for I256Old {
    fn abs(&self) -> Self {
        use num_traits::Bounded;
        // For signed integers, min will be returned if the number is min
        if *self == I256Old::min_value() {
            return *self;
        }

        match self.sign {
            Sign::Plus => *self,
            Sign::Minus => Self { sign: Sign::Plus, ..*self },
            Sign::NoSign => *self,
        }
    }

    fn abs_sub(&self, other: &Self) -> Self {
        use num_traits::Zero;
        if self <= other {
            Self::zero()
        } else {
            *self - *other
        }
    }

    fn signum(&self) -> Self {
        use num_traits::Zero;
        match self.sign {
            Sign::Plus => Self { sign: self.sign, data: 1u64.into() },
            Sign::Minus => Self { sign: self.sign, data: U256::zero() },
            Sign::NoSign => Self { sign: self.sign, data: 1u64.into() },
        }
    }

    fn is_positive(&self) -> bool {
        self.sign == Sign::Plus
    }

    fn is_negative(&self) -> bool {
        self.sign == Sign::Minus
    }
}

impl ops::Neg for I256Old {
    type Output = Self;

    fn neg(self) -> Self::Output {
        Self {
            sign: ops::Neg::neg(self.sign),
            data: self.data,
        }
    }
}

fn sign<T: Ord + num_traits::Zero>(num: T) -> Sign {
    use std::cmp::Ordering;
    match num.cmp(&T::zero()) {
        Ordering::Greater => Sign::Plus,
        Ordering::Less => Sign::Minus,
        Ordering::Equal => Sign::NoSign,
    }
}

